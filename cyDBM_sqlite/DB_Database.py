import sqlite3
import DB_QueryDefinition
import cyCafe
import datetime
import sys,os
from collections import defaultdict


class DBM:
    def __init__(self):
        self.connect = None
        self.cursor = None
        self.QD = DB_QueryDefinition.DataAccessor()
        self.d_ssi = defaultdict(dict)
        # self.d_ssi = dict()
        # self.d_ssi['Study'] = dict()
        # self.set_ssi()

    def database_exist(self):
        if os.path.isfile('test2.db'):
            print("database already exists.")
            return True
        else:
            print("will configure database ")
            self.connect = sqlite3.connect("test2.db")
            self.connect.row_factory = self.dict_factory
            self.cursor = self.connect.cursor()
            f = open('etc/sql_sample1.sql', 'r', encoding='utf-8')
            sql = f.read()

            self.cursor.executescript(sql)
            self.connect.commit()
            return False

    def initialize(self):
        self.db_connect()
        self.set_total_ssi()

        # try:
        #     f = open('etc/sql_sample1.sql', 'r', encoding='utf-8')
        #     sql = f.read()
        #
        #     self.cursor.executescript(sql)
        #     print("Successfully completed")
        #
        # except sqlite3.OperationalError as e:
        #     self.connect.close()
        #     print(e)
        #     os.remove("test2.db")
        #     sys.exit(1)
        #
        # else:
        #     # TODO!!!!
        #     # path = '/Users/jeongjihong/dicom/dicom2/'
        #     # self.DB_Import_DCM(path)
        #     self.connect.commit()

    # sql return value to dict
    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def db_connect(self):
        #TODO!! connect error except
        self.connect = sqlite3.connect("test2.db")
        self.connect.row_factory = self.dict_factory
        self.cursor = self.connect.cursor()

    def dcm_convertor(self, dcm):
        d = dcm
        if len(d['StudyTime']) > 6:
            d['StudyTime'] = d['StudyTime'][:6]

        if len(d['SeriesTime']) > 6:
            d['SeriesTime'] = d['SeriesTime'][:6]

        studydate = datetime.datetime.strptime(d['StudyDate'], "%Y%m%d").strftime('%Y-%d-%m')
        studytime = datetime.datetime.strptime(d['StudyTime'], "%H%M%S").strftime('%H:%M:%S')
        d['StudyDateTime'] = datetime.datetime.strptime(studydate + " " + studytime, "%Y-%d-%m %H:%M:%S")

        seriesdate = datetime.datetime.strptime(d['SeriesDate'], "%Y%m%d").strftime('%Y-%d-%m')
        seriestime = datetime.datetime.strptime(d['SeriesTime'], "%H%M%S").strftime('%H:%M:%S')
        d['SeriesDateTime'] = datetime.datetime.strptime(seriesdate + " " + seriestime, "%Y-%d-%m %H:%M:%S")

        # d['NumberOfSeries'] = None
        d['NumberOfImage'] = int(d['NumberOfImage'])

        d['SeriesNumber'] = d['SeriesNumber'] if d['SeriesNumber'] is None else int(d['SeriesNumber'])
        # d['SeriesNumber'] = int(d['SeriesNumber'])
        d['ImageWidth'] = int(d['ImageWidth'])
        d['ImageHeight'] = int(d['ImageHeight'])
        d['ImageNumber'] = int(d['ImageNumber'])
        d['AcquisitionNumber'] = int(d['AcquisitionNumber']) if d['AcquisitionNumber'] is not None else None
        d['FrameNumber'] = int(d['FrameNumber'])
        d['Examed'] = 0
        # d['Opened'] = None
        # d['Comments'] = None
        # d['Reserved1'] = None
        # d['Reserved2'] = None

        return d

    def import_dcm(self, dirpath):
        r = cyCafe.cyDicomReader()
        l = list()
        r.analyze_dicom(dirpath)
        r.get_dcm_info(l, True)
        #처음 Load 할 때 Model set
        self.set_total_ssi()

        if l is not None:
            converted_d = list()

            for v in l:
                d = dict()
                for k in v.keys():
                    try:
                        d[k] = v[k].decode('utf-8')
                        if d[k] == '':
                            d[k] = None

                    except UnicodeDecodeError:
                        d[k] = v[k].decode('euc-kr')
                        if d[k] == '':
                            d[k] = None

                for i in range(0, len(self.d_ssi['Study'])):
                    if len(self.d_ssi['Study']) == 0:
                        break
                    else:
                        if d['StudyInstanceUID'] == self.d_ssi['Study'][i]['StudyInstanceUID']:
                            print("This Study already exist")
                            return False

                converted_d.append(self.dcm_convertor(d))

            self.study_insert(converted_d)
            self.series_insert(converted_d)
            self.image_insert(converted_d)
            self.connect.commit()
            # ssi insert 후 Model set
            self.set_total_ssi()
            return converted_d

        else:
            return False

    # DCM 한 묶음이 같은 Study 임을 가정
    def set_model_ssi(self, data):
        _data = data
        # self.uid_decision -> dcm 파일 모두 같은 UID를 가졌다면
        _return = self.uid_decision(_data)
        _ssi = defaultdict(dict)
        _ssi['Study'] = {}
        if _return:
            for i in range(0, len(self.d_ssi['Study'])):
                # DB에 추가된 ssi가 없다면
                if len(self.d_ssi['Study']) == 0:
                    pass
                else:
                    if _data[0]['StudyInstanceUID'] == self.d_ssi['Study'][i]['StudyInstanceUID']:
                        _ssi['Study'][i] = self.d_ssi['Study'][i]
                    else:
                        pass
        else:
            pass
        return _ssi

    # dcm 파일중에 UID가 다른 파일이 들어있는지 확인
    def uid_decision(self, data):
        _data = data
        _result = None
        _pUID = None

        for i, v in enumerate(_data):
            if i == 0:
                _pUID = v['StudyInstanceUID']
                continue
            else:
                if _pUID != v['StudyInstanceUID']:
                    print("incorrect sutdyUID" + v['FileName'])
                    _result = False
                    return _result
                else:
                    _result = True

        return _result

    def set_total_ssi(self):
        self.cursor.execute("select * from Study")
        study = self.cursor.fetchall()
        self.cursor.execute("select * from Series")
        series = self.cursor.fetchall()
        self.cursor.execute("select * from Image")
        image = self.cursor.fetchall()

        self.d_ssi["Study"] = {}
        for study_i, study_v in enumerate(study):
            self.d_ssi["Study"][study_i] = study_v
            self.d_ssi["Study"][study_i]["Series"] = {}
            for series_i, series_v in enumerate(series):
                if study_v['Study_Key'] == series_v['Study_Key']:
                    self.d_ssi["Study"][study_i]["Series"][series_i] = series_v
                    self.d_ssi["Study"][study_i]["Series"][series_i]["Image"] = {}
                for image_i, image_v in enumerate(image):
                    if study_v['Study_Key'] == image_v['Study_Key'] and series_v['Series_Key'] == image_v['Series_Key']:
                        self.d_ssi["Study"][study_i]["Series"][series_i]["Image"][image_i] = image_v

                        # for study_i, study_v in enumerate(study):
                        #     self.d_ssi[study_i] = study_v
                        #     for series_i, series_v in enumerate(series):
                        #         if study_v['Study_Key'] == series_v['Study_Key']:
                        #             self.d_ssi[study_i][series_i] = series_v
                        #         for image_i, image_v in enumerate(image):
                        #             if study_v['Study_Key'] == image_v['Study_Key'] and series_v['Series_Key'] == image_v['Series_Key']:
                        #                 self.d_ssi[study_i][series_i][image_i] = image_v

    def get_total_ssi(self):
        return self.d_ssi

    def study_insert(self, converted_d):
        sql = self.QD.StudyQuery_Insert()

        prev_studyUID = None

        for v in converted_d:
            if prev_studyUID != v['StudyInstanceUID']:
                self.cursor.execute(sql, (
                    v['StudyInstanceUID'],
                    v['Examed'],
                    v['NumberOfImage'],
                    v['PatientAge'],
                    v['PatientID'],
                    v['PatientName'],
                    v['PatientSex'],
                    v['StudyDateTime'],
                    v['StudyDesc'],
                    v['StudyID'],
                    v['StudyInstanceUID'],
                    v['StudyInstanceUID']
                    )
                )
            else:
                pass
            prev_studyUID = v['StudyInstanceUID']

        #TODO!! 중복 dict 제거
        # d = [dict(y) for y in set(tuple(x.items()) for x in _converted_d)]
        print("Study Insert Success")

    def series_insert(self, converted_d):
        sql = self.QD.SeriesQuery_Insert()
        prev_seriesUID = None

        self.cursor.execute("select Study_Key from Study Where StudyInstanceUID = (?)",
                            (converted_d[0]['StudyInstanceUID'], ))
        result = self.cursor.fetchall()
        study_key = result[0]['Study_Key']

        for v in converted_d:
            if prev_seriesUID != v['SeriesInstanceUID']:
                self.cursor.execute(sql, (
                    v['BodyPart'],
                    v['Modality'],
                    v['NumberOfImage'],
                    v['SeriesDateTime'],
                    v['SeriesDesc'],
                    v['SeriesInstanceUID'],
                    v['SeriesNumber'],
                    study_key,
                    v['FrameType'],
                    None,
                    v['RefPhysicianName'],
                    v['SeriesInstanceUID']
                    )
                )
            else:
                pass
            prev_seriesUID = v['SeriesInstanceUID']

        print("Series Insert Success")

    def image_insert(self, converted_d):
        sql = self.QD.ImageQuery_Insert()

        prev_imageUID = None

        # TODO!! Series <-> Image 관계 설정(ImageAttr)
        for v in converted_d:
            self.cursor.execute('''
                                select Std.Study_Key, Sri.Series_Key
                                from Study as Std, Series as Sri
                                Where Std.StudyInstanceUID = (?) and Sri.SeriesInstanceUID = (?)
                                ''',
                                (v['StudyInstanceUID'], v['SeriesInstanceUID'],))
            result = self.cursor.fetchall()
            study_key = result[0]['Study_Key']
            series_key = result[0]['Series_Key']

            if prev_imageUID != v['SOPInstanceUID']:
                self.cursor.execute(sql,(
                    v['AcquisitionNumber'],
                    v['FileName'],
                    None,
                    v['ImageWidth'],
                    v['ImageHeight'],
                    v['ImageNumber'],
                    v['ImageOrientation'],
                    v['ImagePosition'],
                    v['ImageResolution'],
                    v['ImgType'],
                    v['PathName'],
                    study_key,
                    series_key,
                    v['SOPClassUID'],
                    v['SOPInstanceUID'],
                    v['FrameNumber'],
                    v['SOPInstanceUID']
                    )
                )
            else:
                pass
            prev_imageUID = v['SOPInstanceUID']

        print("Image Insert Success")

    def study_delete(self, study_key):
        _study_key = study_key
        sql = self.QD.StudyQuery_Delete()
        try:
            self.cursor.execute(sql, (
                _study_key,
            )
        )
        except sqlite3.ProgrammingError as e:
            print(e)
            self.connect.rollback()
            return False

        self.connect.commit()
        return True

    def series_delete(self, study_key, series_key):
        _study_key = study_key
        _series_key = series_key
        sql = self.QD.SeriesQuery_Delete()
        try:
            self.cursor.execute(sql, (
                _study_key,
                _series_key,
                )
            )
        except sqlite3.ProgrammingError as e:
            print(e)
            self.connect.rollback()
            return False

        self.connect.commit()
        return True

    def image_delete(self, study_key, series_key):
        _study_key = study_key
        _series_key = series_key
        sql = self.QD.ImageQuery_Delete()
        try:
            self.cursor.execute(sql, (
                _study_key,
                _series_key,
            )
                                )
        except sqlite3.ProgrammingError as e:
            print(e)
            self.connect.rollback()
            return False

        self.connect.commit()
        return True

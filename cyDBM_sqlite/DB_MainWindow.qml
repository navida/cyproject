import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQml 2.0
import "qml/"

Item{
  id: root
  width: 1100
  height: 650

//  Style{
//    id: style
//  }

  GridLayout{
    id: grid_layout
    anchors.fill: parent

    Layout.preferredWidth: width
    Layout.preferredHeight: height
    Layout.maximumHeight: height
    Layout.maximumWidth: width
    Layout.minimumHeight: height
    Layout.minimumWidth: width

    rows    : 12
    columns : 12

    rowSpacing: 0
    columnSpacing: 0

    property double colMulti : grid_layout.width / grid_layout.columns
    property double rowMulti : grid_layout.height / grid_layout.rows

    function prefWidth(item){
      return colMulti * item.Layout.columnSpan
    }
    function prefHeight(item){
      return rowMulti * item.Layout.rowSpan
    }

    Rectangle{
      id: rect
      objectName: "rect"
      Layout.rowSpan   : 1
      Layout.columnSpan: 12
      Layout.preferredWidth  : grid_layout.prefWidth(this)
      Layout.preferredHeight : grid_layout.prefHeight(this)

      property var column: 0
      property var columnSpan: 12
      property var row: 0
      property var rowSpan: 12

      onColumnChanged: {
        Layout.column = column
      }
      onColumnSpanChanged: {
        Layout.columnSpan = columnSpan
      }
      onRowChanged: {
        Layout.row = row
      }
      onRowSpanChanged: {
        Layout.rowSpan = rowSpan
      }

      RowLayout{
        anchors.centerIn: parent
        spacing: 30

        Button{
          id: load_button
          objectName: "load_button"
          width:50
          height:30
          text: "LOAD"
          checkable: true
        }

        Button{
          id: input_button
          objectName: "input_button"
          width:50
          height:30
          text: "INPUT"

          property string path: ""
          // It property is control the visible of FileDialog
          property bool b: false

          signal sig_input_button_clicked(string path, bool b)

          onClicked: {
            if(input_button.b === true){
              input_button.sig_input_button_clicked(dcm_dialog.folder_path, b)
            }else{
              input_button.sig_input_button_clicked(dcm_dialog.folder_path, b)
            }
          }
        }

        Button{
          id: btn_test
          objectName: "btn_test"
          width:50
          height:30
          text: "btn_test"

          signal sig_btn_test_clicked(var index)

          onClicked: {
//            sig_btn_test_clicked()
            console.log(project_list_treeview_itemselectionmodel.currentIndex.row)
            sig_btn_test_clicked(project_list_treeview_itemselectionmodel.currentIndex)
          }
        }
      }
    }

    Rectangle{
      Layout.rowSpan   : 1
      Layout.columnSpan: 12
      Layout.preferredWidth  : grid_layout.prefWidth(this)
      Layout.preferredHeight : grid_layout.prefHeight(this)

      TextArea{
        id: dcm_file_textarea
        objectName: "dcm_file_textarea"
        anchors.fill: parent
        readOnly: true
      }
    }

    Rectangle{
      Layout.rowSpan   : 3
      Layout.columnSpan: 12
      Layout.preferredWidth  : grid_layout.prefWidth(this)
      Layout.preferredHeight : grid_layout.prefHeight(this)

      Project_TreeView_ItemDelegate{
        id: my_treeview_itemdelegate
      }

      Project_TreeView_HeaderDelegate{
        id: my_treeview_headerdelegate
      }

      TreeView{
        id: project_list_treeview
        objectName: "project_list_treeview"
        //model: project_list_treeview_model
        model: (typeof(project_list_treeview_model) === "undefined") ? null : project_list_treeview_model
        anchors.fill: parent
        headerVisible: true
        sortIndicatorVisible: true

        selectionMode: SelectionMode.ExtendedSelection

        selection: ItemSelectionModel{
          id: project_list_treeview_itemselectionmodel
          objectName: "project_list_treeview_itemselectionmodel"
          model: (typeof(project_list_treeview_model) === "undefined") ? null : project_list_treeview_model

          function set_index(index){
            var _index = index
            project_list_treeview.sample_func(_index)
            project_list_treeview_itemselectionmodel.setCurrentIndex(_index, ItemSelectionModel.ClearAndSelect)
          }
        }

        signal sig_menu_trigger(string type, var index)
        signal sig_header_clicked_for_sort(var sortIndicatorColumn, var sortIndicatorOrder)
        signal sig_childitem_dclick(bool v, var index)

        function sample_func(index){
          console.log("aaaaaaaaaaaAAA")
//          __listView.positionViewAtIndex(index, TreeView.Center)
          __listView.positionViewAtIndex(10, ListView.Beginning)
          console.log("bbbbbbbbbbbBBB")
        }

//        style: TreeViewStyle{
////          backgroundColor: style.root_item2.backgroundcolor
//          backgroundColor: style.root_item1.backgroundcolor
////          backgroundColor: "white"
//          alternateBackgroundColor:"white"
//        }

//        itemDelegate: my_treeview_itemdelegate
//        headerDelegate: my_treeview_headerdelegate

        TableViewColumn{
          width : 320
          role: "PatientID"
          title: "PatientID"
        }
        TableViewColumn{
          width : 180
          role: "PatientName"
          title: "PatientName"
        }
        TableViewColumn{
          width : 150
          role: "DateTime"
          title: "DateTime"
        }
        TableViewColumn{
          width : 160
          role: "Description"
          title: "Description"
        }
        TableViewColumn{
          width : 80
          role: "Imgs"
          title: "Imgs"
        }
        TableViewColumn{
          width : 80
          role: "Modality"
          title: "Modality"
        }
        TableViewColumn{
          width : 80
          role: "Comment"
          title: "Comment"
        }
        TableViewColumn{
          width : 1
          visible: false
          role: "Key"
          title: "Key"
        }
        //TODO!! add Modules(Icon), Project inside

        onDoubleClicked: {
          //TODO!! Project open dialog
          if(isExpanded(project_list_treeview_itemselectionmodel.currentIndex) === true){
            collapse(project_list_treeview_itemselectionmodel.currentIndex)
            if(project_list_treeview_itemselectionmodel.currentIndex.parent.row === 0){
              console.log("This Child")
//              project_open_dialog.open()
              project_open_dialog.visible = true
              project_list_treeview.sig_childitem_dclick(project_open_dialog.visible, project_list_treeview_itemselectionmodel.selectedIndexes[0])
            }else{
              console.log("This Parent")
            }
          }else{
            expand(project_list_treeview_itemselectionmodel.currentIndex)
            if(project_list_treeview_itemselectionmodel.currentIndex.parent.row === 0){
              console.log("This Child")
//              project_open_dialog.open()
              project_open_dialog.visible = true
              project_list_treeview.sig_childitem_dclick(project_open_dialog.visible, project_list_treeview_itemselectionmodel.selectedIndexes[0])
            }else{
              console.log("This Parent")
            }
          }
  //        if(isExpanded(project_list_treeview.currentIndex) === true){
  //          collapse(project_list_treeview.currentIndex)
  //          if(currentIndex.parent.row === 0){
  //            console.log("This Child")
  //          }else{
  //            console.log("This Parent")
  //          }
  //        }else{
  //          expand(project_list_treeview.currentIndex)
  //          if(currentIndex.parent.row === 0){
  //            console.log("This Child")
  //          }else{
  //            console.log("This Parent")
  //          }
  //        }
        }

        onClicked: {
//          console.log("currentIndex : " + currentIndex.internalPointer)
          //        console.log("currentIndex : " + currentIndex.column)
          //        console.log("currentIndex : " + currentIndex.parent.row)
          //        console.log("grid_layoutIndex : " + grid_layoutIndex)
          //        console.log(styleData)
        }
      }
    }
  }

  FileDialog{
    id: dcm_dialog
    objectName: "dcm_dialog"
    visible: load_button.checked
    modality: Qt.NonModal
    selectExisting: true
    selectFolder: true
    sidebarVisible: true
    nameFilters: [ "dcm files (*.dcm)"]
    selectedNameFilter: "All files (*)"

    property string folder_path: ""
    signal sig_dcm_dialog_open(string path)

    onAccepted: {
      if(Qt.platform.os=="windows"){
        folder_path = fileUrl.toString().substring(8);
        console.log(folder_path)
        dcm_dialog.sig_dcm_dialog_open(folder_path)
      }else{
        folder_path = fileUrl.toString().substring(7);
        console.log(folder_path)
        dcm_dialog.sig_dcm_dialog_open(folder_path)
      }

      input_button.path = fileUrls.toString()
      input_button.b = true
      load_button.checked = false
    }
    onRejected: {
      load_button.checked = false
    }
  }

  Project_Open_Dialog{
    id: project_open_dialog
  }
}


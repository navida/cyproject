from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QItemSelectionModel
from PyQt5.QtQuick import QQuickView
from PyQt5.QtGui import QGuiApplication
from PyQt5.Qt import QModelIndex
import DB_DBModelManager as DB
import sys,os
import _qapp


class MainWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.manager = DB.DatabaseModelManager()

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'DB_MainWindow.qml')))
        # self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'Custom_DB_MainWindow.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)

        self.rootContext().setContextProperty('project_list_treeview_model', self.manager.project_treeview_model)

        self.show()

        self.init_sig_slot()

    def init_sig_slot(self):
        self.rect = self.rootObject().findChild(QObject, 'rect')
        self.rect.setProperty("column", 7)
        self.rect.setProperty("columnSpan", 5)
        self.rect.setProperty("row", 0)
        self.rect.setProperty("rowSpan", 4)

        self.rect.setProperty("column", 7)
        self.rect.setProperty("columnSpan", 5)
        self.rect.setProperty("row", 0)
        self.rect.setProperty("rowSpan", 6)
        # self.rootObject().findChild(QObject, 'import_button').clicked.connect(lambda : print("import btn"))
        self.rootObject().findChild(QObject, 'dcm_dialog').sig_dcm_dialog_open.connect(self.sig_dcm_dialog_open)
        self.rootObject().findChild(QObject, 'input_button').sig_input_button_clicked.connect(self.sig_input_button_clicked)
        self.rootObject().findChild(QObject, 'btn_test').sig_btn_test_clicked.connect(self.sig_btn_test_clicked)
        self.rootObject().findChild(QObject, 'project_list_treeview').sig_menu_trigger.connect(self.sig_menu_trigger)
        self.rootObject().findChild(QObject, 'project_list_treeview').sig_header_clicked_for_sort.connect(self.sig_header_clicked_for_sort)
        self.rootObject().findChild(QObject, 'project_list_treeview').sig_childitem_dclick.connect(self.sig_childitem_dclick)
        self.dcm_file_textarea = self.rootObject().findChild(QObject, 'dcm_file_textarea')

        self.itemselect_model = self.rootObject().findChild(QObject,'project_list_treeview_itemselectionmodel')

    @pyqtSlot(QVariant, QVariant)
    def sig_header_clicked_for_sort(self, column, order):
        _column = column
        _order = order
        self.manager.PT_sort_data(_column, _order)

    @pyqtSlot(str, QVariant)
    def sig_menu_trigger(self, type, index):
        if self.manager.PT_change_data_modelitem(type, index):
            print("success delete data and modelitem")
        else:
            print("fail delete data and modelitem")

    @pyqtSlot(str)
    def sig_dcm_dialog_open(self, path):
        self.dcm_file_textarea.setProperty("text", "Road dcm file path : " + path)

    @pyqtSlot(str, bool)
    def sig_input_button_clicked(self, path, b):
        if self.manager.PT_add_data_database_and_model(path, b):
            pass
        else:
            self.dcm_file_textarea.setProperty("text", "Study already exist or Select folder contain dcm file ")

    @pyqtSlot(bool, QVariant)
    def sig_childitem_dclick(self, visible, index):
        if visible:
            self.manager.POT_set_model(index)
            self.rootContext().setContextProperty('project_open_dialog_treeview_model', self.manager.project_open_dialog_treeview_model)
        else:
            pass

    @pyqtSlot(QVariant)
    def sig_btn_test_clicked(self, index):
        _index_t = index
        _index = self.itemselect_model.model().match(self.itemselect_model.model().index(0, 0, QModelIndex()), 262, 52, -1, Qt.MatchRecursive)[1]
        self.itemselect_model.set_index(_index)

if __name__ == '__main__':
    _qapp = QGuiApplication(sys.argv)

    window = MainWindow()

    _qapp.exec()

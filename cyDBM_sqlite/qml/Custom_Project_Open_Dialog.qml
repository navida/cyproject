import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
  id: project_open_dialog_item
  objectName: 'project_open_dialog_item'

  property real drag_minimumX
  property real drag_maximumX
  property real drag_minimumY
  property real drag_maximumY

  Rectangle {
    id: dialog_items
    width: parent.width
    height: parent.height
    implicitWidth: parent.width
    implicitHeight: parent.height
    color: '#efefef'
    radius: 10
    z: 1

    Column{
      anchors.fill: parent
      RowLayout{
        id: content1
        width: dialog_items.width
        height: 30
        spacing: 0

        MouseArea{
          anchors.fill: parent
          drag.target: project_open_dialog_item
          drag.axis: Drag.XAxis | Drag.YAxis
          drag.minimumX: project_open_dialog_item.drag_minimumX
          drag.maximumX: project_open_dialog_item.drag_maximumX
          drag.minimumY: project_open_dialog_item.drag_minimumY
          drag.maximumY: project_open_dialog_item.drag_maximumY
        }

        Rectangle{
          anchors.fill: parent
          color:"black"

          Button {
            id: project_open_dialog_item_close
            objectName: "project_open_dialog_item_close"
            implicitWidth : 30
            implicitHeight : 30
            anchors.top: parent.top
            anchors.right: parent.right
            // test_btn is in outside !
            onClicked: project_open_dialog.visible = false
          }
        }
      }
      RowLayout {
        id: content2
        width: dialog_items.width
        height: dialog_items.height - content1.height
        spacing: 0
        Rectangle{
          anchors.fill: parent
          color:"yellow"
        }
        MouseArea{
          anchors.fill: parent
          onClicked: {
            console.log(" in Mouse Area")
          }
        }
      }
    }
  }
}

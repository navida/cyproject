import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Dialog {
  id: dialog
  objectName: "dialog"
  modality: Qt.ApplicationModal

  contentItem:
    Rectangle {
    color: "transparent"
    implicitWidth: 600
    implicitHeight: 500

    TabView{
      id: project_open_dialog_tabview
      objectName: "project_open_dialog_tabview"
      anchors.fill: parent

      style: TabViewStyle {
        frameOverlap: 1
        tab: Rectangle {
          color: styleData.hovered || styleData.selected ? "#252524" :"#313231"
          //border.color:  "#17375E"
          implicitWidth: Math.max(text.width + 4, 115)
          implicitHeight: 30
          radius: 2
          Label {
            id: text
            anchors.centerIn: parent
            text: styleData.title
            color: styleData.selected ? "#2BC2F3" : "#FFFFFF"
          }
        }
        tabBar: Component {
          Rectangle {
            color: "#313231"
          }
        }
      }

      Tab {
        title: "<b>Normal</b>"
        id: normal_tab
        objectName: "normal_tab"
        active:true

        Rectangle{
          anchors.fill: parent
          color: "#313231"

          TreeView{
            width: parent.width
            height: 250
            //model: project_open_dialog_treeview_model
            model: (typeof(project_open_dialog_treeview_model) === "undefined") ? null : project_open_dialog_treeview_model

            TableViewColumn{
              width : 30
              role: "ID"
              title: "ID"
            }
            TableViewColumn{
              width : 100
              role: "SeriesNo"
              title: "SeriesNo"
            }
            TableViewColumn{
              width : 150
              role: "Direction"
              title: "Direction"
            }
            TableViewColumn{
              width : 50
              role: "Imgs"
              title: "Imgs"
            }
            TableViewColumn{
              width : 50
              role: "Gap"
              title: "Gap"
            }
            TableViewColumn{
              width : 50
              role: "Res"
              title: "Res"
            }
            TableViewColumn{
              width : 1
              visible: false
              role: "SeriesKey"
              title: "SeriesKey"
            }
          }
        }
      }

      Tab {
        title: "<b>Project</b>"
        id: project_tab
        objectName: "project_tab"
        Rectangle{
          anchors.fill: parent
          color: "red"
        }
      }
    }
  }
}

//    Button{
//      id: dialog_open_button
//      anchors{
//        right: parent.right
//        rightMargin: 100
//        bottom: parent.bottom
//        bottomMargin: 10
//      }

//      text: "Open"
//      objectName: "dialog_open_button"
//      onClicked: {
//        //TODO!! Project open
//      }
//    }

//    Button{
//      id: dialog_close_button
//      anchors{
//        left: dialog_open_button.right
//        leftMargin: 5
//        verticalCenter: dialog_open_button.verticalCenter
//      }
//      text: "Close"
//      objectName: "dialog_close_button"
//      onClicked: {
//        dialog.close()
//      }
//    }

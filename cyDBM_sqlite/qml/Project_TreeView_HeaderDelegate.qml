import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQml.Models 2.2

Component {
  Rectangle{
    color: "transparent"
    width: parent.width
    height: 20

    Rectangle{
      width: parent.width
      height: 1
      color: "black"
      anchors.top: parent.top
    }

    Rectangle{
      width: parent.width
      height: 1
      color: "black"
      anchors.bottom: parent.bottom
    }

    Rectangle{
      width: 1
      height: parent.height
      color: "black"
      anchors.right: parent.right
    }

    Text {
      text: String(styleData.value) === "undefined" ? "": styleData.value
    }

    Connections {
      id: header_click_connections
      target: styleData
      property var prev_column: -1
      property bool trigger: true
      onPressedChanged: {
//        if(prev_column !== styleData.column){
//          project_list_treeview.sig_header_clicked_for_sort(prev_column, Qt.AscendingOrder)
//          prev_column = styleData.column
//          console.log(prev_column)
//          console.log(styleData.column)
//        }else{
//          project_list_treeview.sig_header_clicked_for_sort(prev_column, Qt.DescendingOrder)
//          console.log(prev_column)
//          console.log(styleData.column)
//        }
        if(styleData.pressed){

        }else{
          if(prev_column === styleData.column){
            if(header_click_connections.trigger === true){
              project_list_treeview.sig_header_clicked_for_sort(styleData.column, Qt.AscendingOrder)
              header_click_connections.trigger = false
            }else{
              project_list_treeview.sig_header_clicked_for_sort(styleData.column, Qt.DescendingOrder)
              header_click_connections.trigger = true
            }
          }else{
            project_list_treeview.sig_header_clicked_for_sort(styleData.column, Qt.DescendingOrder)
            prev_column = styleData.column
          }
        }
      }
    }
  }
}

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import cyhub 1.0

Dialog {
  id: leaf_implant_dialog_main
  modality: Qt.ApplicationModal

  contentItem:Rectangle {
    width: 800
    height: 300
    implicitWidth: width
    implicitHeight: height
    GridLayout{
      id: root
      anchors.fill: parent

      Layout.preferredWidth: width
      Layout.preferredHeight: height
      Layout.maximumHeight: height
      Layout.maximumWidth: width
      Layout.minimumHeight: height
      Layout.minimumWidth: width

      rows    : 12
      columns : 12

      rowSpacing: 0
      columnSpacing: 0

      property double colMulti : root.width / root.columns
      property double rowMulti : root.height / root.rows

      function prefWidth(item){
        return colMulti * item.Layout.columnSpan
      }
      function prefHeight(item){
        return rowMulti * item.Layout.rowSpan
      }

      Rectangle{
        Layout.rowSpan   : 12
        Layout.columnSpan: 3
        Layout.preferredWidth  : root.prefWidth(this)
        Layout.preferredHeight : root.prefHeight(this)

        TextField{
          id: manufacturer_search_textfield
          objectName: "manufacturer_search_textfield"

          anchors{
            top: parent.top
            horizontalCenter: parent.horizontalCenter
          }

          implicitWidth: parent.width
          implicitHeight: 20
          cursorPosition: 10
          placeholderText: qsTr("Search!!!")

          signal sig_search_textfield(string str)

          style: TextFieldStyle{
            background: Rectangle {
              radius: 5
              border.color: "white"
              border.width: 1
            }
          }

          onTextChanged:{
            manufacturer_search_textfield.sig_search_textfield(manufacturer_search_textfield.text)
          }
        }
//      TODO!!!! custom leaf implant import
        TreeView{
          id: leaf_implant_manufacture_treeview
          objectName: "leaf_implant_manufacture_treeview"
          anchors.top: manufacturer_search_textfield.bottom
          width: parent.width
          height: parent.height - manufacturer_search_textfield.height
          model: (typeof(leaf_implant_manufacture_treeview_model) === "undefined") ? null : leaf_implant_manufacture_treeview_model
          headerVisible: false

          signal sig_manufacture_item_clicked(var index)
          signal sig_manufacture_treeview_key_event(string s)

          selection: ItemSelectionModel{
            id: leaf_implant_manufacture_treeview_itemselectionmodel
            model: (typeof(leaf_implant_manufacture_treeview_model) === "undefined") ? null : leaf_implant_manufacture_treeview_model
          }

          TableViewColumn{
            width : 200
            role: "ManufacturerName"
            title: "ManufacturerName"
          }
          TableViewColumn{
            width : 1
            visible: false
            role: "ID"
            title: "ID"
          }

          onDoubleClicked: {
            if(isExpanded(leaf_implant_manufacture_treeview.currentIndex) === true){
              collapse(leaf_implant_manufacture_treeview.currentIndex)
            }else{
              expand(leaf_implant_manufacture_treeview.currentIndex)
            }
          }

          onClicked: {
            if(currentIndex.parent.row === 0){
              sig_manufacture_item_clicked(leaf_implant_manufacture_treeview_itemselectionmodel.selectedIndexes[0])
            }
          }

        }
      }

      Rectangle{
        Layout.rowSpan   : 12
        Layout.columnSpan: 4
        Layout.preferredWidth  : root.prefWidth(this)
        Layout.preferredHeight : root.prefHeight(this)

        TreeView{
          id: leaf_implants_treeview
          objectName: "leaf_implants_treeview"
          width: parent.width
          height: parent.height
          model: (typeof(leaf_implants_treeview_model) === "undefined") ? null : leaf_implants_treeview_model

          signal sig_implants_item_clicked(var index)
          signal sig_implants_item_dbl_clicked()

          selection: ItemSelectionModel{
            id: leaf_implants_treeview_itemselectionmodel
            model: (typeof(leaf_implants_treeview_model) === "undefined") ? null : leaf_implants_treeview_model
          }

          TableViewColumn{
            width : 100
            role: "ManufacturerName"
            title: "Model Number"
          }
          TableViewColumn{
            width : 1
            visible: false
            role: "ID"
            title: "ID"
          }
          TableViewColumn{
            width : 1
            visible: false
            role: "NominalLength"
            title: "Length"
          }
          TableViewColumn{
            width : 80
            role: "DiameterNominal"
            title: "Diameters"
          }
          TableViewColumn{
            width : 60
            role: "NominalLength"
            title: "Length"
          }

          onDoubleClicked: {
            leaf_implants_treeview.sig_implants_item_dbl_clicked();
          }

          onClicked: {
            leaf_implants_treeview.sig_implants_item_clicked(leaf_implants_treeview_itemselectionmodel.selectedIndexes[0]);
          }

        }
      }
      Rectangle{
        Layout.rowSpan   : 12
        Layout.columnSpan: 5
        Layout.preferredWidth  : root.prefWidth(this)
        Layout.preferredHeight : root.prefHeight(this)
        //color: "black"

        ImageHolder {
            id: vtk_preview
            objectName: name
            property var name: "vtk_preview"
            property var prev_width: 0
            property var prev_height: 0
            //Layout.alignment: Qt.AlignTop
            implicitWidth: parent.width
            implicitHeight: parent.height
        }
      }
//      Rectangle{
//        Layout.rowSpan   : 4
//        Layout.columnSpan: 12
//        Layout.preferredWidth  : root.prefWidth(this)
//        Layout.preferredHeight : root.prefHeight(this)
//        color: "blue"
//      }
    }
  }
}


from PyQt5.QtCore import QObject, QModelIndex, QAbstractItemModel, QByteArray, Qt, QVariant
from PyQt5.QtCore import pyqtSlot as Slot

class DBItem(object):
    def __init__(self, parent=None):
        self.itemData = dict()
        self.parentItem = parent

    def create_parent_item(self, id, createdate, study_key, gender):
        self.itemData['PatientID'] = id
        self.itemData['CreateDate'] = createdate
        self.itemData['Study_Key'] = study_key
        self.itemData['Gender'] = gender

    def parent(self):
        return self.parentItem

    def get_parent_data(self, role):
        column_data = {
            Qt.DisplayRole : QVariant(self.itemData['PatientID']),
            Qt.WhatsThisRole: QVariant(self.itemData['CreateDate']),
            Qt.WhatsThisRole + 1: QVariant(self.itemData['Study_Key']),
            Qt.WhatsThisRole + 2: QVariant(self.itemData['Gender'])
        }
        try :
            return column_data[role]
        except KeyError:
            return QVariant()


class Study_Model(QAbstractItemModel):
    def __init__(self, list, parent=None, *args):
        QAbstractItemModel.__init__(self, parent)

        ##################################################################
        self.studies = []
        self.headers = ['Patient ID', 'CreateDate', 'Key', 'Gender']
        self.columns = len(self.headers)
        self.study_list = list
        ##################################################################

        '''PatientID'''
        id_role = QByteArray()
        id_role.append("PatientID")
        '''PatientName'''
        createdate_role = QByteArray()
        createdate_role.append("CreateDate")
        '''Key'''
        key_role = QByteArray()
        key_role.append("Key")
        '''Gender'''
        gender_role = QByteArray()
        gender_role.append("Gender")

        self.roles = {
            # Study Role
            Qt.DisplayRole: id_role,
            Qt.WhatsThisRole: createdate_role,
            Qt.WhatsThisRole + 1: key_role,
            Qt.WhatsThisRole + 2: gender_role,
        }

        for x in range(0, len(self.study_list)):
            parent_item = DBItem()
            parent_item.create_parent_item(str(self.study_list[x].data[0][5]),
                                           self.study_list[x].data[0][3],
                                           self.study_list[x].data[0][0],
                                           self.study_list[x].data[0][7]
                                           )
            self.studies.append(parent_item)

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headers[section])
        return QVariant()

    def rowCount(self, parent=QModelIndex()):
        if parent.isValid():
            items = parent.internalPointer()
            return len(items.children())

        return len(self.studies)

    def columnCount(self, parent):
        return self.columns

    def index(self, row, column, parent):
        if parent.isValid():
            item = parent.internalPointer()
            return self.createIndex(row, column, item.children()[row])

        return self.createIndex(row, column, self.studies[row])

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        item = index.internalPointer()
        if not item.parentItem:
            return QModelIndex()

        _children = item.children()
        try:
            _index = _children.index(item.parentItem)
        except ValueError:
            _index = 0
        return self.createIndex(_index, 0, item.parentItem)

    def data(self, index, role):
        if not index.isValid():
            return None

        _item = index.internalPointer()

        if _item.parentItem is None:
            _model = self.studies[index.row()]
            return _model.get_parent_data(role)

    def roleNames(self):
        return self.roles
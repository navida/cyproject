from PyQt5.QtCore import QObject, QModelIndex, QAbstractItemModel, QByteArray, Qt, QVariant, QSortFilterProxyModel


class CyProjectTreeViewItem(object):
    def __init__(self, parent=None):
        self.itemData = dict()
        self.childItems = []
        self.parentItem = parent

    def create_parent_item(self, data):
        _data = data
        self.itemData['PatientID'] = _data['PatientID']
        self.itemData['PatientName'] = _data['PatientName']
        self.itemData['DateTime'] = _data['StudyDateTime']
        self.itemData['Description'] = _data['StudyDescription']
        self.itemData['Imgs'] = _data['NumberOfImages']
        self.itemData['Modality'] = None
        self.itemData['Comment'] = _data['Comments']
        self.itemData['Key'] = _data['Study_Key']

    def add_child_item(self, data):
        _data = data
        child_item = CyProjectTreeViewItem(self)
        child_item.itemData['PatientID'] = _data['SeriesNumber']
        child_item.itemData['PatientName'] = None
        child_item.itemData['DateTime'] = _data['SeriesDateTime']
        child_item.itemData['Description'] = _data['SeriesDescription']
        child_item.itemData['Imgs'] = _data['NumberOfImages']
        child_item.itemData['Modality'] = _data['Modality']
        child_item.itemData['Comment'] = _data['Comments']
        child_item.itemData['Key'] = _data['Series_Key']

        self.childItems.append(child_item)

    def childAtRow(self, row):
        return self.childItems[row]

    def children(self):
        return self.childItems

    def parent(self):
        return self.parentItem

    def get_parent_data(self, role):
        column_data = {
            Qt.DisplayRole: QVariant(self.itemData['PatientID']),
            Qt.UserRole: QVariant(self.itemData['PatientName']),
            Qt.UserRole + 1: QVariant(self.itemData['DateTime']),
            Qt.UserRole + 2: QVariant(self.itemData['Description']),
            Qt.UserRole + 3: QVariant(self.itemData['Imgs']),
            Qt.UserRole + 4: QVariant(self.itemData['Modality']),
            Qt.UserRole + 5: QVariant(self.itemData['Comment']),
            Qt.UserRole + 6: QVariant(self.itemData['Key'])
        }
        try:
            return column_data[role]
        except KeyError:
            return QVariant()

    #TODO! will be change series role
    def get_child_data(self, role):
        column_data = {
            Qt.DisplayRole: QVariant(self.itemData['PatientID']),
            Qt.UserRole: QVariant(self.itemData['PatientName']),
            Qt.UserRole + 1: QVariant(self.itemData['DateTime']),
            Qt.UserRole + 2: QVariant(self.itemData['Description']),
            Qt.UserRole + 3: QVariant(self.itemData['Imgs']),
            Qt.UserRole + 4: QVariant(self.itemData['Modality']),
            Qt.UserRole + 5: QVariant(self.itemData['Comment']),
            Qt.UserRole + 6: QVariant(self.itemData['Key'])
        }
        try:
            return column_data[role]
        except KeyError:
            return QVariant()

            # def sort_children(self):
            #     if len(self.childItems) == 0 :
            #         return
            #     sorted_list = sorted(self.childItems, key = lambda _item : (_item.itemData['Series_Key'] ) )
            #     self.childItems = sorted_list[:]


class CyProjectOpenDialogViewItem(object):
    def __init__(self, parent=None):
        self.itemData = dict()
        self.childItems = []
        self.parentItem = parent

    def create_parent_item(self, data):
        _data = data
        self.itemData['ID'] = None
        self.itemData['SeriesNo'] = _data['SeriesNumber']
        self.itemData['Direction'] = _data['SeriesDescription']
        self.itemData['Imgs'] = len(_data['Image'])
        self.itemData['Gap'] = None
        self.itemData['Res'] = None
        self.itemData['SeriesKey'] = _data['Series_Key']

    def childAtRow(self, row):
        return self.childItems[row]

    def children(self):
        return self.childItems

    def parent(self):
        return self.parentItem

    def get_parent_data(self, role):
        column_data = {
            Qt.DisplayRole: QVariant(self.itemData['ID']),
            Qt.UserRole: QVariant(self.itemData['SeriesNo']),
            Qt.UserRole + 1: QVariant(self.itemData['Direction']),
            Qt.UserRole + 2: QVariant(self.itemData['Imgs']),
            Qt.UserRole + 3: QVariant(self.itemData['Gap']),
            Qt.UserRole + 4: QVariant(self.itemData['Res']),
            Qt.UserRole + 5: QVariant(self.itemData['SeriesKey'])
        }
        try:
            return column_data[role]
        except KeyError:
            return QVariant()


class CyTreeModel(QAbstractItemModel):
    def __init__(self, header_item, *args, **kwds):
        super().__init__(*args, **kwds)

        self.items = []
        self.headers = header_item
        self.columns = len(self.headers)
        self.roles = dict()

        #TODO 새로운 Model 생성시 첫번째 Column Role 주의.
        for i, v in enumerate(header_item):
            _temp = QByteArray()
            _temp.append(v)
            if i == 0:
                self.roles[Qt.DisplayRole] = _temp
            else:
                self.roles[Qt.UserRole + i - 1] = _temp

    def project_treeview_item_addData(self, data):
        # TODO!!! exmaple deep tree item
        _data = data
        for v1 in _data['Study'].items():
            _parent_item = CyProjectTreeViewItem()
            _parent_item.create_parent_item(v1[1])
            self.items.append(_parent_item)

            for v2 in v1[1]['Series'].items():
                self.items[v1[0]].add_child_item(v2[1])

    def project_open_dialog_treeview_item_addData(self, data):
        # TODO!!! exmaple deep tree item
        _data = data
        _parent_item = CyProjectOpenDialogViewItem()
        _parent_item.create_parent_item(_data)
        self.items.append(_parent_item)

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headers[section])
        return QVariant()

    def rowCount(self, parent=QModelIndex()):
        if parent.isValid():
            items = parent.internalPointer()
            return len(items.children())
        return len(self.items)

    def columnCount(self, parent):
        return self.columns

    def index(self, row, column, parent):
        if parent.isValid():
            item = parent.internalPointer()
            return self.createIndex(row, column, item.children()[row])

        return self.createIndex(row, column, self.items[row])

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        item = index.internalPointer()
        if not item.parentItem:
            return QModelIndex()

        _children = item.children()
        try:
            _index = _children.index(item.parentItem)
        except ValueError:
            _index = 0
        return self.createIndex(_index, 0, item.parentItem)

    def data(self, index, role):
        if not index.isValid():
            return None

        _item = index.internalPointer()

        if _item.parentItem is None:
            _model = self.items[index.row()]
            return _model.get_parent_data(role)
        else:
            child_item_counts = len(_item.parentItem.childItems)
            if child_item_counts > 0 :
                _model =_item.parentItem.childItems[index.row()]
                return _model.get_child_data(role)

    def roleNames(self):
        return self.roles

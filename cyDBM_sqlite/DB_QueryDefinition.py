

class DataAccessor:
    strJoinQuery = """
    SELECT Distinct Study.AccessionNumber,
                    Study.Comments,
                    Study.Examed,
                    Study.NumberOfImages,
                    Study.NumberOfSeries,
                    Study.PatientAge,
                    Study.PatientID,
                    Study.PatientName,
                    Study.PatientSex,
                    Study.Reserved1,
                    Study.Reserved2,
                    Study.Study_key,
                    Study.StudyDateTime,
                    Study.StudyDescription,
                    Study.StudyID,
                    Study.StudyInstanceUID,
                    Study.Opened
    FROM (
           SELECT Study.AccessionNumber,
                  Study.Comments,
                  Study.Examed,
                  Study.NumberOfImages,
                  Study.NumberOfSeries,
                  Study.PatientAge,
                  Study.PatientID,
                  Study.PatientName,
                  Study.PatientSex,
                  Study.Reserved1,
                  Study.Reserved2,
                  Study.Study_key,
                  Study.StudyDateTime,
                  Study.StudyDescription,
                  Study.StudyID,
                  Study.StudyInstanceUID,
                  Study.Opened,
                  Series.BodyPart,
                  Series.Modality,
                  Series.Series_Key,
                  Series.SeriesDateTime,
                  Series.SeriesDescription,
                  Series.SeriesNumber
           FROM Study INNER JOIN Series ON Study.Study_key = Series.Study_key"""
    strWhereQuery = """
           WHERE ( """
    strGroupQuery = """
           GROUP BY Study.AccessionNumber,
                    Study.Comments,
                    Study.Examed,
                    Study.NumberOfImages,
                    Study.NumberOfSeries,
                    Study.PatientAge,
                    Study.PatientID,
                    Study.PatientName,
                    Study.PatientSex,
                    Study.Reserved1,
                    Study.Reserved2,
                    Study.Study_Key,
                    Study.StudyDateTime,
                    Study.StudyDescription,
                    Study.StudyID,
                    Study.StudyInstanceUID,
                    Study.Opened,
                    Series.BodyPart,
                    Series.Comments,
                    Series.Modality,
                    Series.Series_Key,
                    Series.SeriesDateTime,
                    Series.SeriesDescription,
                    Series.SeriesNumber ORDER BY Series.SeriesNumber ASC
    ) AS Study, Series"""
    strOrderQuery = """ ORDER BY Study.PatientID ASC 
    """
    strFindORQuery = ""
    strQuery = ""

    def StudyQuery_Select(self, search):
        searchString = search
        searchString = searchString.replace(" ", "")
        strToken = searchString.split(',')

        for i in range(len(strToken)):
            self.strFindORQuery = self.strFindORQuery + "(PatientID LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(PatientAge LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(PatientName LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(Study.StudyID LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(StudyDescription LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(BodyPart LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(SeriesNumber LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(SeriesDescription LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(Study.Comments LIKE " + "\'%" + "%s" % strToken[i] + "%\')" + " OR "
            self.strFindORQuery = self.strFindORQuery + "(Series.Comments LIKE " + "\'%" + "%s" % strToken[i] + "%\')"

            if (len(strToken) - 1) != i:
                self.strFindORQuery = self.strFindORQuery + " OR "
            else:
                self.strFindORQuery = self.strFindORQuery + " ) "

        # 쿼리 조립을 시작한다.
        strQuery = self.strJoinQuery + self.strWhereQuery + self.strFindORQuery + self.strGroupQuery + self.strOrderQuery
        return strQuery

    def StudyQuery_Insert(self):
        strQuery = """
            INSERT INTO Study (
                AccessionNumber,
                Examed,
                NumberOfImages,
                PatientAge,
                PatientID,
                PatientName,
                PatientSex,
                StudyDateTime,
                StudyDescription,
                StudyID,
                StudyInstanceUID
            )
            SELECT
              ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
            WHERE NOT EXISTS (
                SELECT 1
                FROM Study
                WHERE StudyInstanceUID = ?
            )
        """
        return strQuery

    def SeriesQuery_Insert(self):
        strQuery = """
            INSERT INTO Series(
                 BodyPart, 
                 Modality, 
                 NumberOfImages, 
                 SeriesDateTime, 
                 SeriesDescription, 
                 SeriesInstanceUID, 
                 SeriesNumber, 
                 Study_Key, 
                 FrameType,  
                 RefPhysicianID, 
                 RefPhysicianName
            )
            SELECT
               ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
            WHERE NOT EXISTS (
                SELECT 1
                FROM Series
                WHERE SeriesInstanceUID = ?
            )
            
        """
        return strQuery

    def ImageQuery_Insert(self):
        strQuery = """
            INSERT INTO Image(
                AcquisitionNumber, 
                FileName, 
                ImageAttr, 
                ImageWidth, 
                ImageHeight, 
                ImageNumber, 
                ImageOrientation, 
                ImagePosition, 
                ImageResolution,  
                ImageType, 
                PathName,
                Study_Key,
                Series_Key,
                SOPClassUID,
                SOPInstanceUID,
                FrameNumber
            )
            SELECT
              ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
            WHERE NOT EXISTS (
                SELECT 1
                FROM Image
                WHERE SOPInstanceUID = ?
            )
        """
        return strQuery

    def StudyQuery_Delete(self):
        strQuery = """
                    DELETE FROM Study
                    WHERE Study_Key = ?
                """
        return strQuery

    def SeriesQuery_Delete(self):
        strQuery = """
                    DELETE FROM Series
                    WHERE Study_Key = ? and Series_Key = ?
                """
        return strQuery

    def ImageQuery_Delete(self):
        strQuery = """
                    DELETE FROM Image
                    WHERE Study_Key = ? and Series_Key = ?
                """
        return strQuery

    def SeriesQuery_Select(self):
        strQuery = """
                    SELECT * FROM Series
                    WHERE Series_Key = ?
                """
        return strQuery

    def ImageQuery_Select(self):
        strQuery = """
                    SELECT * FROM image
                    WHERE Series_Key = ?
                """
        return strQuery
import DB_Database
from model import TreeModel

PROJECT_TREEVIEW_HEADER = [
    'PatientID',   'PatientName',  'DateTime',
    'Description',  'Imgs',        'Modality',
    'Comment',      'Key'
]

PROJECT_OPEN_DIALOG_HEADER = [
    'ID', 'SeriesNo', 'Direction',
    'Imgs', 'Gap', 'Res'
]

class DatabaseModelManager:
    def __init__(self):
        self.db = DB_Database.DBM()
        self.project_treeview_model = TreeModel.CyTreeModel(PROJECT_TREEVIEW_HEADER)
        self.project_open_dialog_treeview_model = TreeModel.CyTreeModel(PROJECT_OPEN_DIALOG_HEADER)
        self.initialize()

    def initialize(self):
        _db_exist = self.db.database_exist()
        if _db_exist is True:
            self.db.initialize()
            _data = self.db.get_total_ssi()
            self.project_treeview_model.project_treeview_item_addData(_data)
            return self.project_treeview_model
        else:
            return self.project_treeview_model

    def PT_add_data_database_and_model(self, path, b):
        if b is True:
            result = self.db.import_dcm(path)
            if result is False:
                return False
            else:
                _data = self.db.set_model_ssi(result)
                if len(_data['Study']) != 0:
                    self.project_treeview_model.beginResetModel()
                    self.project_treeview_model.project_treeview_item_addData(_data)
                    self.project_treeview_model.endResetModel()
                else:
                    pass
                return True
        else:
            print("fail")

    #TODO!! 데이터 삭제 실패 -> return false
    def PT_change_data_modelitem(self, type, index):
        _type = type
        _indexes = index.toVariant()

        if _type == "delete":
            # 선택된 데이터가 1개이상인 경우.
            if len(_indexes) > 1:
                for v in _indexes:
                    _index_pointer = v.internalPointer()
                    if _index_pointer.parentItem is not None:
                        # TODO!! Delete Series and reset model
                        try:
                            _study_index = self.project_treeview_model.items.index(_index_pointer.parentItem)
                            _series_index = self.project_treeview_model.items[_study_index].childItems.index(_index_pointer)
                            _study_key = self.project_treeview_model.items[_study_index].itemData['Key']
                            _series_key = self.project_treeview_model.items[_study_index].childItems[_series_index].itemData['Key']

                            # Delete Database Series
                            if self.db.series_delete(_study_key, _series_key) and self.db.image_delete(_study_key,
                                                                                                       _series_key):
                                # Delete Series Model Item
                                self.project_treeview_model.beginResetModel()
                                del self.project_treeview_model.items[_study_index].childItems[_series_index]
                                self.project_treeview_model.endResetModel()
                            else:
                                self.project_treeview_model.beginResetModel()
                                self.project_treeview_model.endResetModel()
                                return False
                        except ValueError:
                            continue
                    else:
                        # TODO!! Delete Study and reset model
                        _index = self.project_treeview_model.items.index(_index_pointer)

                        _study_key = self.project_treeview_model.items[_index].itemData['Key']
                        _series_key = []

                        for v in self.project_treeview_model.items[_index].childItems:
                            _series_key.append(v.itemData['Key'])

                        if self.db.study_delete(_study_key):
                            for v in _series_key:
                                if self.db.series_delete(_study_key, v) and self.db.image_delete(_study_key, v):
                                    pass
                                else:
                                    return False
                            else:
                                # 만약 선택된 Study가 model에 들어 있다면 삭제
                                if _index_pointer in self.project_treeview_model.items:
                                    self.project_treeview_model.beginResetModel()
                                    del self.project_treeview_model.items[_index]
                                    self.project_treeview_model.endResetModel()
                        else:
                            return False
                else:
                    return True

            # 선택된 데이터가 1개일 경우.
            else:
                _index_pointer = _indexes[0].internalPointer()
                if _index_pointer.parentItem is not None:
                    # TODO!! Delete Series and reset model
                    _study_index = self.project_treeview_model.items.index(_index_pointer.parentItem)
                    _series_index = self.project_treeview_model.items[_study_index].childItems.index(_index_pointer)
                    _study_key = self.project_treeview_model.items[_study_index].itemData['Key']
                    _series_key = self.project_treeview_model.items[_study_index].childItems[_series_index].itemData['Key']

                    # Delete Database Series
                    if self.db.series_delete(_study_key, _series_key) and self.db.image_delete(_study_key,
                                                                                               _series_key):
                        # Delete Series Model Item
                        self.project_treeview_model.beginResetModel()
                        del self.project_treeview_model.items[_study_index].childItems[_series_index]
                        self.project_treeview_model.endResetModel()
                        return True
                    else:
                        self.project_treeview_model.beginResetModel()
                        self.project_treeview_model.endResetModel()
                        return False
                else:
                    # TODO!! Delete Study and reset model
                    _index = self.project_treeview_model.items.index(_index_pointer)

                    _study_key = self.project_treeview_model.items[_index].itemData['Key']
                    _series_key = []

                    for v in self.project_treeview_model.items[_index].childItems:
                        _series_key.append(v.itemData['Key'])

                    if self.db.study_delete(_study_key):
                        for v in _series_key:
                            if self.db.series_delete(_study_key, v) and self.db.image_delete(_study_key, v):
                                pass
                            else:
                                return False
                        else:
                            # 만약 선택된 Study가 model에 들어 있다면 삭제
                            if _index_pointer in self.project_treeview_model.items:
                                self.project_treeview_model.beginResetModel()
                                del self.project_treeview_model.items[_index]
                                self.project_treeview_model.endResetModel()
                            return True
                    else:
                        return False
        else:
            pass

    def PT_sort_data(self, column, order):
        self.project_treeview_model.beginResetModel()
        try:
            if order == 0:
                # study sort
                self.project_treeview_model.items.sort(key=lambda x: x.itemData[self.project_treeview_model.headers[column]], reverse=True)
                # series sort
                for v in self.project_treeview_model.items:
                    v.childItems.sort(key=lambda x: x.itemData[self.project_treeview_model.headers[column]], reverse=True)
            else:
                # study sort
                self.project_treeview_model.items.sort(key=lambda x: x.itemData[self.project_treeview_model.headers[column]])
                # series sort
                for v in self.project_treeview_model.items:
                    v.childItems.sort(key=lambda x: x.itemData[self.project_treeview_model.headers[column]])
        except TypeError:
            # TODO!! NoneType이 있는 column sort 못하게 막기.
            print("None Type not support sort!!!")
            pass

        self.project_treeview_model.endResetModel()

    def POT_set_model(self, index):
        self.project_open_dialog_treeview_model.items.clear()
        _index = index
        _study_key = _index.internalPointer().parentItem.itemData['Key']
        _series_key = _index.internalPointer().itemData['Key']
        _series_item = {}

        for i1, v1 in self.db.d_ssi['Study'].items():
            if _study_key == v1['Study_Key']:
                for i2, v2 in self.db.d_ssi['Study'][i1]['Series'].items():
                    if _series_key == v2['Series_Key']:
                        _series_item = v2
                    else:
                        pass
            else:
                pass

        self.project_open_dialog_treeview_model.beginResetModel()
        self.project_open_dialog_treeview_model.project_open_dialog_treeview_item_addData(_series_item)
        self.project_open_dialog_treeview_model.endResetModel()

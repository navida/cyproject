from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt
from PyQt5.QtQuick import QQuickView
from PyQt5.QtGui import QGuiApplication
import DB_Database
import DB_DBModelManager
from model import TreeModel
from collections import defaultdict
import sys,os


class MainWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.manager = DB_DBModelManager()
        # self.db = DB_Database.DBM()
        # self.model = TreeModel.CyTreeModel()

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'DB_MainWindow.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)

        _db_exist = self.db.database_exist()
        if _db_exist is True:
            self.db.initialize()
            _data = self.db.get_total_ssi()
            self.model.addData(_data)
            self.rootContext().setContextProperty('project_model', self.model)
        else:
            self.rootContext().setContextProperty('project_model', self.model)

        self.show()

        self.init_sig_slot()

    def init_sig_slot(self):
        self.rootObject().findChild(QObject, 'road_button').clicked.connect(lambda : print("Road btn"))
        self.rootObject().findChild(QObject, 'dcm_dialog').sig_dcm_dialog_open.connect(self.sig_dcm_dialog_open)
        self.rootObject().findChild(QObject, 'input_button').sig_input_button_clicked.connect(self.sig_input_button_clicked)
        self.rootObject().findChild(QObject, 'project_list').sig_menu_trigger.connect(self.sig_menu_trigger)
        self.dcm_file_textarea = self.rootObject().findChild(QObject, 'dcm_file_textarea')

    @pyqtSlot(str, QVariant)
    def sig_menu_trigger(self, type, index):
        _type = type
        _indexes = index.toVariant()

        if _type == "delete":
            # 선택된 데이터가 1개이상인 경우.
            if len(_indexes) > 1:
                for v in _indexes:
                    _index_pointer = v.internalPointer()
                    if _index_pointer.parentItem is None:
                        #TODO!! Delete Study and reset model
                        try:
                            _index = self.model.items.index(_index_pointer)
                        except:
                            pass

                        if _index_pointer in self.model.items:
                            self.model.beginResetModel()
                            del self.model.items[_index]
                            self.model.endResetModel()
                    else:
                        #TODO!! Delete Series and reset model
                        try:
                            _study_index = self.model.items.index(_index_pointer.parentItem)
                            _series_index = self.model.items[_study_index].childItems.index(_index_pointer)
                        except:
                            pass

                        self.model.beginResetModel()
                        del self.model.items[_study_index].childItems[_series_index]
                        self.model.endResetModel()

            # 선택된 데이터가 1개일 경우.
            else:
                _index_pointer = _indexes[0].internalPointer()
                if _index_pointer.parentItem is None:
                    #TODO!! Delete Study and reset model
                    try:
                        _index = self.model.items.index(_index_pointer)
                    except:
                        pass

                    if _index_pointer in self.model.items:
                        self.model.beginResetModel()
                        del self.model.items[_index]
                        self.model.endResetModel()
                else:
                    #TODO!! Delete Series and reset model
                    try:
                        _study_index = self.model.items.index(_index_pointer.parentItem)
                        _series_index = self.model.items[_study_index].childItems.index(_index_pointer)
                    except:
                        pass

                    self.model.beginResetModel()
                    del self.model.items[_study_index].childItems[_series_index]
                    self.model.endResetModel()

        print("aa")
        print("aa")

    @pyqtSlot(str)
    def sig_dcm_dialog_open(self, path):
        print(path)
        self.dcm_file_textarea.setProperty("text", "Road dcm file path : " + path)

    @pyqtSlot(str, bool)
    def sig_input_button_clicked(self, path, b):
        if b is True:
            result = self.db.import_dcm(path)
            if result is False:
                self.dcm_file_textarea.setProperty("text", "Study already exist or Select folder contain dcm file")
            else:
                _data = self.db.set_model_ssi(result)
                if len(_data['Study']) != 0:
                    self.model.beginResetModel()
                    self.model.addData(_data)
                    self.model.endResetModel()
                else:
                    pass
        else:
            print("fail")

if __name__ == '__main__':
    _qapp = QGuiApplication(sys.argv)

    window = MainWindow()

    _qapp.exec()

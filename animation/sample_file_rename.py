import os
import glob

path = "/Users/jeongjihong/myproject/animation/images/"
_list = sorted(os.listdir(path), key= lambda x : int(x[:1]) if len(x) <= 5 else int(x[:2]))

for filename in _list:
    if filename[-3:] == "png" and len(filename) == 5:
        os.rename(path + filename, path + str(int(filename[:1]) - 1) + filename[-4:])
    else:
        os.rename(path + filename, path + str(int(filename[:2]) - 1) + filename[-4:])
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: 700
  height: 700

  Rectangle{
    width: 300
    height: 300
    anchors.centerIn: parent
    color:"black"

    TextField {
      width: 50
      text: "0"
      horizontalAlignment: Text.AlignHCenter
      inputMethodHints: Qt.ImhFormattedNumbersOnly
      validator: DoubleValidator {
        bottom: 0
        top: 30
        notation: DoubleValidator.StandardNotation
        decimals: 1
      }
    }
  }
}

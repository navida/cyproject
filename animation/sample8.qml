import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import cyhub 1.0

Item {
  width: 500
  height: 500

  Rectangle {
    id: redRect
    anchors.centerIn: parent
    width: 100
    height: 100
    color: "red"

    MouseArea {
      anchors.fill: parent

      onClicked:{
        geom.prop += "."
      }
    }

    Label{
      text: geom.prop
      anchors.centerIn: parent
    }
  }

  Geom{
    id: geom
    prop: 'my name is prop'
    Component.onCompleted: {
      console.log(" here !!!!!!!")
      console.log(geom.prop)
    }
    onPropChanged:{
      console.log("changed prop")
    }
  }
}

//ScrollView{
//  width: contentItem.width + __verticalScrollBar.width// Don't limit the width.
//  height: 120 // Limit only the height.
//  horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff // You don't need them.
//  verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff // You don't need them.
//  clip: true
//  contentItem:
//    ColumnLayout{
//    spacing: 30
//      Column{
//      spacing: 5
//        Text{
//          id: description_text
//          text: "Description"
//          font.pixelSize: 15
//          color: root.font_color_2
//        }
//        Text{
//          Layout.maximumWidth: patient_info_detail_right_rect.width
//          text:"NoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNone"
//          wrapMode:Text.WordWrap
//          font.pixelSize: 15
//          color: root.font_color_1
//        }
//      }

//      Column{
//      spacing: 5
//        Text{
//          id: comment_text
//          text: "Comment"
//          font.pixelSize: 15
//          color: root.font_color_2
//        }
//        Text{
//          Layout.maximumWidth: patient_info_detail_right_rect.width
//          text:"NoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNoneNone"
//          wrapMode:Text.WordWrap
//          font.pixelSize: 15
//          color: root.font_color_1
//        }
//      }
//  }
//}

//ScrollView{
//  width: contentItem.width + __verticalScrollBar.width// Don't limit the width.
//  height: 110 // Limit only the height.
//  horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff // You don't need them.
//  verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
//  clip: true
//  contentItem:
//    ColumnLayout{
//    Layout.maximumWidth: patient_info_detail_right_rect.width
//    Layout.maximumHeight: 2000
//      Text{
//        id: description_text
//        text: "Description"
//        font.pixelSize: 15
//        color: root.font_color_2
//      }
//      Text{
//        id: testaa
//        anchors.left: description_text.left
//        anchors.top: description_text.bottom
//        anchors.topMargin: 3
//        Layout.maximumWidth: patient_info_detail_right_rect.width
//        text:"ajsdnfjkasdhf asdhfuashdcasdci uashdc chiuds chuishdc shdc s cisudhc suidhciusdhciuhcs dsc"
//        wrapMode:Text.WordWrap
//        font.pixelSize: 15
//        color: root.font_color_1
//      }

//      Text{
//        id: comment_text
//        anchors.top: testaa.bottom
//        anchors.topMargin: 30
//        text: "Comment"
//        font.pixelSize: 15
//        color: root.font_color_2
//      }
//      Text{
//        anchors.left: comment_text.left
//        anchors.top: comment_text.bottom
//        anchors.topMargin: 3
//        Layout.maximumWidth: patient_info_detail_right_rect.width
//        text:"ajsdnfjkasdhf asdhfuashdcasdci uashdc chiuds chuishdc shdc s cisudhc suidhciusdhciuhcs dsc"
//        wrapMode:Text.WordWrap
//        font.pixelSize: 15
//        color: root.font_color_1
//      }
//  }
//  }

//RowLayout{
//  anchors.centerIn: parent
//  spacing: 25
//  Rectangle{
//    id: patient_info_detail_left_rect
//    width: 245
//    height: 110
//    color: "transparent"

import base64

from wincrypto import CryptCreateHash, CryptHashData, CryptDeriveKey, CryptEncrypt, CryptImportKey, CryptExportKey, native
from wincrypto.constants import CALG_SHA1, CALG_AES_256, bType_SIMPLEBLOB, CALG_MD5, CALG_RC4, bType_PLAINTEXTKEYBLOB

from ctypes import FormatError
from ctypes import windll, c_void_p, byref, create_string_buffer, c_int
import struct

from wincrypto.constants import HP_ALGID, HP_HASHSIZE, KP_KEYLEN, KP_ALGID, CRYPT_EXPORTABLE

MY_KEYCONTAINER_NAME = b"Cybermed_Key_Container"
PROFILE_KEY			 = b"TheFirstClassOfWinningPlayerIsJaeHyunBaek"

PASSWORD = b"admin"

hProv = c_void_p()
hKey = c_void_p()
hHash = c_void_p()

a1 = windll.advapi32.CryptAcquireContextA(byref(hProv), MY_KEYCONTAINER_NAME, b"Microsoft Base Cryptographic Provider v1.0", 1, 0)

a2 = windll.advapi32.CryptCreateHash(hProv, CALG_MD5, 0, 0, byref(hHash))

# a3 = native.CryptHashData(hHash, PROFILE_KEY)
_bdata = create_string_buffer(PROFILE_KEY)
# _dwdatalen = c_int(len(PROFILE_KEY))
# _dwKeyLen = c_int(len(PASSWORD))
_dwKeyLen = len(PASSWORD)
_dwdatalen = len(PROFILE_KEY)
if _dwKeyLen > _dwdatalen:
    _dwKeyLen = _dwdatalen

a3 = windll.advapi32.CryptHashData(hHash, _bdata, c_int(_dwKeyLen), 0)

a4 = windll.advapi32.CryptDeriveKey(hProv, CALG_RC4, hHash, 0x00000001, byref(hKey))

# a5 = native.CryptEncrypt(hKey, b'admin')
out_buf_len = c_int(len(b'admin')).value
# encrypt data
bdata = create_string_buffer(b'admin', out_buf_len)
bdatalen = c_int(len(b'admin'))
a5 = windll.advapi32.CryptEncrypt(hKey, 0, 1, 0, bdata, byref(bdatalen), out_buf_len)

print(bdata.raw[:bdatalen.value].hex())

e = base64.encodebytes(bdata.raw[:bdatalen.value])

print(e)


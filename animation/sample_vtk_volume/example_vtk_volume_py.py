import vtk
import cyCafe

import vtk.numpy_interface.dataset_adapter as dsa
import numpy as np

DCM_PATH = r"/Users/jeongjihong/dicom/dicom5 복사본"

c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
p = vtk.vtkVolumeProperty()
m, v = vtk.vtkSmartVolumeMapper(), vtk.vtkVolume()

opacityWindow = 4096
opacityLevel = 2048

dcm_reader = cyCafe.cyDicomReader()
dcm_reader.read_dicom(DCM_PATH)
vtk_img = dcm_reader.get_vtk_img()

# Extent
e = vtk_img.GetExtent()
print("Extent : ", e)
o = vtk_img.GetOrigin()
print("Origin : ", o)
s = vtk_img.GetSpacing()
print("Spacing : ", s)

# Histogram
A = dsa.WrapDataObject(vtk_img).GetPointData().GetArray(0)
_min, _max = np.min(A), np.max(A)
histo = np.histogram(A, bins=10)
print("Min, Max, Numpy Histogram", _min, _max, histo)

# mapper
# m.SetInputConnection(reader.GetOutputPort())
m.SetInputData(vtk_img)
m.SetBlendModeToMaximumIntensity()

# property
p.SetIndependentComponents(True)
c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
o_f.AddSegment(opacityLevel - 0.5*opacityWindow, 0.0, opacityLevel + 0.5*opacityWindow, 1.0)
p.SetColor(c_f)
p.SetScalarOpacity(o_f)
p.SetInterpolationTypeToLinear()

# actor
v.SetMapper(m)
v.SetProperty(p)

RulerLineActor = vtk.vtkAxisActor2D()
RulerLineActor.LabelVisibilityOff()
RulerLineActor.RulerModeOff()
RulerLineActor.AdjustLabelsOff()
RulerLineActor.PickableOff()
RulerLineActor.DragableOff()
RulerLineActor.SetPosition(0.9, 0.5)
RulerLineActor.SetPosition2(0.9, 0.7)

global RulerTextActor
RulerTextActor = vtk.vtkTextActor()
RulerTextActor.SetInput("")
RulerTextActor.SetDisplayPosition(1250, 630)

lineProperty = RulerLineActor.GetProperty()
lineProperty.SetLineWidth(1)

textProperty = RulerTextActor.GetTextProperty()
textProperty.SetFontSize(40)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
ren.AddViewProp(RulerLineActor)
ren.AddViewProp(RulerTextActor)

renWin.SetSize(700, 700)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

ISTYLES = cyCafe.cyVtkInteractorStyles()
istyle = ISTYLES.get_vtk_interactor_style_volume_3d()
# istyle = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(istyle)

# camera
camera = ren.GetActiveCamera()
camera.SetParallelProjection(True)
c = v.GetCenter()
print("VC ", c)
camera.SetFocalPoint(c)
camera.SetPosition(c[0], c[1]-e[1], c[2])
camera.SetViewUp(0, 0, 1)

ren.AddViewProp(v)

renWin.Render()

def modified_event(obj, event):
    print("------------------------")
    # print("GetPosition : ", obj.GetPosition())
    # print("GetParallelProjection : ", obj.GetParallelProjection())
    # print("GetParallelScale : ", obj.GetParallelScale())
    # print("GetThickness : ", obj.GetThickness())
    # print("GetViewAngle : ", obj.GetViewAngle())
    # print("event : ", event)
    print("GetParallelScale : ", obj.GetParallelScale())
    # RulerTextActor.SetInput(str(int(obj.GetDistance() / 100)))

ren.GetActiveCamera().AddObserver('ModifiedEvent', modified_event)

iren.Initialize()
iren.Start()

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Item {
  id: my_dialog
  objectName: 'my_dialog'

  property real drag_minimumX
  property real drag_maximumX
  property real drag_minimumY
  property real drag_maximumY

  Rectangle {
    id: dlg1
    width: parent.width
    height: parent.height
    implicitWidth: parent.width
    implicitHeight: parent.height
    color: '#efefef'
    radius: 10
    z: 1

    Column{
      anchors.fill: parent
      RowLayout{
        id: content1
        width: dlg1.width
        height: 30
        spacing: 0

        MouseArea{
          anchors.fill: parent
          drag.target: my_dialog
          drag.axis: Drag.XAxis | Drag.YAxis
          drag.minimumX: my_dialog.drag_minimumX
          drag.maximumX: my_dialog.drag_maximumX
          drag.minimumY: my_dialog.drag_minimumY
          drag.maximumY: my_dialog.drag_maximumY
        }

        Rectangle{
          anchors.fill: parent
          color:"black"

          Button {
            id: my_dialog_close
            objectName: "my_dialog_close"
            implicitWidth : 30
            implicitHeight : 30
            anchors.top: parent.top
            anchors.right: parent.right
            // test_btn is in outside !
            onClicked: test_btn.checked = false
          }
        }
      }
      RowLayout {
        id: content2
        width: dlg1.width
        height: dlg1.height - content1.height
        spacing: 0
        Rectangle{
          anchors.fill: parent
          color:"yellow"
        }
        MouseArea{
          anchors.fill: parent
          onClicked: {
            console.log(" in Mouse Area")
          }
        }
      }
    }
  }
}

from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QVariant
from PyQt5.QtQuick import QQuickView, QQuickItem, QQuickPaintedItem
from PyQt5.QtWidgets import QApplication
import sys, os
import _qapp

_qapp = QApplication(sys.argv)

view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()
_qapp.exec_()
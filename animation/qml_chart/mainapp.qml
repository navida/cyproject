import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

import QtCharts 2.0

Item{
  width: 850
  height: 490

  Rectangle{
    anchors.fill: parent
    color: "black"
  }

  ChartView {
    id: chart
    property var selectedPoint: undefined

    width:750
    height:450
//    anchors.centerIn: parent
    anchors{
      horizontalCenter: parent.horizontalCenter
      verticalCenter: parent.verticalCenter
    }
    antialiasing: true
    legend{
      visible: false
    }

    theme: ChartView.ChartThemeBrownSand
//    backgroundColor: "black"
//    plotAreaColor: "black"

    property real toleranceX: 0.1

    ValueAxis {
      id: axisX
      titleText: "Distance"
      titleVisible: true
      labelFormat: "%i"
      min: 0
      max: 0
      tickCount: 10
    }

    ValueAxis {
      id: axisY
      titleText: "Intensity"
      titleVisible: true
      labelFormat: "%i"
      min: 0
      max: 0
    }

    Text{
      id: axisY_text
      visible: false
      text: parent.max.toString() > parent.min.toString() ? parent.max.toString() : parent.min.toString()
    }

    AreaSeries {
      axisX: axisX
      axisY: axisY
      pointLabelsVisible: false
      upperSeries: series1

//      color: "gray"
//      borderColor: "yellow"
    }

    LineSeries {
      id: series1
      pointsVisible: false

      width: 0
//      color: "black"
    }

    Rectangle{
      id: left_rect
      width: 1
      height: chart.plotArea.height
      x: chart.plotArea.x
      y: chart.plotArea.y

      transform: Scale{
        id: left_rect_tform3
        xScale: 1
      }

      color: 'red'

      Label{
        id: label1
      }

      Rectangle{
        id: active_pos
        width: 7
        height: 7
        radius: 3
        color: 'yellow'
      }

      MouseArea{
        id: left_rect_mouse
        width: 25
        height: parent.height
        anchors{
          horizontalCenter: parent.horizontalCenter
        }
        hoverEnabled: true
        drag{
          id: left_rect_mouse_drag
          target:left_rect
          axis: Drag.XAxis
          minimumX: chart.plotArea.x
//          maximumX: (chart.plotArea.x + chart.plotArea.width)
          maximumX: right_rect.x
          threshold: 1
        }
        onHoveredChanged: {
          if(containsMouse)
            cursorShape = Qt.SizeHorCursor
          else
            return
        }
      }

      onXChanged: {
        var l_m_x = (x - chart.plotArea.x) * axisX.max / chart.plotArea.width

        for(var i = 0; i < series1.count; i++){
          var p_x = series1.at(i).x
          var p_y = series1.at(i).y

          if(Math.abs(l_m_x - p_x) <= chart.toleranceX){

            console.log(l_m_x)
            console.log(p_x)

            if(i < 50){
              label1.anchors.right = undefined
              label1.anchors.left = left_rect.right
              label1.anchors.leftMargin = 25
            }
            else{
              label1.anchors.left = undefined
              label1.anchors.right = left_rect.left
              label1.anchors.rightMargin = 25
            }

            label1.text = series1.at(i).y

            label1.x = series1.at(i).x / (axisX.max - axisX.min)
            label1.y = chart.plotArea.height - ((series1.at(i).y - axisY.min) * chart.plotArea.height / (axisY.max - axisY.min))

            active_pos.x = series1.at(i).x / (axisX.max - axisX.min) - (active_pos.width / 2)
            active_pos.y = (chart.plotArea.height - ((series1.at(i).y - axisY.min) * chart.plotArea.height / (axisY.max - axisY.min))) - (active_pos.height / 2)

            break;
          }
        }
      }
    }

    Rectangle{
      id: distance_rect
      width: right_rect.x - left_rect.x
      height: 1
      x: left_rect.x
      anchors.verticalCenter: left_rect.verticalCenter
      color: "green"

      property double t_width: + chart.plotArea.width

      Label{
        id: distance_label
        anchors{
          horizontalCenter: parent.horizontalCenter
          bottom: parent.top
          bottomMargin: 10
        }
      }

      onWidthChanged: {
        var _distance = Math.abs(((axisX.max - axisX.min) * width) / chart.plotArea.width)
        distance_label.text = "Distance " + _distance.toFixed(2) + " mm"
      }
    }

    Rectangle{
      id: right_rect
      width: 1
      height: chart.plotArea.height
      x: (chart.plotArea.x + chart.plotArea.width)
      y: chart.plotArea.y

      transform: Scale{
        id: right_rect_tform
        xScale: 1
      }

      color: 'blue'

      Label{
        id: label2
      }

      MouseArea{
        id: right_rect_mouse
        width: 25
        height: parent.height
        anchors{
          horizontalCenter: parent.horizontalCenter
        }
        hoverEnabled: true
        drag{
          target:right_rect
          axis: Drag.XAxis
//          minimumX: chart.plotArea.x
          minimumX: left_rect.x
          maximumX: (chart.plotArea.x + chart.plotArea.width)
          threshold: 1
        }

        onHoveredChanged: {
          if(containsMouse)
            cursorShape = Qt.SizeHorCursor
          else
            return
        }
      }

      onXChanged: {
        var r_m_x = (x - chart.plotArea.x) * axisX.max / chart.plotArea.width
        for(var i = 0; i < series1.count; i++){
          var p_x = series1.at(i).x
          var p_y = series1.at(i).y

          if(Math.abs(r_m_x - p_x) <= chart.toleranceX){
            label2.text = series1.at(i).y

            if(i < 50){
              label2.anchors.right = undefined
              label2.anchors.left = right_rect.right
              label2.anchors.leftMargin = 25
            }
            else{
              label2.anchors.left = undefined
              label2.anchors.right = right_rect.left
              label2.anchors.rightMargin = 25
            }

            label1.x = p_x / (axisX.max - axisX.min)
            label2.y = chart.plotArea.height - ((p_y - axisY.min) * chart.plotArea.height / (axisY.max - axisY.min))
            break;
          }
        }

      }
    }
  }
  // Add data dynamically to the series
  Component.onCompleted: {
    var x_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 97, 98, 99]
    var y_list = [-987, -962, -955, -938, -908, -891, -776, -754, -660, -648, -632, -615, -515, -464, -444, -399, -368, -363, -204, -203, -191, -188, -162, -137, -95, 7, 33, 49, 64, 111, 161, 220, 224, 227, 260, 309, 323, 386, 460, 599, 666, 733, 778, 779, 791, 829, 832, 887, 935, 965, 989, 941, 876, 872, 852, 846, 836, 810, 791, 677, 644, 595, 577, 539, 522, 508, 460, 422, 346, 235, 200, 197, 184, 136, 25, -38, -39, -64, -158, -163, -236, -333, -347, -351, -354, -364, -451, -456, -484, -501, -568, -593, -643, -699, -721, -722, -851, -919, -994]

    var x_max = Math.max.apply(null, x_list)
    var x_min = Math.min.apply(null, x_list)

    var x_length = x_max - x_min + 1

    var y_max = Math.max.apply(null, y_list)
    var y_min = Math.min.apply(null, y_list)

    axisX.max = x_max
    axisX.min = x_min
    axisY.max = Math.round(y_max / 100) * 100
    axisY.min = Math.round(y_min / 100) * 100

    for (var i = 0; i < x_length; i++) {
      series1.insert(i, x_list[i], y_list[i]);
    }

    //    console.log(chart.plotArea.width)
    //    console.log(chart.plotArea.height)
    //    console.log(chart.plotArea.x)
    //    console.log(chart.plotArea.y)

    // add Rectangle for max value
    var aa = series1.at(y_list.indexOf(Math.max.apply(null, y_list)))

//    console.log(aa)

//    var x_aa = ((aa.x / (axisX.max - axisX.min)) * (chart.plotArea.width + 2 * chart.plotArea.x))
//    var y_aa = (chart.plotArea.height - ((aa.y - axisY.min) * chart.plotArea.height / (axisY.max - axisY.min))) + chart.plotArea.y

//    console.log(x_aa)
//    console.log(y_aa)

    if(axisY.max.toString().length > axisY.min.toString().length)
      axisY_text.text = axisY.max.toString()
    else
      axisY_text.text = axisY.min.toString()


    // x 구하는 방법
//    chart.plotArea.x + axisY_text.contentWidth + 1

//    var _str1 =
//        'import QtQuick 2.0;
//             Rectangle {
//               x:' + chart.x + '\n' + '
//               y:' + chart.y + '\n' + '
//               width: 10
//               height: 10
//               color: "blue"
//             }'
//    var _str2 =
//        'import QtQuick 2.0;
//             Rectangle {
//               x:' + chart.plotArea.x + '\n' + '
//               y:' + chart.plotArea.y + '\n' + '
//               width: 10
//               height: 10
//               color: "yellow"
//             }'
//    var _str3 =
//        'import QtQuick 2.0;
//             Rectangle {
//               x:' + chart.plotArea.width + '\n' + '
//               y:' + chart.plotArea.height + '\n' + '
//               width: 10
//               height: 10
//               color: "green"
//             }'
//    var _str4 =
//        'import QtQuick 2.0;
//             Rectangle {
//               x:' + (chart.plotArea.width - chart.plotArea.x)+ '\n' + '
//               y:' + (chart.plotArea.height - chart.plotArea.y) + '\n' + '
//               width: 10
//               height: 10
//               color: "orange"
//             }'

//    Qt.createQmlObject(_str1, chart);
//    Qt.createQmlObject(_str2, chart);
//    Qt.createQmlObject(_str3, chart);
//    Qt.createQmlObject(_str4, chart);

  }
}

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Item{
  id: root
  width: 1100
  height: 680

  Button{
    text: "a"

    onClicked: {
      if(dlg_sample.visible)
        dlg_sample.close()
      else
        dlg_sample.open()
    }
  }

  Rectangle{
    anchors{
      horizontalCenter: parent.horizontalCenter
      verticalCenter: parent.verticalCenter
    }
    width: 400
    height: 400
    color: "yellow"
    MouseArea{
      anchors.fill: parent
      onClicked: {
        console.log("aa")
      }
    }
  }

  Rectangle{
    id: i2g_dim
    anchors.fill: parent
    color: "black"
    opacity: 0.0
    visible: false

    MouseArea {
      anchors.fill: parent
      acceptedButtons: Qt.AllButtons
    }

    function on(){
      i2g_dim.enabled = true;
      i2g_dim.visible = true;
      opacity = 0.8;
    }

    function off(){
      opacity = 0.0;
    }

    onOpacityChanged: {
      if (opacity == 0.0){
        i2g_dim.enabled = false;
        i2g_dim.visible = false;
      }
    }

    Behavior on opacity {
      id: behavior_opacity
      NumberAnimation {
          target: i2g_dim
          property: "opacity"
          duration: 300
      }
      enabled: true
    }
  }

  Sample_Custom_Dialog_2{
    id: dlg_sample
    x: 100
    y: 100
  }
}

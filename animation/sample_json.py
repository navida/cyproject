import json

exam = {
    'SampleProject': 33973970,
    'matrix': [0],
    '3DObject': [
        {'LeafImplant1': "object", 'LeafImplant2': "object"},
        {'WaxUp1': "object", 'WaxUp2': "object"}
    ]
}

with open('sample.json', 'w', encoding="utf-8") as make_file:
    json.dump(exam, make_file, indent=4)

# jsonString = json.dumps(exam, indent=4)
# print(jsonString)
# print(type(jsonString))
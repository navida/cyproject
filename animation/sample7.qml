import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

//Item {
//  width: 200; height: 100

//  Rectangle {
//    id: redRect
//    width: 100; height: 100
//    color: "red"
//  }

//  Rectangle {
//    id: blueRect
//    x: redRect.width
//    width: 50; height: 50
//    color: "blue"

//    states: State {
//      name: "reparented"
//      ParentChange { target: blueRect; parent: redRect; x: 10; y: 10 }
//    }

//    transitions: Transition {
//      ParentAnimation {
//        NumberAnimation { properties: "x,y"; duration: 1000 }
//      }
//    }

//    MouseArea { anchors.fill: parent; onClicked: blueRect.state = "reparented" }
//  }
//}

Item{
  width: 500
  height: 500

  Button{
    implicitWidth:500
    implicitHeight:50
    text: "aaaa"

    onClicked: {
      console.log(text)
    }
  }
}

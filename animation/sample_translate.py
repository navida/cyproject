from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys,os

# NOTE!!!
# /Users/jeongjihong/Qt5.7.1/5.7/clang_64/bin/Linguist
# /Users/jeongjihong/Qt5.7.1/5.7/clang_64/bin/lupdate /Users/jeongjihong/CyProject/animation/sample_translate.qml -ts translating_qml_kr.ts


class MainWindow(QQuickView):
    def __init__(self, app, *args, **kwds):
        super().__init__(*args, **kwds)
        self.app = app

        self.translator = QTranslator()
        self.translator.load("/Users/jeongjihong/CyProject/animation/translate/translating_qml_kr2.qm")
        self.app.installTranslator(self.translator)
        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample_translate.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self.init_sig_slot()

    def init_sig_slot(self):
        # self.findChild(QObject, 'btn_kr').btn_kr_clicked.connect(self.btn_kr_clicked)
        # self.findChild(QObject, 'btn_en').btn_en_clicked.connect(self.btn_en_clicked)
        root_item = self.findChild(QObject, 'root')
        print(root_item.property("_tmpstr2"))
        root_item.setProperty("_tmpstr1", self.tr("tmpstr"))
        print(root_item.property("_tmpstr1"))
        root_item.setProperty("_tmpstr1", "tmpstr")
        print(root_item.property("_tmpstr1"))

    # @pyqtSlot()
    # def btn_kr_clicked(self):
    #     self.app.installTranslator(self.translator)
    #
    # @pyqtSlot()
    # def btn_en_clicked(self):
    #     self.app.removeTranslator(self.translator)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow(app)

    app.exec()

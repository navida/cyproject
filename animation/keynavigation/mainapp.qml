import QtQuick 2.0

Grid {
  width: 150; height: 50
  rows: 1
  columns: 3

  Rectangle {
    id: left
    width: 50; height: 50
    color: focus ? "red" : "lightgray"
    focus: true

    KeyNavigation.tab: middle
  }

  Rectangle {
    id: middle
    width: 50; height: 50
    color: focus ? "red" : "lightgray"

    KeyNavigation.tab: right
  }

  Rectangle {
    id: right
    width: 50; height: 50
    color: focus ? "red" : "lightgray"

    KeyNavigation.tab: left
  }
}

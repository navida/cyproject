import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2

Item{
  id: root
  objectName: "root"
  width: 500
  height: 500

//  Button{
//    id: btn_kr
//    objectName: "btn_kr"
//    text: "btn_kr"

//    signal btn_kr_clicked()

//    onClicked: {
//      btn_kr_clicked()
//    }
//  }
//  Button{
//    id: btn_en
//    objectName: "btn_en"
//    anchors{
//      left: btn_kr.right
//    }

//    text: "btn_en"

//    signal btn_en_clicked()

//    onClicked: {
//      btn_en_clicked()
//    }
//  }

  property string _tmpstr1 : qsTr("")
  property string _tmpstr2 : qsTr("tmpstr")

  RowLayout{
    implicitWidth: 300
    implicitHeight: 150
    anchors.centerIn: parent
    spacing: 0

    Rectangle{
      width: parent.width / 4
      height: parent.height
      color: "transparent"

      Text{
        id: seriesno_text
        objectName: "seriesno_text"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: ""
        font.pixelSize: 35
        font.bold: true
        color: "white"
      }
      Text{
        id: seriesno_title_text
        anchors.horizontalCenter: seriesno_text.horizontalCenter
        anchors.top: seriesno_text.bottom
        anchors.topMargin: 3
        text: qsTr("Series No")
        font.pixelSize: 15
        color: "#989898"
      }
    }
    Rectangle{
      width: parent.width / 4
      height: parent.height
      color: "transparent"

      Text{
        id: operator_text
        objectName: "operator_text"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: ""
        font.pixelSize: 35
        font.bold: true
        color: "white"
      }
      Text{
        id: operator_title_text
        anchors.horizontalCenter: operator_text.horizontalCenter
        anchors.top: operator_text.bottom
        anchors.topMargin: 3
        text: qsTr("Operator")
        font.pixelSize: 15
        color: "#989898"
      }
    }
    Rectangle{
      width: parent.width / 4
      height: parent.height
      color: "transparent"

      Text{
        id: create_date_time_text
        objectName: "create_date_time_text"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: ""
        font.pixelSize: 20
        font.bold: true
        color: "white"
      }
      Text{
        id: create_date_title_text
        anchors.horizontalCenter: create_date_time_text.horizontalCenter
        anchors.top: create_date_time_text.bottom
        anchors.topMargin: 12
        text: qsTr("Datetime")
        font.pixelSize: 15
        color: "#989898"
      }
    }
  }
}


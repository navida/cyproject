from PyQt5.QtGui import QGuiApplication, QImage, qAlpha
from PyQt5.QtCore import QUrl, Qt, pyqtSignal, pyqtSlot, pyqtProperty
from PyQt5.QtQuick import QQuickView, QQuickItem
from PyQt5 import QtQml

import sys, os
# import _qapp

_qapp = QGuiApplication(sys.argv)

class MaskedMouseArea(QQuickItem):
    pressedChanged = pyqtSignal()
    maskSourceChanged = pyqtSignal()
    containsMouseChanged = pyqtSignal()
    alphaThresholdChanged = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFlag(QQuickItem.ItemHasContents, True)
        self.setAcceptedMouseButtons(Qt.AllButtons)
        self.setAcceptHoverEvents(True)
        self._pressed = False
        self._maskSource = None
        self._maskImage = None
        self._pressPoint = None
        self._alphaThreshold = 0.0
        self._containsMouse = False

    def qBound(self, _minVal, _current, _maxVal):
        return max(min(_current, _maxVal), _minVal)

    @pyqtProperty(bool, notify=pressedChanged)
    def pressed(self):
        return self._pressed

    @pressed.setter
    def pressed(self, pressed):
        if pressed != self._pressed:
            self._pressed = pressed
            self.pressedChanged.emit()

    @pyqtProperty(bool, notify=containsMouseChanged)
    def containsMouse(self):
        return self._containsMouse

    @containsMouse.setter
    def containsMouse(self, containsMouse):
        if self._containsMouse != containsMouse:
            self._containsMouse = containsMouse
            self.containsMouseChanged.emit()

    @pyqtProperty(QUrl, notify=maskSourceChanged)
    def maskSource(self):
        return self._maskSource

    @maskSource.setter
    def maskSource(self, source):
        if self._maskSource != source:
            self._maskSource = source
            self._maskImage = QImage()
            self._maskImage.load(self._maskSource.toLocalFile())
            self.maskSourceChanged.emit()

    @pyqtProperty(float, notify=alphaThresholdChanged)
    def alphaThreshold(self):
        return self._alphaThreshold

    @alphaThreshold.setter
    def alphaThreshold(self, threshold):
        if self._alphaThreshold != threshold:
            self._alphaThreshold = threshold
            self.alphaThresholdChanged.emit()

    def contains(self, Union, QPointF=None, QPoint=None):
        if not super().contains(Union) or self._maskImage is None:
            return False
        p = Union.toPoint()

        if p.x() < 0 or p.x() >= self._maskImage.width() or \
            p.y() < 0 or p.y() >= self._maskImage.height():
            return False

        r = int(self.qBound(0, self._alphaThreshold * 255, 255))
        return qAlpha(self._maskImage.pixel(p)) > r

    def mousePressEvent(self, e):
        self._pressed = True
        self._pressPoint = e.pos()
        self.pressedChanged.emit()

    def mouseReleaseEvent(self, e):
        self._pressed = False
        # self.pressedChanged.emit()

        # _qapp.qApp = QCoreApplication(sys.argv)
        #
        # threshold = _qapp.qApp.styleHints.stratDragDistance()
        # isClick = (threshold >= qAbs(e.x() - self._pressPoint.x()) and
        #            threshold >= qAbs(e.y() - self._pressPoint.y()))
        # if isClick:
        #     self.sig_clicked.emit()

    def mouseUngrabEvent(self):
        self._pressed = False
        self.pressedChanged.emit()

    def hoverEnterEvent(self, e):
        self._containsMouse = True
        self.containsMouseChanged.emit()

    def hoverLeaveEvent(self, e):
        self._containsMouse = False
        self.containsMouseChanged.emit()

v = QQuickView()

QtQml.qmlRegisterType(MaskedMouseArea, 'Example', 1, 0, 'MaskedMouseArea')

v.setSource(QUrl.fromLocalFile('sample_maskedmousearea.qml'))

v.show()

_qapp.exec_()


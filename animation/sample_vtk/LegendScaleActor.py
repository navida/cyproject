import vtk

def main():
    sphereSource = vtk.vtkSphereSource()
    sphereSource.Update()

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(sphereSource.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    renderWindowsInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowsInteractor.SetRenderWindow(renderWindow)

    # legendScaleActor = vtk.vtkLegendScaleActor()
    #
    # legendScaleActor.SetLeftAxisVisibility(False)
    # legendScaleActor.SetTopAxisVisibility(False)
    # legendScaleActor.SetBottomAxisVisibility(False)
    #
    # legendScaleActor.LegendVisibilityOff()
    # # legendScaleActor.SetCornerOffsetFactor(50)
    # # legendScaleActor.SetRightBorderOffset(50)

    # caxisActor2D = vtk.vtkCubeAxesActor2D()
    # caxisActor2D.SetCamera(renderer.GetActiveCamera())
    # caxisActor2D.SetLabelFormat("%6.4g")
    # caxisActor2D.SetFlyModeToOuterEdges()
    # caxisActor2D.SetFontFactor(0.8)
    #
    # caxisActor2D.ZAxisVisibilityOff()
    # caxisActor2D.XAxisVisibilityOff()
    #
    # renderWindow.GetRenderers().GetFirstRenderer().AddViewProp(caxisActor2D)

    axisActor2D = vtk.vtkAxisActor2D()

    axisActor2D.SetPosition(0.9, 0.5)
    axisActor2D.SetPosition2(0.9, 0.7)
    axisActor2D.SetRulerMode(True)
    # axisActor2D.SetWidth(5)
    # axisActor2D.SetHeight(100)
    # axisActor2D.GetPoint1Coordinate().SetCoordinateSystemToDisplay()
    # axisActor2D.GetPoint2Coordinate().SetCoordinateSystemToDisplay()
    # axisActor2D.RulerModeOn()
    # axisActor2D.SetNumberOfMinorTicks(10)
    # axisActor2D.SetRange(0, 100)
    # axisActor2D.SetRulerDistance(100)

    # axisActor2D.SetTickLength(10)
    # axisActor2D.TickVisibilityOff()
    # axisActor2D.LabelVisibilityOff()
    # axisActor2D.SetMinorTickLength(5)

    renderer.AddViewProp(axisActor2D)
    # renderer.AddActor(caxisActor2D)
    # renderer.AddActor(actor)

    renderer.SetBackground(0, 0, 0)

    renderWindow.SetSize(900, 900)
    renderWindow.Render()
    renderWindowsInteractor.Start()


if __name__ == "__main__":
    main()
import vtk

renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

distanceWidget = vtk.vtkDistanceWidget()
distanceWidget.SetInteractor(renderWindowInteractor)
distanceWidget.CreateDefaultRepresentation()
distanceWidget.GetDistanceRepresentation().SetLabelFormat("%-#6.3g mm")

renderWindow.Render()

renderWindowInteractor.Initialize()
renderWindow.Render()

distanceWidget.On()

renderWindowInteractor.Start()
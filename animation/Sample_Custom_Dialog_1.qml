import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

Item {
  id: my_dialog
  objectName: 'my_dialog'

  property real drag_minimumX
  property real drag_maximumX
  property real drag_minimumY
  property real drag_maximumY

  signal open()
  onOpen: {
    my_dialog.visible = true;
  }

  signal close()
  onClose: {
    my_dialog.visible = false;
  }

  Rectangle {
    id: dlg1
    width: parent.width
    height: parent.height
    implicitWidth: parent.width
    implicitHeight: parent.height
    color: '#efefef'
    radius: 10
    z: 1

    ColumnLayout {
      anchors.fill: parent
      spacing: 0

      Rectangle {
        implicitHeight: 20
        Layout.fillWidth: true
        color: '#313231'

        Text {
          color: "white"
          text: "Leaf Implant"
          anchors.fill: parent
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          font.pointSize: 13
        }

        MouseArea{
          anchors.fill: parent
          drag.target: my_dialog
          drag.axis: Drag.XAxis | Drag.YAxis
          drag.minimumX: my_dialog.drag_minimumX
          drag.maximumX: my_dialog.drag_maximumX
          drag.minimumY: my_dialog.drag_minimumY
          drag.maximumY: my_dialog.drag_maximumY
        }

        Text {
          color: "white"
          text: "X"
          anchors{
            right: parent.right
            rightMargin: 5
            verticalCenter: parent.verticalCenter
          }

          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          font.pointSize: 16

          MouseArea{
            anchors.fill: parent
            onClicked: my_dialog.close()
          }
        }
      }
      Rectangle {
        Layout.fillWidth: true
        Layout.fillHeight: true
        color: 'red'
        MouseArea{
          anchors.fill: parent
          onClicked: {
            console.log(" in Mouse Area")
          }
        }
      }
    }
  }
}

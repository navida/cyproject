import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml 2.0
import ".."

Item{
  id: viewcontrol
  width: 600
  height: 600

  Item {
    id: range
    anchors.centerIn: parent
    width: 250
    height: 8

    property real minimum: 1
    property real maximum: 400
    property real value1: maximum
    property real value2: minimum

    Component.onCompleted: {
//      console.log("range_handle1_mousearea._width : "+range_handle1_mousearea._width)
//      console.log("range_handle2_mousearea._width : "+range_handle2_mousearea._width)
//      console.log(range_handle1_mousearea.drag.maximumX)
//      console.log(range_handle1_mousearea.drag.minimumX)
    }

    Rectangle {
      id: range_groove
      anchors.fill: parent
      color: "#100F0F"
      radius: 8
    }

    Rectangle{
      id: range_handle1
      anchors.verticalCenter: parent.verticalCenter
      smooth: true
      x: range.width - width
      width: 10
      height: 20
      color: "#A6A9AB"

      onXChanged: {
        var _value = (range.maximum) * (x - width) / (range.width - width * 2)
        range.value1 = _value
      }

      MouseArea{
        id: range_handle1_mousearea
        anchors.fill: parent
        drag.target: parent
        drag.axis: Drag.XAxis
        drag.minimumX: range_handle2.x + range_handle2.width
        drag.maximumX: range.width - range_handle1.width
        drag.threshold : 0

        property real _width: parseInt(range_handle1_mousearea.drag.maximumX - range_handle1_mousearea.drag.minimumX)

        onPressedChanged: {
          if(pressed){
            range_handle1.color = "#2BC2F3"
          }
          else{
            range_handle1.color = "#A6A9AB"
          }
        }
      }

      Label {
        anchors{
          horizontalCenter: range_handle1.horizontalCenter
          top: range_handle1.bottom
          topMargin: 2
        }
        text: parseInt(range.value1)
        font.pixelSize: 10
        color: "black"
      }
    }

    Rectangle{
      id: range_handle2
      anchors.verticalCenter: parent.verticalCenter
      smooth: true
      x: 0
      width: 10
      height: 20
      color: "#A6A9AB"

      onXChanged: {
        var _value = (range.maximum) * x / (range.width - width * 2)
        range.value2 = _value
      }

      MouseArea{
        id: range_handle2_mousearea
        anchors.fill: parent
        drag.target: parent
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: range_handle1.x - range_handle1.width
        drag.threshold : 0

        property real _width: parseInt(range_handle2_mousearea.drag.maximumX - range_handle2_mousearea.drag.minimumX)

        onPressedChanged: {
          if(pressed){
            range_handle2.color = "#2BC2F3"
          }
          else{
            range_handle2.color = "#A6A9AB"
          }
        }
      }
      Label {
        anchors{
          horizontalCenter: range_handle2.horizontalCenter
          top: range_handle2.bottom
          topMargin: 2
        }
        text: parseInt(range.value2)
        font.pixelSize: 10
        color: "black"
      }
    }
  }

}

//  Text Gradient Test!!!

//  Rectangle {
//    id: gradientRect;
//    width: 10
//    height: 10
//    gradient: Gradient {
//      GradientStop { position: 0; color: "white" }
//      GradientStop { position: 1; color: "steelblue" }
//    }
//    visible: false; // should not be visible on screen.
//    layer.enabled: true;
//    layer.smooth: true
//  }

//  Text {
//    id: textItem
//    font.pixelSize: 48
//    text: "Gradient Text"
//    anchors.centerIn: parent
//    layer.enabled: true
//    // This item should be used as the 'mask'
//    layer.samplerName: "maskSource"
//    layer.effect: ShaderEffect {
//      property var colorSource: gradientRect;
//      fragmentShader: "
//                  uniform lowp sampler2D colorSource;
//                  uniform lowp sampler2D maskSource;
//                  uniform lowp float qt_Opacity;
//                  varying highp vec2 qt_TexCoord0;
//                  void main() {
//                      gl_FragColor =
//                          texture2D(colorSource, qt_TexCoord0)
//                          * texture2D(maskSource, qt_TexCoord0).a
//                          * qt_Opacity;
//                  }
//              "
//    }
//  }

//  DropArea Test!!!

//DropArea {
//  anchors.fill: parent

//  onDropped: {
//    console.log("[Droparea] dropped")
//    console.log(drop.urls)
//  }
//}

//  Key Event Test!!!

//  ColumnLayout{
//    width: parent.width
//    height: parent.height

//    Rectangle{
//      id: myitem
//      width: root.width/10
//      height: root.height/10

//      Layout.maximumWidth : 100
//      Layout.maximumHeight : 100
//      Layout.minimumWidth : 0
//      Layout.minimumHeight : 0
//      Layout.preferredWidth : width
//      Layout.preferredHeight : height
//      color:"black"
//    }
//  }
//  TreeView{
//    width: 300
//    height: 300
//    focus: true
//    TableViewColumn{
//      width : 200
//      role: "ManufacturerName"
//      title: "ManufacturerName"
//    }
//    Keys.onPressed: {
//      if(!event.isAutoRepeat){
//        console.log("aa")
//        return
//      }
//      console.log(event.text)
//    }
//  }

//  font family Test!!!

//  ColumnLayout{
//    anchors.centerIn: parent
//    Text{
//      text:"I can't thank you enough."
//      font.family: "Helvetica"
//      font.pointSize: 60
//      color: "black"
//    }
//    Text{
//      text:"I can't thank you enough."
//      font.family: "Lucida Sans"
//      font.pointSize: 60
//      color: "black"
//    }
//    Text{
//      text:"I can't thank you enough."
//      font.family: "Lucida Fax"
//      font.pointSize: 60
//      color: "black"
//    }
//    Text{
//      text:"I can't thank you enough."
//      font.family: "Lucida Sans Typewriter"
//      font.pointSize: 60
//      color: "black"
//    }
//    Text{
//      text:"I can't thank you enough."
//      font.family: "Lucida Sans Console"
//      font.pointSize: 60
//      color: "black"
//    }
//  }


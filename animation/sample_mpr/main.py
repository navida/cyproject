import os, sys, time, math

import numpy as np
import vtk, SimpleITK
from vtk.util import numpy_support
from vtk.numpy_interface import dataset_adapter as dsa
import SimpleITK as sitk
import cyCafe

import concurrent.futures
from functools import partial
import multiprocessing
from multiprocessing import Process, Queue
import threading

from cyhub.cy_image_holder import CyQQuickView
from cyhub.cy_vtk2 import Vtk_image_holder
from cyhub.Reader import cyReader

from PyQt5.QtQuick import QQuickView
from PyQt5.QtCore import QObject, QUrl, QTimer, Qt, pyqtSlot, pyqtSignal
from PyQt5.QtQml import QQmlProperty

import _qapp

DCM_INTERVAL = 10
SEED = []


# def calc_mean_divide(n, r, _min_v, _max_v, _list):
#     # print("processing: {} ..< {}".format(n, n + r), end="... ")
#     _num = 0
#     _mean = 0.0
#     print("mean3 func :: ", n, " ", n + r)
#     for x in range(n, n + r):
#         if _min_v <= _list[x] and _list[x] <= _max_v:
#             _mean += _list[x]
#             _num += 1
#     if _num > 1:
#         _mean /= _num
#
#     # print(_mean)
#
#     return _mean

"""
Detect peaks in data based on their amplitude and other features.
"""
def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    """Detect peaks in data based on their amplitude and other features.

    Parameters
    ----------
    x : 1D array_like
        data.
    mph : {None, number}, optional (default = None)
        detect peaks that are greater than minimum peak height.
    mpd : positive integer, optional (default = 1)
        detect peaks that are at least separated by minimum peak distance (in
        number of data).
    threshold : positive number, optional (default = 0)
        detect peaks (valleys) that are greater (smaller) than `threshold`
        in relation to their immediate neighbors.
    edge : {None, 'rising', 'falling', 'both'}, optional (default = 'rising')
        for a flat peak, keep only the rising edge ('rising'), only the
        falling edge ('falling'), both edges ('both'), or don't detect a
        flat peak (None).
    kpsh : bool, optional (default = False)
        keep peaks with same height even if they are closer than `mpd`.
    valley : bool, optional (default = False)
        if True (1), detect valleys (local minima) instead of peaks.
    show : bool, optional (default = False)
        if True (1), plot data in matplotlib figure.
    ax : a matplotlib.axes.Axes instance, optional (default = None).

    Returns
    -------
    ind : 1D array_like
        indeces of the peaks in `x`.
   """

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)

    return ind


class Manager:
    class SliceImage(Vtk_image_holder):
        def __init__(self, mgr, _img_type='axial'):
            super().__init__()
            self.mapper = vtk.vtkImageResliceMapper()
            self.mapper.SetSlicePlane(vtk.vtkPlane())
            # self.actor = vtk.vtkImageSlice()
            self.actor = vtk.vtkImageActor()
            self.actor.SetMapper(self.mapper)
            self.property = None
            self.vtk_img = None

            self.picker = vtk.vtkVolumePicker()

            self.img_type = _img_type
            self.mgr = mgr

            # Interactor
            ISTYLES = cyCafe.cyVtkInteractorStyles()
            istyle = ISTYLES.get_vtk_interactor_style_image(False)
            self.view._Iren.SetInteractorStyle(istyle)
            istyle.AddObserver('MouseWheelForwardEvent', self.on_mouse_wheel)
            istyle.AddObserver('MouseWheelBackwardEvent', self.on_mouse_wheel)
            istyle.AddObserver('LeftButtonPressEvent', self.on_mouse_press)
            istyle.AddObserver('RightButtonPressEvent', self.on_mouse_press)

        def __del__(self):
            del self.mapper
            del self.actor
            del self.property
            del self.vtk_img
            del self.picker

        def set_vtk_img(self, vtk_img):
            if hasattr(self, 'vtk_img'):
                del self.vtk_img
            self.vtk_img = vtk_img
            self.mapper.SetInputData(self.vtk_img)

            ac = self.ren.GetViewProps()
            ac.InitTraversal()
            for i in range(ac.GetNumberOfItems()):
                next_actor = ac.GetNextItemAsObject()
                if next_actor == self.actor:
                    return

            self.ren.AddActor(self.actor)
            # self.ren.ResetCameraClippingRange()

            # Camera
            e = vtk_img.GetExtent()
            o = vtk_img.GetOrigin()
            s = vtk_img.GetSpacing()
            _c_x = e[1] * 0.5 * s[0] + o[0]
            _c_y = e[3] * 0.5 * s[1] + o[1]
            _c_z = e[5] * 0.5 * s[2] + o[2]
            c = [_c_x, _c_y, _c_z]
            camera = self.ren.GetActiveCamera()
            camera.SetFocalPoint(c)
            camera.ParallelProjectionOn()

            b = self.ren.ComputeVisiblePropBounds()
            camera.SetParallelScale((b[3] - b[2]) / 2 / 1.0)

            if self.img_type == 'axial':
                camera.SetPosition(c[0], c[1], c[2] + 170)
                camera.SetViewUp(0, -1, 0)
                self.set_plane(c, [0, 0, 1])
            elif self.img_type == 'coronal':
                camera.SetPosition(c[0], c[1] - 170, c[2])
                camera.SetViewUp(0, 0, 1)
                self.set_plane(c, [0, -1, 0])
            elif self.img_type == 'sagittal':
                camera.SetPosition(c[0] + 170, c[1], c[2])
                camera.SetViewUp(0, 0, 1)
                self.set_plane(c, [1, 0, 0])

        def set_windowing(self, level, window):
            _prop = self.actor.GetProperty()
            _prop.SetColorLevel(level + window)
            _prop.SetColorWindow(window * 4)
            _prop.Modified()

        def set_actor_property(self, img_prop):
            self.property = img_prop
            self.actor.SetProperty(img_prop)
            self.actor.Modified()

        def set_plane(self, origin, normal):
            _p = self.mapper.GetSlicePlane()
            _p.SetOrigin(origin)
            _p.SetNormal(normal)
            _p.Modified()
            self.mapper.Update()

        def get_plane(self):
            return self.mapper.GetSlicePlane()

        def get_actor(self):
            return self.actor

        def set_slice_plane(self, plane):
            self.mapper.SetSlicePlane(plane)

        def set_thickness(self, thickness):
            if not self.vtk_img is None:
                # self.mapper.SetSlabTypeToMean()
                self.mapper.SetSlabThickness(thickness)
                self.mapper.Update()

        def set_rendering_type(self, _value):
            if not self.vtk_img is None:
                if _value == "MIP":
                    self.mapper.SetSlabTypeToMax()
                elif _value == "minIP":
                    self.mapper.SetSlabTypeToMin()
                elif _value == "MPR":
                    self.mapper.SetSlabTypeToMean()
                self.mapper.Update()

        def on_mouse_wheel(self, _rwi, _event):
            self.mgr.on_mouse_wheel(self.img_type, _event)

        def on_mouse_press(self, _rwi, _event):
            i = _rwi.GetInteractor()
            x, y = i.GetEventPosition()
            if _event == "LeftButtonPressEvent":
                self.picker.Pick(x, y, 0, self.ren)
                pos = self.picker.GetPickPosition()
                _s = np.array(self.vtk_img.GetSpacing())
                _o = np.array(self.vtk_img.GetOrigin())
                _pos = np.array(pos)
                _local_pos = ((_pos - _o) / _s).astype(int)
                print("LOCAL POS :: ", _local_pos)

        def update(self):
            self.vtk_img.Modified()
            self.mapper.Modified()
            self.mapper.Update()
            self.actor.Modified()
            self.refresh()

        def clear_seeds(self):
            if hasattr(self, '_A_SEEDS'):
                for _a in self._A_SEEDS:
                    self.ren.RemoveActor(_a)
                self._A_SEEDS.clear()
                del self._A_SEEDS
                self.refresh()

    class VOL(Vtk_image_holder):
        def __init__(self, vtk_img, name):
            super().__init__()

            self.c_f, self.o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
            self.p = vtk.vtkVolumeProperty()
            self.m = vtk.vtkGPUVolumeRayCastMapper()
            self.v = vtk.vtkVolume()

            self.opacityWindow = 4096
            self.opacityLevel = 2048 + 1024

            # property
            self.p.SetIndependentComponents(True)
            self.p.SetInterpolationTypeToLinear()

            self.m.SetUseJittering(True)
            self.m.SetBlendModeToComposite()

            self.clipping_planes = [vtk.vtkPlane(), vtk.vtkPlane(), vtk.vtkPlane()]

            self.p.SetColor(self.c_f)
            self.p.SetScalarOpacity(self.o_f)

            # actor
            self.v.SetMapper(self.m)
            self.v.SetProperty(self.p)

            self.ren.AddActor(self.v)

            self.txt_actor = vtk.vtkTextActor()
            self.txt_actor.GetTextProperty().SetFontSize(20)

            if vtk_img:
                self.set_vtk_img(vtk_img)

            if name:
                self.txt_actor.SetInput(name)
                self.ren.AddActor(self.txt_actor)

            self.picker = vtk.vtkVolumePicker()

            # Interactor
            ISTYLES = cyCafe.cyVtkInteractorStyles()
            istyle = ISTYLES.get_vtk_interactor_style_volume_3d()
            self.view._Iren.SetInteractorStyle(istyle)
            istyle.AddObserver('MouseMoveEvent', self.on_mouse_move)
            istyle.AddObserver('MouseWheelForwardEvent', self.on_mouse_wheel)
            istyle.AddObserver('MouseWheelBackwardEvent', self.on_mouse_wheel)
            istyle.AddObserver('LeftButtonPressEvent', self.on_mouse_press)
            istyle.AddObserver('RightButtonPressEvent', self.on_mouse_press)
            istyle.AddObserver('LeftButtonReleaseEvent', self.on_mouse_release)
            istyle.AddObserver('RightButtonReleaseEvent', self.on_mouse_release)
            istyle.AddObserver('KeyPressEvent', self.on_key_press)

            # params
            self.th_lower = -1024
            self.th_upper = 6

        def __del__(self):
            del self.m
            del self.v
            del self.p
            del self.vtk_img
            del self.txt_actor

            for p in self.clipping_planes:
                del p
            del self.clipping_planes

            if hasattr(self, '__preset_type__'):
                del self.__preset_type__

        def set_vtk_img(self, vtk_img):
            self.vtk_img = vtk_img

            e = self.vtk_img.GetExtent()
            s = self.vtk_img.GetSpacing()
            o = self.vtk_img.GetOrigin()
            c = self.vtk_img.GetCenter()
            b = self.vtk_img.GetBounds()
            d = self.vtk_img.GetDimensions()
            print("e : ", e)
            print("s : ", s)
            print("o : ", o)
            print("c : ", c)
            print("b : ", b)
            print("d : ", d)

            # Camera
            e = vtk_img.GetExtent()
            o = vtk_img.GetOrigin()
            s = vtk_img.GetSpacing()
            _c_x = e[1] * 0.5 * s[0] + o[0]
            _c_y = e[3] * 0.5 * s[1] + o[1]
            _c_z = e[5] * 0.5 * s[2] + o[2]
            c = [_c_x, _c_y, _c_z]
            camera = self.ren.GetActiveCamera()
            camera.SetFocalPoint(c)
            camera.SetPosition(c[0], c[1] - e[1] / 2, c[2])
            camera.SetViewUp(0, 0, 1)

            self.m.SetInputData(self.vtk_img)
            self.m.Update()
            self.set_tf_preset1()

        def set_threshold_param(self, lower, upper):
            self.th_lower = lower
            self.th_upper = upper

        def test(self, _input):
            e = self.vtk_img.GetExtent()
            s = self.vtk_img.GetSpacing()
            o = self.vtk_img.GetOrigin()
            c = self.vtk_img.GetCenter()
            b = self.vtk_img.GetBounds()
            d = self.vtk_img.GetDimensions()
            c = list(c)
            c = np.add(c, DATA[2])
            src = _input
            t = vtk.vtkTransform()
            t.Scale(0.95, 0.95, 1.00)
            t.Update()
            tpd = vtk.vtkTransformPolyDataFilter()
            tpd.SetTransform(t)
            tpd.SetInputData(src)
            tpd.Update()
            src = tpd.GetOutput()

            whiteImage = vtk.vtkImageData()
            whiteImage.SetDimensions(d)
            whiteImage.SetOrigin(o)
            whiteImage.SetSpacing(s)
            whiteImage.SetExtent(e)
            whiteImage.AllocateScalars(vtk.VTK_CHAR, 1)
            inval = 1
            outval = 0
            count = whiteImage.GetNumberOfPoints()
            ndarray_buf = np.zeros(count, dtype=np.int8)
            ndarray_buf.fill(inval)
            whiteImage.GetPointData().SetScalars(dsa.numpyTovtkDataArray(ndarray_buf))
            pol2stenc = vtk.vtkPolyDataToImageStencil()
            pol2stenc.SetInputData(src)
            pol2stenc.SetOutputOrigin(o)
            pol2stenc.SetOutputSpacing(s)
            pol2stenc.SetOutputWholeExtent(e)
            pol2stenc.Update()
            imgstenc = vtk.vtkImageStencil()
            imgstenc.SetInputData(whiteImage)
            imgstenc.SetStencilData(pol2stenc.GetOutput())
            imgstenc.ReverseStencilOff()
            imgstenc.SetBackgroundValue(outval)
            imgstenc.Update()
            sampled_img = imgstenc.GetOutput()
            _mask = numpy_support.vtk_to_numpy(sampled_img.GetPointData().GetArray(0))
            # Subtract threshold img to convexhull mask
            B = numpy_support.vtk_to_numpy(self.threshold_img.GetPointData().GetArray(0))
            C = np.zeros(len(B), dtype=np.int16)
            C.fill(-1024)
            C[np.logical_and(_mask == 1, B == -1024)] = 1000
            # C[np.logical_and(_convexhull_vol == True, B == -1024)] = 1000

            C = C.reshape((d[2], d[1], d[0]))  # order : z-y-x
            _sitk_img = sitk.GetImageFromArray(C)
            filter = sitk.BinaryErodeImageFilter()
            filter.SetKernelRadius(3)
            filter.SetForegroundValue(1000)
            filter.SetBackgroundValue(-1024)
            _imgOutput = filter.Execute(_sitk_img)
            C = sitk.GetArrayFromImage(_imgOutput).ravel()

            self.threshold_img.GetPointData().SetScalars(dsa.numpyTovtkDataArray(C))

            self.m.SetInputData(self.threshold_img)
            self.m.Update()
            self.v.Modified()
            self.refresh()

        def get_actor(self):
            return self.v

        def set_tf_preset1(self, _target_vol=None):
            # if hasattr(self, '__preset_type__') and self.__preset_type__ == 1:
            #     return

            self.m.SetMaximumImageSampleDistance(5)
            self.m.Modified()

            _level = 1000
            _offset = 1000
            _min = 0
            _max = 4000

            target_volume = _target_vol
            if _target_vol is None:
                target_volume = self.vtk_img if hasattr(self, 'vtk_img') else None

            if target_volume is not None:
                self.__preset_type__ = 1
                buf_volume = numpy_support.vtk_to_numpy(target_volume.GetPointData().GetArray(0))
                F, B = np.histogram(buf_volume, 1000)
                _max = np.max(buf_volume)
                _min = np.min(buf_volume)
                _sum = np.sum(F[1:])

                # _peak_indexies = detect_peaks(F[1:], mph=len(buf_volume) * 0.05, show=True)
                _peak_indexies = detect_peaks(F[1:], mph=_sum * 0.005, mpd=30, show=False)
                # _valley_indexies = detect_peaks(F[1:], valley=True, mpd=30, show=False)
                # if len(_peak_indexies) > 0 and len(_valley_indexies) > 0:
                if len(_peak_indexies) > 1:
                    # _peak_idx = _peak_indexies[-1]
                    # for i in reversed(_valley_indexies):
                    #     if i < _peak_idx:
                    #         _valley_idx = i
                    #         _level = B[_peak_idx+1]
                    #         _offset = B[_peak_idx+1] - B[_valley_idx+1]
                    #         break
                    _level = B[_peak_indexies[-1] + 1]
                    _offset = _level - B[_peak_indexies[0] + 1]
                else:
                    # TODO
                    _level = np.mean(buf_volume)
                    _offset = (_max - _min) * 0.15  # experimental param : 15%
                    _level = _level - (_offset * 2)

            self._level = _level
            self._offset = _offset

            self.c_f.RemoveAllPoints()
            self.CP = [[_min, 0, 0, 0],
                       [_level, 0.2, 0.3, 0.7],
                       # [_level+_offset*2, 253 / 255, 106 / 255, 50 / 255],
                       # [_level+_offset*2, 0 / 255, 106 / 255, 50 / 255],
                       [_level + _offset * 3.5, 0.7, 0.8, 0.9],
                       [_level + _offset * 5, 0.9, 0.9, 0.9],
                       [_max, 1, 1, 1]]

            for c in self.CP:
                self.c_f.AddRGBPoint(*c)

            self.o_f.RemoveAllPoints()
            self.OP = [[_min, 0],
                       [_level, 0.0],

                       # [_level+_offset*5, 0.0],
                       # [_level+_offset*5, 1.0],

                       [_level + _offset / 2, 0.0],
                       [_level + _offset, 1.0],

                       [_max, 1.0]]
            for o in self.OP:
                self.o_f.AddPoint(*o)

            self.p.SetScalarOpacityUnitDistance(1.5)
            self.p.SetInterpolationTypeToLinear()
            self.p.ShadeOn()
            self.p.SetAmbient(0.7)
            self.p.SetDiffuse(0.8)
            self.p.SetSpecular(0.3)
            self.p.SetSpecularPower(100)
            self.p.Modified()

            if self.m.GetInput():
                self.m.Update()

        def on_mouse_move(self, _rwi, _event):
            pass

        def on_mouse_press(self, _rwi, _event):
            i = _rwi.GetInteractor()
            x, y = i.GetEventPosition()
            if _event == "LeftButtonPressEvent":

                self.picker.Pick(x, y, 0, self.ren)
                pos = self.picker.GetPickPosition()
                _s = np.array(self.vtk_img.GetSpacing())
                _o = np.array(self.vtk_img.GetOrigin())
                _pos = np.array(pos)
                _local_pos = ((_pos - _o) / _s).astype(int)
                print("LOCAL POS :: ", _local_pos)

                # if hasattr(self, '__segment__'):
                #     self.picker.Pick(x, y, 0, self.ren)
                #     pos = self.picker.GetPickPosition()
                #     _s = np.array(self.vtk_img.GetSpacing())
                #     _o = np.array(self.vtk_img.GetOrigin())
                #     _pos = np.array(pos)
                #     _local_pos = ((_pos / _s) - _o).astype(int)
                #     print("LOCAL POS :: ", _local_pos)
                # sphere = vtk.vtkSphereSource()
                # sphere.SetCenter(pos[0:3])
                # sphere.Update()
                # m = vtk.vtkPolyDataMapper()
                # m.SetInputData(sphere.GetOutput())
                # a = vtk.vtkActor()
                # a.SetMapper(m)
                # self.ren.AddActor(a)
                # # self.__TMP_ACTORS__
                # self.view.Render()
            elif _event == "RightButtonPressEvent":
                pass

        def on_mouse_release(self, _rwi, _event):
            i = _rwi.GetInteractor()
            if _event == "LeftButtonReleaseEvent":
                pass
            elif _event == "RightButtonReleaseEvent":
                pass

        def on_mouse_wheel(self, _rwi, _event):
            i = _rwi.GetInteractor()

        def on_key_press(self, obj, _event):
            key = obj.GetKeySym()  # works fine

            if key == 's':
                print("MODE :: NONE")
                if hasattr(self, '__segment__'):
                    del self.__segment__
                    del self.__pts__
            elif key == 'h':
                print("MODE :: SEGMENT")
                if not hasattr(self, '__segment__'):
                    self.__segment__ = True
                    self.__pts__ = []
            elif key == 'z':
                self.segment()
                self.iren.Render()

    def __init__(self):
        self.vtk_img = None
        self.plane = vtk.vtkPlane()
        self.coronal = self.SliceImage(self, _img_type='coronal')
        self.sagittal = self.SliceImage(self, _img_type='sagittal')
        self.axial = self.SliceImage(self, _img_type='axial')
        self.vol = self.VOL(None, "vol")

    def set_vtk_img(self, vtk_img):
        self.vtk_img = vtk_img

        self.vol.set_vtk_img(self.vtk_img)
        self.coronal.set_vtk_img(self.vtk_img)
        self.sagittal.set_vtk_img(self.vtk_img)
        self.axial.set_vtk_img(self.vtk_img)

        self.coronal.set_windowing(self.vol._level, self.vol._offset)
        self.sagittal.set_windowing(self.vol._level, self.vol._offset)
        self.axial.set_windowing(self.vol._level, self.vol._offset)

    def get_vtk_img(self, _img_type):
        if _img_type == 'axial':
            return self.axial
        elif _img_type == 'coronal':
            return self.coronal
        elif _img_type == 'sagittal':
            return self.sagittal
        elif _img_type == 'vol':
            return self.vol

    def set_slice_plane(self, _plane):
        self.axial.mapper.SetSlicePlane(_plane)
        self.coronal.mapper.SetSlicePlane(_plane)
        self.sagittal.mapper.SetSlicePlane(_plane)

    def on_mouse_press(self, _img_type):
        print(_img_type)

    def on_mouse_wheel(self, _img_type, _event):
        _sign = -1 if _event == r'MouseWheelForwardEvent' else 1
        _interval = 1

        interval = _interval * _sign

        interval_mat = [0] * 3

        normalVector = None

        if _img_type == 'axial':
            # < class 'tuple'>: (0.0, 0.0, 1.0)
            self.plane = self.axial.get_plane()
            normalVector = self.plane.GetNormal()
            interval_mat[2] = 1 * interval
        elif _img_type == 'coronal':
            # < class 'tuple'>: (0.0, -1.0, 0.0)
            self.plane = self.coronal.get_plane()
            normalVector = self.plane.GetNormal()
            interval_mat[1] = 1 * interval
        elif _img_type == 'sagittal':
            # < class 'tuple'>: (1.0, 0.0, 0.0)
            self.plane = self.sagittal.get_plane()
            normalVector = self.plane.GetNormal()
            interval_mat[0] = 1 * interval

        upVector = [0, 0, 1] if(_img_type == 'sagittal' or
                                _img_type == 'coronal') else [0, 1, 0]

        newVec1 = [0] * 3
        vtk.vtkMath.Cross(normalVector, upVector, newVec1)
        newVec2 = [0] * 3
        vtk.vtkMath.Cross(normalVector, newVec1, newVec2)

        if _img_type == 'sagittal':
            V = np.array([normalVector, newVec1, newVec2]).transpose()
        elif _img_type == 'coronal':
            V = np.array([newVec1, normalVector, newVec2]).transpose()
        elif _img_type == 'axial':
            V = np.array([newVec1, newVec2, normalVector]).transpose()

        newXYZ = np.dot(V, interval_mat)

        self.plane.SetOrigin(self.plane.GetOrigin() + newXYZ)

        if _img_type == 'sagittal':
            self.sagittal.mapper.SetSlicePlane(self.plane)
            self.sagittal.refresh()
        elif _img_type == 'coronal':
            self.coronal.mapper.SetSlicePlane(self.plane)
            self.coronal.refresh()
        elif _img_type == 'axial':
            self.axial.mapper.SetSlicePlane(self.plane)
            self.axial.refresh()


class Sample:
    def __init__(self, vtk_img):
        self.vtk_img = vtk_img
        self.buf_volume = numpy_support.vtk_to_numpy(self.vtk_img.GetPointData().GetArray(0))
        self.F, self.B = np.histogram(self.buf_volume, 100)
        self._min_value, self._max_value = np.min(self.buf_volume), np.max(self.buf_volume)

        self.calc_volume_segmentation_infos()


    def calc_volume_segmentation_infos(self):
        threshold_min = 0.0
        threshold_max = self.calc_cumulative_value_in_reverse(0.0005)

        st = time.time()
        mean1 = self.calc_data_mean(threshold_min, threshold_max)
        print("11111 :: ", time.time() - st, "----- ", mean1)
        st = time.time()
        mean_of_low_intensity = self.calc_data_mean(threshold_min, mean1)
        print("22222 :: ", time.time() - st, "----- ", mean_of_low_intensity)

        m_value_max = self.B.size - 1
        m_histogram_bin_min = math.ceil(threshold_min)
        m_histogram_bin_max = math.floor(threshold_max)

        _threshold = self.calculate_from_histogram(self.F.size)

        for x in _threshold:
            x += m_histogram_bin_min

        nIntervalSize = (m_histogram_bin_max + 1) * 0.01
        if nIntervalSize > 50:
            nIntervalSize = 50
        elif nIntervalSize < 1:
            nIntervalSize = 1

        nIntervals = (m_histogram_bin_max + 1) / nIntervalSize

        dataCountAvg = [0]*int(nIntervals)

        dataCountAvgMinPos = m_histogram_bin_min / nIntervalSize

        for i in range(0, int(dataCountAvgMinPos)):
            dataCountAvg[i] = 0.0

        bin_idx = dataCountAvgMinPos * nIntervalSize

        for i in range(int(dataCountAvgMinPos), int(nIntervals)):
            dataCountAvg[i] = 0.0
            for j in range(0, nIntervalSize):
                bin_idx += 1
                dataCountAvg[i] += self.F[int(bin_idx)]
            dataCountAvg[i] /= nIntervalSize

        bin_idx = 0
        air_region_count = 0
        start_threshold = _threshold[0]
        for i in range(0, 3):
            while True:
                if bin_idx < _threshold[i] and bin_idx != self.F.size:
                    air_region_count += self.F[int(bin_idx)]
                    bin_idx += 1
                else:
                    break
            if (air_region_count / self.buf_volume.size) > 0.05:
                start_threshold = _threshold[i]
                break
            # for j in range(int(bin_idx), int(_threshold[i])):
            #     bin_idx += 1
            #     air_region_count += self.F[int(bin_idx)]
            #     if (air_region_count / self.buf_volume.size) > 0.05:
            #         start_threshold = _threshold[i]
            #         break

        if start_threshold < mean_of_low_intensity:
            start_threshold = (mean_of_low_intensity + start_threshold) * 0.5

        dataCountAvgStartIdxRef = int(start_threshold / nIntervalSize)
        if dataCountAvgStartIdxRef < dataCountAvgMinPos:
            dataCountAvgStartIdxRef = dataCountAvgMinPos

        dataCountAvgMaxIdxRef = dataCountAvgStartIdxRef
        for i in range(int(dataCountAvgMaxIdxRef) + 1, int(nIntervals)):
            if dataCountAvg[int(dataCountAvgMaxIdxRef)] < dataCountAvg[i]:
                dataCountAvgMaxIdxRef = i

        dataCountMaxRef = dataCountAvg[int(dataCountAvgMaxIdxRef)] * 0.25



        # st = time.time()
        # mean2 = self.calc_data_mean2(threshold_min, threshold_max)
        # print("22222 :: ", time.time() - st, "----- ", mean2)
        #
        # st = time.time()
        # mean3 = self.calc_data_mean3(threshold_min, threshold_max)
        # print("33333 :: ", time.time() - st, "----- ", mean3)

        aa = 100

        print("-------------- end --------------")

    def calculate_from_histogram(self, n_hiso_bins, n_threshold=3):
        B = np.array(self.buf_volume)

        e = self.vtk_img.GetExtent()
        s = self.vtk_img.GetSpacing()
        o = self.vtk_img.GetOrigin()
        c = self.vtk_img.GetCenter()
        b = self.vtk_img.GetBounds()
        d = self.vtk_img.GetDimensions()
        _tmp_img = vtk.vtkImageData()
        _tmp_img.SetDimensions(d)
        _tmp_img.SetOrigin(o)
        _tmp_img.SetSpacing(s)
        _tmp_img.SetExtent(e)
        _tmp_img.GetPointData().SetScalars(dsa.numpyTovtkDataArray(B))
        seperated_img = _tmp_img

        B = B.reshape((d[2], d[1], d[0]))
        _sitk_img = sitk.GetImageFromArray(B)

        # _imgOutput = sitk.VotingBinaryHoleFilling(image1=_sitk_img,
        #                                           radius=[2, 2, 1],
        #                                           majorityThreshold=1,
        #                                           backgroundValue=-1024,
        #                                           foregroundValue=1000)
        filter = sitk.OtsuMultipleThresholdsImageFilter()
        filter.SetNumberOfHistogramBins(n_hiso_bins)
        filter.SetNumberOfThresholds(n_threshold)
        filter.Execute(_sitk_img)

        otsuThresholds = filter.GetThresholds()

        return otsuThresholds


    def calc_cumulative_value_in_reverse(self, _fraction):
        n_bins = self.B.size
        tot_hist = self.buf_volume.size

        fraction_value = 0
        cur_hist, prev_hist = 0, 0
        target_hist = tot_hist * _fraction + 0.5

        for x in range(0, n_bins - 1):
            j = (n_bins-2) - x
            prev_hist = cur_hist
            cur_hist += self.F[j]
            if target_hist > prev_hist and target_hist <= cur_hist:
                fraction_value = (j - 0.5) + (cur_hist - target_hist) / (self.F[j])
                break

        if(fraction_value > n_bins - 1):
            fraction_value = n_bins - 1

        return fraction_value

    def calc_min(self, a, b):
        return a if a < b else b

    def calc_data_mean(self, threshold_min, threshold_max):
        _min = threshold_min
        _max = threshold_max
        _size = self.buf_volume.size
        _volume_list = self.buf_volume.tolist()

        nCPUs = multiprocessing.cpu_count()
        n_threads = self.calc_min(nCPUs, (_size * 0.0001))

        split_data_size = _size / n_threads
        remainder = _size % n_threads

        process_list = []
        result = Queue()
        total = 0

        def _do(start, end, result):
            _num = 0
            _mean = 0.0

            print("mean1 func :: ", start, " ", end)
            for x in range(start, end):
                if _min <= _volume_list[x] and _volume_list[x] <= _max:
                    _mean += _volume_list[x]
                    _num += 1

            if _num > 1:
                _mean /= _num

            result.put(_mean)
            return

        for x in range(0, n_threads):
            if x == 0:
                start = 0
                end = split_data_size
            elif x >= 1:
                start = split_data_size * x
                end = split_data_size * (x + 1)

            # t = threading.Thread(target=_do, args=(int(start), int(end), result))
            p = Process(target=_do, args=(int(start), int(end), result))
            process_list.append(p)

        for x in range(0, n_threads):
            process_list[x].start()

        for x in range(0, n_threads):
            process_list[x].join()

        result.put('STOP')
        while True:
            tmp = result.get()
            if tmp == 'STOP':
                break
            else:
                total += tmp

        return total

        # def calc_data_mean2(self, threshold_min, threshold_max):
        #     _volume_list = self.buf_volume.tolist()
        #     split_data_size = len(_volume_list) / 8
        #     result = []
        #
        #     for x in range(0, 8):
        #         if x == 0:
        #             start = 0
        #             end = split_data_size
        #         elif x >= 1:
        #             start = split_data_size * x
        #             end = split_data_size * (x + 1)
        #
        #         _num = 0
        #         _mean = 0.0
        #
        #         print("mean2 func :: ", start, " ", end)
        #         for x in range(int(start), int(end)):
        #
        #             if threshold_min <= _volume_list[x] and _volume_list[x] <= threshold_max:
        #                 _mean += _volume_list[x]
        #                 _num += 1
        #
        #         if _num > 1:
        #             _mean /= _num
        #
        #         result.append(_mean)
        #
        #     return sum(result)

        # def calc_data_mean3(self, threshold_min, threshold_max):
        #     _min = threshold_min
        #     _max = threshold_max
        #     _size = self.buf_volume.size
        #     _volume_list = self.buf_volume.tolist()
        #
        #     r = _size // 8
        #
        #     # with concurrent.futures.ProcessPoolExecutor(max_workers=8) as exe:
        #     #     result = 0
        #     #     #    .map() -> 개별 작업이 동시에 실행된 후, 먼저 종료된 작업부터 내놓는 리턴값을 내놓음
        #     #     #    1번째 파라미터 => 동작함수, 2번째 파라미터 => 입력데이터
        #     #     for i in exe.map(partial(calc_mean_divide, r=r, _min_v=_min, _max_v=_max, _list=_volume_list), range(0, _size, r)):
        #     #         result += i
        #
        #     with concurrent.futures.ProcessPoolExecutor(max_workers=2) as exe:
        #         # result = 0
        #         #    .map() -> 개별 작업이 동시에 실행된 후, 먼저 종료된 작업부터 내놓는 리턴값을 내놓음
        #         #    1번째 파라미터 => 동작함수, 2번째 파라미터 => 입력데이터
        #         # for i in exe.map(partial(calc_mean_divide, r=r, _min_v=_min, _max_v=_max, _list=_volume_list), range(0, _size, r)):
        #         #     result += i
        #         fs = {exe.submit(calc_mean_divide, n, r, _min, _max, _volume_list) for n in range(0, _size, r)}
        #         done, _ = concurrent.futures.wait(fs)
        #         result = sum(f.result() for f in done)
        #
        #     return result

class my_app(CyQQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), './main.qml')))
        self.resize(1300, 650)
        self.show(isMaximize=False)

        # DCM Read
        dir_path = "/Users/jeongjihong/dicom/S0000000033/S0000000012/"
        dcm_list = os.listdir(dir_path)
        # dcm_reader = cyCafe.cyDicomReader()
        self.reader = cyReader()

        self.reader.free_dicom_image()
        # for f in files[len(files)//2:-1:DCM_INTERVAL]:
        for f in dcm_list[::DCM_INTERVAL]:
            self.reader.read_next(dir_path + f)
        self.reader.generate_vtk_img()
        self.vtk_img = self.reader.get_vtk_img()

        self.sample = Sample(self.vtk_img)

        self.manager = Manager()
        self.manager.set_vtk_img(self.vtk_img)

        # init size of img holder
        self.vtk_vol_item = self.rootObject().findChild(QObject, 'vtk_vol')
        self.vtk_coronal_item = self.rootObject().findChild(QObject, 'vtk_coronal')
        self.vtk_sagittal_item = self.rootObject().findChild(QObject, 'vtk_sagittal')
        self.vtk_axial_item = self.rootObject().findChild(QObject, 'vtk_axial')

        self.vtk_vol_item.set_vtk(self.manager.get_vtk_img('vol'))
        self.vtk_coronal_item.set_vtk(self.manager.get_vtk_img('coronal'))
        self.vtk_sagittal_item.set_vtk(self.manager.get_vtk_img('sagittal'))
        self.vtk_axial_item.set_vtk(self.manager.get_vtk_img('axial'))

        self.init_sig_slot()
        self.init_controls()

    def init_sig_slot(self):
        self.rootObject().findChild(QObject, 'btn').btn_clicked.connect(self.btn_clicked)

    def init_controls(self):
        #histogram
        f = open('I.dat', 'r')
        H = eval(f.readline())
        minX = 0
        maxX = len(H)
        lenX = maxX - minX
        offsetX = minX
        minY = int(min(H))
        maxY = int(max(H))

        item = self.rootObject().findChild(QObject, 'histogram_item')
        item.set_datas(H, minX, maxX, minY, maxY)

        # #colormap
        # _, rng = self.reader.get_histogram()
        # minimumValue, maximumValue = min(rng), max(rng)
        #
        # """
        # Colormap
        # """
        # color_values = self.vol.GetColorValues()
        # # normalize to [0-1]
        # for v in color_values:
        #     v[0] = (v[0] - minimumValue) / (maximumValue - minimumValue)
        # self.colormap = self.root_qml_control.findChild(QObject, 'colormap')
        # assert self.colormap
        # self.colormap.init_colormap(color_values)

    @pyqtSlot()
    def btn_clicked(self):
        print("bb")


if __name__ == '__main__':

    app = my_app()

    # Start Qt event loop.
    _qapp.exec_(None, False)

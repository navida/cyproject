import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.0


Item {
    id: colormap
    width: 420
    height: 20

    signal sig_update()

    function get_gradient_stops(){
        return color_rect.gradient.stops;
    }

    ColorDialog {
        id: colorDialog
        title: "Please choose a color"

        property var response_func: null

        function set_response_func(_func){
            response_func = _func;
        }

        onAccepted: {
            // TODO !!!
            console.log("You chose: " + colorDialog.color);
            if(response_func != null){
                response_func(colorDialog.color);
                response_func = null;
            }
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false
    }



    // ColorMap
    Item {
        id: color_item
        width: parent.width
        height: 20

        // TODO well.. it's sloppy.. :(
        property real new_color_tick_id: 0
        property var arrPositionsCol: { "0": 0.0, "1": 0.6, "2": 0.8, "3": 1.0 }
        property var arrColors: { "0": Qt.rgba(0, 0, 1, 1), "1": Qt.rgba(0.3, 0.6, 0.1, 1), "2": Qt.rgba(1, 0, 0, 1), "3": Qt.rgba(0, 1, 1, 1) }

        property real offset: 10
        property real size_tick: 16

        function init_colormap(){
            //colormap.new_tick_id = 0;

            var arrTicks = []
            for(var p in color_item.arrPositionsCol){
                var s = stopComponent.createObject(color_rect, {"position":color_item.arrPositionsCol[p], "color":color_item.arrColors[p]});
                arrTicks.push(s)

                var t = color_mouse.create_color_tick(color_item.arrPositionsCol[p], color_item.arrColors[p]);
            }
            color_rect.gradient.stops = arrTicks;
        }

        function update_colormap()
        {
            // TODO tricky update method... :(
            // what is the best way to update "linear gradient" ... ?
            var grad = color_rect.gradient;
            color_rect.gradient = null;
            color_rect.gradient = grad;
        }

        LinearGradient {
            id: color_rect
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - color_item.offset
            height: parent.height
            start: Qt.point(0, 0)
            end: Qt.point(parent.width, 0)
            cached: false

            gradient: Gradient {
            }

            Component{
                id:stopComponent
                GradientStop {}
            }

            Component.onCompleted: {
                // init !!!
                color_item.init_colormap();
                colormap.sig_update.connect(color_item.update_colormap);
                colormap.sig_update();
            }
        }

        MouseArea {
            id: color_mouse
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - color_item.offset
            height: parent.height

            function get_tick_string(_posX, _color)
            {
                var str_tick =
'import QtQuick 2.7;
import QtGraphicalEffects 1.0;
Rectangle {
id: tick_' + color_item.new_color_tick_id + '\n' +
'anchors.verticalCenter: parent.verticalCenter
width: color_item.size_tick
height: color_item.size_tick
x: ' + _posX + '\n' +
'color: \'' + _color + '\'\n' +
'border.color: "lightgray"
border.width: 2
radius: 15

property var tick_id: ' + color_item.new_color_tick_id + '\n

signal sig_changed_color(var _col)

function on_sig_changed_color(_col){
    console.log("changed color!", _col);
    color = _col;
    var index_of_tick = Object.keys(color_item.arrPositionsCol).indexOf(tick_id.toString());
    color_rect.gradient.stops[index_of_tick].color = color;
    colormap.sig_update();
}

Component.onCompleted: {
    sig_changed_color.connect(on_sig_changed_color);
}

Component{
    id: stop_of_tick
    GradientStop {}
}

MouseArea {
    anchors.fill: parent
    drag.target: parent
    drag.axis: Drag.XAxis
    drag.minimumX: 0 - (width/2)
    drag.maximumX: colormap.width - color_item.offset - (width/2)
    onDoubleClicked: {
        colorDialog.visible = true;
        colorDialog.set_response_func(parent.on_sig_changed_color);
    }
    onPositionChanged: {
        // TODO!!!!
        var index_of_tick = Object.keys(color_item.arrPositionsCol).indexOf(parent.tick_id.toString());
        var newPos = (parent.x+width/2) / color_mouse.width;
        color_rect.gradient.stops[index_of_tick].position = newPos;
        colormap.sig_update();
    }
}
}';

                color_item.new_color_tick_id++;
                return str_tick;
            }

            function create_color_tick(_posX, _color){
                var posX =  _posX * width - (color_item.size_tick/2);
                var col = _color;
                return Qt.createQmlObject(get_tick_string(posX, col), color_mouse, "color_tick");
            }

            function create_stop(_x){
                // re-generate existing stops
                var arrTicks = []
                for(var p in color_rect.gradient.stops){
                    var _s = color_rect.gradient.stops[p];
                    arrTicks.push(_s)
                }

                // create new stop
                var new_idx = Object.keys(color_item.arrPositionsCol).length;
                color_item.arrPositionsCol[color_item.new_color_tick_id] = _x / color_mouse.width;
                color_item.arrColors[color_item.new_color_tick_id] = colorDialog.color;
                var s = stopComponent.createObject(color_rect, {"position":color_item.arrPositionsCol[color_item.new_color_tick_id], "color":color_item.arrColors[color_item.new_color_tick_id]});
                var t = color_mouse.create_color_tick(color_item.arrPositionsCol[color_item.new_color_tick_id], color_item.arrColors[color_item.new_color_tick_id]);
                arrTicks.push(s);
                color_item.new_color_tick_id++;

                // assign new stop
                color_rect.gradient.stops = arrTicks;
                colormap.sig_update();
            }

            onDoubleClicked: {
                // first of all, select new color!
                // and then, set response function
                colorDialog.visible = true;
                colorDialog.set_response_func(_response);
                var _x = mouse.x;

                function _response(_col){
                    create_stop(_x);
                }
            }
        }
    }
}

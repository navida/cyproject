import QtQuick 2.0
//import QtGraphicalEffects 1.0


Item {
  id: opacitymap
  width: 400
  height: 100

  // TODO well.. it's sloppy.. :(
  property real new_opacity_tick_id: 0
  //property var arrPositionsOp: { "0": 0, "1": 0.3, "2": 0.4, "3": 0.7, "4": 0.9, "5": 1.0 }
  property var arrPositionsOp: { "3": 1.0, "5": 0.3, "2": 0.2, "1": 0.7, "4": 0.1, "0": 0.0 }
  //property var arrOpacitys: { "0": 0.4, "1": 0.3, "2": 0.5, "3": 0.8, "4": 1.0, "5": 0.8 }
  property var arrOpacitys: { "0": 1.0, "1": 0.3, "2": 0.5, "3": 0.8, "4": 0, "5": 0.8 }

  property var arrObjTick: { "0":null }

  property var arrSortedKeys: []

  property real offset: 10
  property real size_tick: 16

  signal sig_update()

  // TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
  // Hex to RGB
  function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
  function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
  function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
  function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}


  function sort_by_value(_dict){
    // inner function for sort
    function sortNumber(a, b) {
      return a - b;
    }

    // inner function for sort
    function findKey(_dict, _value){
      for(var k in _dict){
        if(_dict[k] == _value){
          return k;
        }
      }
      return null;
    }

    var keys = Object.keys(_dict);
    var values = [];
    for(var i = 0; i < keys.length; i++) {
      values[values.length] = _dict[keys [i]];
      console.log(" 111111111111111 :: " + _dict[keys [i]])
    }

    var sortedValues = values.sort(sortNumber);
    console.log(" 222222222222222 :: " + sortedValues)

    var sortedKeys = []
    for(var k in sortedValues){
      sortedKeys.push(findKey(_dict, sortedValues[k]));
    }

    console.log(" 222222222222222 :: " + sortedKeys)

    return sortedKeys;
  }

  function init_opacitymap() {
    // first of all, sort by values
    arrSortedKeys = sort_by_value(arrPositionsOp);

    for(var p in arrSortedKeys){
      var _pos = arrPositionsOp[p] * opacitymap_canvas.width - (size_tick/2);
      var _op = opacitymap_canvas.height - (arrOpacitys[p] * opacitymap_canvas.height) - (size_tick/2);
      arrObjTick[arrSortedKeys[p].toString()] = opacitymap_canvas.create_opacity_tick(_pos, _op);
      new_opacity_tick_id++;
      console.log(" 33333333333333 :: " + _pos + " " + _op)
    }

    sig_update();
  }

  function insert_opacity_control(_pos, _opacity) {
    // asign pos, opacity to arr
    arrPositionsOp[new_opacity_tick_id.toString()] = _pos;
    arrOpacitys[new_opacity_tick_id.toString()] = _opacity;

    // first of all, sort by values
    arrSortedKeys = sort_by_value(arrPositionsOp);

    // for tick
    var pos = (_pos*opacity_mouse.width)-(size_tick/2);
    var opacity = opacitymap_canvas.height - (_opacity * opacitymap_canvas.height) - (size_tick/2);
    arrObjTick[new_opacity_tick_id.toString()] = opacitymap_canvas.create_opacity_tick(pos, opacity);
    new_opacity_tick_id++;

    sig_update();
  }

  function delete_opacity_control(_tick_id) {
    delete arrPositionsOp[_tick_id.toString()];
    delete arrOpacitys[_tick_id.toString()];

    arrSortedKeys = sort_by_value(arrPositionsOp);
    sig_update();
  }

  function update_opacitymap() {
    opacitymap_canvas.requestPaint();
  }

  Canvas {
    id: opacitymap_canvas
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width - opacitymap.offset
    height: parent.height

    onPaint: {
      var ctx = getContext("2d");
      ctx.resetTransform();
      //            ctx.fillStyle = Qt.rgba(255, 255, 255, 0.5);
//      ctx.fillStyle = "transparent"
//      ctx.fillRect(0, 0, width, height);
      ctx.strokeStyle = Qt.rgba(255, 255, 255, 1);

      ctx.reset()
      for(var i=1; i<opacitymap.arrSortedKeys.length; i++)
      {
        var prev_idx = opacitymap.arrSortedKeys[i-1];
        var cur_idx = opacitymap.arrSortedKeys[i];
        var x1 = arrPositionsOp[prev_idx] * width;
        var x2 = arrPositionsOp[cur_idx] * width;
        var y1 = height - (arrOpacitys[prev_idx] * height);
        var y2 = height - (arrOpacitys[cur_idx] * height);

        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
      }
    }

    function create_opacity_tick(_pos, _opacity) {
      var _str = opacity_mouse.get_tick_string(_pos, _opacity);
      return Qt.createQmlObject(_str, opacitymap_canvas, "opacity_tick");
    }

    Component.onCompleted: {
      // init !!!
      opacitymap.init_opacitymap();
      opacitymap.sig_update.connect(opacitymap.update_opacitymap);
    }

    MouseArea {
      id: opacity_mouse
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: parent.height

      function get_tick_string(_posX, _opacity){
        var str_tick =
            'import QtQuick 2.7;
Rectangle {
id: tick_' + opacitymap.new_opacity_tick_id + '\n' +
'width: opacitymap.size_tick
height: opacitymap.size_tick
x: ' + _posX + '\n' +
'y: ' + _opacity + '\n' +
'color: color_normal
border.color: "darkgray"
border.width: 4
radius: 15

property var tick_id: ' + opacitymap.new_opacity_tick_id + '\n
property var color_normal: "lightblue"
property var color_pressed: "red"

Component.onCompleted: {
}

MouseArea {
    anchors.fill: parent
    drag.target: parent
    drag.axis: Drag.XAndYAxis
    drag.minimumX: 0 - (width/2)
    drag.maximumX: opacitymap.width - opacitymap.offset - (width/2)
    drag.minimumY: 0 - (height/2)
    drag.maximumY: opacitymap.height - (height/2)
    acceptedButtons: Qt.LeftButton

    Text {
        id: text_of_tick
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.y - height
        color: "white"
        visible: false
    }

    onPositionChanged: {
        var newPos = (parent.x+width/2) / opacity_mouse.width;
        var newOpacity = (opacity_mouse.height - (parent.y+height/2)) / opacity_mouse.height;
        opacitymap.arrPositionsOp[tick_id.toString()] = newPos;
        opacitymap.arrOpacitys[tick_id.toString()] = newOpacity;
//        opacitymap.update_opacitymap();

        text_of_tick.text = (newOpacity.toFixed(3));

        if((parent.y+height/2) < parent.height*1.5){
            text_of_tick.y = height;
        }
        else{
            text_of_tick.y = height * -1;
        }

        opacitymap.arrSortedKeys = opacitymap.sort_by_value(arrPositionsOp);
        opacitymap.sig_update();
    }

    onPressed: {
        text_of_tick.visible = true;
        parent.color = color_pressed;
    }

    onReleased: {
        text_of_tick.visible = false;
        parent.color = color_normal;
    }

}
MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.RightButton
    onPressAndHold: {
        opacitymap.delete_opacity_control(parent.tick_id);
        parent.visible = false;
    }
}
}';

        return str_tick;

      }

      onDoubleClicked: {
        var _x = mouse.x / parent.width;
        var _op = (parent.height - mouse.y) / parent.height;
        opacitymap.insert_opacity_control(_x, _op);
      }
    }
  }
}

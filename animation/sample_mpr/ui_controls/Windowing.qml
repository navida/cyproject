import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
  id: windowing
  width: 410
  height: 30

  Column {
    width: parent.width
    height: parent.height

    LinearGradient {
      id: windowig_grad
      width: parent.width - ticks.tick_size
      height: parent.height - ticks.tick_size
      x: 5
      cached: false
      start: Qt.point(0, 0)
      end: Qt.point(width, 0)
      gradient: Gradient {
        GradientStop { id: grad_left; position: 0.0; color: "black" }
        GradientStop { id: grad_right; position: 1.0; color: "white" }
      }
    }

    Item {
      id: ticks
      width: parent.width
      height: tick_size
      property real tick_size: 10

      Rectangle {
        id: left
        width: parent.tick_size
        height: parent.tick_size
        color: "black"
        border.color: "gray"

        MouseArea {
          anchors.fill: parent
          drag.target: parent
          drag.axis: Drag.XAxis
          drag.minimumX: 0
          drag.maximumX: right.x - width
          onPositionChanged: {
            grad_left.position = left.x / windowig_grad.width
          }
        }
      }

      Rectangle {
        id: right
        width: parent.tick_size
        height: parent.tick_size
        x: parent.width-width
        color: "white"
        border.color: "gray"

        MouseArea {
          anchors.fill: parent
          drag.target: parent
          drag.axis: Drag.XAxis
          drag.minimumX: left.x + width
          drag.maximumX: windowing.width - width
          onPositionChanged: {
            grad_right.position = right.x / windowig_grad.width
          }
        }
      }
    }
  }
}

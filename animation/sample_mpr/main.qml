import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import cyhub 1.0


Item {
  id: win_root
  width: 1000
  height: 700

  Rectangle{
    id: bg
    anchors.fill: parent
    color: 'darkgray'
  }

  RowLayout {
    anchors.fill: parent
    implicitWidth: parent.width
    implicitHeight: parent.height
    width: parent.width
    height: parent.height
    spacing: 2

    ColumnLayout {
      Layout.fillWidth: true
      Layout.fillHeight: true
      spacing: 2

      GridLayout {
        implicitWidth: parent.width
        implicitHeight: parent.height - 100
        width: parent.width
        height: parent.height - 100
        columns: 2
        rows: 2

        ImageHolder {
          id: vtk_coronal
          objectName: name
          property var name: "vtk_coronal"
          property var prev_width: 0
          property var prev_height: 0
          Layout.alignment: Qt.AlignTop
          Layout.fillWidth: true
          Layout.fillHeight: true

          Rectangle {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 10
            width: 60
            height: 20
            color: 'black'
            opacity: 0.5
            radius: 15

            Text{
              color: "white"
              text: "SLICE : Coronal"
              anchors.fill: parent
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              font.pointSize: 12
            }
          }
        }

        ImageHolder {
          id: vtk_sagittal
          objectName: name
          property var name: "vtk_sagittal"
          property var prev_width: 0
          property var prev_height: 0
          Layout.alignment: Qt.AlignTop
          Layout.fillWidth: true
          Layout.fillHeight: true

          Rectangle {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 10
            width: 60
            height: 20
            color: 'black'
            opacity: 0.5
            radius: 15

            Text{
              color: "white"
              text: "SLICE : Sagittal"
              anchors.fill: parent
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              font.pointSize: 12
            }
          }
        }

        ImageHolder {
          id: vtk_axial
          objectName: name
          property var name: "vtk_axial"
          property var prev_width: 0
          property var prev_height: 0
          Layout.alignment: Qt.AlignTop
          Layout.fillWidth: true
          Layout.fillHeight: true

          Rectangle {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 10
            width: 60
            height: 20
            color: 'black'
            opacity: 0.5
            radius: 15

            Text{
              color: "white"
              text: "SLICE : Axial"
              anchors.fill: parent
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              font.pointSize: 12
            }
          }
        }

        ImageHolder {
          id: vtk_vol
          objectName: name
          property var name: "vtk_vol"
          property var prev_width: 0
          property var prev_height: 0
          Layout.alignment: Qt.AlignTop
          Layout.fillWidth: true
          Layout.fillHeight: true

          Rectangle {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 10
            width: 60
            height: 20
            color: 'black'
            opacity: 0.5
            radius: 15

            Text{
              color: "white"
              text: "VOL"
              anchors.fill: parent
              horizontalAlignment: Text.AlignHCenter
              verticalAlignment: Text.AlignVCenter
              font.pointSize: 12
            }
          }
        }
      }

      Rectangle{
        width: parent.width
        height: 100 - parent.spacing
        implicitWidth: width
        implicitHeight: height
        color: "black"

        Button{
          id:"btn"
          objectName: "btn"
          anchors.centerIn: parent
          text: "aa"

          signal btn_clicked
          onClicked: {
            this.btn_clicked()
            windowing_dialog.open()
          }
        }

        Windowing_Dialog{
          id:windowing_dialog
        }
      }
    }
  }
}

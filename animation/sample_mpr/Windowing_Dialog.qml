import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

import "ui_controls/"

Dialog{
  id: windowing_dialog
  objectName: "windowing_dialog"
  modality: Qt.ApplicationModal

  contentItem: Rectangle{
    width: 420
    height: 300
    color: "black"
    implicitWidth: width
    implicitHeight: height


    Column {
      width: parent.width - 10
      height: 280
      anchors.centerIn: parent
      spacing: 10

      Colormap {
        width: parent.width
        height: 20
      }

      Histogram {
        width: parent.width-10
        height: 200
        x: 5

        Opacitymap_ctrl {
          id: opacity_ctrl
          width: parent.width
          height: parent.height
        }
      }

      Windowing {
        width: parent.width
        height: 30
      }
    }
  }
}

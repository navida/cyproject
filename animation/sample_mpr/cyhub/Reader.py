import os
import vtk
import math
from PyQt5 import QtCore


try:
    import cyCafe

    class Reader(QtCore.QObject):
        def __init__(self, *args, **kwds):
            super().__init__(*args, **kwds)
            self.dcm_reader = cyCafe.cyDicomReader()

        # deprecated
        # def read(self, dcm):
        #     assert dcm
        #     self.is_cyCafe = True
        #     self.dcm_reader.read_dicom(dcm)
        #     self.vtk_image = self.dcm_reader.get_vtk_img()
        #     return self.vtk_image

        def release(self):
            self.dcm_reader.release_vtk_img()
            del self.dcm_reader

        def set_with_analyze(self, _with):
            self.dcm_reader.set_with_analyze(_with)

        def get_dcm_info(self, as_byte=False):
            _L = []
            self.dcm_reader.get_dcm_info(_L, as_byte)
            return _L

        # step 1
        def free_dicom_image(self):
            self.dcm_reader.free_dicom_image()

        # step 2
        def read_next(self, dcm_path):
            return self.dcm_reader.read_next(dcm_path)

        # step 3
        def generate_vtk_img(self):
            self.dcm_reader.generate_vtk_img()
            self.vtk_image = self.dcm_reader.get_vtk_img()

        # step 4
        def get_vtk_img(self):
            return self.vtk_image


except ImportError:
    class Reader(QtCore.QObject):
        pass
        # def __init__(self, *args, **kwds):
        #     super().__init__(*args, **kwds)
        #
        # def read(self, dcm):
        #     self.vtk_image = vtk.vtkDICOMImageReader()
        #     self.vtk_image.SetDirectoryName(dcm)
        #     self.vtk_image.Update()
        #
        # def release(self):
        #     pass


class cyReader(Reader):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.vtk_image = None
        self.dcm_path = None
        self.histo_array = None
        self.histo_range = None

        self.__determine_characterset = {'ISO_IR 6': 'ASCII',
                                         'ISO_IR 100':'ISO-8859-1', 'ISO_IR 101':'ISO-8859-2', 'ISO_IR 109':'ISO-8859-3',
                                         'ISO_IR 110':'ISO-8859-4', 'ISO_IR 144':'ISO-8859-5', 'ISO_IR 127':'ISO-8859-6',
                                         'ISO_IR 126':'ISO-8859-7', 'ISO_IR 138':'ISO-8859-8', 'ISO_IR 148':'ISO-8859-9',
                                         'ISO_IR 13':'Shift_JIS', 'ISO_IR 166':'TIS-620',
                                         'ISO_IR 192':'UTF-8',
                                         'GB18030':'GB18030', 'GBK':'GBK'}

    def clear(self):
        del self.dcm_reader
        del self.vtk_image
        del self.dcm_path
        del self.histo_array
        del self.histo_range
        self.vtk_image = None
        self.dcm_path = None
        self.histo_array = None
        self.histo_range = None

    def release(self):
        super().release()

    def read(self, dcm):
        assert dcm, 'dcm path is invalid!!'
        self.dcm_path = dcm
        if super().read(self.dcm_path):
            return self.vtk_image
        else:
            print('vtk_image is invalid.')
            return None

    def get_vtkimage(self):
        return self.vtk_image

    def get_range_of_volume(self):
        """
        :return: xMin, xMax, yMin, yMax, zMin, zMax
        """
        return self.vtk_image.GetExecutive().GetWholeExtent(self.vtk_image.GetOutputInformation(0))

    def get_spacing_of_volume(self):
        """
        :return: xSpacing, ySpacing, zSpacing
        """
        return self.vtk_image.GetOutput().GetSpacing()

    def get_origin_of_volume(self):
        """
        :return: OriginX, OriginY, OriginZ
        """
        return self.vtk_image.GetOutput().GetOrigin()

    def get_dimensions_of_volume(self):
        """
        :return: xDim, yDim, zDim
        """
        return self.vtk_image.GetOutput().GetDimensions()

    def calc_histogram(self):
        extract = vtk.vtkImageExtractComponents()
        extract.SetInputConnection(self.vtk_image.GetOutputPort())
        extract.SetComponents(0)
        extract.Update()

        histo_range = extract.GetOutput().GetScalarRange()
        total_count = int(math.fabs(histo_range[1] - histo_range[0]))

        histogram = vtk.vtkImageAccumulate()
        histogram.SetInputConnection(extract.GetOutputPort())
        histogram.SetComponentExtent(
            int(histo_range[0] + 1),
            int(histo_range[1]), 0, 0, 0, 0)
        # histogram.SetComponentOrigin( int(histoRange[0]),0,0 );
        histogram.SetComponentSpacing(1, 0, 0)
        histogram.SetIgnoreZero(0)
        histogram.Update()

        arr1 = []
        for i in range(0, total_count):
            val = int(i - math.fabs(histo_range[0]))
            arr1.append(histogram.GetOutput().GetPointData().GetScalars().GetTuple1(i))

        self.histo_array = arr1
        self.histo_range = histo_range

    def get_histogram(self):
        if self.histo_array is None or self.histo_range is None:
            self.calc_histogram()

        return self.histo_array, self.histo_range

    def get_reader_type(self):
        return 'cyCafe' if self.is_cyCafe else 'vtk'

    def determine_characterset(self, from_encoding):
        try:
            return self.__determine_characterset[from_encoding.split('\\')[-1]]
        except KeyError:
            return self.__determine_characterset['ISO_IR 6']

    def decode_bytes_to_string(self, _bytes, _charset):
        try:
            return _bytes.decode(self.determine_characterset(_charset.decode() if type(_charset) is bytes else _charset))
        except UnicodeDecodeError:
            # TODO
            return _bytes.decode('euc-kr')
import vtk
import numpy as np

from vtk.numpy_interface import dataset_adapter as dsa
from vtk.util import numpy_support

from PyQt5.QtCore import QObject, pyqtSignal


COLOR_NORMAL = (0/255, 255/255, 190/255)
COLOR_ACTIVE = (255/255, 47/255, 0/255)


class Arch(QObject):

    sigUpdatedArch = pyqtSignal(object)

    def __init__(self, renderer):
        super().__init__()

        self.ren = renderer

        self.markDict = dict()
        self.initializeSpline()
        self.pickedActor = vtk.vtkActor()

        self.current_mark = None
        self.new_id = 0

        _Iren = self.ren.GetRenderWindow().GetInteractor()
        _Iren.GetInteractorStyle().AddObserver('LeftButtonPressEvent', self.MousePressEvent)
        _Iren.GetInteractorStyle().AddObserver('LeftButtonReleaseEvent', self.MouseReleaseEvent)
        _Iren.GetInteractorStyle().AddObserver('MouseMoveEvent', self.MouseMoveEvent)

    def MousePressEvent(self, _rwi, _event=""):
        i = _rwi.GetInteractor()
        x, y = i.GetEventPosition()

        if _event == "LeftButtonPressEvent":
            self.get_actor(x, y)
            self.ren.GetRenderWindow().Render()
        elif _event == "RightButtonPressEvent":
            pass
            # self.prev_cam_pos = self.ren.GetActiveCamera().GetPosition()

    def MouseReleaseEvent(self, _rwi, _event=""):
        if _event == "LeftButtonReleaseEvent":
            if self.pickedActor in self.markDict:
                self.sigUpdatedArch.emit(False)
            self.release_actor()
        elif _event == "RightButtonReleaseEvent":
            pass

    def MouseMoveEvent(self, _rwi, _event=""):
        i = _rwi.GetInteractor()
        x, y = i.GetEventPosition()
        prev_x, prev_y = i.GetLastEventPosition()

        if self.pickedActor in self.markDict:
            self.ren.SetDisplayPoint(x, y, 1)
            self.ren.DisplayToWorld()
            pt = list(self.ren.GetWorldPoint())[0:3]
            pt[2] = self.markDict[self.pickedActor][3][2]
            self.modify_arch(pt)
            self.sigUpdatedArch.emit(True)

    def mark(self, pos_marker):
        mark_source = vtk.vtkSphereSource()
        mark_source.SetCenter(pos_marker[0], pos_marker[1], pos_marker[2])
        mark_source.SetRadius(2.5)
        mark_source.SetThetaResolution(36)
        mark_mapper = vtk.vtkPolyDataMapper2D()
        mark_mapper.SetInputConnection(mark_source.GetOutputPort())
        mark = vtk.vtkActor2D()
        mark.SetMapper(mark_mapper)
        mark.GetProperty().SetColor(COLOR_NORMAL)
        mark.GetProperty().SetOpacity(0.3)

        coordinate = vtk.vtkCoordinate()
        coordinate.SetCoordinateSystemToWorld()
        mark_mapper.SetTransformCoordinate(coordinate)

        return mark_source, mark_mapper, mark

    def draw_arch(self, pos):
        self.current_mark = [self.new_id] + list(self.mark(pos))
        self.markDict[self.current_mark[3]] = self.current_mark[0:3] + [pos]
        self.new_id += 1

        if len(self.markDict) > 1:
            self.make_polyLine()

        # call after make_polyline to place at top on the spline actor
        self.ren.AddActor(self.current_mark[3])

    def initializeSpline(self):
        self.points = vtk.vtkPoints()
        self.polyline = vtk.vtkPolyLine()
        self.cells = vtk.vtkCellArray()
        self.polyData = vtk.vtkPolyData()

        self.spline = vtk.vtkCardinalSpline()
        self.spline.SetLeftConstraint(2)
        self.spline.SetLeftValue(0.0)
        self.spline.SetRightConstraint(2)
        self.spline.SetRightValue(0.0)

        self.spline_filter = vtk.vtkSplineFilter()
        self.spline_filter.SetInputData(self.polyData)
        self.spline_filter.SetSpline(self.spline)
        self.polyDataMapper = vtk.vtkPolyDataMapper2D()
        self.polyDataMapper.SetInputConnection(self.spline_filter.GetOutputPort())
        self.polyDataActor = vtk.vtkActor2D()
        self.polyDataActor.SetMapper(self.polyDataMapper)

        coordinate = vtk.vtkCoordinate()
        coordinate.SetCoordinateSystemToWorld()
        self.polyDataMapper.SetTransformCoordinate(coordinate)

    def make_polyLine(self):
        if hasattr(self, '_tangent_vectors'):
            del self._tangent_vectors
        if hasattr(self, '_horizontal_vectors'):
            del self._horizontal_vectors
        if hasattr(self, '_spline_tangent_vectors'):
            del self._spline_tangent_vectors
        if hasattr(self, '_spline_horizontal_vectors'):
            del self._spline_horizontal_vectors

        P = self.get_point_data()

        self.points.Reset()
        self.polyData.Reset()
        for p in P:
            self.points.InsertNextPoint(p)

        self.polyline.GetPointIds().SetNumberOfIds(len(P))
        for i in range(len(P)):
            self.polyline.GetPointIds().SetId(i, i)
        self.cells.InsertNextCell(self.polyline)
        self.polyData.SetPoints(self.points)
        self.polyData.SetLines(self.cells)
        self.spline_filter.SetInputData(self.polyData)
        self.spline_filter.Update()
        self.polyDataActor.GetProperty().SetColor(COLOR_NORMAL)
        self.polyDataActor.GetProperty().SetLineWidth(2)
        self.ren.AddViewProp(self.polyDataActor)

    def set_number_of_subdivisions(self, number):
        self.spline_filter.SetSubdivideToSpecified()
        self.spline_filter.SetNumberOfSubdivisions(number)
        self.spline_filter.Update()
        self.spline_filter.Modified()

    def set_length_of_subdivisions(self, length):
        self.spline_filter.SetSubdivideToLength()
        self.spline_filter.SetLength(length)
        self.spline_filter.Update()
        self.spline_filter.Modified()

    def clear_all(self):
        for m in self.markDict.keys():
            self.ren.RemoveActor(m)
            del self.markDict[m][2]
            del self.markDict[m][1]
        self.markDict.clear()
        self.ren.RemoveActor(self.polyDataActor)
        self.new_id = 0

        if hasattr(self, '_tangent_vectors'):
            del self._tangent_vectors
        if hasattr(self, '_horizontal_vectors'):
            del self._horizontal_vectors
        if hasattr(self, '_spline_tangent_vectors'):
            del self._spline_tangent_vectors
        if hasattr(self, '_spline_horizontal_vectors'):
            del self._spline_horizontal_vectors

    def get_actor(self, x, y):
        picker = vtk.vtkPropPicker()
        picker.Pick(x, y, 0, self.ren)

        actor = picker.GetActor2D()
        if actor in self.markDict:
            self.pickedActor = actor
            self.pickedActor.GetProperty().SetColor(COLOR_ACTIVE)
            return True
        else:
            return False

    def release_actor(self):
        if self.pickedActor is not None:
            self.pickedActor.GetProperty().SetColor(COLOR_NORMAL)
            self.pickedActor = None

    def modify_arch(self, pos):
        id, mark_source, mark_mapper, pos_marker = self.markDict.get(self.pickedActor)
        mark_source.SetCenter(pos[0], pos[1], pos[2])
        mark_source.Update()
        mark_mapper.Update()
        self.markDict[self.pickedActor] = [id, mark_source, mark_mapper, np.asarray(pos)]
        self.make_polyLine()

    def extend(self, displacement):
        L = [[self.markDict[key][0], key] for key in self.markDict]
        L.sort()
        _P = [self.markDict[k][3] for _, k in L]
        _T, _H = self.get_tan_hor_vector()
        _NEW_P = np.add(_P, _H * displacement)

        for i, IK in enumerate(L):
            self.markDict[IK[1]][1].SetCenter(_NEW_P[i])
            self.markDict[IK[1]][3] = _NEW_P[i]
            self.markDict[IK[1]][1].Update()
            self.markDict[IK[1]][2].Update()
        self.make_polyLine()

    def set_visible(self, visible):
        for a in self.markDict:
            a.SetVisibility(visible)
        self.polyDataActor.SetVisibility(visible)

    def __calc_tangent_vector(self, _points):
        """
        calc tangent vector of polyline
        """
        # calculate tangent vector
        TANGET_V = []
        _prev_p = None
        _prev_prev_p = None
        for _i, _p in enumerate(_points):
            if _i == 1:
                _v = np.subtract(_p, _prev_p)
                _v = _v / np.linalg.norm(_v)
                TANGET_V.append(_v.tolist())
            elif _i == len(_points) - 1:
                _v = np.subtract(_p, _prev_prev_p)
                _v = _v / np.linalg.norm(_v)
                TANGET_V.append(_v.tolist())
                _v = np.subtract(_p, _prev_p)
                _v = _v / np.linalg.norm(_v)
                TANGET_V.append(_v.tolist())
            elif 1 < _i < len(_points) - 1:
                _v = np.subtract(_p, _prev_prev_p)
                _v = _v / np.linalg.norm(_v)
                TANGET_V.append(_v.tolist())

            _prev_prev_p = _prev_p
            _prev_p = _p

        return TANGET_V

    def get_point_data(self):
        L = [[self.markDict[key][0], key] for key in self.markDict]
        L.sort()
        P = [self.markDict[k][3] for _, k in L]
        return P

    def get_tan_hor_vector(self):
        if not hasattr(self, '_tangent_vectors'):
            self._tangent_vectors = self.__calc_tangent_vector(self.get_point_data())
        _T = self._tangent_vectors
        if not hasattr(self, '_horizontal_vectors'):
            self._horizontal_vectors = np.cross(_T, [0, 0, 1])
        _H = self._horizontal_vectors
        return _T, _H

    def get_spline_point_data(self):
        return numpy_support.vtk_to_numpy(self.spline_filter.GetOutput().GetPoints().GetData()).tolist()

    def get_spline_tan_hor_vector(self):
        if not hasattr(self, '_spline_tangent_vectors'):
            self._spline_tangent_vectors = self.__calc_tangent_vector(self.get_spline_point_data())
        _T = self._spline_tangent_vectors
        if not hasattr(self, '_spline_horizontal_vectors'):
            self._spline_horizontal_vectors = np.cross(_T, [0, 0, 1])
        _H = self._spline_horizontal_vectors
        return _T, _H

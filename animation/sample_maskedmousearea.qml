import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2
import Example 1.0


Rectangle {
  id: root
  width: 1000
  height: 400
  color: "black"


//  TODO!! Maskmousearea basic example
  Image {
    id: no1
    anchors.centerIn: parent
    scale: no1Area.pressed ? 1.1 : 1.0
    opacity: no1Area.containsMouse ? 1.0 : 0.7
    source: Qt.resolvedUrl("images/2.png")

    MaskedMouseArea {
      id: no1Area
      anchors.fill: parent
      alphaThreshold: 0.9
      maskSource: no1.source
    }

    Behavior on opacity {
      NumberAnimation { duration: 200 }
    }
    Behavior on scale {
      NumberAnimation { duration: 100 }
    }
  }


////  TODO!! Universal numbering!
//  RowLayout{
//    anchors{
//      horizontalCenter: parent.horizontalCenter
//      top: parent.top
//      topMargin: 10
//    }

//    Repeater {
//      model: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"]

//      Image {
//        objectName: _id
//        property string _id: "teeth_" + modelData
//        scale: children[0].pressed ? 1.1 : 1.0
//        opacity: children[0].containsMouse ? 1.0 : 0.7
//        source: Qt.resolvedUrl("images/" + modelData + ".png")

//        MaskedMouseArea {
//          anchors.fill: parent
//          alphaThreshold: 0.9
//          maskSource: parent.source
//          onPressedChanged: {
//            if (pressed === true){
//              console.log("pressed" + parent.objectName)
//            }else{
//              console.log("released" + parent.objectName)
//            }
//          }
//        }

//        Behavior on opacity {
//            NumberAnimation { duration: 200 }
//        }
//        Behavior on scale {
//            NumberAnimation { duration: 100 }
//        }
//      }
//      Rectangle {
//        anchors{
//          horizontalCenter: parent.horizontalCenter
//          verticalCenter: parent.verticalCenter
//        }
//        width: 15
//        height: 15
//        color: 'black'
//        opacity: 0.4
//        radius: 4

//        Label{
//          text: modelData
//          anchors{
//            horizontalCenter: parent.horizontalCenter
//            verticalCenter: parent.verticalCenter
//          }
//          color: "white"
//        }
//      }
//    }
//  }

//  RowLayout{
//    anchors{
//      horizontalCenter: parent.horizontalCenter
//      bottom: parent.bottom
//      bottomMargin: 10
//    }

//    Repeater {
//      model: ["18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]

//      Image {
//        objectName: _id
//        property string _id: "teeth_" + modelData
//        scale: children[0].pressed ? 1.1 : 1.0
//        opacity: children[0].containsMouse ? 1.0 : 0.7
//        source: Qt.resolvedUrl("images/" + modelData + ".png")

//        MaskedMouseArea {
//          anchors.fill: parent
//          alphaThreshold: 0.9
//          maskSource: parent.source
//          onPressedChanged: {
//            if (pressed === true){
//              console.log("pressed" + parent.objectName)
//            }else{
//              console.log("released" + parent.objectName)
//            }
//          }
//        }

//        Behavior on opacity {
//            NumberAnimation { duration: 200 }
//        }
//        Behavior on scale {
//            NumberAnimation { duration: 100 }
//        }
//      }
//      Rectangle {
//        anchors{
//          horizontalCenter: parent.horizontalCenter
//          verticalCenter: parent.verticalCenter
//        }
//        width: 15
//        height: 15
//        color: 'black'
//        opacity: 0.4
//        radius: 4

//        Label{
//          text: modelData
//          anchors{
//            horizontalCenter: parent.horizontalCenter
//            verticalCenter: parent.verticalCenter
//          }
//          color: "white"
//        }
//      }
//    }
//  }
//}


////  TODO!! Universal FDI convert
//  Button{
//    id: btn1
//    text: "btn"
//    onClicked:{
//      defalut_dental_numbering = "universal"
//    }
//  }
//  Button{
//    id: btn2
//    anchors.left: btn1.right
//    text: "btn"
//    onClicked:{
//      defalut_dental_numbering = "FDI"
//    }
//  }

//  property string defalut_dental_numbering: "universal"

//  function get_defalut_dental_numbering(){
//    return defalut_dental_numbering
//  }

//  function numbering_convert_func(s){
//    var value = Number(s)
//    if(value <= 8){
//      value = 19 - value
//      return String(value)
//    }else if (value > 8 && value <= 15){
//      value += 12
//      return String(value)
//    }else if (value >= 18 && value < 25){
//      value = 55 - value
//      return String(value)
//    }else if (value >= 25 && value <= 31){
//      value += 16
//      return String(value)
//    }
//  }

//  Rectangle {
//    width: parent.width
//    height: 50
//    anchors{
//      horizontalCenter: parent.horizontalCenter
//      top: parent.top
//      topMargin: 10
//    }

//    color: "transparent"
//    RowLayout{
//      spacing: 7
//      anchors.centerIn: parent

//      Repeater {
//        model: ["2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"]

//        Rectangle{
//          id: maxilla_masked_image1
//          width: 50
//          height: 50
//          color: "transparent"

//          Image {
//            anchors{
//              horizontalCenter: parent.horizontalCenter
//              verticalCenter: parent.verticalCenter
//            }

//            objectName: _id
//            property string _id: "teeth_" + maxilla_label.text
//            opacity: checked_state ? 1.0 : 0.7
//            source: Qt.resolvedUrl("images/" + modelData + ".png")

//            property bool checked_state: false

//            MaskedMouseArea {
//              anchors.fill: parent
//              alphaThreshold: 0.9
//              maskSource: parent.source
//              onPressedChanged: {
//                if (pressed === false){
//                  if(parent.checked_state){
//                    parent.checked_state = false
//                    console.log(parent.objectName)
//                  }else{
//                    parent.checked_state = true
//                    console.log(parent.objectName)
//                  }
//                }
//              }
//            }

//            onChecked_stateChanged: {
//            }

//            Behavior on opacity {
//              NumberAnimation { duration: 200 }
//            }
//          }

//          Rectangle {
//            anchors{
//              horizontalCenter: parent.horizontalCenter
//              verticalCenter: parent.verticalCenter
//            }
//            width: 15
//            height: 15
//            color: 'black'
//            opacity: 0.4
//            radius: 4

//            Label{
//              id: maxilla_label
//              text: get_defalut_dental_numbering() == "universal" ? modelData : root.numbering_convert_func(modelData)
//              anchors{
//                horizontalCenter: parent.horizontalCenter
//                verticalCenter: parent.verticalCenter
//              }
//              color: "white"
//            }
//          }
//        }
//      }
//    }
//  }

//  Rectangle {
//    width: parent.width
//    height: 50
//    anchors{
//      horizontalCenter: parent.horizontalCenter
//      bottom: parent.bottom
//      bottomMargin: 10
//    }

//    color: "transparent"
//    RowLayout{
//      spacing: 7
//      anchors.centerIn: parent

//      Repeater {
//        model: ['31', '30', '29', '28', '27', '26', '25', '24', '23', '22', '21', '20', '19', '18']

//        Rectangle{
//          id: maxilla_masked_image2
//          width: 50
//          height: 50
//          color: "transparent"

//          Image {
//            anchors{
//              horizontalCenter: parent.horizontalCenter
//              verticalCenter: parent.verticalCenter
//            }

//            objectName: _id
//            property string _id: "teeth_" + mandible_label.text
//            opacity: checked_state ? 1.0 : 0.7
//            source: Qt.resolvedUrl("images/" + modelData + ".png")

//            property bool checked_state: false

//            MaskedMouseArea {
//              anchors.fill: parent
//              alphaThreshold: 0.9
//              maskSource: parent.source
//              onPressedChanged: {
//                if (pressed === false){
//                  if(parent.checked_state){
//                    parent.checked_state = false
//                    console.log(parent.objectName)
//                  }else{
//                    parent.checked_state = true
//                    console.log(parent.objectName)
//                  }
//                }
//              }
//            }

//            onChecked_stateChanged: {
//            }

//            Behavior on opacity {
//              NumberAnimation { duration: 200 }
//            }
//          }

//          Rectangle {
//            anchors{
//              horizontalCenter: parent.horizontalCenter
//              verticalCenter: parent.verticalCenter
//            }
//            width: 15
//            height: 15
//            color: 'black'
//            opacity: 0.4
//            radius: 4

//            Label{
//              id: mandible_label
//              text: get_defalut_dental_numbering() == "universal" ? modelData : root.numbering_convert_func(modelData)
//              anchors{
//                horizontalCenter: parent.horizontalCenter
//                verticalCenter: parent.verticalCenter
//              }
//              color: "white"
//            }
//          }
//        }
//      }
//    }
//  }
}



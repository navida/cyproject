#include <QGuiApplication>
#include <QQuickView>
#include <stdio.h>

#include "maskedmousearea.h"


int main(int argc, char* argv[])
{
    QGuiApplication app(argc,argv);
    QQuickView view;

    std::cout << tr("Name") << std::endl;

    qmlRegisterType<MaskedMouseArea>("Example", 1, 0, "MaskedMouseArea");

    view.setSource(QUrl("qrc:///customitems/maskedmousearea/maskedmousearea.qml"));
    view.show();
    return app.exec();
}

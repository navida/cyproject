import QtQuick 2.0

Rectangle {
  width: 75; height: 75
  id: button
  state: "RELEASED"

  MouseArea {
    anchors.fill: parent
    onPressed: button.state = "PRESSED"
    onReleased: button.state = "RELEASED"
  }

  states: [
    State {
      name: "PRESSED"
      PropertyChanges { target: button; color: "white"}
    },
    State {
      name: "RELEASED"
      PropertyChanges { target: button; color: "black"}
    }
  ]

  transitions: [
    Transition {
      from: "PRESSED"
      to: "RELEASED"
      ColorAnimation { target: button; duration: 1000}
    },
    Transition {
      from: "RELEASED"
      to: "PRESSED"
      ColorAnimation { target: button; duration: 1000}
    }
  ]
}

from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
import numpy as np
from vtk.numpy_interface import dataset_adapter as dsa
from vtk.util import numpy_support
from Reader import cyReader
import sys,os

import _qapp

class MainWindow(QQuickView):
    def __init__(self, app, *args, **kwds):
        super().__init__(*args, **kwds)
        self.vtk_img = None

        self.reader = cyReader()
        self.reader.free_dicom_image()

        dir_str = "/Users/jeongjihong/dicom/test_dicom/dicom/S0000000013/"
        file_list = os.listdir(dir_str)
        for f in file_list[::10]:
            f_path = dir_str + f
            self.reader.read_next(f_path)
        self.reader.generate_vtk_img()
        self.vtk_img = self.reader.get_vtk_img()

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self.init_sig_slot()

    def init_sig_slot(self):
        buf_volume = numpy_support.vtk_to_numpy(self.vtk_img.GetPointData().GetArray(0))
        F, B = np.histogram(buf_volume, 100)
        _max = np.max(buf_volume)
        _min = np.min(buf_volume)

        H = F.tolist()
        _minX = 0
        _maxX = len(H)
        _minY = float(min(H))
        _maxY = float(max(H))
        item = self.rootObject().findChild(QObject, 'histogram_item')
        item.set_datas(H, _minX, _maxX, _minY, _maxY)

    # @pyqtSlot()
    # def btn_kr_clicked(self):
    #     self.app.installTranslator(self.translator)
    #
    # @pyqtSlot()
    # def btn_en_clicked(self):
    #     self.app.removeTranslator(self.translator)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow(app)

    app.exec()

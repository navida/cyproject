import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

import "qml/"


Item{
  width: 1200
  height: 500

  Column {
    width: 400
    height: 390
    spacing: 10

    Colormap {
      width: parent.width
      height: 20
    }

    Histogram {
      width: parent.width
      height: 200

      Opacitymap_ctrl {
        id: opacity_ctrl
        width: parent.width
        height: parent.height
      }
    }

    Windowing {
      width: 535
      height: 30
    }
  }
}

import QtQuick 2.0

Item {
  id: colormap
  width: 420
  height: 20

  function onUpdated() {
  }

  Column {
    width: parent.width
    height: parent.height
    spacing: 10

//    Opacitymap_ctrl {
//      id: opacity_ctrl
//      width: parent.width
//      height: parent.height - (color_ctrl.height + parent.spacing)
//    }

    Colormap_ctrl {
      id: color_ctrl
      width: parent.width
      height: 20
    }
  }

  Component.onCompleted: {
    color_ctrl.sig_update.connect(colormap.onUpdated);
  }
}

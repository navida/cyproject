import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

import "../../style"

import QtCharts 2.0

Dialog{
  id: filter_dialog
  objectName: "filter_dialog"
  modality: Qt.ApplicationModal

  contentItem: Rectangle{
    width: 300
    height: 200
    color: CyStyle.dbmwindow.db_connect_rect_bg_color
    implicitWidth: width
    implicitHeight: height

    RowLayout{
      anchors.fill: parent
      spacing: 0

      ListView {
        id: filter_type_listview
        width: 100
        height: parent.height
        highlightFollowsCurrentItem: true
        clip: true

        currentIndex: 0

        model:ListModel {
          ListElement {
            no: 0
            type: "Unsharpen"
            param1: 1
            param2: 0.5
            param3: 0
          }
          ListElement {
            no: 1
            type: "Highboost"
            param1: 1
            param2: 0.5
            param3: 0
          }
          ListElement {
            no: 2
            type: "Anisotropic"
            param1: 1
            param2: 0.5
            param3: 0
          }
          ListElement {
            no: 3
            type: "Bilateral"
            param1: 1
            param2: 0.5
            param3: 0
          }
        }

        delegate: Component {
          Rectangle{
            width: parent.width
            height: 35
            color: ListView.isCurrentItem ? CyStyle.dbmwindow.treeview_select_bg_color : CyStyle.dbmwindow.db_connect_rect_bg_color

            Rectangle{
              width: parent.width
              height: 1
              anchors.top: parent.top
              color: "#282A27"
            }

            Label{
              anchors{
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 10
              }
              text: type
              color: CyStyle.dbmwindow.common_font_color
              font.pointSize: 12
            }

            Rectangle{
              width: parent.width
              height: 1
              anchors.bottom: parent.bottom
              color: "#282A27"
            }

            MouseArea {
              anchors.fill: parent
              onClicked: filter_type_listview.currentIndex = index
            }
          }
        }

        onCurrentIndexChanged: {
        }
      }

      Rectangle{
        width: 1
        height: parent.height
        color:"black"
      }

      Rectangle{
        Layout.fillWidth: true
        Layout.fillHeight: true
        color:"transparent"

        ColumnLayout{
          anchors.centerIn: parent
          spacing: 10
//          anchors.fill: parent

          RowLayout{
            spacing: 10
            Rectangle{
              width:60
              height:40
              color: "transparent"

              Label{
                text: "Param1"
                anchors.centerIn: parent
                font.pointSize: CyStyle.dentaldummy._tools_title_label_size
                color: CyStyle.dentaldummy._tools_title_label_color
              }
            }
            Rectangle{
              width:60
              height:40
              color: "transparent"

              TextField{
                anchors.centerIn: parent
                width: 50
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                maximumLength: 4
                validator: DoubleValidator {
                  bottom: 0
                  top: 1
                  notation: DoubleValidator.StandardNotation
                }
                text : "0"
              }
            }
          }

          RowLayout{
            spacing: 10
            Rectangle{
              width:60
              height:40
              color: "transparent"

              Label{
                text: "Param2"
                anchors.centerIn: parent
                font.pointSize: CyStyle.dentaldummy._tools_title_label_size
                color: CyStyle.dentaldummy._tools_title_label_color
              }
            }
            Rectangle{
              width:60
              height:40
              color: "transparent"

              TextField{
                anchors.centerIn: parent
                width: 50
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                maximumLength: 4
                validator: DoubleValidator {
                  bottom: 0
                  top: 1
                  notation: DoubleValidator.StandardNotation
                }
                text : "0"
              }
            }
          }

          RowLayout{
            spacing: 10
            Rectangle{
              width:60
              height:40
              color: "transparent"

              Label{
                text: "Param3"
                anchors.centerIn: parent
                font.pointSize: CyStyle.dentaldummy._tools_title_label_size
                color: CyStyle.dentaldummy._tools_title_label_color
              }
            }
            Rectangle{
              width:60
              height:40
              color: "transparent"

              TextField{
                anchors.centerIn: parent
                width: 50
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                maximumLength: 4
                validator: DoubleValidator {
                  bottom: 0
                  top: 1
                  notation: DoubleValidator.StandardNotation
                }
                text : "0"
              }
            }
          }
        }
      }
    }
  }
}



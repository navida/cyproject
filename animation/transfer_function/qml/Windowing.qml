import QtQuick 2.7
import QtGraphicalEffects 1.0

Item {
  id: windowing
  objectName : "windowing"
  width: 400
  height: 30

  signal sig_changed(var _l, var _r, var _d)

//  Column {
//    width: parent.width
//    height: parent.height

//    LinearGradient {
//      id: windowig_grad
//      width: parent.width
//      height: parent.height
//      x: 5
//      cached: false
//      start: Qt.point(0, 0)
//      end: Qt.point(width, 0)
//      gradient: Gradient {
//        GradientStop { id: grad_left; position: 0.0; color: "black" }
//        GradientStop { id: grad_right; position: 1.0; color: "blue" }
//      }
//    }

//    Item {
//      id: ticks
//      width: parent.width
//      height: tick_size
//      property real tick_size: 10
//      property var left_right_distance : right.x - left.x

//      Component.onCompleted: {

//      }

//      Rectangle {
//        id: left
//        visible: false
//        width: parent.tick_size
//        height: parent.tick_size
//        color: "black"
//        border.color: "grey"

//        MouseArea {
//          id: left_mouse
//          anchors.fill: parent
//          drag.target: parent
//          drag.axis: Drag.XAxis
//          drag.minimumX: 0
//          drag.maximumX: right.x - width
//          onPositionChanged: {
//            grad_left.position = left.x / windowig_grad.width
//          }
//        }
//      }

//      Rectangle {
//        id: middle
////        width: ticks.left_right_distance - (ticks.tick_size)
//        width: ticks.left_right_distance
//        height: ticks.tick_size
//        x: left.x + (ticks.tick_size/2)
//        z: -1
//        color: "red"
//        border.color: "gray"

//        MouseArea {
//          anchors.fill: parent
//          drag.target: parent
//          drag.axis: Drag.XAxis
//          drag.minimumX: left_mouse.drag.minimumX  + ticks.tick_size
//          drag.maximumX: right_mouse.drag.maximumX - middle.width
//          onPositionChanged: {
//            var temp_left_position = middle.x / (windowig_grad.width - ticks.tick_size/2)
//            var temp_right_position = (middle.x + middle.width) / (windowig_grad.width - ticks.tick_size/2)
//            console.log(temp_left_position)
//            console.log(temp_right_position)
//          }
//          Component.onCompleted: {
//            console.log(windowig_grad.width)
//            console.log(ticks.left_right_distance)
//            console.log(drag.minimumX)
//            console.log(drag.maximumX)
//          }
//        }
//      }

//      Rectangle {
//        id: right
//        visible: false
//        width: parent.tick_size
//        height: parent.tick_size
//        x: parent.width
//        color: "white"
//        border.color: "grey"

//        MouseArea {
//          id: right_mouse
//          anchors.fill: parent
//          drag.target: parent
//          drag.axis: Drag.XAxis
//          drag.minimumX: left.x + width
//          drag.maximumX: windowing.width - width
//          onPositionChanged: {
//            grad_right.position = right.x / windowig_grad.width
//          }
//        }
//      }
//    }
//  }
  Column {
    width: parent.width
    height: parent.height

    LinearGradient {
      id: windowig_grad
      width: parent.width
      height: parent.height
      x: 5
      cached: true
      start: Qt.point(0, 0)
      end: Qt.point(width, 0)
      gradient: Gradient {
        GradientStop { id: grad_left; position: 0.0; color: "black" }
        GradientStop { id: grad_right; position: 1.0; color: "blue" }
      }
    }

    Item {
      id: ticks
      width: parent.width
      height: tick_size
      property real tick_size: 10
      property var left_right_distance : right.x - left.x

      signal sig_update()

      Component.onCompleted: {
//        ticks.sig_update.connect(windowing.sig_changed_position)
      }

      Canvas {
        id: left
        antialiasing: true
        width: parent.tick_size
        height: parent.tick_size
        onPaint: {
          var ctx = getContext("2d");
          ctx.strokeStyle = "gray"
          ctx.fillStyle = "black";
          ctx.beginPath();
          ctx.moveTo(0, 10);
          ctx.lineTo(width/2, 0);
          ctx.lineTo(width, height);
          ctx.closePath();
          ctx.fill();
          ctx.stroke();
        }
        MouseArea {
          id: left_mouse
          anchors.fill: parent
          drag.target: parent
          drag.axis: Drag.XAxis
          drag.minimumX: 0
          drag.maximumX: right.x - width
          drag.threshold: 3
          onPositionChanged: {
            grad_left.position = left.x / windowig_grad.width
//            ticks.sig_update()
          }
        }
      }

      Rectangle {
        id: middle
        width: ticks.left_right_distance
        height: ticks.tick_size
        x: left.x + (ticks.tick_size/2)
        z: -1
        color: "grey"
        opacity: middle_mouse.containsMouse ? 0.2 : 0

        MouseArea {
          id: middle_mouse
          anchors.fill: parent
          drag.target: parent
          drag.axis: Drag.XAxis
          drag.minimumX: left_mouse.drag.minimumX  + (ticks.tick_size / 2)
          drag.maximumX: right_mouse.drag.maximumX - middle.width + (ticks.tick_size / 2)
          hoverEnabled: true
          preventStealing: true

          onPositionChanged: {
            console.log("aa")
            var temp_left_position = (middle.x - ticks.tick_size / 2) / windowig_grad.width
            var temp_right_position = ((middle.x - ticks.tick_size / 2) + middle.width) / windowig_grad.width

            grad_left.position = temp_left_position
            left.x = grad_left.position * windowig_grad.width
            grad_right.position = temp_right_position
            right.x = grad_right.position * windowig_grad.width

//            if(containsMouse){
//              middle_mouse.cursorShape = Qt.ClosedHandCursor
//            }else{
//              middle_mouse.cursorShape = Qt.OpenHandCursor
//            }
          }
        }
      }

      Canvas {
        id: right
        antialiasing: true
        width: parent.tick_size
        height: parent.tick_size
        x: parent.width
        onPaint: {
          var ctx = getContext("2d");
          ctx.strokeStyle = "gray"
          ctx.fillStyle = "white";
          ctx.beginPath();
          ctx.moveTo(0, 10);
          ctx.lineTo(width/2, 0);
          ctx.lineTo(width, height);
          ctx.closePath();
          ctx.fill();
          ctx.stroke();
        }
        MouseArea {
          id: right_mouse
          anchors.fill: parent
          drag.target: parent
          drag.axis: Drag.XAxis
          drag.minimumX: left.x + width
          drag.maximumX: windowing.width
          drag.threshold: 3
          onPositionChanged: {
            grad_right.position = right.x / windowig_grad.width
//            ticks.sig_update()
          }
        }
      }
    }
  }
}

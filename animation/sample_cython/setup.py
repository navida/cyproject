from distutils.core import setup, Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize

ext_modules = [
    Extension('cy_sample', ['cy_sample.pyx'],
            language="c++",
            )
]
setup(
  name='cy_cython',
  cmdclass={'build_ext': build_ext},
  ext_modules=cythonize(ext_modules),
)

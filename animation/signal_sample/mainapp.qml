import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: 500
  height: 500

  Rectangle{
    width: 300
    height: 300
    anchors.centerIn: parent
    MouseArea{
      anchors.fill: parent
      onClicked: {

      }
    }
  }

  Column{
    id: col_axial_thickness
    objectName: "axial_thickness"
    width: 100
    height: 100

    signal sigChanged(real val)

    visible: true

    function reset(){
      cb_axial_thickness.currentIndex = 0;
    }

    anchors{
      top: parent.top
      topMargin: 10
      right: parent.right
      rightMargin: 10
    }

    Rectangle{
      id: rect_axial_thickness_title
      width: 80
      height: 25
      color: "black"

      Text{
        id: text_axial_thickness
        objectName: "text_axial_thickness"
        text: "TH: " + cb_axial_thickness.currentText + " mm"
        color: cb_axial_thickness.visible ? "orange" : "white"
        anchors.centerIn: parent
      }

      MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
          if(!cb_axial_thickness.visible)
            cb_axial_thickness.visible = true
          else
            cb_axial_thickness.visible = false
        }
        onContainsMouseChanged: {
          if(containsMouse)
            cursorShape = Qt.PointingHandCursor
          else
            return
        }
      }
    }

    ListModel {
      id: axial_cbItems

      property double maximumValue: 30.0
      property double minimumValue: 0.0

      ListElement { val: "0.0" }
      ListElement { val: "0.5" }
      ListElement { val: "1.0" }
      ListElement { val: "2.0" }
      ListElement { val: "3.0" }
      ListElement { val: "4.0" }
      ListElement { val: "5.0" }
      ListElement { val: "10.0" }
      ListElement { val: "20.0" }
    }

    ComboBox {
      id: cb_axial_thickness
      objectName: "cb_axial_thickness"
      width: 80
      editable: true
      visible: false
      focus: visible

      signal sig_changed

      inputMethodHints: Qt.ImhFormattedNumbersOnly
      validator: DoubleValidator {
        objectName:"cb_dv"
        bottom: axial_cbItems.minimumValue
        top: axial_cbItems.maximumValue
        notation: DoubleValidator.StandardNotation
        decimals: 1
      }
      model: axial_cbItems

      function set_edittext(_str){
        cb_axial_thickness.editText = _str

        accepted()
      }

      onCurrentIndexChanged: {
        if(currentText != ""){
//          col_axial_thickness.sigChanged(parseFloat(currentText));
          this.visible = false
        }
      }

      onAccepted: {
//        col_axial_thickness.sigChanged(parseFloat(currentText));
        console.log("11")
        cb_axial_thickness.sig_changed()
        this.visible = false
        console.log("22")
      }

      Keys.onReturnPressed: {
        accepted()
      }

      onEditTextChanged: {
        if(editText.length >= 2 && editText[0] == "0" && editText[1] != ".")
          editText = "0"
        if(parseInt(editText) > axial_cbItems.maximumValue)
          editText = "30"
        if(editText[0] == "3" && editText[1] == "0" && editText[2] == ".")
          editText = "30"
      }
      // Temp Source!!!!!!! - by jhjeong
    }
  }
}

from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtGui import QGuiApplication
from PyQt5.Qt import QQmlApplicationEngine
import sys,os
import _qapp

class MainWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self._cb = None
        self.init_sig_slot()

    def init_sig_slot(self):
        self._cb = self.findChild(QObject, "cb_axial_thickness")
        self._cb.set_edittext("22")

        self._cb.sig_changed.connect(self.sig_changed)

    def sig_changed(self):
        print("aa")

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    window = MainWindow()

    app.exec()



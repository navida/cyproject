import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

Rectangle {
    width: 200
    height: 200
    color: "red"
//    focus 필수
    focus: true

    Keys.onPressed: {
        console.log("Bar");
    }
}
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2
import QtQml 2.0
import "."

Item{
  width: 600
  height: 400

  Rectangle{
    id: root
    anchors.fill: parent
    color: "black"

    Column{
      id: col_thickness
      objectName: "col_thickness"

      visible: true

      anchors{
        top: parent.top
        topMargin: 25
        right: parent.right
        rightMargin: 10
      }

      Rectangle{
        id: rect_thickness_title
        width: 80
        height: 25
        color: "red"

        Text{
          id: text_thickness
          text: "TH: " + cb_thickness.currentText + " mm"
          color: cb_thickness.visible ? "orange" : "white"
          anchors.centerIn: parent
        }

        MouseArea{
          anchors.fill: parent
          hoverEnabled: true

          onClicked: {
            if(!cb_thickness.visible)
              cb_thickness.visible = true
            else
              cb_thickness.visible = false

            dialog.open()
          }
          onContainsMouseChanged: {
            if(containsMouse)
              cursorShape = Qt.PointingHandCursor
            else
              return
          }
        }
      }

      ListModel {
        id: cbItems

        property double maximumValue: 30.0
        property double minimumValue: 0.0

        ListElement { val: "0.0" }
        ListElement { val: "0.5" }
        ListElement { val: "1.0" }
        ListElement { val: "2.0" }
        ListElement { val: "3.0" }
        ListElement { val: "4.0" }
        ListElement { val: "5.0" }
        ListElement { val: "10.0" }
        ListElement { val: "20.0" }
      }

      ComboBox {
        id: cb_thickness
        width: 80
        editable: true
        visible: false
        focus: visible

        inputMethodHints: Qt.ImhFormattedNumbersOnly
        validator: DoubleValidator {
          bottom: cbItems.minimumValue
          top: cbItems.maximumValue
          notation: DoubleValidator.StandardNotation
          decimals: 1
        }
        model: cbItems

        onCurrentIndexChanged: {
          if(currentText != ""){
            console.log(parseFloat(currentText))
            this.visible = false
          }
        }

        onAccepted: {
          console.log(parseFloat(currentText))
          this.visible = false
        }

        onEditTextChanged: {
          if(editText.length >= 2 && editText[0] == "0" && editText[1] != ".")
            editText = "0"
          if(parseInt(editText) > cbItems.maximumValue)
            editText = "30"
          if(editText[0] == "3" && editText[1] == "0" && editText[2] == ".")
            editText = "30"
        }
      }
    }
  }
}



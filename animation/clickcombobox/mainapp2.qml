import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQml.Models 2.2
import QtQml 2.0

Item{
  width: 600
  height: 400

  Rectangle{
    id: root
    anchors.fill: parent
    color: "black"

    property var a: [1,2,3,4]
    property var arrPositionsCol: { "0": 0.0, "1": 1.0, "2": 0.0, "3": 1.0 }
    property var aa

    LinearGradient {
      id: grad
      width: 300
      height: 30
      anchors.horizontalCenter: parent.horizontalCenter
      start: Qt.point(0, 0)
      end: Qt.point(width, 0)
      gradient: Gradient {
        GradientStop { position: 0.0; color: "white" }
        GradientStop { position: 0.3; color: "blue" }
        GradientStop { position: 0.7; color: "red" }
        GradientStop { position: 1.0; color: "black" }
      }
    }

    Button{
      text: "aaa"

      onClicked: {
//        parent.aa = this.states
//        console.log(parent.aa[0])
//        for(var i = 0; i < this.aa.length; i++){
//          console.log(this.states[i].name)
//        }
//        console.log(this.states[1].name)
//        this.states[1].destroy()
//        delete this.states[1]
//        this.states.remove(1)
//        console.log(this.states[1].name)
//        for(var i = 0; i < this.states.length; i++){
//          console.log(this.states[i].name)
//        }
        dialog.open()
      }
      Component.onCompleted: {
//        root.aa = [grad.gradient.stops[0], grad.gradient.stops[1]]
//        grad.gradient.stops = []
//        console.log(grad.gradient.stops.length)
//        grad.gradient.stops = root.aa
//        console.log(grad.gradient.stops.length)
//        console.log(grad.gradient.stops[0])
//        console.log(grad.gradient.stops[1])
//        console.log(grad.gradient.stops[2])
//        root.children[0].destroy()
//        delete root.children[0]
//        console.log(root.children[0])
//        console.log(root.children.length)
//        var test
//        for(var v in root.arrPositionsCol){
//          if(v == "3"){
//            delete root.arrPositionsCol[v]
//            console.log(root.arrPositionsCol[0])
//            console.log(root.arrPositionsCol[1])
//            console.log(root.arrPositionsCol[2])
//            console.log(root.arrPositionsCol[3])
//          }
//        }
        console.log(Object.keys(root.arrPositionsCol).indexOf("3"))
      }
    }

    ComboBox{
      id: cbbox
      anchors.centerIn: parent
      width: 200

      property var h_index : 0
      property var filter_type : ["Off", "Banana", "Apple", "Coconut"]
      property var history : []

      model: ListModel {
        id: cbItems
        ListElement { text: "Off"; _index: 0}
        ListElement { text: "Banana"; _index: 0}
        ListElement { text: "Apple"; _index: 0}
        ListElement { text: "Coconut"; _index: 0}
      }

      style: ComboBoxStyle {
        id: comboBox
      }

      onCurrentIndexChanged: {
        if(cbItems.get(currentIndex).text != "Off"){
          this.history.push(currentIndex)
          this.h_index++
          if(h_index == 4)
            h_index = 1

          cbItems.get(currentIndex)._index = this.h_index
          var s_str = cbItems.get(currentIndex).text.split(" ")

          if(s_str.length >= 2){
            var c_index = currentIndex

            for(var i = 1; i < cbItems.count; i++){
              if(cbItems.get(i)._index == 0){
                continue;
              }else{
                if(i != c_index)
                  cbItems.get(i)._index++
              }
            }
          }else{
          }

          for(var i = 1; i < cbItems.count; i++){
            if(cbItems.get(i)._index == 0){
              continue;
            }else{
              cbItems.get(i).text = cbItems.get(i)._index + " " + filter_type[i]
            }
          }

//          cbItems.get(currentIndex).text = cbItems.get(currentIndex)._index + " " + cbItems.get(currentIndex).text
        }else{
//          for(var i = 0; i < cbItems.count; i++){
//            cbItems.get(i)._index = 0
//            cbItems.get(i).text = filter_type[i]
//          }
        }

//        if(cbItems.get(currentIndex).text != "Off"){
//          this.h_index++
//          if(h_index == 4)
//            h_index = 1
//          cbItems.get(currentIndex)._index = this.h_index

////          var s_str = cbItems.get(currentIndex).text.split(" ")

////          if(s_str.length >= 2){
////            var c_index = currentIndex
////            for(var i = 0; i < cbItems.count; i++){
////            }
////          }else{
////            console.log("bb")
////          }

//          cbItems.get(currentIndex).text = cbItems.get(currentIndex)._index + " " + cbItems.get(currentIndex).text
//        }else{
//          this.h_index = 0
//          for(var i = 0; i < cbItems.count; i++){
//            cbItems.get(i)._index = 0
//            cbItems.get(i).text = filter_type[i]
//          }
//        }
      }
    }
  }
  Sample_dialog{
    id: dialog
  }
}



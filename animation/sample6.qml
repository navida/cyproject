import QtQuick 2.0


Item {
  id: container
  width: 200; height: 200

  Rectangle {
    id: myRect
    width: 100; height: 100
    color: "red"
  }

  states: State {
    name: "reanchored"
    AnchorChanges { target: myRect; anchors.right: container.right }
  }

  transitions: Transition {
    // smoothly reanchor myRect and move into new position
    AnchorAnimation { duration: 1000 }
  }

  Component.onCompleted: container.state = "reanchored"
}

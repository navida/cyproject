import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Item{
  id: root
  width: 1100
  height: 680

  MouseArea{
    anchors.fill: parent
    onClicked: {
      console.log("root layout")
    }
  }

  Button{
    anchors{
      horizontalCenter: parent.horizontalCenter
      top: parent.top
    }
    text:"dlg"
    onClicked: my_dialog.open()
  }

  Sample_Custom_Dialog_1 {
    id: my_dialog
    width: 800
    height: 300
    implicitWidth: width
    implicitHeight: height
    x: 30
    y: 30
    z: 0

    visible: false

    drag_minimumX: 0
    drag_maximumX: root.width - width
    drag_minimumY: 0
    drag_maximumY: root.height - height
  }
}

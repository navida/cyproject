from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QVariant
from PyQt5.QtQuick import QQuickView, QQuickItem, QQuickPaintedItem
import sys, os
import _qapp

_qapp = QGuiApplication(sys.argv)

view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample_focus.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()
_qapp.exec_()
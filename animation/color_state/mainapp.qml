import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: 100
  height: 100
  Rectangle{
    anchors.fill: parent
    color: "#2E2E2E"

    Button{
      id: reset
      anchors.centerIn: parent
      implicitWidth : 35
      implicitHeight : 35

      property color _color : "transparent"

      Image{
        id: reset_img
        anchors.fill: parent
        source: "/Users/jeongjihong/projects/cyhub/_IN2GUIDE_DENTAL/layout/resources/icon/measure/reset.png"
      }

      style: ButtonStyle{
        background: Rectangle{
          id: reset_background
          implicitWidth : 35
          implicitHeight : 35
          radius: 4
          color: reset._color
        }
      }

      // radio btn
//      onHoveredChanged: {
//        if(hovered){
//          _color = "#404042"
//        }else if (reset.checked){
//          _color = "#212222"
//        }else{
//          _color = "transparent"
//        }
//      }

//      onPressedChanged: {
//        if(!pressed){
//          if(hovered && checked){
//            _color = "#212222"
//            checked = true
//          }else{
//            _color = "transparent"
//            checked = false
//          }
//        }
//      }

      // basic btn
      onHoveredChanged: {
        if(hovered){
          _color = "#404042"
        }else{
          _color = "transparent"
        }
      }

      onPressedChanged: {
        if(hovered && pressed){
          _color = "#212222"
        }else{
          _color = "transparent"
        }
      }

    }
  }
}



//Item{
//  width: 100
//  height: 100
//  Rectangle{
//    anchors.fill: parent
//    color: "#2E2E2E"

//    Button{
//      id: reset
//      anchors.centerIn: parent
//      implicitWidth : 35
//      implicitHeight : 35

//      checkable: true

//      Rectangle{
//        anchors.fill: parent
//        color: "transparent"
//        MouseArea{
//          anchors.fill: parent
//          hoverEnabled: true

//          onContainsMouseChanged: {
//            if(containsMouse){
//              parent.color = "#404042"
//            }else if(reset.checked){
//              parent.color = "#212222"
//            }else{
//              parent.color = "transparent"
//            }
//          }

//          onReleased: {
//            if(containsMouse && !reset.checked){
//              parent.color = "#212222"
//              parent.parent.checked = true
//            }else{
//              parent.color = "transparent"
//              parent.parent.checked = false
//            }
//          }
//        }
//      }

//      Image{
//        id: reset_img
//        anchors.fill: parent
//        source: "/Users/jeongjihong/projects/cyhub/_IN2GUIDE_DENTAL/layout/resources/icon/measure/reset.png"
//      }

//      style: ButtonStyle{
//        background: Rectangle{
//          id: reset_background
//          color: "transparent"
//        }
//      }

//      onCheckedChanged: {
//        console.log(checked)
//      }
//    }
//  }
//}



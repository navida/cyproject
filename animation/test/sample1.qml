import QtQuick 2.7
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0

Item {
  width: 600
  height: 600

  Column{
    width: parent.width
    height: parent.height

    Rectangle{
      width: parent.width
      height: 500
      color: "black"

      Image{
        id: ioi_image
        width: parent.width - 100; height: parent.height - 100
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        sourceSize: Qt.size(width, height)
        smooth: true
        source: "KakaoTalk_20180903_135246196.jpg"
        fillMode: Image.PreserveAspectFit

        BrightnessContrast {
          id: test
          anchors.fill: ioi_image
          source: ioi_image
          brightness: 0.0
          contrast: 0.0
        }
      }

      Row{
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Button{
          text:  "a"
          onClicked: {
            test.brightness += 0.1
          }
        }
        Button{
          text:  "a"
          onClicked: {
            test.brightness -= 0.1
          }
        }
        Button{
          text:  "a"
          onClicked: {
            test.contrast += 0.1
          }
        }
        Button{
          text:  "a"
          onClicked: {
            test.contrast -= 0.1
          }
        }
      }
    }
  }
}

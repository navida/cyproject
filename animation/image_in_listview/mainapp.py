from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtGui import QGuiApplication
from PyQt5.Qt import QQmlApplicationEngine
import sys,os


class MainWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.icon_list = os.listdir("/Users/jeongjihong/Downloads/free_icons_for_developers_icons_pack_120612/PNG")

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self.init_sig_slot()

    def init_sig_slot(self):
        self.findChild(QObject, "_rect").set_model_data(self.icon_list)
        print("")

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    window = MainWindow()

    app.exec()


import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: 500
  height: 200
  Rectangle{
    id: _rect
    objectName: "_rect"
    anchors.fill: parent
    color: "black"

    property var icon_list : []

    function set_model_data(data){

      for(var i = 0; i < data.length; i++){
        _listmodel.append({
                 _type: data[i] + "png",
                 _image_source: "/Users/jeongjihong/Downloads/free_icons_for_developers_icons_pack_120612/PNG/" + data[i]
               })
      }
    }

    ListModel{
      id: _listmodel
    }

    ScrollView{
      width: parent.width
      height: parent.height
      ListView{
        id: _listview
        objectName: "_listview"

        model: _listmodel

        orientation : ListView.Horizontal

        delegate: Component{
          Image{
            width: 200
            height: 200
            source: _image_source
          }
        }
      }
    }
  }
}

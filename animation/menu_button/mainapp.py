from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtGui import QGuiApplication
from PyQt5.Qt import QQmlApplicationEngine
import sys,os


class MainWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self.init_sig_slot()

    def init_sig_slot(self):
        m = self.findChild(QObject, "root")
        # m.test_func(1, None)

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    window = MainWindow()

    app.exec()



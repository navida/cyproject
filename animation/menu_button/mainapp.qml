import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: 500
  height: 500

  GridLayout{
    id: root
    width: parent.width
    height: parent.height

    Layout.preferredWidth: width
    Layout.preferredHeight: height
    Layout.maximumHeight: height
    Layout.maximumWidth: width
    Layout.minimumHeight: height
    Layout.minimumWidth: width

    rows    : 12
    columns : 12

    rowSpacing: 0
    columnSpacing: 0

    property double colMulti : root.width / root.columns
    property double rowMulti : root.height / root.rows

    function prefWidth(item){
      return colMulti * item.Layout.columnSpan
    }
    function prefHeight(item){
      return rowMulti * item.Layout.rowSpan
    }

    Rectangle{
      Layout.rowSpan   : 12
      Layout.columnSpan: 12
      Layout.preferredWidth  : root.prefWidth(this)
      Layout.preferredHeight : root.prefHeight(this)

      border.color: "black"

      Rectangle{
        width: parent.width
        height: parent.height
        color: "red"
      }
    }
  }
//  Rectangle{
//    id: root
//    objectName: "root"
//    width: parent.width
//    height: parent.height
//    color: "green"

//    function test_func(a, b){
//      if(b === undefined) b = 2
//      console.log("a : " + a)
//      console.log("b : " + b)
//    }

//    RowLayout{
//      width: parent.width
//      height: parent.height
//      spacing: 0

//      ColumnLayout{
//        width: 200
//        height: parent.height
//        spacing: 0
//        z:1

//        Rectangle{
//          Layout.fillWidth: true
//          height: parent.height / 2
//          z : 1
//          color: "red"

//          Button{
//            id: my_btn
//            anchors.centerIn: parent
//            text: "hahaha"
//            onClicked: {
//              my_rect.visible = !my_rect.visible
//            }

//            Rectangle{
//              id: my_rect
//              visible: false
//              width:150
//              height:250
//              x: parent.width
//              color: "black"

//              ListModel {
//                id: my_model
//                ListElement {
//                  name:'abc'
//                  _color: 'red'
//                }
//                ListElement {
//                  name:'efg'
//                  _color: 'blue'
//                }
//                ListElement {
//                  name:'xyz'
//                  _color: 'yellow'
//                }
//              }

//              ListView{
//                width: parent.width
//                height: parent.height
//                model: my_model
//                delegate: Component{
//                  Rectangle{
//                    width: 150
//                    height: 30
//                    color: _color
//                    Text{
//                      text: name
//                    }

//                    MouseArea{
//                      anchors.fill: parent
//                      onClicked: {
//                        my_btn.text = name
//                      }
//                    }
//                  }
//                }
//              }
//            }
//          }
//        }
//        Rectangle{
//          Layout.fillWidth: true
//          Layout.fillHeight: true
//          color: "yellow"
//        }
//      }

//      ColumnLayout{
//        width: 300
//        height: parent.height
//        Rectangle{
//          Layout.fillWidth: true
//          Layout.fillHeight: true
//          color: "blue"
//        }
//      }
//    }
//  }
}




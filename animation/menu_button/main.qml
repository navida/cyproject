import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Item {
  id: win_root
//  anchors.fill: parent
  width: 1000
  height: 700

  Rectangle{
    id: bg
    anchors.fill: parent
    color: 'darkgray'
  }

  RowLayout {
    anchors.fill: parent
    implicitWidth: parent.width
    implicitHeight: parent.height
    width: parent.width
    height: parent.height
    spacing: 2

    ColumnLayout {
      anchors.fill: parent
      implicitWidth: parent.width
      implicitHeight: parent.height
      width: parent.width
      height: parent.height
      spacing: 2

      RowLayout {
        anchors.fill: parent
        implicitWidth: parent.width
        implicitHeight: parent.height
        width: parent.width
        height: parent.height
        spacing: 2
      }

      Rectangle{
//        Layout.fillWidth: true
        width: parent.width
        height: 60
        implicitWidth: parent.width
        implicitHeight: 60
        color:'green'

        Column{
          width: parent.width
          height: parent.height
//          anchors.centerIn: parent

          Label {
            anchors{
              left: range_slider.left
              bottom: range_slider.top
              bottomMargin: range_slider.height
            }
            text: "• Binary Threshold (Lower/Upper)"
            font.pixelSize: 12
            color: "white"
          }

          Item {
            id: range_slider
            objectName: "range_slider"
            anchors.centerIn: parent
            width: parent.width - 30
            height: 8

            property real minimum: 1
            property real maximum: 100
            property real value1: maximum
            property real value2: minimum

            onMaximumChanged: {
              value1 = maximum
              range_slider_handle1.x = range_slider.width - range_slider_handle1.width
              range_slider_handle2.x = 0
              var _value = (range_slider.maximum) * range_slider_handle2.x / (range_slider.width - width * 2)
              range_slider.value2 = _value
            }

            Rectangle {
              id: range_slider_groove
              anchors.fill: parent
              color: "#100F0F"
              radius: 8
            }

            Rectangle{
              id: range_slider_handle1
              anchors.verticalCenter: parent.verticalCenter
              smooth: true
              x: range_slider.width - width
              width: 10
              height: 20
              color: "#A6A9AB"

              onXChanged: {
                var _value = (range_slider.maximum) * (x - width) / (range_slider.width - width * 2)
                range_slider.value1 = _value
              }

              MouseArea{
                id: range_slider_handle1_mousearea
                width: parent.width * 4
                height: parent.height
                hoverEnabled: true
                anchors{
                  horizontalCenter: parent.horizontalCenter
                }
                drag.target: parent
                drag.axis: Drag.XAxis
                drag.minimumX: range_slider_handle2.x + range_slider_handle2.width
                drag.maximumX: range_slider.width - range_slider_handle1.width
                drag.threshold : 0

                property real _width: parseInt(range_slider_handle1_mousearea.drag.maximumX - range_slider_handle1_mousearea.drag.minimumX)

                onPressedChanged: {
                  if(pressed){
                    range_slider_handle1.color = "#2BC2F3"
                  }
                  else{
                    range_slider_handle1.color = "#A6A9AB"
                  }
                }
                onHoveredChanged: {
                  if(containsMouse)
                    cursorShape = Qt.SizeHorCursor
                  else
                    return
                }
              }
              Label {
                anchors{
                  horizontalCenter: range_slider_handle1.horizontalCenter
                  top: range_slider_handle1.bottom
                  topMargin: 2
                }
                text: parseInt(range_slider.value1) + 1
                font.pixelSize: 10
                color: "white"
              }
            }

            Rectangle{
              id: range_slider_handle2
              anchors.verticalCenter: parent.verticalCenter
              smooth: true
              x: 0
              width: 10
              height: 20
              color: "#A6A9AB"

              onXChanged: {
                var _value = (range_slider.maximum) * x / (range_slider.width - width * 2)
                range_slider.value2 = _value
              }

              MouseArea{
                id: range_slider_handle2_mousearea
                width: parent.width * 4
                height: parent.height
                hoverEnabled: true
                anchors{
                  horizontalCenter: parent.horizontalCenter
                }
                drag.target: parent
                drag.axis: Drag.XAxis
                drag.minimumX: 0
                drag.maximumX: range_slider_handle1.x - range_slider_handle1.width
                drag.threshold : 0

                property real _width: parseInt(range_slider_handle2_mousearea.drag.maximumX - range_slider_handle2_mousearea.drag.minimumX)

                onPressedChanged: {
                  if(pressed){
                    range_slider_handle2.color = "#2BC2F3"
                  }
                  else{
                    range_slider_handle2.color = "#A6A9AB"
                  }
                }
                onHoveredChanged: {
                  if(containsMouse)
                    cursorShape = Qt.SizeHorCursor
                  else
                    return
                }
              }
              Label {
                anchors{
                  horizontalCenter: range_slider_handle2.horizontalCenter
                  top: range_slider_handle2.bottom
                  topMargin: 2
                }
                text: parseInt(range_slider.value2) + 1
                font.pixelSize: 10
                color: "white"
              }
            }
          }
        }

      }
    }

    Rectangle {
      implicitWidth: 100
      Layout.fillHeight: true
      color: 'gray'

      ColumnLayout {
        anchors.fill: parent
        spacing: 2

        Rectangle {
          objectName: "btn_seg"
          Layout.fillWidth: true
          Layout.fillHeight: true
          color: 'darkgray'

          signal clicked()

          MouseArea {
            anchors.fill: parent
            onClicked:{
              parent.clicked()
            }
          }

          Text{
            color: "white"
            text: "Segment!"
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 15
          }
        }
      }
    }

  }
}

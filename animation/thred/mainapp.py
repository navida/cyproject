import time

# # TODO!!! step1
# import threading
#
# def worker(number):
#     for i in range(1000):
#         print(number, "working - ", i, "times")
#
# for i in range(100):
#     t = threading.Thread(target= worker, args=(i,))
#     t.start()

# # TODO!!! step2
# import threading
#
#
# class MyThread(threading.Thread):
#
#     def __init__(self, group=None, target=None, name=None, args=()):
#         threading.Thread.__init__(self, group=group, target=target, name=name)
#         self.args = args
#
#     def run(self):
#         for i in range(10):
#             print(self.args, "working - ", i, "times")
#
# for i in range(5):
#     t = MyThread(args=(i))
#     t.start()

# # TODO!!! step3
# from multiprocessing import Process
#
# def worker(number):
#     for i in range(47360000):
#         print(number, "working - ", i, "times")
#
# if __name__ == '__main__':
#     for i in range(10):
#         p = Process(target=worker, args=(i,))
#         p.start()

# # # TODO!!! step4
# import concurrent.futures
# from functools import partial
#
# def is_prime(n):
#     if n < 2:
#         return False
#     if n is 2 or n is 3:
#         return True
#     if n % 2 is 0 or n % 3 is 0:
#         return False
#     if n < 9:
#         return True
#     k, l = 5, n ** 0.5
#     while k <= l:
#         if n % k is 0 or n % (k+2) is 0:
#             return False
#         k += 6
#     return True
#
# def process(n, r, a, b):
#     print("processing: {} ..< {}".format(n, n+r), end="... ")
#     s = sum((x for x in range(n, n+r) if is_prime(x) if x <= 1120000))
#     print(s)
#     return s
#
# def main():
#     r = 10000
#
#     with concurrent.futures.ProcessPoolExecutor(max_workers=4) as exe:
#         result = 0
#     #    .map() -> 개별 작업이 동시에 실행된 후, 먼저 종료된 작업부터 내놓는 리턴값을 내놓음
#     #    1번째 파라미터 => 동작함수, 2번째 파라미터 => 입력데이터
#         for i in exe.map(partial(process, r=r, a=100, b=200), range(0, 1120000, r)):
#             result += i
#             print(result)
#
#     # result = 0
#     # fs = {exe.submit(process, n, r) for n in range(0, 10000000, r)}
#     # done, _ = concurrent.futures.wait(fs)
#     # result = sum((f.result() for f in done))
#     # print(result)
#
# if __name__ == "__main__":
#     start_time = time.time()
#     main()
#     print(time.time() - start_time)

# # # TODO!!! step5
# import threading
#
# def calc_square(numbers):
#     print("calculate square numbers")
#     for n in numbers:
#         # time.sleep(0.2)
#         print('square : ', n * n)
#
# def calc_cube(numbers):
#     print("calculate cube of numbers")
#     for n in numbers:
#         # time.sleep(0.2)
#         print('cube : ', n * n * n)
#
# arr = [2, 3, 8, 9]
#
# t = time.time()
# # calc_square(arr)
# # calc_cube(arr)
#
# t1 = threading.Thread(target=calc_square, args=(arr,))
# t2 = threading.Thread(target=calc_cube, args=(arr,))
#
# t1.start()
# t2.start()
#
# t1.join()
# t2.join()
#
# print("done in : ", time.time() - t)
#


# def is_prime(n):
#     if n < 2:
#         return False
#     if n is 2 or n is 3:
#         return True
#     if n % 2 is 0 or n % 3 is 0:
#         return False
#     if n < 9:
#         return True
#     k, l = 5, n ** 0.5
#     while k <= l:
#         if n % k is 0 or n % (k+2) is 0:
#             return False
#         k += 6
#     return True
# st = time.time()
# sum((x for x in range(2, 2000000) if is_prime(x)))
# print(time.time() - st)

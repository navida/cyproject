from PyQt5.QtCore import QUrl, Qt, QObject, pyqtSlot, QVariant, pyqtSignal, QPoint, qAbs, pyqtProperty
from PyQt5.QtQuick import QQuickView, QQuickItem, QQuickPaintedItem
from PyQt5 import QtQml

import _qapp

class _vtk_meta_class(type(QObject)):
    def __new__(cls, name, bases, namespace):
        for attr_name, attr_value in list(namespace.items()):

            # Handle qt_property.
            if isinstance(attr_value, qt_property):
                # Create and replace.
                p, sig = attr_value.create(attr_name)
                namespace[attr_name] = p

                # Set signal if provided.
                if sig is not None:
                    assert attr_name + 'Changed' not in namespace, attr_name
                    namespace[attr_name + 'Changed'] = sig

        return super(_vtk_meta_class, cls).__new__(cls, name, bases, namespace)


class qt_property:
    """
    Decorator for specifying Qt Property concisely especially in vtk context,
      which uses method of self's member to get and set value.

    ptype: type of property
    member: attribute name that provides methods (e.g., 'mapper' means self.mapper)
    getm: (chain of) get method (e.g., 'GetInputConnection', 'GetProperty.GetPointSize')
    setm: (chain of) get method
    render: whether self.render() is called after set method
    notify: whether notification signal is created
    reset_camera: whether self.reset_camera() is called after set method (If set,
      self.render() will not be called, even though render is true.)
    gtrans, strans: value transformation functions for get and set method
    kwds: additional keyword arguments to be passed to pyqtProperty()
    """

    def __init__(self, ptype, member=None, getm=None, setm=None, render=True,
                 notify=True, reset_camera=False, gtrans=None, strans=None,
                 **kwds):
        self.args = (ptype, member, getm, setm, render, notify, reset_camera,
                     gtrans, strans, kwds)
        self.getf, self.setf = None, None

    def setter(self, fn):
        self.setf = fn
        return self

    def create(self, attr_name):
        (ptype, member, getm, setm, render, notify, reset_camera, gtrans, strans,
         kwds) = self.args

        if self.getf is None:
            GM = getm

            def _get(this):
                # Supporting chaining of methods, e.g., 'GetProperty.GetPointSize'
                o = getattr(this, member)
                v = getattr(o, GM)()

                if gtrans is not None:
                    v = gtrans(v)

                return v

            self.getf = _get

        if notify:
            sig = kwds['notify'] = pyqtSignal(ptype)
        else:
            sig = None

        if self.setf is None and setm is not None:
            sig_name = attr_name + 'Changed'
            SM = setm

            def _set(this, v):
                if strans:
                    v = strans(v)
                print('Set:', this, member, setm, repr(v))

                o = getattr(this, member)
                v = getattr(o, SM)(v)

                if reset_camera:
                    # This applies only to RenderItem.
                    this.reset_camera()  # also doing render() in it
                elif render:
                    this.render()

                if notify:
                    # Above 'sig' cannot be used but signal needs to be looked
                    #   up at runtime, because PyQt will instantiate the real
                    #   signal as an instance attribute using 'sig.'
                    getattr(this, sig_name).emit(v)

            self.setf = _set

        return pyqtProperty(ptype, self.getf, self.setf, **kwds), sig


class Resource(QObject, metaclass=_vtk_meta_class):
    def __init__(self, parent=None):
        super().__init__(parent)
        print('New:', self)


class Geom_sample:
    def __init__(self):
        self._prop = ""

    def get_prop(self):
        return self._prop

    def set_prop(self, prop):
        self._prop = prop


class Geom(Resource):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.hong = Geom_sample()

    prop = qt_property('QVariant', "hong", 'get_prop')

    @prop.setter
    def prop(self, prop):
        print('Set: ', prop)
        self.hong.set_prop(prop)

QtQml.qmlRegisterType(Geom, 'cyhub', 1, 0, 'Geom')

v = QQuickView()
v.setResizeMode(QQuickView.SizeRootObjectToView)
v.setSource(QUrl.fromLocalFile('sample8.qml'))

v.show()

_qapp.exec_()

# 
# class Channel(QQuickItem):
# 
#     nameChanged = pyqtSignal()
# 
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._name = ""
# 
#     @pyqtProperty('QString', notify=nameChanged)
#     def name(self):
#         return self._name
# 
#     @name.setter
#     def name(self, name):
#         if name != self._name:
#             self._name = name
#             self.nameChanged.emit()
# 
# 
# 
# class MaskedMouseArea(QQuickItem):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._pressed = False
#         self._maskSource = QUrl()
#         self._maskImage = QImage()
#         self._pressPoint = QPointF()
#         self._alphaThreshold = 0.0
#         self._containsMouse = False
# 
#         self.setAcceptHoverEvents(True)
#         self.setAcceptedMouseButtons(Qt.LeftButton)
# 
#     def qBound(self, _minVal, _current, _maxVal):
#         return max(min(_current, _maxVal), _minVal)
# 
#     # Get Property
#     def IsPressed(self):
#         return self._pressed
# 
#     def ContainsMouse(self):
#         return self._containsMouse
# 
#     def MaskSource(self):
#         return self._maskSource
# 
#     def AlphaThreshold(self):
#         return self._alphaThreshold
# 
#     # Set Property
#     def setPressed(self, pressed):
#         if self._pressed != pressed:
#             self._pressed = pressed
#             self.sig_pressedChanged.emit(self._pressed)
# 
#     def setContainsMouse(self, containsMouse):
#         if self._containsMouse != containsMouse:
#             self._containsMouse = containsMouse
#             self.sig_containsMouseChanged.emit(self._containsMouse)
# 
#     def setMaskSource(self, source):
#         if self._maskSource != source:
#             self._maskSource = source
#             self._maskImage = QImage("/images/" + self._maskSource.fileName())
#             self.sig_maskSourceChanged.emit(self._maskSource)
# 
#     def setAlphaThreshold(self, threshold):
#         if self._alphaThreshold != threshold:
#             self._alphaThreshold = threshold
#             self.sig_alphaThresholdChanged.emit(self._alphaThreshold)
# 
#     pressed = pyqtProperty(QVariant, fget=IsPressed, notify=sig_pressedChanged)
#     containsMouse = pyqtProperty(QVariant, fget=ContainsMouse, notify=sig_containsMouseChanged)
#     maskSource = pyqtProperty(QVariant, fget=MaskSource, fset=setMaskSource, notify=sig_maskSourceChanged)
#     alphaThreshold = pyqtProperty(QVariant, fget=AlphaThreshold, fset=setAlphaThreshold, notify=sig_alphaThresholdChanged)
# 
#     def contains(self, point):
#         if not(QQuickItem().contains(point)) or self._maskImage is None:
#             return False
# 
#         p = point.toPoint()
# 
#         if p.x() < 0 or p.x() >= self._maskImage.width() or \
#             p.y() < 0 or p.y() >= self._maskImage.height():
#             return False
# 
#         r = self.qBound(0, self._alphaThreshold * 255, 255)
#         return qAlpha(self._maskImage.pixel(p)) > r
# 
#     def mousePressEvent(self, e):
#         self.setPressed(True)
#         self._pressPoint = e.pos()
#         self.sig_pressed.emit()
# 
#     def mouseReleaseEvent(self, e):
#         self.setPressed(False)
#         self.sig_released.emit()
# 
#         threshold = _qapp.styleHints().stratDragDistance()
#         isClick = (threshold >= qAbs(e.x() - self._pressPoint.x()) and
#                    threshold >= qAbs(e.y() - self._pressPoint.y()))
#         if isClick:
#             self.sig_clicked.emit()
# 
#     def mouseUngrabEvent(self):
#         self.setPressed(False)
#         self.sig_canceled.emit()
# 
#     def hoverEnterEvent(self, e):
#         self.setContainsMouse(True)
# 
#     def hoverLeaveEvent(self, e):
#         self.setContainsMouse(False)
# 
# 































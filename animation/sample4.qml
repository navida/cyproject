import QtQuick 2.0

Item {
  width: 320
  height: 120

  Rectangle {
    id: rectangle
    color: "yellow"
    width: 120
    height: 120

    MouseArea {
      anchors.fill: parent
      // The animation starts running when you click within the rectange
      onClicked: anim.running = true;
    }
  }

  // This animation specifically targets the Rectangle's properties to animate
  SequentialAnimation {
    id: anim
    // Animations on their own are not running by default
    // The default number of loops is one, restart the animation to see it again

    NumberAnimation { target: rectangle; property: "x"; from: 0; to: 200; duration: 500 }

    NumberAnimation { target: rectangle; property: "x"; from: 200; to: 0; duration: 500 }
  }
}

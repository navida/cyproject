from PyQt5.QtCore import QObject, QUrl, pyqtSlot, QVariant, Qt, QTranslator, QLocale, QLibraryInfo
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys,os


class MainWindow(QQuickView):
    def __init__(self, app, *args, **kwds):
        super().__init__(*args, **kwds)
        self.app = app

        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'mainapp.qml')))
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.show()
        self.init_sig_slot()

    def init_sig_slot(self):
        pass

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow(app)

    app.exec()

import QtQuick 2.0
import "style"

Item{
  width: 300
  height: 300
  Rectangle {
    anchors.centerIn : parent
    width: 100
    height: 100
    color: CyStyle.root_item1.backgroundcolor
    Rectangle {
      anchors.centerIn : parent
      width: 70
      height: 70
      color: CyStyle.root_item1.root_item1_1.backgroundcolor
      Rectangle {
        anchors.centerIn : parent
        width: 40
        height: 40
        color: CyStyle.root_item1.root_item1_2.backgroundcolor
        Rectangle {
          anchors.centerIn : parent
          width: 10
          height: 10
          color: CyStyle.root_item1.root_item1_3.backgroundcolor
        }
      }
    }
  }
}

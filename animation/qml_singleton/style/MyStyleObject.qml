pragma Singleton  
import QtQuick 2.0  

Item{
  id: my_root

  StyleMac{ id: style_mac }
  StyleWin{ id: style_win }

  function check_os(){
    if(Qt.platform.os === "osx"){
      return style_mac
    }else if(Qt.platform.os === "windows"){
      return style_win
    }else{
      // can be added.
    }
  }

  property var style_obejct: check_os()

  property QtObject root_item1: QtObject{
    property color backgroundcolor: style_obejct.root_item1.backgroundcolor

    property QtObject root_item1_1: QtObject{
      property color backgroundcolor: style_obejct.root_item1.root_item1_1.backgroundcolor
    }
    property QtObject root_item1_2: QtObject{
      property color backgroundcolor: style_obejct.root_item1.root_item1_2.backgroundcolor
    }
    property QtObject root_item1_3: QtObject{
      property color backgroundcolor: style_obejct.root_item1.root_item1_3.backgroundcolor
    }
  }

//  property QtObject root_item1: QtObject{
//    property color backgroundcolor: "green";

//    property QtObject root_item1_1: QtObject{
//      property color backgroundcolor: "red";
//    }
//    property QtObject root_item1_2: QtObject{
//      property color backgroundcolor: "blue";
//    }
//    property QtObject root_item1_3: QtObject{
//      property color backgroundcolor: "yellow";
//    }
//  }

//  property QtObject root_item2: QtObject{
//    property color backgroundcolor: "red";
//  }

//  property QtObject root_item3: QtObject{
//    property color backgroundcolor: "white";
//  }

//  property variant root_item4:{
//    "backgroundcolor" : "blue"
//  }

}

﻿import QtQuick 2.0

QtObject{
  property QtObject root_item1: QtObject{
    property color backgroundcolor: "red";

    property QtObject root_item1_1: QtObject{
      property color backgroundcolor: "green";
    }
    property QtObject root_item1_2: QtObject{
      property color backgroundcolor: "yellow";
    }
    property QtObject root_item1_3: QtObject{
      property color backgroundcolor: "orange";
    }
  }
}


import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2

Item {
  id: registration_dialog
  objectName: 'registration_dialog'
  visible: false

  width: 800
  height: 300

  signal open()
  onOpen: {
    registration_dialog.visible = true;
    i2g_dim.on();
  }

  signal close()
  onClose: {
    registration_dialog.visible = false;
    i2g_dim.off();
  }

  Item {
    id: registration_dialog_item
    objectName: 'registration_dialog_item'
    anchors.fill: parent

    ColumnLayout {
      anchors.fill: parent
      spacing: 0

      Rectangle {
        implicitHeight: 20
        Layout.fillWidth: true
        color: '#313231'

        Text {
          color: "white"
          text: "Leaf Implant"
          anchors.fill: parent
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          font.pointSize: 13
        }

        MouseArea {
          anchors.fill: parent
          acceptedButtons: Qt.LeftButton
          propagateComposedEvents: true
          preventStealing: true

          property real lastMouseX: 0
          property real lastMouseY: 0
          onPressed: {
            lastMouseX = mouseX
            lastMouseY = mouseY
          }
          onMouseXChanged: registration_dialog.x += (mouseX - lastMouseX)
          onMouseYChanged: registration_dialog.y += (mouseY - lastMouseY)
        }

        Text {
          color: "white"
          text: "X"
          anchors{
            right: parent.right
            rightMargin: 5
            verticalCenter: parent.verticalCenter
          }

          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          font.pointSize: 16

          MouseArea{
            anchors.fill: parent
            onClicked: registration_dialog.close()
          }
        }
      }

      Rectangle {
        Layout.fillWidth: true
        Layout.fillHeight: true
        color: 'red'
      }
    }
  }

  onVisibleChanged: {
    if (visible == true)
    {
//      contentItem.ApplicationWindow.window.flags |= Qt.WindowStaysOnTopHint;
    }
    else
    {
      registration_dialog_item.closed();
    }
  }
}

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml 2.0

GridLayout{
  id: root
  width: 800
  height: 650

  Layout.preferredWidth: width
  Layout.preferredHeight: height
  Layout.maximumHeight: height
  Layout.maximumWidth: width
  Layout.minimumHeight: height
  Layout.minimumWidth: width

  rows    : 12
  columns : 12

  rowSpacing: 0
  columnSpacing: 0

  property double colMulti : root.width / root.columns
  property double rowMulti : root.height / root.rows

  function prefWidth(item){
    return colMulti * item.Layout.columnSpan
  }
  function prefHeight(item){
    return rowMulti * item.Layout.rowSpan
  }

  Rectangle{
    Layout.rowSpan   : 1
    Layout.columnSpan: 12
    Layout.preferredWidth  : root.prefWidth(this)
    Layout.preferredHeight : root.prefHeight(this)

    RowLayout{
      anchors.centerIn: parent
      spacing: 30

      Button{
        width:50
        height:30
        text: "ROAD"
      }

      Button{
        width:50
        height:30
        text: "INPUT"
      }
    }
  }

  Rectangle{
    Layout.rowSpan   : 5
    Layout.columnSpan: 12
    Layout.preferredWidth  : root.prefWidth(this)
    Layout.preferredHeight : root.prefHeight(this)

    TextArea{
      anchors.fill: parent
      readOnly: true
    }
  }

  Rectangle{
    Layout.rowSpan   : 5
    Layout.columnSpan: 12
    Layout.preferredWidth  : root.prefWidth(this)
    Layout.preferredHeight : root.prefHeight(this)
    TextArea{
      anchors.fill: parent
      readOnly: true
    }
  }

  Rectangle{
    Layout.rowSpan   : 1
    Layout.columnSpan: 12
    Layout.preferredWidth  : root.prefWidth(this)
    Layout.preferredHeight : root.prefHeight(this)
    RowLayout{
      anchors.centerIn: parent
      spacing: 30

      Button{
        width:50
        height:30
        text: "Sutdy"
      }

      Button{
        width:50
        height:30
        text: "Series"
      }

      Button{
        width:50
        height:30
        text: "Image"
      }
    }
  }
}

import QtQuick 2.0

Item{
  width: 320
  height: 120

  Rectangle {
    id: my_rect
    objectName: "my_rect"
    color: "green"
    width: 120
    height: 120

    Behavior on x {
      NumberAnimation {
        duration: 600
        easing.type: Easing.OutBounce
      }
    }

    MouseArea {
      anchors.fill: parent
      onClicked: parent.x == 0 ? parent.x = 200 : parent.x = 0
    }
  }
}

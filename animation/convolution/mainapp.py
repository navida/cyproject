from PIL import Image
import numpy as np

image = Image.open("/Users/jeongjihong/Downloads/Images/JPG/VEGAS.jpg")

image.show()

pixels = image.load()

_l1 = list(image.getdata())

image_width = image.width
image_height = image.height

norm = 0
orig = list()
temp = list()

# blur
# r2 = np.array([1, 1, 1])
# r3 = np.array([1, 4, 1])
# r4 = np.array([1, 1, 1])
# arr = np.column_stack((r2, r3, r4))

# sharpend
# r2 = np.array([0, -1, 0])
# r3 = np.array([-1, 5, -1])
# r4 = np.array([0, -1, 0])
# arr = np.column_stack((r2, r3, r4))

# r2 = np.array([-1, -1, -1])
# r3 = np.array([-1, 8, -1])
# r4 = np.array([-1, -1, -1])
# arr = np.column_stack((r2, r3, r4))

# h edge
# r2 = np.array([-1, -1, -1])
# r3 = np.array([0, 0, 0])
# r4 = np.array([1, 1, 1])
# arr = np.column_stack((r2, r3, r4))

# v edge
# r2 = np.array([-1, 0, 1])
# r3 = np.array([-1, 0, 1])
# r4 = np.array([-1, 0, 1])
# arr = np.column_stack((r2, r3, r4))

# r edge
r2 = np.array([1, 1, 1])
r3 = np.array([1, -8, 1])
r4 = np.array([1, 1, 1])
arr = np.column_stack((r2, r3, r4))

for r in range(0, arr.shape[1]):
    for c in range(0, arr.shape[0]):
        norm += arr[r][c]
if norm == 0:
    norm = 1
else:
    norm = 1 / norm

for x in range(0, image_width * image_height):
    temp.append(0)

for r1 in range(1, image_height - 1):
    for c1 in range(1, image_width - 1):
        for m in range(0, 3):
            for n in range(0, 3):
                temp[r1 * image_width + c1] += (arr[m][n] * _l1[(r1 - 1 + m) * image_width + (c1 - 1 + n)])
        temp[r1 * image_width + c1] *= norm

for r4 in range(0, image_height):
    for c4 in range(0, image_width):
        pixels[c4, r4] = int(temp[r4*image_width+c4])

image.show()
import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
  id: root
  width: 600
  height: 300

  Rectangle {
    id: rect1
    width: 240
    height: 240
    border.width: 1
    border.color: 'black'
    MouseArea {
      anchors.fill: parent
      onClicked: {
        rect1.forceActiveFocus();
        rect1.color = 'red';
      }
    }
    Keys.onPressed: {
      rect1.color = 'green';
      event.accepted = true;
    }
  }

  Rectangle {
    id: rect2
    width: 240
    height: 240
    border.width: 1
    border.color: 'black'
    x: 250
    Keys.onPressed: {
      rect2.color = 'blue';
    }
  }
  Keys.forwardTo: [rect2]
}

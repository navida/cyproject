import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Window 2.2
Item{
  width: 500
  height: 500

  Column {
    FocusScope {
        focus: false
        width: 100
        height: 100
        Text {
            focus: true
            text: 'has focus ' + focus + '\nhas activeFocus ' + activeFocus
        }
    }
    FocusScope {
        focus: true
        width: 100
        height: 100
        Text {
            focus: true
            text: 'has focus ' + focus + '\nhas activeFocus ' + activeFocus
            elide: Text.ElideMiddle
        }
    }

    Rectangle{
      width: 200
      height: 200
      color: "yellow"
      Text {
        width: parent.width
        text: 'has focus has activeFocus '
        horizontalAlignment: Text.AlignHCenter
      }
    }
  }
}

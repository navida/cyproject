from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QVariant, QTranslator
from PyQt5.QtQuick import QQuickView, QQuickItem, QQuickPaintedItem

import sys, os
import _qapp

#TODO!! RadialBar https://github.com/arunpkqt/RadialBarDemo/blob/master/radialbar.cpp
# class RadialBar(QQuickPaintedItem):
#     def __init__(self, parent=QQuickItem):
#         super().__init__(parent)
#

class LangTransfer(QObject):
    def __init__(self, parent=None):
        super().__init__()

        str1 = self.tr("sample1")
        str2 = self.tr("sample2")

_qapp = QGuiApplication(sys.argv)

print('Localization loaded: ',translator.load(QtCore.QLocale.system().name() + '.qm', 'translate'))

view = QQuickView()
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(_®_file__), 'sqlite_qml.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample2.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample3.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample4.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample5.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample6.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample7.qml')))
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample_mask_image.qml')))
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'Sample_Custom_Dialog_1_main.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()

_qapp.exec_()

# @pyqtSlot()
# def clicked():
#     a = view.rootObject().findChild(QObject, 'my_rect').children()[0].children()[0]
#     a.setProperty("text", "Changed Text")

# _qapp = QGuiApplication(sys.argv)
# #
# view = QQuickView()
# view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'sample2.qml')))
#
# view.setResizeMode(QQuickView.SizeRootObjectToView)
#
# view.show()
#
# a = view.rootObject().findChild(QObject, 'my_rect').property("width")
#
# _qapp.exec_()


# from PyQt5 import QtCore, QtQuick, QtQml
# import vtk
# import _qapp


# class _vtk_meta_class(type(QtCore.QObject)):
#     def __new__(cls, name, bases, namespace):
#         for attr_name, attr_value in list(namespace.items()):
#
#             # Handle qt_property.
#             if isinstance(attr_value, qt_property):
#                 # Create and replace.
#                 p, sig = attr_value.create(attr_name)
#                 namespace[attr_name] = p
#
#                 # Set signal if provided.
#                 if sig is not None:
#                     assert attr_name + 'Changed' not in namespace, attr_name
#                     namespace[attr_name + 'Changed'] = sig
#
#         return super(_vtk_meta_class, cls).__new__(cls, name, bases, namespace)
#
#
# class qt_property:
#     """
#     Decorator for specifying Qt Property concisely especially in vtk context,
#       which uses method of self's member to get and set value.
#
#     ptype: type of property
#     member: attribute name that provides methods (e.g., 'mapper' means self.mapper)
#     getm: (chain of) get method (e.g., 'GetInputConnection', 'GetProperty.GetPointSize')
#     setm: (chain of) get method
#     render: whether self.render() is called after set method
#     notify: whether notification signal is created
#     reset_camera: whether self.reset_camera() is called after set method (If set,
#       self.render() will not be called, even though render is true.)
#     gtrans, strans: value transformation functions for get and set method
#     kwds: additional keyword arguments to be passed to pyqtProperty()
#     """
#
#     def __init__(self, ptype, member=None, getm=None, setm=None, render=True,
#                  notify=True, reset_camera=False, gtrans=None, strans=None,
#                  **kwds):
#         self.args = (ptype, member, getm, setm, render, notify, reset_camera,
#                      gtrans, strans, kwds)
#         self.getf, self.setf = None, None
#
#     def setter(self, fn):
#         self.setf = fn
#         return self
#
#     def create(self, attr_name):
#         (ptype, member, getm, setm, render, notify, reset_camera, gtrans, strans,
#          kwds) = self.args
#
#         if self.getf is None:
#             GM = getm
#
#             def _get(this):
#                 # Supporting chaining of methods, e.g., 'GetProperty.GetPointSize'
#                 o = getattr(this, member)
#                 v = getattr(o, GM)()
#
#                 if gtrans is not None:
#                     v = gtrans(v)
#
#                 return v
#
#             self.getf = _get
#
#         if notify:
#             sig = kwds['notify'] = QtCore.pyqtSignal(ptype)
#         else:
#             sig = None
#
#         if self.setf is None and setm is not None:
#             sig_name = attr_name + 'Changed'
#             SM = setm
#
#             def _set(this, v):
#                 if strans:
#                     v = strans(v)
#                 print('Set:', this, member, setm, repr(v))
#
#                 o = getattr(this, member)
#                 v = getattr(o, SM)(v)
#
#                 if reset_camera:
#                     # This applies only to RenderItem.
#                     this.reset_camera()  # also doing render() in it
#                 elif render:
#                     this.render()
#
#                 if notify:
#                     # Above 'sig' cannot be used but signal needs to be looked
#                     #   up at runtime, because PyQt will instantiate the real
#                     #   signal as an instance attribute using 'sig.'
#                     getattr(this, sig_name).emit(v)
#
#             self.setf = _set
#
#         return QtCore.pyqtProperty(ptype, self.getf, self.setf, **kwds), sig
#
#
# class Resource(QtCore.QObject, metaclass=_vtk_meta_class):
#     def __init__(self, parent=None):
#         super().__init__(parent)
#         print('New:', self)
#
#
# class Geom_sample:
#     def __init__(self):
#         self._prop = ""
#
#     def get_prop(self):
#         return self._prop
#
#     def set_prop(self, prop):
#         self._prop = prop
#
#
# class Geom(Resource):
#     def __init__(self, parent=None):
#         super().__init__(parent)
#         self.hong = Geom_sample()
#
#     prop = qt_property('QVariant', "hong", 'get_prop')
#
#     @prop.setter
#     def prop(self, prop):
#         print('Set: ', prop)
#         self.hong.set_prop(prop)

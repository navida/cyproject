import cv2
import numpy as np


_img = cv2.imread('./obj2.png', cv2.IMREAD_GRAYSCALE)

sift = cv2.xfeatures2d.SIFT_create()
# sift = cv2.xfeatures2d.SURF_create(400)
# sift.setHessianThreshold(50000)

kp1, des1 = sift.detectAndCompute(_img, None)
print("kp, des :: ", kp1, des1, des1.shape)

train_img = cv2.drawKeypoints(_img, kp1, _img)
cv2.imwrite('./results/keypoints_obj.jpg', train_img)

# BFMatcher with default params
bf = cv2.BFMatcher()

# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks=50)  # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params, search_params)

cap = cv2.VideoCapture(0)
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

while 1:
    ret, frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    kp2, des2 = sift.detectAndCompute(frame_gray, None)
    frame = cv2.drawKeypoints(frame, kp2, frame)

    matches = bf.knnMatch(des1, des2, k=2)

    good = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    result_img = cv2.drawMatchesKnn(train_img, kp1, frame, kp2, good, None, flags=2)

    cv2.imshow('frame', result_img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    old_gray = frame_gray.copy()

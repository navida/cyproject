import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4


Item {
    id: main_item
    width: 305
    height: 100

    RowLayout {
        spacing:5
        anchors.fill: parent

        Canvas {
            id: mycanvas
            width: 100
            height: 100
            Layout.fillWidth: parent.width
            Layout.fillHeight: parent.height

            property bool arrowFormState: false
            function toggle() { arrowFormState = !arrowFormState }

            property real angle: 0

            states: State {
                when: mycanvas.arrowFormState
                PropertyChanges { angle: 180; target: mycanvas }
            }
            transitions: Transition {
                RotationAnimation {
                    property: "angle"
                    easing.type: Easing.InOutCubic
                    duration: 500
                }
            }

            onAngleChanged: requestPaint()

            renderTarget: Canvas.FramebufferObject
            renderStrategy: Canvas.Cooperative

            onPaint: {
                var ctx = getContext("2d");

                ctx.resetTransform()

                ctx.fillStyle = 'white'
                ctx.fillRect(0, 0, width, height)

                // Rotate from the center
                ctx.translate(width / 2, height / 2)
                ctx.rotate(angle * Math.PI / 180)
                ctx.translate(-width / 2, -height / 2)

                var left = width * 0.25
                var right = width * 0.75
                var vCenter = height * 0.5
                var vDelta = height / 6

                ctx.lineCap = "square"
                ctx.lineWidth = vDelta * 0.4
                ctx.strokeStyle = Qt.rgba(1, 0, 0, 1);

                ctx.beginPath()
                ctx.moveTo(left, vCenter - vDelta)
                ctx.lineTo(right, vCenter - vDelta)
                ctx.moveTo(left, vCenter)
                ctx.lineTo(right, vCenter)
                ctx.moveTo(left, vCenter + vDelta)
                ctx.lineTo(right, vCenter + vDelta)
                ctx.stroke()
            }
            Timer { repeat: true; running: true; onTriggered: mycanvas.toggle() }
        }

        Canvas {
            id: mycanvas2
            width: 100
            height: 100
            Layout.fillWidth: parent.width
            Layout.fillHeight: parent.height

            property bool arrowFormState: false
            function toggle() { arrowFormState = !arrowFormState }

            property real angle: 0
            property real morphProgress: 0
            states: State {
                when: mycanvas2.arrowFormState
                PropertyChanges { target: mycanvas2; angle: 180 }
                PropertyChanges { target: mycanvas2; morphProgress: 1  }
            }
            transitions: Transition {
                RotationAnimation {
                    property: "angle"
                    direction: RotationAnimation.Clockwise
                    easing.type: Easing.InOutCubic
                    duration: 900
                }
                NumberAnimation {
                    property: "morphProgress"
                    easing.type: Easing.InOutCubic
                    duration: 900
                }
            }

            onAngleChanged: { mycanvas2.requestPaint() }
            onMorphProgressChanged: { mycanvas2.requestPaint() }

            renderTarget: Canvas.FramebufferObject
            renderStrategy: Canvas.Cooperative

            onPaint: {
                var ctx = getContext('2d')
                // The context keeps its state between paint calls, reset the transform
                ctx.resetTransform()

                ctx.fillStyle = 'white'
                ctx.fillRect(0, 0, width, height)

                // Rotate from the center
                ctx.translate(width / 2, height / 2)
                ctx.rotate(angle * Math.PI / 180)
                ctx.translate(-width / 2, -height / 2)

                var left = width * 0.25
                var right = width * 0.75
                var vCenter = height * 0.5
                var vDelta = height / 6

                // Use our cubic-interpolated morphProgress to extract
                // other animation parameter values
                function interpolate(first, second, ratio) {
                    return first + (second - first) * ratio;
                };
                var vArrowEndDelta = interpolate(vDelta, vDelta * 1.25, morphProgress)
                var vArrowTipDelta = interpolate(vDelta, 0, morphProgress)
                var arrowEndX = interpolate(left, right - vArrowEndDelta, morphProgress)

                ctx.lineCap = "square"
                ctx.lineWidth = vDelta * 0.4
                ctx.strokeStyle = 'black'
                var lineCapAdjustment = interpolate(0, ctx.lineWidth / 2, morphProgress)

                ctx.beginPath()
                ctx.moveTo(arrowEndX, vCenter - vArrowEndDelta)
                ctx.lineTo(right, vCenter - vArrowTipDelta)
                ctx.moveTo(left + lineCapAdjustment, vCenter)
                ctx.lineTo(right - lineCapAdjustment, vCenter)
                ctx.moveTo(arrowEndX, vCenter + vArrowEndDelta)
                ctx.lineTo(right, vCenter + vArrowTipDelta)
                ctx.stroke()
            }
            Timer { repeat: true; running: true; onTriggered: mycanvas2.toggle() }
        }

        Button {
            text: "hello"
        }
    }
}

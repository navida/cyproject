import vtk
import numpy as np


class PolyData:
    def __init__(self, stl_path, *args, **kwds):
        self.mapper = vtk.vtkPolyDataMapper()
        # self.mapper.SetInputData(self.reader.GetOutput())

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        # init from stl
        self.init_from_stl(stl_path)

    def init_from_stl(self, stl_path):
        self.reader = vtk.vtkSTLReader()
        self.reader.SetFileName(stl_path)
        self.reader.Update()

        self.mapper.SetInputData(self.reader.GetOutput())

        cnt = self.reader.GetOutput().GetNumberOfPoints()
        self.lut = vtk.vtkLookupTable()
        self.lut.SetNumberOfTableValues(cnt)

        for i in range(cnt):
            print("Coord", self.reader.GetOutput().GetPoints().GetData().GetTuple(i))
            color = np.random.rand(3).tolist()
            self.lut.SetTableValue(i, *color)
        self.lut.Build()

        colorData1 = vtk.vtkUnsignedCharArray()
        colorData1.SetName("colors")
        colorData1.SetNumberOfComponents(3)
        for i in range(cnt):
            rgb = [0, 0, 0]
            self.lut.GetColor(i/(cnt-1), rgb)
            rgb = (np.array(rgb) * 255).tolist()
            # print("RGB", rgb)
            colorData1.InsertNextTuple3(rgb[0], rgb[1], rgb[2])

        self.reader.GetOutput().GetPointData().SetScalars(colorData1)

        # self.mapper.SetScalarModeToUseCellData()
        # self.mapper.InterpolateScalarsBeforeMappingOn()

        # self.mapper.ScalarVisibilityOn()
        # self.mapper.SetScalarModeToUsePointData()
        # self.mapper.SetColorModeToMapScalars()

        self.mapper.Update()


ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

p = PolyData('/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/GroupImplant_3.stl')
ren.AddActor(p.actor)


import os

def notify(title, text):
    os.system("""
              osascript -e 'display notification "{}" with title "{}"'
              """.format(text, title))

notify("cyCafe", "lsdjfkljdslfjdslkfdslkf")

iren.Start()


import asyncio


async def fun2():
    return 10

async def fun3():
    return 7

async def fun1():
    a = await fun2()
    b = await fun3()
    print(a+b)
    return a+b


loop = asyncio.get_event_loop()
task = loop.create_task(fun1())
loop.run_forever()
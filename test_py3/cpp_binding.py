"""
C++ Class Binding & Wrapping Test

C++ Binding Reference Sites:
https://github.com/swig/swig/wiki/FAQ#shared-libraries
http://www.math.uiuc.edu/~gfrancis/illimath/windows/aszgard_mini/bin/SWIG-1.3.21/Doc/Manual/Python.html

Class Wrapping Reference Sites :
http://web.mit.edu/svn/src/swig-1.3.25/Examples/python/class/index.html
http://books.gigatux.nl/mirror/pythonprogramming/0596000855_python2-CHP-19-SECT-8.html
"""

import sys
sys.path.append("/Users/scott/california/swig_cpp/swig_cpp")
import coffee as e

import vtk
#
#
# class CoffeePtr(object):
#     def __init__(self, _self):
#         super().__init__()
#         self._self = _self
#
#     def __del__(self):
#         e.delete_Coffee(self._self)
#
#     def __repr__(self):
#         return "<C++ Coffee instance>>"
#
#     def __setattr__(self, name, value):
#         if name == "sugar":
#             e.Coffee_sugar_set(self._self, value)
#             return
#
#         propobj = getattr(self.__class__, name, None)
#         if isinstance(propobj, property):
#             propobj.fset(self, value)
#         else:
#             super().__setattr__(name, value)
#
#     def __getattr__(self, name):
#         if name == "sugar":
#             return e.Coffee_sugar_get(self._self)
#         raise AttributeError
#
#     @property
#     def water(self):
#         return e.Coffee_water(self._self)
#
#     @water.setter
#     def water(self, _water):
#         e.Coffee_water(self._self, _water)
#
#     def how_much(self, number):
#         return e.Coffee_how_much(self._self, number)
#
#     def give_me_sugar(self):
#         return e.Coffee_give_me_sugar(self._self)
#
#
# class MyCoffee(CoffeePtr):
#     def __init__(self):
#         super().__init__(e.new_Coffee())


if __name__ == "__main__":
    # m = MyCoffee()
    m = e.Coffee()
    print(m, dir(m))

    m.water(11)
    print(m.water())

    m.give_me_sugar()
    src = m.m_coneSource
    print(src, dir(src))
    # src = m.get_src()
    # src = vtk.vtkConeSource()
    # print("!!!", type(src), dir(src))
    # print("######", src.acquire())

    # m.give_me_sugar()


    # m.water = 500
    # print("water : ", m.water)

    # m.sugar = 0.5
    # print("sugar : ", m.sugar)

    # print("how much?? ", m.how_much(number=3))

    # m.give_me_sugar()




    # Create a mapper and actor
    # mapper = vtk.vtkPolyDataMapper()
    # mapper.SetInput(src.GetOutput())
    # actor = vtk.vtkActor()
    # actor.SetMapper(mapper)
    #
    # # Visualize
    # renderer = vtk.vtkRenderer()
    # renderWindow = vtk.vtkRenderWindow()
    # renderWindow.AddRenderer(renderer)
    # renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    # renderWindowInteractor.SetRenderWindow(renderWindow)
    #
    # renderer.AddActor(actor)
    # renderer.SetBackground(.1, .2, .3)  # Background color dark blue
    # renderer.SetBackground(.3, .2, .1)  # Background color dark red
    # renderWindow.Render()
    # renderWindowInteractor.Start()


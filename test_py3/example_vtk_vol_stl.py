import vtk
import cyCafe
from PyQt5 import Qt, QtWidgets
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from _qapp import qapp

import numpy as np


DCM_PATH = r"../../Dicom/I2G_EX2"
STL_GUIDE_PATH = r"../../Dicom/I2G_EX2/data/guide.stl"
STL_BONE_PATH = r"../../Dicom/I2G_EX2/data/bone.stl"
STL_WAXUP_PATH = r"../../Dicom/I2G_EX2/data/waxup.stl"
IMPLANT1_PATH = r"../../Dicom/I2G_EX2/data/implant1.stl"
SLEEVE1_PATH = r"../../Dicom/I2G_EX2/data/sleeve1.stl"
IMPLANT2_PATH = r"../../Dicom/I2G_EX2/data/implant2.stl"
SLEEVE2_PATH = r"../../Dicom/I2G_EX2/data/sleeve2.stl"

I_MAT = [1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0,
         0, 0, 0, 1]

GUIDE_MAT = [-0.99886471, -0.047273602, 0.0058701425, 0.00000000,
             0.045200247, -0.97945809, -0.19651660, 0.00000000,
             0.015039607, -0.19602819, 0.98048294, 0.00000000,
             76.090843, 57.633541, 36.370956, 1.0000000]

WAXUP_MAT = [-0.99886471, -0.047273602, 0.0058701425, 0.00000000,
             0.045200247, -0.97945809, -0.19651660, 0.00000000,
             0.015039607, -0.19602819, 0.98048294, 0.00000000,
             76.090843, 57.633541, 36.370956, 1.0000000]

IMPLANT1_MAT = [0.98525196, -0.015733864, 0.17038479, 0.00000000,
                0.0022419244, 0.99686497, 0.079089671, 0.00000000,
                -0.17109501, -0.077541269, 0.98219848, 0.00000000,
                101.37509, 58.496227, 24.592852, 1.0000000]
SLEEVE1_MAT = [1, 0, 0, 0,
               0, 1, 0, 0,
               0, 0, 1, 0,
               0, 0, 6.5, 1]

IMPLANT2_MAT = [0.98319644, -0.016789401, 0.18177685, 0.00000000,
                0.0027734712, 0.99702054, 0.077086329, 0.00000000,
                -0.18252949, -0.075286858, 0.98031366, 0.00000000,
                104.97882, 64.808975, 22.742775, 1.0000000]
SLEEVE2_MAT = [1, 0, 0, 0,
               0, 1, 0, 0,
               0, 0, 1, 0,
               0, 0, 8.5, 1]


    # EX1
# IMPLANT1_PATH = r"../../Dicom/I2G_EX1/implant1.stl"
# IMPLANT1_MAT = [0.30359748, -0.78071934, 0.54617387, 0.00000000,
#                 -0.93913335, -0.34194028, 0.033247907, 0.00000000,
#                 0.16080156, -0.52302408, -0.83701175, 0.00000000,
#                 71.599846, 23.791491, 54.216507, 1.0000000]
# SLEEVE1_PATH = r"../../Dicom/I2G_EX1/sleeve1.stl"
# SLEEVE1_MAT = [1.0000000, 0.00000000, 0.00000000, 0.00000000,
#                0.00000000, 1.0000000, 0.00000000, 0.00000000,
#                0.00000000, 0.00000000, 1.0000000, 0.00000000,
#                0.00000000, 0.00000000, 7.5000000, 1.0000000]


# cube source
# MINI_PATH, MINI_SCALE, MINI_ROTATE = None, 5, 0
# mario
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/mario.stl", 200, 90
# bob(minions)
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/minions bob.stl", 300, 90
# stuart(minions)
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/minions stuart.stl", 200, 90
# gaonasi
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/gaonasi.stl", 3500, 90
# spiderman
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/spiderman.stl", 200, 90
# ironman
MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/Ironman.stl", 600, 17
# groot
# MINI_PATH, MINI_SCALE, MINI_ROTATE = r"./stl/babygroot.stl", 250, 0

DEPTH_PEELING = True


class In2GuideView(QVTKRenderWindowInteractor):

    class VOL:
        def __init__(self):
            self.c_f, self.o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
            self.p = vtk.vtkVolumeProperty()
            # self.m = vtk.vtkSmartVolumeMapper()
            # self.m.SetRequestedRenderMode(vtk.vtkSmartVolumeMapper.GPURenderMode)
            self.m = vtk.vtkGPUVolumeRayCastMapper()
            self.v = vtk.vtkVolume()

            self.opacityWindow = 4096
            self.opacityLevel = 2048

            # property
            self.p.SetIndependentComponents(True)
            self.p.SetInterpolationTypeToLinear()

            # self.c_f.AddRGBSegment(0.0, 0.8, 0.8, 0.8, 255.0, 0.8, 0.8, 0.8)
            # self.o_f.AddSegment(self.opacityLevel - 0.5 * self.opacityWindow, 0.0, self.opacityLevel + 0.5 * self.opacityWindow, 0.7)
            # self.m.SetBlendModeToMaximumIntensity()

            # ColorMap
            self.CP = [[-3024, 0, 0, 0],
                       [-1000, 0, 0, 0],
                       [500, 164 / 255, 28 / 255, 12 / 255],
                       [1000, 213 / 255, 156 / 255, 70 / 255],
                       [2300, 1, 1, 1],
                       [3071, 1, 1, 1]]
            for c in self.CP:
                self.c_f.AddRGBPoint(*c)
            # OpacityMap
            self.OP = [[-3024, 0],
                       [600, 0.0],
                       [1200, 0.9],
                       [3071, 0.9]]
            for o in self.OP:
                self.o_f.AddPoint(*o)

            self.m.SetUseJittering(True)
            # self.m.SetBlendModeToMaximumIntensity()
            self.m.SetBlendModeToComposite()
            self.p.ShadeOff()
            self.p.SetAmbient(0.3)
            self.p.SetDiffuse(0.6)
            self.p.SetSpecular(0.3)
            self.p.SetSpecularPower(10)
            self.p.SetScalarOpacityUnitDistance(7.0)
            #########

            self.p.SetColor(self.c_f)
            self.p.SetScalarOpacity(self.o_f)


            # actor
            self.v.SetMapper(self.m)
            self.v.SetProperty(self.p)

            self.init_materials()

        def set_vtk_img(self, vtk_img):
            self.vtk_img = vtk_img
            self.m.SetInputData(self.vtk_img)

        def get_actor(self):
            return self.v

        def init_materials(self):
            # self.v.GetProperty().SetAmbient(0.3)
            # self.v.GetProperty().SetDiffuse(0.3)
            # self.v.GetProperty().SetSpecular(0.5)
            # self.v.GetProperty().SetSpecularPower(0.2)
            # self.v.GetProperty().ShadeOn()
            pass

    class STL:
        def __init__(self, stl_path):
            self.reader = vtk.vtkSTLReader()
            self.reader.SetFileName(stl_path)
            self.reader.Update()

            self.mapper = vtk.vtkPolyDataMapper()
            self.mapper.SetInputData(self.reader.GetOutput())

            self.actor = vtk.vtkActor()
            self.actor.SetMapper(self.mapper)

        def get_actor(self):
            return self.actor

        def set_color(self, color):
            self.actor.GetProperty().SetColor(color)

        def set_opacity(self, opacity):
            self.actor.GetProperty().SetOpacity(opacity)

        def set_material_preset(self):
            color = [0.9, 0.9, 0]
            self.actor.GetProperty().SetColor(color)
            self.actor.GetProperty().SetAmbient(0.3)
            self.actor.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
            self.actor.GetProperty().SetDiffuse(0.5)
            self.actor.GetProperty().SetDiffuseColor(color)
            self.actor.GetProperty().SetSpecular(1)
            self.actor.GetProperty().SetSpecularPower(10)
            self.actor.GetProperty().SetSpecularColor([0.9, 0.9, 0.1])

    class MINIMI:
        def __init__(self):
            if MINI_PATH is None:
                # Cube Source
                self.source = vtk.vtkCubeSource()
            else:
                # STL
                self.source = vtk.vtkSTLReader()
                self.source.SetFileName(MINI_PATH)

            self.mapper = vtk.vtkPolyDataMapper()
            self.mapper.SetInputConnection(self.source.GetOutputPort())
            self.actor = vtk.vtkActor()
            self.actor.SetMapper(self.mapper)

            if MINI_PATH is None:
                self.actor.GetProperty().SetRepresentationToWireframe()
            else:
                self.actor.RotateX(MINI_ROTATE)

            self.init_materials()

        def get_actor(self):
            return self.actor

        def init_materials(self):
            color = (119 / 255, 20 / 255, 20 / 255)
            # color = (255 / 255, 0 / 255, 128 / 255)
            # color = (190/255, 186/255, 70/255)
            # color = (1, 1, 0)
            self.actor.GetProperty().SetColor(color)
            self.actor.GetProperty().SetAmbient(0.3)
            self.actor.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
            self.actor.GetProperty().SetDiffuse(0.5)
            self.actor.GetProperty().SetDiffuseColor(color)
            self.actor.GetProperty().SetSpecular(1)
            self.actor.GetProperty().SetSpecularPower(10)
            self.actor.GetProperty().SetSpecularColor([0.8, 0.1, 0.1])

            # self.actor.GetProperty().SetAmbient(1)
            # self.actor.GetProperty().SetAmbientColor(color)
            # self.actor.GetProperty().SetDiffuse(1)
            # self.actor.GetProperty().SetDiffuseColor(color)
            # self.actor.GetProperty().SetSpecular(1)
            # self.actor.GetProperty().SetSpecularPower(100)
            # self.actor.GetProperty().SetSpecularColor(color)

    class GeomScene:
        def __init__(self, _geom, _matrix):
            self.parent = None
            self.geom = _geom
            self.geom_scenes = []
            self.transform = _matrix

            # Set Transforms
            if _geom:
                self.geom.get_actor().SetUserMatrix(self.transform)

        def add_geom_scene(self, _geom_scene):
            _geom_scene.parent = self
            self.geom_scenes.append(_geom_scene)

            # Set Transform
            _geom_scene.geom.get_actor().SetUserMatrix(_geom_scene.get_transform_from_top())

        def get_transform_from_top(self):
            if self.parent is None:
                return self.transform
            else:
                mat1 = self.parent.get_transform_from_top()
                mat2 = self.transform
                mat3 = vtk.vtkMatrix4x4()
                vtk.vtkMatrix4x4.Multiply4x4(mat1, mat2, mat3)
                return mat3

    def __init__(self):
        super().__init__()

        # Renderer Setting
        self._RenderWindow.SetNumberOfLayers(2)

        # Main Renderer
        self.ren = vtk.vtkRenderer()
        self.ren.SetLayer(0)
        self._RenderWindow.AddRenderer(self.ren)
        self.ren.SetViewport(0, 0, 1, 1)
        self.ren.SetBackground(0.2, 0.3, 0.4)

        # Minimi Renderer (Right-Bottom)
        self.ren2 = vtk.vtkRenderer()
        self.ren2.SetLayer(1)
        self._RenderWindow.AddRenderer(self.ren2)
        self.ren2.SetViewport(0.85, 0, 1, 0.2)
        # transparent renderer
        self.ren2.SetPreserveColorBuffer(1)
        # only for observer interactor
        self.ren2.SetInteractive(False)

        # Interactor Style
        ISTYLES = cyCafe.cyVtkInteractorStyles()
        istyle = ISTYLES.get_vtk_interactor_style_volume_3d()
        # istyle = vtk.vtkInteractorStyleTrackballCamera()
        # istyle.HandleObserversOff()
        self._Iren.SetInteractorStyle(istyle)

        # sync camera between main ren and mini ren
        istyle.AddObserver('MouseMoveEvent', self.on_mouse_move)
        istyle.AddObserver('MouseWheelForwardEvent', self.on_mouse_wheel)
        istyle.AddObserver('MouseWheelBackwardEvent', self.on_mouse_wheel)

        # Camera
        self.camera = self.ren.GetActiveCamera()

    def initialize(self):
        self.dcm_reader = cyCafe.cyDicomReader()
        self.dcm_reader.read_dicom(DCM_PATH)
        self.vtk_img = self.dcm_reader.get_vtk_img()

        # Extent
        e = self.vtk_img.GetExtent()
        print("e", e)
        o = self.vtk_img.GetOrigin()
        print("o", o)
        s = self.vtk_img.GetSpacing()
        print("s", s)

        c = [_e*0.5*_s for _e, _s in zip([e[1], e[3], e[5]], s)]
        print("c", c)
        self.camera.SetFocalPoint(c)
        self.camera.SetPosition(c[0], c[1] - e[1], c[2])
        self.camera.SetViewUp(0, 0, 1)

        #
        self.minimi = self.MINIMI()
        self.ren2.AddViewProp(self.minimi.get_actor())

        self.vol = self.VOL()
        self.vol.set_vtk_img(self.vtk_img)
        self.ren.AddViewProp(self.vol.get_actor())

        self.guide = self.STL(STL_GUIDE_PATH)
        self.guide.set_color([31 / 255, 126 / 255, 150 / 255])
        self.guide.set_opacity(0.6)
        self.ren.AddViewProp(self.guide.get_actor())

        self.bone = self.STL(STL_BONE_PATH)
        self.bone.set_color([1, 1, 1])
        # self.bone.set_color([190 / 255, 190 / 255, 33 / 255])
        self.bone.set_opacity(0.5)
        self.ren.AddViewProp(self.bone.get_actor())

        self.waxup = self.STL(STL_WAXUP_PATH)
        self.waxup.set_color([0.9, 0.9, 0])
        self.waxup.set_material_preset()
        self.ren.AddViewProp(self.waxup.get_actor())

        self.implant1 = self.STL(IMPLANT1_PATH)
        self.implant1.set_color([255 / 255, 0 / 255, 128 / 255])
        self.ren.AddViewProp(self.implant1.get_actor())
        self.sleeve1 = self.STL(SLEEVE1_PATH)
        self.sleeve1.set_color([0 / 255, 0 / 255, 200 / 255])
        self.ren.AddViewProp(self.sleeve1.get_actor())

        self.implant2 = self.STL(IMPLANT2_PATH)
        self.implant2.set_color([255 / 255, 0 / 255, 128 / 255])
        self.ren.AddViewProp(self.implant2.get_actor())
        self.sleeve2 = self.STL(SLEEVE2_PATH)
        self.sleeve2.set_color([0 / 255, 0 / 255, 200 / 255])
        self.ren.AddViewProp(self.sleeve2.get_actor())

        ROOT_GEOM_SCENE = self.GeomScene(None, self.d3dmat4x4_to_vtkmat(I_MAT))

        GUIDE = self.GeomScene(self.guide, self.d3dmat4x4_to_vtkmat(GUIDE_MAT))

        WAXUP = self.GeomScene(self.waxup, self.d3dmat4x4_to_vtkmat(WAXUP_MAT))
        IMPLANT1 = self.GeomScene(self.implant1, self.d3dmat4x4_to_vtkmat(IMPLANT1_MAT))
        SLEEVE1 = self.GeomScene(self.sleeve1, self.d3dmat4x4_to_vtkmat(SLEEVE1_MAT))
        IMPLANT1.add_geom_scene(SLEEVE1)
        IMPLANT2 = self.GeomScene(self.implant2, self.d3dmat4x4_to_vtkmat(IMPLANT2_MAT))
        SLEEVE2 = self.GeomScene(self.sleeve2, self.d3dmat4x4_to_vtkmat(SLEEVE2_MAT))
        IMPLANT2.add_geom_scene(SLEEVE2)

        ROOT_GEOM_SCENE.add_geom_scene(GUIDE)
        ROOT_GEOM_SCENE.add_geom_scene(WAXUP)
        ROOT_GEOM_SCENE.add_geom_scene(IMPLANT1)
        ROOT_GEOM_SCENE.add_geom_scene(IMPLANT2)



        if DEPTH_PEELING:
            # Depth Peeling
            self._RenderWindow.SetAlphaBitPlanes(1)
            self._RenderWindow.SetMultiSamples(0)
            self.ren.SetUseDepthPeeling(True)
            self.ren.SetUseDepthPeelingForVolumes(True)
            self.ren.SetMaximumNumberOfPeels(100)
            self.ren.SetOcclusionRatio(0.1)

    def set_actor_visible(self, actor_type, visible):
        if actor_type == "volume":
            self.vol.get_actor().SetVisibility(visible)
        elif actor_type == "bone":
            self.bone.get_actor().SetVisibility(visible)
        elif actor_type == "guide":
            self.guide.get_actor().SetVisibility(visible)
        elif actor_type == "implant":
            self.implant1.get_actor().SetVisibility(visible)
            self.implant2.get_actor().SetVisibility(visible)
        elif actor_type == "sleeve":
            self.sleeve1.get_actor().SetVisibility(visible)
            self.sleeve2.get_actor().SetVisibility(visible)
        elif actor_type == "waxup":
            self.waxup.get_actor().SetVisibility(visible)

        self.update()
        self.Render()

    def on_mouse_move(self, _iren, _event):
        if _iren.GetState() in [vtk.VTKIS_ROTATE, vtk.VTKIS_SPIN, vtk.VTKIS_DOLLY]:
            self.sync_cameras()

    def on_mouse_wheel(self, _iren, _event):
        if _iren.GetState() in [vtk.VTKIS_ROTATE, vtk.VTKIS_SPIN, vtk.VTKIS_DOLLY]:
            self.sync_cameras()
        depthPeelingWasUsed = self.ren.GetLastRenderingUsedDepthPeeling()
        depthPeelingWasUsed2 = self.ren2.GetLastRenderingUsedDepthPeeling()

    def sync_cameras(self):
        up_vec = self.camera.GetViewUp()
        p = self.camera.GetPosition()
        f = self.camera.GetFocalPoint()

        scale = MINI_SCALE
        view_vec = np.subtract(p, f)
        view_vec = view_vec / np.linalg.norm(view_vec) * scale
        o = self.ren2.GetActors().GetItemAsObject(0).GetCenter()
        self.ren2.GetActiveCamera().SetPosition(o + view_vec)
        self.ren2.GetActiveCamera().SetViewUp(up_vec)
        # self.ren2.ResetCamera()
        self.ren2.ResetCameraClippingRange()

    def d3dmat4x4_to_vtkmat(self, d3dmat4x4):
        """
        Set OD3D's D3DMATRIX to vtkMatrix
        """
        matrix = vtk.vtkMatrix4x4()
        for i, v in enumerate(d3dmat4x4):
            r = i // 4
            c = i - r * 4
            matrix.SetElement(r, c, v)
        matrix.Transpose()
        return matrix

if __name__ == "__main__":
    main_widget = QtWidgets.QWidget()
    main_layout = QtWidgets.QHBoxLayout()
    main_widget.setLayout(main_layout)

    i2g = In2GuideView()
    i2g.initialize()
    i2g.Start()
    i2g.sync_cameras()

    right_widget = QtWidgets.QWidget()
    right_layout = QtWidgets.QVBoxLayout()
    right_widget.setLayout(right_layout)

    main_layout.addWidget(i2g)
    main_layout.addWidget(right_widget)

    check1 = QtWidgets.QCheckBox(r"Volume", stateChanged=lambda s: i2g.set_actor_visible(r"volume", s))
    check1.setTristate(False)
    check1.setChecked(True)
    check2 = QtWidgets.QCheckBox(r"Bone", stateChanged=lambda s: i2g.set_actor_visible(r"bone", s))
    check2.setTristate(False)
    check2.setChecked(True)
    check2.setChecked(False)
    check3 = QtWidgets.QCheckBox(r"Guide", stateChanged=lambda s: i2g.set_actor_visible(r"guide", s))
    check3.setTristate(False)
    check3.setChecked(True)
    check4 = QtWidgets.QCheckBox(r"Implant", stateChanged=lambda s: i2g.set_actor_visible(r"implant", s))
    check4.setTristate(False)
    check4.setChecked(True)
    check5 = QtWidgets.QCheckBox(r"Sleeve", stateChanged=lambda s: i2g.set_actor_visible(r"sleeve", s))
    check5.setTristate(False)
    check5.setChecked(True)
    check6 = QtWidgets.QCheckBox(r"Wax Up", stateChanged=lambda s: i2g.set_actor_visible(r"waxup", s))
    check6.setTristate(False)
    check6.setChecked(True)
    right_layout.addWidget(check1)
    right_layout.addWidget(check2)
    right_layout.addWidget(check3)
    right_layout.addWidget(check4)
    right_layout.addWidget(check5)
    right_layout.addWidget(check6)

    main_widget.show()
    main_widget.resize(1000, 600)

    qapp.exec_()

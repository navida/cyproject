from PyQt5.Qt import QApplication
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import *
import PyQt5.QtCore
import sys, os
import _qapp


class MyItem(QQuickItem):

    def __init__(self, *args, **kwds):
        super().__init__(args, kwds)
        self.setFlag(QQuickItem.ItemHasContents, True)
        self.setAcceptedMouseButtons(PyQt5.QtCore.Qt.AllButtons)
        self.setAcceptHoverEvents(True)
        self.index = 0

    def hoverMoveEvent(self, e):
        print("hi", self.index)
        self.index += 1


if __name__ == '__main__':
    print("Qt version:", PyQt5.QtCore.QT_VERSION_STR)
    # app = QApplication(sys.argv)

    view = QQuickView()
    view.setResizeMode(QQuickView.SizeRootObjectToView)
    qmlRegisterType(MyItem, 'Test', 1, 0, "MyItem")
    view.setSource(PyQt5.QtCore.QUrl.fromLocalFile(os.path.join('main.qml')))
    view.resize(200,200)
    view.show()
    # sys.exit(app.exec_())
    _qapp.exec_()
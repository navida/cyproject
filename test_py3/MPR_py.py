import sys
import vtk
import SimpleITK as sitk
from PyQt5 import QtCore, QtGui
from PyQt5 import uic
from PyQt5.QtWidgets import *
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import numpy as np
import math

class Slice():

    def __init__(self , slice_orientation, image , center):
        #self.image = image
        self.im = vtk.vtkImageResliceMapper()
        self.ip = vtk.vtkImageProperty()
        self.ia = vtk.vtkImageSlice()
        self.plane = vtk.vtkPlane()
        self.slice_orientation = slice_orientation
        self.image = image
        self.center = center

        # initial interval
        self.thickness = 1

        if self.slice_orientation == 0:
            self.vec_plane = [0, 0, 1]
        elif self.slice_orientation == 1:
            self.vec_plane = [0, 1, 0]
        elif self.slice_orientation == 2:
            self.vec_plane = [1, 0, 0]

        self.im.SetInputConnection(self.image.GetOutputPort())
        self.plane.SetNormal(self.vec_plane)

        self.plane.SetOrigin(self.center)
        self.im.SetSlicePlane(self.plane)

        self.ip.SetColorWindow(2000)
        self.ip.SetColorLevel(1000)
        self.ip.SetAmbient(0.0)
        self.ip.SetDiffuse(1.0)
        self.ip.SetOpacity(1.0)
        self.ip.SetInterpolationTypeToLinear()

        self.ia.SetMapper(self.im)
        self.ia.SetProperty(self.ip)

class Volume():

    def __init__(self, DicomPath, opacityWindow, opacityLevel):
        self.image = None
        self.DicomPath = DicomPath
        self.opacityWindow = opacityWindow #4096
        self.opacityLevel = opacityLevel #2048
        self.volume =''

    def GetDimInfo(self):
        return self.dim

    def readDICOM(self):
        # GDCM Reader
        self.reader = sitk.ImageSeriesReader()
        paths = self.reader.GetGDCMSeriesFileNames( self.DicomPath )
        self.reader.SetFileNames( paths )
        img = sitk.ReadImage( paths )

        # Convert to vtkImage
        data = sitk.GetArrayFromImage( img )

        self.img_spacing = img.GetSpacing()
        img_cen = img.GetOrigin()

        img_data = data.astype('int16')
        img_string = img_data.tostring()
        self.dim = data.shape

        importer = vtk.vtkImageImport()
        importer.CopyImportVoidPointer(img_string, len(img_string))
        importer.SetNumberOfScalarComponents(1)

        extent = importer.GetDataExtent()

        importer.SetDataExtent(extent[0], extent[0] + self.dim[2] - 1,
                               extent[2], extent[2] + self.dim[1] - 1,
                               extent[4], extent[4] + self.dim[0] - 1)
        importer.SetWholeExtent(extent[0], extent[0] + self.dim[2] - 1,
                                extent[2], extent[2] + self.dim[1] - 1,
                                extent[4], extent[4] + self.dim[0] - 1)

        importer.SetDataSpacing( self.img_spacing[0], self.img_spacing[1], self.img_spacing[2])
        importer.SetDataOrigin(img_cen)

        self.center = [img_cen[0] + self.img_spacing[0] * 0.5 * (extent[0] + extent[0] + self.dim[2] - 1),
                       img_cen[1] + self.img_spacing[1] * 0.5 * (extent[2] + extent[2] + self.dim[1] - 1),
                       img_cen[2] + self.img_spacing[2] * 0.5 * ( extent[4] + extent[4] + self.dim[0] - 1)]

        self.image = importer

    def GetImage(self):
        return self.image

    def SetWindow(self, window):
        self.opacityWindow = window

    def SetLevel(self, level):
        self.opacityLevel = level

    def SetVolumeAttribute(self):
        # Volume
        colorFun = vtk.vtkColorTransferFunction()
        opacityFun = vtk.vtkPiecewiseFunction()
        property = vtk.vtkVolumeProperty()
        mapper = vtk.vtkSmartVolumeMapper()
        self.volume = vtk.vtkVolume()

        # Mapper
        mapper.SetInputConnection( self.image.GetOutputPort())
        mapper.SetBlendModeToMaximumIntensity()

        # property
        property.SetIndependentComponents(True)
        colorFun.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)

        opacityFun.AddSegment(self.opacityLevel - 0.5 * self.opacityWindow, 0.0,
                              self.opacityLevel + 0.5 * self.opacityWindow, 1.0)
        property.SetColor(colorFun)
        property.SetScalarOpacity(opacityFun)
        property.SetInterpolationTypeToLinear()

        # volume
        self.volume.SetMapper(mapper)
        self.volume.SetProperty(property)

        return self.volume


class MyQVTKRenderWindowInteractor(QVTKRenderWindowInteractor):

    def __init__(self, parent=None, wflags=QtCore.Qt.WindowFlags(), **kw):
        super().__init__(parent, wflags, **kw)

        self.istyle = vtk.vtkInteractorStyleTrackballCamera()
        self.sliceObj = ''
        self.volumeObj = ''
        self.ren = ''

        self.opacityWindow = 0
        self.opacityLevel = 0
        self.orientation = 0

        self.pickerX = 0
        self.pickerY = 0
        self.press = False

        coneSource = vtk.vtkConeSource()
        coneSource.CappingOn()
        coneSource.SetHeight(12)
        coneSource.SetRadius(5)
        coneSource.SetResolution(31)
        coneSource.SetCenter(6, 0, 0)
        coneSource.SetDirection(-1, 0, 0)

        coneMapper = vtk.vtkDataSetMapper()
        coneMapper.SetInputConnection(coneSource.GetOutputPort())

        self.redCone = vtk.vtkActor()
        self.redCone.PickableOff()
        self.redCone.SetMapper(coneMapper)
        self.redCone.GetProperty().SetColor(1, 0, 0)

        self.greenCone = vtk.vtkActor()
        self.greenCone.PickableOff()
        self.greenCone.SetMapper(coneMapper)
        self.greenCone.GetProperty().SetColor(0, 1, 0)

        self.picker = vtk.vtkVolumePicker()
        self.picker.SetTolerance(1e-6)
        self.picker.SetVolumeOpacityIsovalue(0.3)

    '''
    def SetOrientation(self, orientation):
        self.orientation = orientation
    '''

    def SetRenderer(self, ren):
        self.ren = ren

    def SetSliceObj(self, sliceObj):
        self.sliceObj = sliceObj

    def SetVolumeObj(self, volumeObj):
        #self.volume = volume
        self.volumeObj = volumeObj

    def move_plane(self, event):
        # calculate Hor/Ver Vector
        normalVector = self.sliceObj.vec_plane

        if self.sliceObj.slice_orientation == 0 :
            upVector = [0, -1, 0]
        elif  self.sliceObj.slice_orientation == 1 or self.sliceObj.slice_orientation == 2 :
            upVector = [0, 0, 1]

        newVec1 = [0] * 3
        vtk.vtkMath.Cross(normalVector, upVector, newVec1)
        newVec2 = [0] * 3
        vtk.vtkMath.Cross(normalVector, newVec1, newVec2)

        d = event.angleDelta().y()
        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

        if d >= 0:
            direction = 1
        else:
            direction = -1
        interval = self.sliceObj.thickness * direction

        # generate interval(thick) vector to axis
        interval_mat = [0] * 3
        interval_mat[self.sliceObj.slice_orientation] = 1 * interval

        if self.sliceObj.slice_orientation == 0:
            V = np.array([normalVector, newVec1, newVec2]).transpose()
        elif self.sliceObj.slice_orientation == 1:
            V = np.array([newVec1, normalVector, newVec2]).transpose()
        elif self.sliceObj.slice_orientation == 2:
            V = np.array([newVec1, newVec2, normalVector]).transpose()

        newXYZ = np.dot(V, interval_mat)
        self.sliceObj.plane.SetOrigin(self.sliceObj.plane.GetOrigin() + newXYZ)
        self.sliceObj.im.SetSlicePlane(self.sliceObj.plane)
        self.Render()

    def wheelEvent(self, e):
        d = e.angleDelta().y()
        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

        if self.sliceObj:
            self.move_plane(e)

        else:
            pass
            '''
            if d >= 0:
                e.type = QtCore.Qt.LeftButton
                self.mousePressEvent(self, e)


            else:
                e.type = QtCore.Qt.RightButton
                #self._Iren.MouseWheelBackwardEvent()
                #self.istyle.OnLeftButtonDown() #OnRightButtonDown() #OnLeftButtonDown()
                #self.mousePressEvent(e)
                self.mousePressEvent(self,e)
            '''

    def PointCone(self,actor, nx, ny, nz):
        actor.SetOrientation(0.0, 0.0, 0.0)
        n = math.sqrt(nx ** 2 + ny ** 2 + nz ** 2)
        if (nx < 0.0):
            actor.RotateWXYZ(180, 0, 1, 0)
            n = -n
        actor.RotateWXYZ(180, (nx + n) * 0.5, ny * 0.5, nz * 0.5)

    def mousePressEvent(self, e):

        if not self.sliceObj:
            pass

        if e.type() == QtCore.Qt.LeftButton:
            pass
        elif e.type() == QtCore.Qt.RightButton:
            self.press = True
            #self.pickerX = e.pos().x()
            #self.pickerY = e.pos().y()

            #ren = self.GetRenderWindow().GetRenderers()
            #iren = self.GetRenderWindow().GetInteractor()
            self.ren.AddViewProp(self.redCone)
            self.ren.AddViewProp(self.greenCone)




    def mouseMoveEvent(self, e):

        if self.sliceObj:
            pass

        else:
            if self.press == True:
                self.pickerX = e.pos().x()
                self.pickerY = self.height() - e.pos().y()
                # x,y = self.GetEventPosition()
                x, y = self.GetEventPosition()

                print('aaaaa', self.pickerX, self.pickerY)
                print('bbbb', x, y)
                '''
                coordinate = vtk.vtkCoordinate()
                coordinate.SetCoordinateSystemToDisplay()
                coordinate.SetValue(x,y,0)
                '''

                #renWin.HideCursor()
                #self.pickerX, self.pickerY = self.GetEventPosition()

                self.picker.Pick(self.pickerX, self.pickerY, 0, self.ren)
                p = self.picker.GetPickPosition()
                n = self.picker.GetPickNormal()

                self.redCone.SetPosition(p[0], p[1], p[2])
                self.PointCone(self.redCone, n[0], n[1], n[2])
                self.greenCone.SetPosition(p[0], p[1], p[2])
                self.PointCone(self.greenCone, -n[0], -n[1], -n[2])

                self.Render()


    def mouseReleaseEvent(self, e):

        if not self.sliceObj:
            pass

        self.press = False
        #self.EpotX = e.pos().x()
        #self.EpotY = e.pos().y()

        #2016.11.11 - Control event
        '''
        gap = self.EpotX - self.SpotX
        self.opacityWindow = self.opacityWindow - gap

        colorer = vtk.vtkImageMapToWindowLevelColors()
        #colorer.SetInput(self.image)
        colorer.SetInput(  self.volume.image )
        colorer.SetWindow(gap)
        #colorer.SetLevel(self.window_level)
        colorer.SetOutputFormatToRGB()
        colorer.Update()

        #print(gap)
        '''


class MainWindow(QMainWindow):

    def reset_camera(self, position, ren):  # 0: initial 3d view, 1: left, 2: right, 3: front, 4: back, 5: top, 6: bottom

        focal, position, viewup = self.__camera_positions_dict[position]
        cam1 = ren.GetActiveCamera()
        cam1.SetFocalPoint(focal)
        cam1.SetPosition(position)
        cam1.SetViewUp(viewup)
        ren.ResetCameraClippingRange()

    def __init__(self):
        super().__init__()
        self.frame = QFrame()
        self.gridLayout = QGridLayout()

        self.renAxial = vtk.vtkRenderer()
        self.renCoronal = vtk.vtkRenderer()
        self.renSagittal = vtk.vtkRenderer()
        self.renVolume = vtk.vtkRenderer()

        ObjVolume = Volume(r"/Users/scott/Dicom/1", 4096, 2048)
        ObjVolume.readDICOM()

        self.centralWidget = QWidget()
        self.vtkWidgetAxial = MyQVTKRenderWindowInteractor(self.frame)
        self.vtkWidgetCoronal = MyQVTKRenderWindowInteractor(self.frame)
        self.vtkWidgetSagittal = MyQVTKRenderWindowInteractor(self.frame)
        self.vtkWidgetVolume = MyQVTKRenderWindowInteractor(self.frame)

        self.vtkWidgetVolume.GetRenderWindow().AddRenderer(self.renVolume)
        self.irenVolume = self.vtkWidgetVolume.GetRenderWindow().GetInteractor()
        self.vtkWidgetVolume.SetRenderer( self.renVolume )


        self.vtkWidgetVolume.SetVolumeObj(ObjVolume)
        self.vtkWidgetAxial.SetVolumeObj(ObjVolume)
        self.vtkWidgetCoronal.SetVolumeObj(ObjVolume)
        self.vtkWidgetSagittal.SetVolumeObj(ObjVolume)

        sliceObj_Axial = Slice(0 , ObjVolume.image, ObjVolume.center)
        sliceObj_Sagittl = Slice(1, ObjVolume.image, ObjVolume.center)
        sliceObj_Coronal = Slice(2, ObjVolume.image, ObjVolume.center)

        self.vtkWidgetAxial.SetSliceObj(sliceObj_Axial)
        self.vtkWidgetCoronal.SetSliceObj(sliceObj_Sagittl)
        self.vtkWidgetSagittal.SetSliceObj(sliceObj_Coronal)

        self.gridLayout.addWidget(self.vtkWidgetAxial, 0, 0)
        self.gridLayout.addWidget(self.vtkWidgetCoronal, 0, 1)
        self.gridLayout.addWidget(self.vtkWidgetSagittal, 1, 0)
        self.gridLayout.addWidget(self.vtkWidgetVolume, 1, 1)

        volume = ObjVolume.SetVolumeAttribute( )

        self.resize(1000, 500)
        self.frame.setLayout(self.gridLayout)
        self.setCentralWidget(self.frame)
        self.renVolume.AddViewProp( volume )

        # Volume Camera
        cameraVolume = self.renVolume.GetActiveCamera()
        centerPos = volume.GetCenter()
        cameraVolume.SetFocalPoint(centerPos[0], centerPos[1], centerPos[2])
        cameraVolume.SetPosition(centerPos[0], centerPos[1] - 400, centerPos[2])
        cameraVolume.SetViewUp(0, 0, 1)

        image = ObjVolume.GetImage()
        extent = image.GetDataExtent()
        dim = ObjVolume.GetDimInfo()
        self.__camera_positions_dict = {

            0: ((centerPos[0],centerPos[1],centerPos[2]), (0, 0, (extent[0] + dim[2]-200 )), (0, -1, 0)),  # Axial
            1: ((centerPos[0],centerPos[1],centerPos[2]), (0, (extent[2] + dim[1] -200), 0), (0, 0, 1)),  # Coronal
            2: ((centerPos[0],centerPos[1],centerPos[2]), ((extent[4] + dim[0] + 200), 0, 0), (0, 0, 1)),  # Sagital
        }

        self.reset_camera( 0, self.renAxial )
        self.reset_camera( 1, self.renCoronal )
        self.reset_camera( 2, self.renSagittal )

        self.renAxial.AddViewProp( sliceObj_Axial.ia )
        self.vtkWidgetAxial.GetRenderWindow().AddRenderer( self.renAxial )
        self.irenAxial = self.vtkWidgetAxial.GetRenderWindow().GetInteractor()

        self.renCoronal.AddViewProp( sliceObj_Sagittl.ia )
        self.vtkWidgetCoronal.GetRenderWindow().AddRenderer( self.renCoronal )
        self.irenCoronal = self.vtkWidgetCoronal.GetRenderWindow().GetInteractor()

        self.renSagittal.AddViewProp( sliceObj_Coronal.ia )
        self.vtkWidgetSagittal.GetRenderWindow().AddRenderer( self.renSagittal )
        self.irenSagittal = self.vtkWidgetSagittal.GetRenderWindow().GetInteractor()

        self.vtkWidgetVolume.GetRenderWindow().GetRenderers()
        self.show()
        self.irenAxial.Initialize()
        self.irenCoronal.Initialize()
        self.irenSagittal.Initialize()
        self.irenVolume.Initialize()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
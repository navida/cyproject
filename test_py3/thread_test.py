from PyQt5.QtCore import QObject, QThread, QTimer, pyqtSignal, pyqtSlot
import time
import _qapp


class Main(QObject):

    class Worker(QThread):
        sig_update = pyqtSignal(object)

        def __init__(self, parent, fun, *args, **kwds):
            super().__init__(*args, **kwds)
            self.parent = parent
            self.n = 0

            setattr(self, "run", fun)

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.worker = self.Worker(self, self._run)
        self.n = 0
        self.worker.sig_update.connect(self.update)
        self.worker.finished.connect(self.finish)

        self.worker.start()

    @pyqtSlot(object)
    def update(self, msg):
        print("This is Update@@@@@@@@@@@@@@@", msg, QThread.currentThread())

    @pyqtSlot()
    def finish(self):
        print("This is Finish###############", QThread.currentThread())

    def _run(self):
        for i in range(10):
            print("This is Worker Thread", self.worker.n, QThread.currentThread())
            self.worker.sig_update.emit("Hello! : %d" % self.worker.n)
            self.worker.n += 1
            QThread.sleep(3)


if __name__ == "__main__":
    main = Main()

    _qapp.exec_()


from asyncio import coroutine


@coroutine
def fun1():
    a = yield
    b = yield
    return a+b


@coroutine
def fun2():
    a = yield
    b = yield
    return a*b


@coroutine
def fun3():
    a = yield from fun1()
    b = yield from fun2()
    return a, b

try:
    f = fun3()
    f.send(None)

    f.send(10)
    f.send(7)

    f.send(2)
    f.send(3)

except StopIteration as s:
    print(s.value)

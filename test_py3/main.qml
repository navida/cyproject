import QtQuick 2.0
import QtQuick.Controls 1.3

import Test 1.0

Rectangle {
    width: 400
    height: 400
    visible: true

    MyItem {
        anchors.fill: parent
    }

    Button {
        x: 100
        y: 100
        text: "Button"
    }
}
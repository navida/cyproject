import vtk
from vtk.util import numpy_support


# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/daniele/moving')
# reader.SetDirectoryName(r'/Users/scott/Dicom/daniele/moving')
reader.Update()

p = vtk.vtkImageProperty()
m, a = vtk.vtkImageResliceMapper(), vtk.vtkImageSlice()
plane = vtk.vtkPlane()

# data extent
(xMin, xMax, yMin ,yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
(xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
(x0, y0, z0) = reader.GetOutput().GetOrigin()

# center of volume
center = [x0 + xSpacing * 0.5 * (xMin + xMax),
          y0 + ySpacing * 0.5 * (yMin + yMax),
          z0 + zSpacing * 0.5 * (zMin + zMax)]

# set cutting plane
plane.SetOrigin(center)
plane.SetNormal(0, 0, 1)
viewUp = [0, -1, 0]
i, j, k = 0, 0, -zMax

# mapper
m.SetInputConnection(reader.GetOutputPort())
m.SetSlicePlane(plane)

# property
p.SetColorWindow(2000)
p.SetColorLevel(800)
p.SetAmbient(0.0)
p.SetDiffuse(1.0)
p.SetOpacity(1.0)
p.SetInterpolationTypeToLinear()

# actor
a.SetMapper(m)
a.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
istyle = vtk.vtkInteractorStyleUser()
iren.SetInteractorStyle(istyle)

# camera
camera = ren.GetActiveCamera()
camera.ParallelProjectionOn()
ren.ResetCameraClippingRange()
camera.SetParallelScale(0.5 * (yMax-yMin+1)*ySpacing)
camera.SetFocalPoint(center)
camera.SetPosition(center[0]+i, center[1]+j, center[2]+k)
camera.SetViewUp(viewUp)

# Picking
picker = vtk.vtkVolumePicker()
picker.SetTolerance(1e-6)
picker.SetVolumeOpacityIsovalue(0.1)


def _left_button_press(_iren, _e):
    x, y = _iren.GetEventPosition()
    picker.Pick(x, y, 0, ren)
    _p = picker.GetPickPosition()
    _n = picker.GetPickNormal()
    print('position : ', _p)
    print('normal : ', _n)

    # method 1
    # pos = _get_intensity_by_point(*_p)
    # print('Pos', pos)
    # method 2
    arr = _get_intensity_array()
    print('Possss!!', arr[int(_p[0])][int(_p[1])][int(_p[2])])


def _mouse_wheel(_iren, _e):
    if _e == r'MouseWheelForwardEvent':
        d = 1
    else:
        d = -1

    center[2] += d
    plane.SetOrigin(center)
    iren.Render()


# Mouse Event
iren.AddObserver('LeftButtonPressEvent', _left_button_press)
iren.AddObserver('MouseWheelForwardEvent', _mouse_wheel)
iren.AddObserver('MouseWheelBackwardEvent', _mouse_wheel)


def _get_intensity_by_point(_x, _y, _z):
    # Piece of cake.. -.-;;
    vtkimage = reader.GetOutput()
    pos = vtkimage.GetScalarComponentAsDouble(int(_x), int(_y), int(_z), 0)
    return pos


def _get_intensity_array():
    vtkimage = reader.GetOutput()
    narray = numpy_support.vtk_to_numpy(vtkimage.GetPointData().GetScalars())
    # order F means Fortran : column major
    narray = narray.reshape(vtkimage.GetDimensions(), order='F')
    return narray


# add actor
ren.AddViewProp(a)

# render start
renWin.Render()
iren.Initialize()
iren.Start()
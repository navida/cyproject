from PyQt5.QtWidgets import QWidget, QSizePolicy
from PyQt5.QtCore import QObject, Qt, QSize, QTimer
from PyQt5.QtGui import QImage, QPainter
from vtk import vtkRenderer, vtkRenderWindow, vtkRenderWindowInteractor, vtkUnsignedCharArray
from vtk import vtkConeSource, vtkPolyDataMapper, vtkActor
import _qapp


class MyQVTk(QWidget):

    def __init__(self, *args, **kwds):
        # create qt-level widget
        QWidget.__init__(self, None, Qt.WindowFlags() | Qt.MSWindowsOwnDC)

        # self.setSizePolicy(QSizePolicy(*([QSizePolicy.Expanding] * 2)))
        # self.setAttribute(Qt.WA_OpaquePaintEvent)  # prevent Qt's background fill
        self.setMouseTracking(True)  # get all mouse events
        # self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))

        self.img = QImage()
        self.__last_image_num, self.__image_num = 0, 0

        self._RenderWindow = vtkRenderWindow()
        self._Iren = vtkRenderWindowInteractor()

        self.ren = vtkRenderer()
        self._RenderWindow.AddRenderer(self.ren)
        self._Iren.SetRenderWindow(self._RenderWindow)

        self._RenderWindow.AddObserver('EndEvent', self.__on_image_rendered)


        self._RenderWindow.OffScreenRenderingOn()

        # Source
        s = vtkConeSource()
        # Mapper
        m = vtkPolyDataMapper()
        m.SetInputConnection(s.GetOutputPort())
        # Actor
        a = vtkActor()
        a.SetMapper(m)
        self.ren.AddActor(a)

        # self._RenderWindow.Initialize()
        self._RenderWindow.Start()
        # self._RenderWindow.Render()

        # def _do():
        #     self.resize(800, 600)
        #     self._RenderWindow.SetSize(800, 600)
        #     self._RenderWindow.UpdateContext()
        # QTimer.singleShot(1000, _do)

    # def show(self):
    #     # Render Start
    #     super(self, QWidget).show()
    #     self._RenderWindow.Render()
    #     self._Iren.Initialize()
    #     self._Iren.Start()


    # def closeEvent(self, e):
    #     super().closeEvent(e)
    #     self.closeEvent(e)

    def finalize(self):
        self.Finalize()

    def sizeHint(self):
        return QSize(400, 400)

    def __on_image_rendered(self, _o, _e):
        # print('    1. rendered!!!')
        self.__image_num += 1

        # self.update()
        self.repaint()

    def paintEvent(self, _e):
        # print('    2. paint ', end='')
        if self.__last_image_num < self.__image_num:
            # print('+ capture ', end='')
            w, h = self._RenderWindow.GetSize()

            if self.img.width() != w or self.img.height() != h:
                self.img = QImage(w, h, QImage.Format_RGB32)

            b = self.img.bits()  # sip.voidptr object
            b.setsize(self.img.byteCount())  # enabling Python buffer protocol

            va = vtkUnsignedCharArray()
            # va.SetVoidArray(b, self.img.byteCount(), 1)
            va.SetVoidArray(b, w * h * 4, 1)
            self._RenderWindow.GetRGBACharPixelData(0, 0, w - 1, h - 1, 1, va)

            self.img = self.img.rgbSwapped()

            self.__last_image_num = self.__image_num

            print("RENDER IMG", self.img.width(), self.img.height())
            self.img.save("FFFFFFFFFFF.png")

        # print(_e.type(), _e.rect())

        p = QPainter(self)

        # Do transformation. This makes img.mirrored() avoided.
        p.scale(1, -1)
        p.translate(0, -self.height())

        p.drawImage(0, 0, self.img)

    def resizeEvent(self, e):
        # NOTE Even though we request view.resize() here, view.resizeEvent()
        #   will not be triggered, because view is not shown. Hence, we do
        #   necessary things here.
        # print('  resize', e.size())
        w, h = self.width(), self.height()

        self.resize(w, h)
        self._RenderWindow.SetSize(w, h)
        self._Iren.SetSize(w, h)
        self._Iren.ConfigureEvent()
        self._RenderWindow.UpdateContext()

        # self._RenderWindow.Render()


if __name__ == "__main__":
    w = MyQVTk()
    w.show()

    _qapp.exec_()
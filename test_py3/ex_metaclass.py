from PyQt5 import QtCore


class my_meta(type):
# class my_meta(type(QtCore.QObject)):
    def __new__(cls, name, bases, namespace):
        namespace['var1'] = [1,2,3,4,5]
        namespace['test2'] = lambda _self: print("here is test2")
        # namespace['my_slot1'] = QtCore.pyqtSlot(int)(lambda _self, _i: print("here is slot1", _i))

        print("name :", name)
        print("bases :", bases)
        print("namespace :", namespace)

        return super(my_meta, cls).__new__(cls, name, bases, namespace)


class A(metaclass=my_meta):
# class A(QtCore.QObject, metaclass=my_meta):

    # sig1 = QtCore.pyqtSignal(int)

    def __init__(self):
        # super().__init__()
        print("Hello?")
        # self.sig1.connect(self.my_slot1)

    def test1(self):
        print("here is test1")
        # self.sig1.emit(10)


if __name__ == '__main__':
    # print("!!!", type(A))
    # a = A()
    # print(a.var1)
    # a.test1()
    # a.test2()
    pass

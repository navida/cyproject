from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QObject, QUrl
from PyQt5.QtQuick import QQuickView
from PyQt5 import QtQml
import sys, os


class InVisibleItem(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        print("Hello")


app = QGuiApplication(sys.argv)

QtQml.qmlRegisterType(InVisibleItem, "cyhub", 1, 0, "InVisibleItem")
view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'app2.qml')))
view.show()

app.exec_()
import cyCafe


def get_data(_byte_str):
    try:
        return _byte_str.decode('UTF-8')
    except UnicodeDecodeError:
        return _byte_str.decode('ISO-8859-1')


def eval_dcm_info(_L):
    for l in _L:
        for key in l:
            l[key] = get_data(l[key])



if __name__ == "__main__":
    r = cyCafe.cyDicomReader()
    L = list()

    # if r.analyze_dicom('/Users/scott/Dicom/test_2study/'):
    # if r.analyze_dicom('/Users/scott/Dicom/HeadCrack/S0000000008/S0000000008/'):
    if r.analyze_dicom('/Users/scott/Dicom/OD3DData/20170629/S0000000031/'):
        # key's type is str, value's type is byte
        r.get_dcm_info(L, True)
        eval_dcm_info(L)

        for l in L:
            print(l)

        global mark
        mark = []
        global tmp
        tmp = 0

        def key_study_id(x):
            global tmp
            global mark
            if tmp != x['StudyInstanceUID']:
                tmp = x['StudyInstanceUID']
                mark.append(tmp)
            return x['StudyInstanceUID']

        def key_filename(x):
            return x['FileName']

        L = sorted(L, reverse=False, key=key_study_id)

        for l in L:
            print(l)
        print(mark)

import os
import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QGuiApplication, QSurface, QSurfaceFormat, QOpenGLContext, QOffscreenSurface, \
    QOpenGLFramebufferObject
from PyQt5.QtQuick import QQuickView, QQuickWindow, QQuickRenderControl
from PyQt5.QtQml import QQmlComponent, QQmlEngine

from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt
from PyQt5.QtGui import QImage
from PyQt5.Qt import QObject, QApplication


class MyRenderControl(QQuickRenderControl):
    def __init__(self, w, *args, **kwds):
        super().__init__(*args, **kwds)
        self.window = w

    def renderWindow(self, QPoint):
        return self.window


class MyQQuickWindow(QQuickView):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.setSurfaceType(QSurface.OpenGLSurface)

        format_surface = QSurfaceFormat()
        format_surface.setDepthBufferSize(16)
        format_surface.setStencilBufferSize(4)
        self.setFormat(format_surface)

        self.ctx = QOpenGLContext()
        self.ctx.setFormat(format_surface)
        self.ctx.create()

        self.offscreen_surface = QOffscreenSurface()
        self.offscreen_surface.setFormat(self.ctx.format())

        self.offscreen_surface.create()

        self.render_control = MyRenderControl(self)

        qml_engine = QQmlEngine()
        if not qml_engine.incubationController():
            qml_engine.setIncubationController(self.incubationController())
        self.setResizeMode(QQuickView.SizeRootObjectToView)
        self.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'app2.qml')))

        self.sceneGraphInitialized.connect(self.create_fbo, Qt.DirectConnection)
        self.sceneGraphInvalidated.connect(lambda: print('eeeee'))
        self.render_control.renderRequested.connect(lambda: print('render requested'))
        self.render_control.sceneChanged.connect(lambda: print('scene changed'))
        self.screenChanged.connect(lambda: print('screen changed'))

        # if self.ctx.makeCurrent(self.offscreen_surface):
        #     dpr = self.devicePixelRatio()
        #     # fbo = QOpenGLFramebufferObject(self.size() * dpr)
        #     fbo = QOpenGLFramebufferObject(self.size()*dpr, QOpenGLFramebufferObject.NoAttachment)
        #     self.setRenderTarget(fbo)
        #     self.ctx.doneCurrent()
        #
        #     self.render_control.polishItems()
        #     self.render_control.sync()
        #     self.render_control.render()
        #     self.resetOpenGLState()
        #     QOpenGLFramebufferObject.bindDefault()



    def create_fbo(self):
        print('create fbo!')
        dpr = self.devicePixelRatio()
        self.fbo = QOpenGLFramebufferObject(self.size()*dpr, QOpenGLFramebufferObject.NoAttachment)
        self.setRenderTarget(self.fbo)



    def resize_fbo(self):
        if self.ctx.makeCurrent(self.offscreen_surface):
            self.create_fbo()
            self.ctx.doneCurrent()
            # self.update
            # self.render()
    #
    # def render(self):
    #     if not self.ctx.makeCurrent(self.offscreen_surface):
    #         return
    #
    #     self.render_control.polishItems()
    #     self.render_control.sync()
    #     self.render_control.render()
    #
    #     self.resetOpenGLState()
    #     QOpenGLFramebufferObject.bindDefault()
    #
    #     # self.ctx.functions

        # if (m_rootItem & & m_context->makeCurrent(m_offscreenSurface)) {
        #     delete m_fbo;
        #     createFbo();
        #     m_context->doneCurrent();
        #     updateSizes();
        #     render();

    def resizeEvent(self, e):
        print('resize')
        self.resize_fbo()






class GrabWindow(QQuickView):

    changeImage = pyqtSignal(QImage)

    def __init__(self, _parent=0, *args, **kwds):
        super().__init__(*args, **kwds)

        # self.setSurfaceType(QSurface.OpenGLSurface)

        self.fbo = None

        self.setClearBeforeRendering(False)
        self.setPosition(100, 100)

        self.setSurfaceType(QSurface.OpenGLSurface)

        format_surface = QSurfaceFormat()
        format_surface.setDepthBufferSize(16)
        format_surface.setStencilBufferSize(4)
        self.setFormat(format_surface)

        self.ctx = QOpenGLContext()
        self.ctx.setFormat(format_surface)
        self.ctx.create()

        self.offscreen_surface = QOffscreenSurface()
        self.offscreen_surface.setFormat(self.ctx.format())

        self.offscreen_surface.create()

        self.render_control = MyRenderControl(self)

        super().afterRendering.connect(self.afterRendering, Qt.DirectConnection)
        super().beforeRendering.connect(self.beforeRendering, Qt.DirectConnection)

    @pyqtSlot()
    def afterRendering(self):
        if self.fbo is not None:
            # self.changeImage.emit(self.fbo.toImage())
            print('ddddddddddd', self.fbo.width(), self.fbo.height())
            fboImage = self.fbo.toImage()
            print(fboImage.save('captured.png'), fboImage.width(), fboImage.height())

    @pyqtSlot()
    def beforeRendering(self):
        if self.fbo is None:
            self.fbo = QOpenGLFramebufferObject(self.size(), QOpenGLFramebufferObject.NoAttachment)
            self.setRenderTarget(self.fbo)

            # dpr = self.devicePixelRatio()
            # fbo = QOpenGLFramebufferObject(self.size() * dpr, QOpenGLFramebufferObject.NoAttachment)
            # self.setRenderTarget(fbo)
            # self.ctx.doneCurrent()







if __name__ == '__main__':
    app = QApplication(sys.argv)

    view = QQuickView()
    # view = GrabWindow()
    view.setResizeMode(QQuickView.SizeRootObjectToView)
    view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'app2.qml')))
    # view.setFlags(Qt.Popup)

    # def _func(_img):
    #     print('after rendering!!!', _img)
    #     t = _img.save('captured.png')
    # view.changeImage.connect(_func)

    view.show()
    # view.setVisible(False)
    # view.hide()

    sys.exit(app.exec_())

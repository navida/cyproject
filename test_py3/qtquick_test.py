from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QRectF, Qt, QUrl, QByteArray, QBuffer, QIODevice, QSize, QTimer, QFile
from PyQt5.QtGui import QColor, QGuiApplication, QPainter, QPen, QImage
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import QQuickPaintedItem, QQuickView


class MyQQuickItem(QQuickPaintedItem):

    sigText = pyqtSignal()
    sigCapture = pyqtSignal()

    @pyqtProperty(str)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @pyqtProperty(QColor)
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = QColor(color)

    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self._name = ''
        self._color = QColor()

        self.setAcceptedMouseButtons(Qt.AllButtons)

    @pyqtSlot()
    def on_component_completed(self):
        print('connect Sig, Slot')
        self.sigCapture.connect(pyqtSlot()(self.capture))

    def paint(self, painter):
        print('this is paint')
        painter.setPen(QPen(self._color, 2))
        painter.setRenderHints(QPainter.Antialiasing, True)

        rect = QRectF(0, 0, self.width(), self.height()).adjusted(1, 1, -1, -1)
        painter.drawPie(rect, 90 * 16, 290 * 16)

        # self.sig_capture.emit()

        ##########################

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # self.wimage.save(buf, 'JPG')
        # self.wimage.save('cccccc.jpg')
        # img.save('ccc.png')

        # QTimer.singleShot(0, self.capture)

        # self.sigCapture.connect(pyqtSlot()(lambda: print('sdfsdfsdf')))

        self.sigCapture.emit()

    def mouseMoveEvent(self, e):
        print('m_move', e)

    def mousePressEvent(self, e):
        print('m_press', e)
        self.sigText.emit()

    @pyqtSlot()
    def capture(self):
        print('captured!')
        win = self.window()
        img = win.grabWindow()
        # img.save('captured.png')

        # Encode.
        ba = QByteArray()
        buf = QBuffer(ba)
        buf.open(QIODevice.WriteOnly)
        img.save(buf, 'JPG')
        img.save('captured.png')

        print('captured!')


if __name__ == '__main__':
    import os
    import sys

    app = QGuiApplication(sys.argv)

    qmlRegisterType(MyQQuickItem, "MyItems", 1, 0, "MyQQuickItem")

    view = QQuickView()
    view.setResizeMode(QQuickView.SizeRootObjectToView)
    view.setSource(
            QUrl.fromLocalFile(
                    os.path.join(os.path.dirname(__file__), 'app.qml')))
    view.show()

    sys.exit(app.exec_())


import numpy as np
import vtk
import vtk.numpy_interface.dataset_adapter as dsa
import matplotlib.pyplot as plt


BACKGOUND_LEVEL = -10000


def get_slice(img_src, o):
    """
    o: slice origin
    ux, uy, uz: slice axes
    """
    # Sample slice.
    reslice = vtk.vtkImageReslice()
    reslice.SetInputConnection(img_src.GetOutputPort())
    reslice.SetOutputDimensionality(2)
    reslice.SetResliceAxesOrigin(*o)
    reslice.SetInterpolationModeToLinear()
    reslice.SetBackgroundLevel(BACKGOUND_LEVEL)  # TODO
    reslice.Update()

    # Return numpy array.
    O = reslice.GetOutput()
    origin, spacing = O.GetOrigin()[:2], O.GetSpacing()[:2]
    A = dsa.WrapDataObject(O).GetPointData().GetArray(0)
    d = O.GetDimensions()
    return A.reshape([d[1], d[0]]), origin, spacing


if __name__ == "__main__":

    MODE = 4

    if MODE == 1:
        """
        Tutorial 1
        How to use the matplotlib
        How to draw 1-D and 2-D polynomial graph
        """
        plt.figure(1)
        _X = np.linspace(-99, 99)
        fn1 = np.poly1d([0.5, 100])              # y = 0.5x + 100 (line)
        plt.plot(_X, fn1(_X), 'r')
        fn2 = np.poly1d([0.02, 0, 0])           # y = 0.02x^2 (parabola)
        plt.plot(_X, fn2(_X), '--b')
        plt.legend(["y = 0.5x + 100 (Line)", "y = 0.02x^2     (Parabola)"])
        plt.axis('equal')
        plt.autoscale(False)

    elif MODE == 2:
        """
        Tutorial 2
        How to polynomial fitting (Least Squares Fitting)
        """
        plt.figure(1)
        _X = np.linspace(0, 500)
        _PX, _PY = np.random.rand(2, 20)
        _PX, _PY = _PX * 500, _PY * 200
        plt.plot(_PX, _PY, 'ro', ms=1.5)
        plt.axis('equal')
        plt.autoscale(False)

        _N = [1, 2, 5]
        for n in _N:
            # Fit polynomial
            coefficient = np.polyfit(_PX, _PY, n)
            fn = np.poly1d([*coefficient])
            plt.plot(_X, fn(_X))
        plt.legend(["random data"] + ["%d-d polynomial" % n for n in _N])

    elif MODE == 3:
        """
        Tutorial 3
        Advanced Tip!!
        How to access dicom raw data
        How to apply polynomial fit(Least Squares Fitting) on dicom
        """
        dicom, o, D0 = '/Users/scott/Dicom/1', [110, 110, 105], 500
        src = vtk.vtkDICOMImageReader()
        src.SetDirectoryName(dicom)
        src.Update()

        S, origin, spacing = get_slice(src, o)  # vtkImageData
        y, x = (S > D0).nonzero()

        plt.figure(1)
        plt.imshow(S, vmin=-3000, vmax=3000, origin='lower', cmap='gray')
        plt.plot(x, y, "bo", ms=0.5)
        plt.axis('equal')
        plt.autoscale(False)

        _N = [1, 2, 6]
        _X = np.linspace(0, S.shape[1])
        for n in _N:
            # Fit polynomial
            k = np.polyfit(x, y, n)
            fn = np.poly1d([*k])
            plt.plot(_X, fn(_X))
        plt.legend(["Intensity >= %d" % D0] + ["%d-d polynomial" % n for n in _N])

    elif MODE == 4:
        """
        Tutorial 4
        Advanced Tip!!
        """
        dicom, o, D0 = '/Users/scott/Dicom/1', [110, 110, 105], 500
        src = vtk.vtkDICOMImageReader()
        src.SetDirectoryName(dicom)
        src.Update()

        S, origin, spacing = get_slice(src, o)  # vtkImageData
        y, x = (S > D0).nonzero()

        plt.figure(1)
        plt.imshow(S, vmin=-3000, vmax=3000, origin='lower', cmap='gray')
        plt.plot(x, y, "bo", ms=0.5)
        plt.axis('equal')
        plt.autoscale(False)

        y, x = (S[220:-1] > D0).nonzero()
        y += 220
        plt.plot(x, y, "go", ms=0.5)

        _N = [1, 2, 6]
        _X = np.linspace(0, S.shape[1])
        for n in _N:
            # Fit polynomial
            k = np.polyfit(x, y, n)
            fn = np.poly1d([*k])
            plt.plot(_X, fn(_X))
        plt.legend(["Intensity >= %d" % D0, "and Y Pos >= 220"] + ["%d-d polynomial" % n for n in _N])

    plt.title("Tutorial %d" % MODE)
    plt.show()

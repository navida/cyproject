import sys
import vtk
from PyQt5 import QtCore, QtGui
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from PyQt5.QtWidgets import *

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        # self.setMouseTracking(True)  # get all mouse events

        self.frame = QFrame()

        self.vl = QVBoxLayout()
        self.vtkWidget = QVTKRenderWindowInteractor()
        self.vl.addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()

        self.vtkWidget._Iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

        # Create source
        source = vtk.vtkSphereSource()
        source.SetCenter(0, 0, 0)
        source.SetRadius(5.0)

        # Create a mapper
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(source.GetOutputPort())

        # Create an actor
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        self.ren.AddActor(actor)

        self.ren.ResetCamera()

        # w = QWidget()
        # w.setLayout(self.vl)
        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)


        self.show()
        self.iren.Initialize()
        # self.vtkWidget.Start()

    def wheelEvent(self, QWheelEvent):
        print('wheel')


# class Main(QMainWindow):
#     def __init__(self):
#         super().__init__()
#
#         self.setCentralWidget(self.frame)




if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()

    sys.exit(app.exec_())
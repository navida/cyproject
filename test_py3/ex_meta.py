class my_meta(type):
    def __new__(cls, name, bases, namespace):

        namespace['test_function2'] = lambda _self: print("test222")

        print("name :", name)
        print("bases :", bases)
        print("namespace :", namespace)
        return super(my_meta, cls).__new__(cls, name, bases, namespace)


class A(metaclass=my_meta):

    def __init__(self):
        print("Hello?")

    def test_function(self):
        print('test1')


class B(metaclass=my_meta):
    def __init__(self):
        print("Hello?")

    def test_function(self):
        print('test1')

class C(metaclass=my_meta):
    def __init__(self):
        print("Hello?")

    def test_function(self):
        print('test1')


# a = A()
# a.test_function2()
# b = B()
pass
from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QRectF, Qt, QUrl, QByteArray, QBuffer, QIODevice, \
    QSize, QTimer, QFile, QObject, QPoint
from PyQt5.QtGui import QColor, QGuiApplication, QPainter, QPen, QImage, QOpenGLFramebufferObject, \
    QOpenGLFramebufferObjectFormat, QWindow, QSurface, QSurfaceFormat, QOpenGLContext, QOffscreenSurface, QOpenGLContext
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import QQuickPaintedItem, QQuickItem, QQuickView, QQuickRenderControl, QQuickWindow, QQuickFramebufferObject


class MyQQuickItem(QQuickPaintedItem):

    sigText = pyqtSignal()
    sigCapture = pyqtSignal()

    @pyqtProperty(str)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @pyqtProperty(QColor)
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = QColor(color)

    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self._name = ''
        self._color = QColor()

        self.setAcceptedMouseButtons(Qt.AllButtons)

        # self.fbo = None
        # print('render target (prev) : ', self.renderTarget())
        # self.setRenderTarget(QQuickPaintedItem.FramebufferObject)
        # print('render target (after) : ', self.renderTarget())
        # fbo = QQuickFramebufferObject(self)
        # print(fbo)
        # print(fbo)

    @pyqtSlot()
    def on_component_completed(self):
        print('connect Sig, Slot')
        self.sigCapture.connect(self.capture)

        self.tm = QTimer()
        self.tm.timeout.connect(self.capture)
        self.tm.start(3000)

    def paint(self, painter):
        print('this is paint')
        painter.setPen(QPen(self._color, 2))
        painter.setRenderHints(QPainter.Antialiasing, True)

        rect = QRectF(0, 0, self.width(), self.height()).adjusted(1, 1, -1, -1)
        painter.drawPie(rect, 90 * 16, 290 * 16)

        self.sigCapture.emit()

        ##########################

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # self.wimage.save(buf, 'JPG')
        # self.wimage.save('cccccc.jpg')
        # img.save('ccc.png')

        # QTimer.singleShot(0, self.capture)

        # self.sigCapture.connect(pyqtSlot()(lambda: print('sdfsdfsdf')))

        # self.sigCapture.emit()

    def mouseMoveEvent(self, e):
        print('m_move', e)

    def mousePressEvent(self, e):
        print('m_press', e)
        # self.sigText.emit()

    def test_fun(self):
        print('this is test function!!', self.window().geometry())

    @pyqtSlot()
    def capture(self):
        print('captured!')
        print('w, h of qquicpainteditem : ', self.width(), self.height())
        win = self.window()
        print('w, h of qquickview : ', win.width(), win.height())
        img = win.grabWindow()
        print('w, h of qimage : ', img.width(), img.height())
        img.save('captured.png')

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # img.save(buf, 'JPG')
        # img.save('captured.png')
        #
        # print('captured!')


# class RenderControl(QQuickRenderControl):
#     def __init__(self, w, *args, **kwds):
#         super().__init__(w, *args, **kwds)
#         print(w)
#         self.win = w
#
#     def renderWindow(self, offset):
#         # if offset:
#         #     offset = QPoint(0, 0);
#
#         return self.win
#

class MyQQuickView(QWindow):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        # self.fbo = None

        # self.renderControl = RenderControl(self)
        # self.view = QQuickWindow(renderControl=self.renderControl)

        self.view = QQuickView()
        self.view.setResizeMode(QQuickView.SizeRootObjectToView)
        self.view.setSource(
            QUrl.fromLocalFile(
                os.path.join(os.path.dirname(__file__), 'app.qml')))

        self.view.beforeRendering.connect(self.on_before_rendering)
        self.view.afterRendering.connect(self.on_after_rendering)
        self.view.sceneGraphInitialized.connect(self.createFbo)

        self.rootItem = self.view.rootObject()
        # rootItem.setFlag(QQuickItem.ItemHasContents)
        # rootItem.update()

        # self.view.show()

        def _do_offscreen():
            # self.view.setVisibility(QWindow.Hidden)
            self.view.show()
            self.view.setVisibility(QWindow.Hidden)
            # self.view.setVisible(False)
            # self.view.hide()
        QTimer.singleShot(0, _do_offscreen)

        self.show()


        # self.view.setVisibility(QWindow.Hidden)
        # self.view.hide()
        # self.view.setVisible(True)
        # self.view.show()

        # if self.fbo is None:
        # self.surface_format = QSurfaceFormat()
        # self.surface_format.setDepthBufferSize(16)
        # self.surface_format.setStencilBufferSize(8)
        # self.setFormat(self.surface_format)

        # self.context = QOpenGLContext(self.view)
        # self.context.setFormat(self.surface_format)
        # self.context.create()
        # self.context.makeCurrent(self.view)

        # self.offscreenSurface = QOffscreenSurface()
        # self.offscreenSurface.setFormat(self.context.format())
        # self.offscreenSurface.create()

        # self.format = QOpenGLFramebufferObjectFormat()
        # self.format.setAttachment(QOpenGLFramebufferObject.NoAttachment)
        # self.fbo = QOpenGLFramebufferObject(self.view.size()*self.view.devicePixelRatio(), self.format)
        # self.view.setRenderTarget(self.fbo)

    @pyqtSlot()
    def on_before_rendering(self):
        # print('before rendering!')

        # if self.fbo is None:
        #     format = QOpenGLFramebufferObjectFormat()
        #     format.setAttachment(QOpenGLFramebufferObject.CombinedDepthStencil)
        #     self.fbo = QOpenGLFramebufferObject(self.view.size()*self.view.devicePixelRatio(), format)

            # self.fbo = QOpenGLFramebufferObject(self.view.size())
            # self.view.setRenderTarget(self.fbo.data())
        pass

    @pyqtSlot()
    def on_after_rendering(self):
        # print('after rendering!')
        pass

    def resizeEvent(self, e):
        print('resize!!!!!!!!!!!!!!!!!!!!!!!!!1', e.size())
        rootItem = self.view.rootObject()
        rootItem.setWidth(e.size().width())
        rootItem.setHeight(e.size().height())

        for i in rootItem.childItems():
            if isinstance(i, MyQQuickItem):
                i.test_fun()

        self.view.resize(e.size())
        self.resize(e.size())
        self.view.resizeEvent(e)
        self.view.update()

        # self.win.resizeEvent(e)

        # self.view.setWidth(e.size().width())
        # self.view.setHeight(e.size().height())

        # self.view.contentItem().setWidth(e.size().width())
        # self.view.contentItem().setHeight(e.size().height())

        # self.view.setTextureSize(e.size())
        # self.view.update()
        # self.resizeFbo()

        super().resizeEvent(e)

    def resizeFbo(self):
        self.createFbo()

    def createFbo(self):
        print('create fbo')
        # fbo_format = QOpenGLFramebufferObjectFormat()
        # fbo_format.setAttachment(QOpenGLFramebufferObject.NoAttachment)
        # self.fbo = QOpenGLFramebufferObject(self.view.size() * self.view.devicePixelRatio(), fbo_format)
        # self.view.setRenderTarget(self.fbo)


if __name__ == '__main__':
    import os
    import sys

    app = QGuiApplication(sys.argv)

    qmlRegisterType(MyQQuickItem, "MyItems", 1, 0, "MyQQuickItem")

    # view = QQuickView()
    v = MyQQuickView()
    # view = v.view
    # view.setResizeMode(QQuickView.SizeRootObjectToView)
    # view.setSource(
    #         QUrl.fromLocalFile(
    #                 os.path.join(os.path.dirname(__file__), 'app.qml')))
    # view.show()

    # rootItem = view.rootObject()
    # rootItem.setFlag(QQuickItem.ItemHasContents)
    # rootItem.update()

    sys.exit(app.exec_())


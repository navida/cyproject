import vtk
from vtk.util import numpy_support as ns
from vtk.numpy_interface import dataset_adapter as dsa
import numpy
import cyCafe


# b = poly_data.GetBounds()
# points = vtk.vtkBoundedPointSource()
# points.SetNumberOfPoints(10000)
# points.ProduceRandomScalarsOn()
# points.ProduceCellOutputOff()
# points.Update()

STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/GroupGuide_1.stl"
reader = vtk.vtkSTLReader()
reader.SetFileName(STL_PATH)
reader.Update()

cube = vtk.vtkBox()
# cube.SetBounds(-33.82523727416992, 34.65827178955078, -33.928985595703125, 27.589263916015625, 205.47540283203125, 226.9934539794922)
cube.SetBounds(-5.0, 5.0, 20.928985595703125, 27.589263916015625, 205.47540283203125, 226.9934539794922)
extract = vtk.vtkExtractPoints()
# extract.SetInputConnection(points.GetOutputPort())
extract.SetInputData(reader.GetOutput())
extract.SetImplicitFunction(cube)
extract.Update()

P = ns.vtk_to_numpy(extract.GetOutput().GetPoints().GetData())
P = P[::10]

_vpts = vtk.vtkPoints()
_vpts.SetData(dsa.numpyTovtkDataArray(P))
_subsampled = vtk.vtkStructuredGrid()
_subsampled.SetPoints(_vpts)

sphere_source = vtk.vtkSphereSource()
sphere_source.SetRadius(0.05)
glyp = vtk.vtkGlyph3D()
glyp.ScalingOff()
glyp.SetInputData(_subsampled)
glyp.SetSourceConnection(sphere_source.GetOutputPort())
glyp.Update()
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputData(glyp.GetOutput())
mapper.Update()
actor = vtk.vtkActor()
actor.SetMapper(mapper)


# Create an outline
outline = vtk.vtkOutlineFilter()
outline.SetInputData(reader.GetOutput())
outlineMapper = vtk.vtkPolyDataMapper()
outlineMapper.SetInputConnection(outline.GetOutputPort())
outlineActor = vtk.vtkActor()
outlineActor.SetMapper(outlineMapper)

ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

ren.AddActor(actor)
ren.AddActor(outlineActor)

ISTYLES = cyCafe.cyVtkInteractorStyles()
istyle1 = ISTYLES.get_vtk_interactor_style_volume_3d()
iren.SetInteractorStyle(istyle1)

ren.ResetCamera()
iren.Initialize()
renWin.SetSize(1200, 1200)
iren.Start()

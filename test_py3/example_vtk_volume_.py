import vtk
from vtk.util import numpy_support


# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/1')
reader.Update()

# thresh = vtk.vtkThreshold()
# thresh.ThresholdByUpper(3000)
# thresh.AllScalarsOff()
# thresh.SetInputConnection(reader.GetOutputPort())
# print('ttttttttt', thresh.GetOutput())

# trifilter = vtk.vtkDataSetTriangleFilter()
# trifilter.SetInputConnection(thresh.GetOutputPort())
# print('kkkkkkkkkkkkkk', trifilter.GetOutput())

c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
p = vtk.vtkVolumeProperty()
m = vtk.vtkSmartVolumeMapper()
# m = vtk.vtkUnstructuredGridVolumeRayCastMapper()
v = vtk.vtkVolume()

opacityWindow = 4096
opacityLevel = 2048

# mapper
m.SetInputConnection(reader.GetOutputPort())
# m.SetInputConnection(thresh.GetOutputPort())
# m.SetBlendModeToMaximumIntensity()

# property
p.SetIndependentComponents(True)
c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
o_f.AddSegment(opacityLevel - 0.5*opacityWindow, 0.0, opacityLevel + 0.5*opacityWindow, 1.0)
p.SetColor(c_f)
p.SetScalarOpacity(o_f)
p.SetInterpolationTypeToLinear()

# actor
v.SetMapper(m)
v.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
istyle = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(istyle)

# camera
camera = ren.GetActiveCamera()
c = v.GetCenter()
camera.SetFocalPoint(c)
camera.SetPosition(c[0], c[1]+400, c[2])
camera.SetViewUp(0, 0, -1)

# # Picking
# picker = vtk.vtkVolumePicker()
# picker.SetTolerance(1e-6)
# picker.SetVolumeOpacityIsovalue(0.1)
#
#
# def _left_button_press(_iren, _e):
#     x, y = _iren.GetEventPosition()
#     picker.Pick(x, y, 0, ren)
#     _p = picker.GetPickPosition()
#     _n = picker.GetPickNormal()
#     print('position : ', _p)
#     print('normal : ', _n)
#
#     # method 1
#     # pos = _get_intensity_by_point(*_p)
#     # print('Pos', pos)
#     # method 2
#     arr = _get_intensity_array()
#     print('Possss!!', arr[int(_p[0])][int(_p[1])][int(_p[2])])
#
# # Mouse Event
# iren.AddObserver('LeftButtonPressEvent', _left_button_press)
#
#
# def _get_intensity_by_point(_x, _y, _z):
#     # Piece of cake.. -.-;;
#     vtkimage = reader.GetOutput()
#     pos = vtkimage.GetScalarComponentAsDouble(int(_x), int(_y), int(_z), 0)
#     return pos
#
#
# def _get_intensity_array():
#     vtkimage = reader.GetOutput()
#     narray = numpy_support.vtk_to_numpy(vtkimage.GetPointData().GetScalars())
#     # order F means Fortran : column major
#     narray = narray.reshape(vtkimage.GetDimensions(), order='F')
#     return narray


ren.AddViewProp(v)
renWin.Render()
iren.Initialize()
iren.Start()

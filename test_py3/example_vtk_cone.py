import vtk

# Source
s = vtk.vtkConeSource()
s.Update()

# Mapper
m = vtk.vtkPolyDataMapper()
m.SetInputData(c.GetOutput())

# Actor
a = vtk.vtkActor()
a.SetMapper(m)

# Render
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
ren.AddViewProp(a)

# Render Start
renWin.Render()
iren.Initialize()
iren.Start()

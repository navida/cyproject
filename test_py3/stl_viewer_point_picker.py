"""
stl view & get view vector & get point set
"""

import sys
import time
import datetime
import math

from PyQt5 import Qt, QtCore, QtWidgets
from PyQt5.Qt import QPushButton, QHBoxLayout, QVBoxLayout, QTextEdit, QApplication

import vtk

# Specify marker type : Sphere or Cone
MARKER_TYPE = 'Cone'


class Viewer():
    def __init__(self, stl_file):
        super().__init__()

        # double click
        self.prev_time = time.time()

        # marker set
        self.pos_moving_marker = [0, 0, 0]
        self.POINT_SET = []
        self.MARKER_SET = []

        # new marker id
        self.new_marker_id = 0

        # 'Sphere' or 'Cone'
        self.marker_type = MARKER_TYPE

        reader = vtk.vtkSTLReader()
        reader.SetFileName(stl_file)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        # Create a rendering window and renderer
        self.ren = vtk.vtkRenderer()
        self.renWin = vtk.vtkRenderWindow()
        self.renWin.AddRenderer(self.ren)

        # Create a renderwindowinteractor
        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)
        istyle = vtk.vtkInteractorStyleTrackballCamera()
        self.iren.SetInteractorStyle(istyle)

        # Assign actor to the renderer
        self.ren.AddActor(actor)

        # picker
        self.extractor = vtk.vtkMarchingCubes()
        self.locator = vtk.vtkCellLocator()
        self.picker = vtk.vtkVolumePicker()
        self.redSphere = vtk.vtkActor()
        self.moving_cone = vtk.vtkActor()

        # Extractor
        # self.extractor.SetInputConnection(reader.GetOutputPort())
        # self.extractor.SetValue(0, 1150)

        # Locator
        self.locator.SetDataSet(reader.GetOutput())
        self.locator.LazyEvaluationOn()

        color = [31 / 255, 126 / 255, 150 / 255]

        if self.marker_type == 'Sphere':
            # init marker sphere
            sphereSource = vtk.vtkSphereSource()
            sphereSource.SetRadius(1)
            sphereMapper = vtk.vtkDataSetMapper()
            sphereMapper.SetInputConnection(sphereSource.GetOutputPort())

            self.redSphere.PickableOff()
            self.redSphere.SetMapper(sphereMapper)
            self.redSphere.GetProperty().SetColor(color)

            # Add Marker
            self.ren.AddViewProp(self.redSphere)
        elif self.marker_type == 'Cone':
            # init marker cone
            coneSource = vtk.vtkConeSource()
            coneSource.CappingOn()
            coneSource.SetHeight(2)
            coneSource.SetRadius(1)
            coneSource.SetResolution(31)
            coneSource.SetCenter(1, 0, 0)
            coneSource.SetDirection(-1, 0, 0)
            coneMapper = vtk.vtkDataSetMapper()
            coneMapper.SetInputConnection(coneSource.GetOutputPort())

            self.moving_cone.PickableOff()
            self.moving_cone.SetMapper(coneMapper)
            self.moving_cone.GetProperty().SetColor(color)

            # Add Marker
            self.ren.AddViewProp(self.moving_cone)

        # picker
        self.picker.SetTolerance(1e-6)
        self.picker.SetVolumeOpacityIsovalue(0.1)
        self.picker.AddLocator(self.locator)

        #
        self.renWin.SetSize(800, 600)
        self.iren.AddObserver("MouseMoveEvent", self.MoveCursor)
        self.iren.AddObserver("LeftButtonPressEvent", self.MouseLeftPress)
        self.iren.AddObserver("KeyPressEvent", self.KeyPress)

    def set_viewvec_text(self, text_box):
        self.text_box = text_box

    def set_point_tree(self, tree_view):
        self.tree_view = tree_view

    def start(self):
        # Enable user interface interactor
        self.iren.Initialize()
        self.renWin.Render()
        self.iren.Start()

    def get_view_vector(self):
        c = self.ren.GetActiveCamera()
        fp = c.GetFocalPoint()
        pos = c.GetPosition()

        self.text_box.setText('')

        # view vec
        view_vec = (fp[0] - pos[0], fp[1] - pos[1], fp[2] - pos[2])
        self.text_box.append(("View Vector : %lf, %lf, %lf" % view_vec))

        # magnitude of view_vec
        mag = math.sqrt(sum(view_vec[i] * view_vec[i] for i in range(len(view_vec))))
        self.text_box.append(("Magnitude : %lf" % mag))

        # normalized
        normal_vec = [view_vec[i] / mag for i in range(len(view_vec))]
        self.text_box.append(("Normalized : %s" % normal_vec))
        self.text_box.append('\n')

    # A function to point an actor along a vector
    def PointCone(self, actor, vec):
        actor.SetOrientation(0.0, 0.0, 0.0)
        n = math.sqrt(vec[0] ** 2 + vec[1] ** 2 + vec[2] ** 2)
        if (vec[0] < 0.0):
            actor.RotateWXYZ(180, 0, 1, 0)
            n = -n
        actor.RotateWXYZ(180, (vec[0] + n) * 0.5, vec[1] * 0.5, vec[2] * 0.5)

    # A function to move the cursor with the mouse
    def MoveCursor(self, iren, event=""):
        x, y = iren.GetEventPosition()
        renders = iren.GetRenderWindow().GetRenderers()
        ren = renders.GetFirstRenderer()
        self.picker.Pick(x, y, 0, ren)
        self.pos_moving_marker = self.picker.GetPickPosition()
        self.vec_moving_marker = self.picker.GetPickNormal()

        if self.marker_type == 'Sphere':
            self.redSphere.SetPosition(self.pos_moving_marker)
        elif self.marker_type == 'Cone':
            self.moving_cone.SetPosition(self.pos_moving_marker)
            self.PointCone(self.moving_cone, self.vec_moving_marker)
        self.iren.Render()

    def MouseLeftPress(self, iren, event=""):
        cur = time.time()
        elapses = cur - self.prev_time
        self.prev_time = cur

        # double click
        if elapses < 0.2:
            self.place_marker()

    def KeyPress(self, iren, event=""):
        if iren.GetKeySym() == 'space':
            self.place_marker()

    def place_marker(self):
        self.POINT_SET.append(self.pos_moving_marker)
        row = Qt.QTreeWidgetItem(self.tree_view)
        row.setText(0, str(self.new_marker_id))
        self.new_marker_id += 1
        row.setTextAlignment(0, QtCore.Qt.AlignLeft)
        row.setText(1, str(self.pos_moving_marker))
        marker = self.mark(self.pos_moving_marker, self.vec_moving_marker)
        self.MARKER_SET.append(marker)

    def mark(self, pos, vec):
        """mark the picked location with a sphere or cone"""

        # Sphere
        if self.marker_type == 'Sphere':
            marker_source = vtk.vtkSphereSource()
            marker_source.SetRadius(1)
            res = 20
            marker_source.SetThetaResolution(res)
            marker_source.SetPhiResolution(res)
            marker_source.SetCenter(pos[0], pos[1], pos[2])
        elif self.marker_type == 'Cone':
            # Cone
            marker_source = vtk.vtkConeSource()
            marker_source.CappingOn()
            marker_source.SetHeight(2)
            marker_source.SetRadius(1)
            marker_source.SetResolution(31)
            marker_source.SetCenter(1, 0, 0)
            marker_source.SetDirection(-1, 0, 0)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(marker_source.GetOutputPort())

        marker = vtk.vtkActor()
        marker.SetMapper(mapper)
        self.ren.AddActor(marker)
        marker.GetProperty().SetColor((0, 1, 0))

        if self.marker_type == 'Cone':
            marker.SetPosition(pos)
            self.PointCone(marker, vec)

        self.iren.Render()

        return marker

    def notify_marker(self, index):
        for idx, M in enumerate(self.MARKER_SET):
            col = (1, 0, 0) if idx == index else (0, 1, 0)
            M.GetProperty().SetColor(col)

        self.iren.Render()

    def removeMarkerAt(self, idx):
        del self.POINT_SET[idx]
        self.ren.RemoveActor(self.MARKER_SET[idx])
        self.iren.Render()
        del self.MARKER_SET[idx]


class MainWindow(QtWidgets.QWidget):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        # selected point idx
        self.idx_selected_point = -1

        self.isRemoving = False

        self.filename = ""

        self.resize(600, 800)

        btn1 = QPushButton('Load STL File', clicked=self.on_click_stl_load)
        btn2 = QPushButton('Current View Vector', clicked=self.on_click_btn2)
        btn3 = QPushButton('Save to .txt file', clicked=self.on_click_btn3)
        btn4 = QPushButton('delete selected point', clicked=self.on_click_btn4)

        # for current view vector
        self.text_box = QTextEdit()

        # for clicked pos
        self.tree_view = QtWidgets.QTreeWidget()

        header = Qt.QTreeWidgetItem()
        header.setText(0, r'Id')
        header.setText(1, r'Coord')
        self.tree_view.setHeaderItem(header)
        self.tree_view.header().resizeSection(0, 50)
        #         self.tree_view.itemClicked.connect(self.treeitem_clicked)
        #         self.tree_view.itemPressed.connect(self.treeitem_clicked)
        self.tree_view.itemSelectionChanged.connect(self.treeitem_changed)

        layout1 = QHBoxLayout()

        # Left
        layout2 = QVBoxLayout()
        layout2.addWidget(btn1)
        layout2.addWidget(btn2)
        layout2.addWidget(btn3)
        layout2.addWidget(self.text_box)
        layout2.addWidget(self.tree_view)
        layout2.addWidget(btn4)
        left_widget = QtWidgets.QWidget()
        left_widget.setLayout(layout2)

        layout1.addWidget(left_widget)

        self.setLayout(layout1)
        self.show()

        # load stl
        self.open_stl_file()

    def open_stl_file(self):
        qfile_dlg = Qt.QFileDialog()
        qfile_dlg.setFileMode(Qt.QFileDialog.AnyFile)
        if qfile_dlg.exec_():
            filenames = qfile_dlg.selectedFiles()
            self.filename = filenames[0]

            if self.filename[len(self.filename) - 3:len(self.filename)] != 'stl':
                Qt.QMessageBox.information(QtWidgets.QWidget(), 'msg', "is not stl file!")
                return

            self.v = Viewer(self.filename)
            self.v.set_viewvec_text(self.text_box)
            self.v.set_point_tree(self.tree_view)
            self.v.start()

    @QtCore.pyqtSlot()
    def on_click_stl_load(self):
        # clean up
        self.idx_selected_point = -1
        self.text_box.clear()
        self.tree_view.clear()
        self.filename = ""

        if hasattr(self, 'v'):
            self.v.renWin.Finalize()
            self.v.iren.TerminateApp()
            del self.v

        self.open_stl_file()

    @QtCore.pyqtSlot()
    def on_click_btn2(self):
        if hasattr(self, 'v'):
            self.v.get_view_vector()
        else:
            Qt.QMessageBox.information(QtWidgets.QWidget(), 'msg', ".stl file isn't loaded!")

    @QtCore.pyqtSlot()
    def on_click_btn3(self):
        if self.filename != "":
            # view vector
            txt = self.text_box.toPlainText()
            txt += '\n'

            # point set
            root = self.tree_view.invisibleRootItem()
            child_cnt = root.childCount()
            txt += 'Point Set(' + repr(child_cnt) + ')\n'
            for i in range(child_cnt):
                item = root.child(i)
                txt += item.text(1)
                txt += '\n'

            #
            cur_date = datetime.datetime.now().date().isoformat()
            cur_time = datetime.datetime.now().time().isoformat()
            cur_time = cur_time[0:8]
            cur_time = cur_time.replace(':', '')

            txt_file = self.filename[0:len(self.filename) - 4] + '_' + cur_date + '_' + cur_time + '.txt'
            file = open(txt_file, 'w')
            file.write(txt)
            file.close()

            Qt.QMessageBox.information(QtWidgets.QWidget(), 'msg', ("Saved!\n %s" % txt_file))
        else:
            Qt.QMessageBox.information(QtWidgets.QWidget(), 'msg', ".stl file isn't loaded!")

    @QtCore.pyqtSlot()
    def on_click_btn4(self):
        """
        remove tree item
        """
        if hasattr(self, 'v'):
            if self.idx_selected_point != -1:
                self.isRemoving = True
                self.v.removeMarkerAt(self.idx_selected_point)
                self.tree_view.takeTopLevelItem(self.idx_selected_point)
                self.tree_view.clearSelection()
                self.idx_selected_point = -1
                self.isRemoving = False
        else:
            Qt.QMessageBox.information(QtWidgets.QWidget(), 'msg', ".stl file isn't loaded!")

    def treeitem_clicked(self, column):
        self.idx_selected_point = self.tree_view.indexOfTopLevelItem(column)
        self.v.notify_marker(self.idx_selected_point)

    def treeitem_changed(self):
        if self.isRemoving:
            return

        selected_items = self.tree_view.selectedItems()

        for sel in selected_items:
            self.idx_selected_point = self.tree_view.indexOfTopLevelItem(sel)
            self.v.notify_marker(self.idx_selected_point)

    def closeEvent(self, e):
        print('Close')

        if hasattr(self, 'v'):
            self.v.renWin.Finalize()
            self.v.iren.TerminateApp()
            del self.v

        sys.exit(0)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()

    app.exec()

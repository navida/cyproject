from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QRectF, Qt, QUrl,QSize, QTimer
from PyQt5.QtGui import QColor, QPainter, QPen, QImage
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import QQuickPaintedItem, QQuickView
from PyQt5.Qt import QQuickWidget, QApplication


class MyQQuickItem(QQuickPaintedItem):

    sigText = pyqtSignal()
    sigCapture = pyqtSignal()

    @pyqtProperty(str)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @pyqtProperty(QColor)
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = QColor(color)

    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self._name = ''
        self._color = QColor()
        self.setAcceptedMouseButtons(Qt.AllButtons)

    @pyqtSlot()
    def on_component_completed(self):
        print('connect Sig, Slot')

    def paint(self, painter):
        print('this is paint')
        painter.setPen(QPen(self._color, 2))
        painter.setRenderHints(QPainter.Antialiasing, True)

        rect = QRectF(0, 0, self.width(), self.height()).adjusted(1, 1, -1, -1)
        painter.drawEllipse(rect)

    def mouseMoveEvent(self, e):
        print('m_move', e)

    def mousePressEvent(self, e):
        print('m_press', e)


class MyQQuickWidget(QQuickWidget):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.tm = QTimer()
        self.tm.timeout.connect(self.capture)
        self.tm.start(3000)

        self.setAttribute(103)
        self.show()

    def capture(self):
        print('capture!!')
        img = self.grabFramebuffer()
        img.save('captured.png')


class MyQQuickWidget_Dummy(QQuickWidget):
    def __init__(self, origin, *args, **kwds):
        super().__init__(*args, **kwds)
        self.origin = origin
        self.resize(self.origin.size())
        self.show()

    def resizeEvent(self, e):
        self.origin.resize(e.size())

        self.origin.resizeEvent(e)
        super().resizeEvent(e)


if __name__ == '__main__':
    import os
    import sys

    app = QApplication(sys.argv)

    qmlRegisterType(MyQQuickItem, "MyItems", 1, 0, "MyQQuickItem")

    # view = MyQQuickWidget()
    view = QQuickView()
    view.setResizeMode(QQuickView.SizeRootObjectToView)
    view.setSource(
            QUrl.fromLocalFile(
                    os.path.join(os.path.dirname(__file__), 'app.qml')))

    view.show()

    # dummy_view = MyQQuickWidget_Dummy(view)

    sys.exit(app.exec_())


import math
import numpy as np

"""
Prest Camera angle
"""
a = math.radians(135)

"""
rotation matrix on z-axis
cos -sin 0
sin  cos 0
0    0   1
"""
m1 = np.matrix([[math.cos(a), -1*math.sin(a), 0],
                [math.sin(a), math.cos(a), 0],
                [0, 0, 1]])

"""
PlaneNormal for Camera
"""
m2 = np.matrix([0, -1, 0])


"""
Distance
"""
d = 511


x = m1 * m2.transpose() * d

# new position = center position + x
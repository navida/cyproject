import vtk
import cyCafe

def MakePolyhedron():
    '''
      Make a regular dodecahedron. It consists of twelve regular pentagonal
      faces with three faces meeting at each vertex.
    '''
    filename = '423013-M'
    pcmloader = cyCafe.cyPCMLoader()
    # filename = '/Users/yjkwon/_sqlite/125.099'
    # pcmloader.loadpcmfile(filename + '.pcm')
    pcmloader.loadpcmfile('/Users/scott/california/cybermed_cafe/__Source__/Cybermed/cyCafe/cyPCMLoader/FALIHR6011.pcm')
    # pcmloader.loadstzfile(filename + '.stz')
    # pcmloader.loadstzfile(filename)
    vertexlist = list()
    facelist = list()
    pcmloader.getVertex(vertexlist)
    pcmloader.getFace(facelist)
    numberOfFaces = len(facelist)

    points = vtk.vtkPoints()
    for i in range(len(vertexlist)):
        points.InsertNextPoint(vertexlist[i])

    dodechedronFace = list()
    for i in range(len(facelist)):
        dodechedronFace.append(facelist[i])
    dodechedronFacesIdList = vtk.vtkIdList()

    dodechedronFacesIdList.InsertNextId(numberOfFaces)
    for face in dodechedronFace:
        dodechedronFacesIdList.InsertNextId(len(face))
        [dodechedronFacesIdList.InsertNextId(i) for i in face]

    uGrid = vtk.vtkUnstructuredGrid()
    uGrid.InsertNextCell(vtk.VTK_POLYHEDRON, dodechedronFacesIdList)
    uGrid.SetPoints(points)

    surfaceFilter = vtk.vtkDataSetSurfaceFilter()
    surfaceFilter.SetInputData(uGrid)
    surfaceFilter.Update()

    writer = vtk.vtkSTLWriter()
    writer.SetFileName(filename+".stl")
    writer.SetInputData(surfaceFilter.GetOutput())
    writer.Write()

    return surfaceFilter.GetOutput()


def WritePNG(renWin, fn, magnification=1):
    '''
      Screenshot

      Write out a png corresponding to the render window.

      :param: renWin - the render window.
      :param: fn - the file name.
      :param: magnification - the magnification.
    '''
    windowToImageFilter = vtk.vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.SetMagnification(magnification)
    # Record the alpha (transparency) channel
    # windowToImageFilter.SetInputBufferTypeToRGBA()
    windowToImageFilter.SetInputBufferTypeToRGB()
    # Read from the back buffer
    windowToImageFilter.ReadFrontBufferOff()
    windowToImageFilter.Update()

    writer = vtk.vtkPNGWriter()
    writer.SetFileName(fn)
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()


def DisplayBodies():

    polyData = MakePolyhedron()

    renWin = vtk.vtkRenderWindow()
    renWin.SetSize(1200, 1200)

    iRen = vtk.vtkRenderWindowInteractor()
    iRen.SetRenderWindow(renWin)

    mapper = vtk.vtkPolyDataMapper()
    actor = vtk.vtkActor()
    renderer = vtk.vtkRenderer()

    # MATERIAL_IMPLANT_METAL_1 = {}
    # MATERIAL_NERVE['Color'] = [1, 0, 0.5]
    # MATERIAL_NERVE['Ambient'] = 0.2
    # MATERIAL_NERVE['AmbientColor'] = [0.2, 0.2, 0.2]
    # MATERIAL_NERVE['Diffuse'] = 0.7
    # MATERIAL_NERVE['DiffuseColor'] = [1, 0, 0]
    # MATERIAL_NERVE['Specular'] = 50
    # MATERIAL_NERVE['SpecularPower'] = 100
    # MATERIAL_NERVE['SpecularColor'] = [246 / 255, 246 / 255, 246 / 255]

    _color = [0, 0.5, 1.0]
    _vol_prop = vtk.vtkProperty()
    _vol_prop.SetColor(_color)
    _vol_prop.SetAmbient(0.2)
    _vol_prop.SetAmbientColor([0.2, 0.2, 0.2])
    _vol_prop.SetDiffuse(0.7)
    _vol_prop.SetDiffuseColor(_color)
    _vol_prop.SetSpecular(0.7)
    _vol_prop.SetSpecularPower(10)
    _vol_prop.SetSpecularColor([246 / 255, 246 / 255, 246 / 255])
    actor.SetProperty(_vol_prop)

    mapper.SetInputData(polyData)
    actor.SetMapper(mapper)
    renderer.AddViewProp(actor)

    renWin.AddRenderer(renderer)

    iRen.Initialize()
    renWin.Render()
    return iRen


if __name__ == '__main__':
    iRen = DisplayBodies()
    # WritePNG(iRen.GetRenderWindow(), "Cell3DDemonstration.png")
    iRen.Start()
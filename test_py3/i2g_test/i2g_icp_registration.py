from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5.QtWidgets import *

import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtk.util import numpy_support
import numpy as np
import math

import cyCafe
import _qapp


class MainWindow(QMainWindow):
    # landmark
    class _landmark:
        def __init__(self):
            self.src = vtk.vtkSphereSource()
            self.src.SetRadius(2)
            res = 20
            self.src.SetThetaResolution(res)
            self.src.SetPhiResolution(res)
            self.src.SetCenter(0, 0, 0)

            self.mapper = vtk.vtkPolyDataMapper()
            self.mapper.SetInputConnection(self.src.GetOutputPort())

            self.actor = vtk.vtkActor()
            self.actor.SetMapper(self.mapper)

        def update(self):
            self.src.Update()
            self.src.Modified()
            self.mapper.Update()
            self.mapper.Modified()
            self.actor.Modified()

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        self.is_cycamstyle = False

        self.setMouseTracking(True)  # get all mouse events

        self.frame = QFrame()

        self.vl = QVBoxLayout()
        self.hl = QHBoxLayout()

        btn = QPushButton("Registration!", clicked=self.fn_registration)
        self.vl.addWidget(btn)

        self.w2 = QWidget()
        self.vl.addWidget(self.w2)
        self.w2.setLayout(self.hl)

        self.vtkWidget1 = QVTKRenderWindowInteractor()
        self.vtkWidget2 = QVTKRenderWindowInteractor()
        self.vtkWidget3 = QVTKRenderWindowInteractor()
        self.hl.addWidget(self.vtkWidget1)
        self.hl.addWidget(self.vtkWidget2)
        self.hl.addWidget(self.vtkWidget3)

        self.ren1 = vtk.vtkRenderer()
        self.vtkWidget1.GetRenderWindow().AddRenderer(self.ren1)
        self.iren1 = self.vtkWidget1.GetRenderWindow().GetInteractor()
        self.iren1.AddObserver("MouseMoveEvent", self.MoveCursor1)
        self.iren1.AddObserver("LeftButtonPressEvent", self.MouseLeftPress1)

        self.ren2 = vtk.vtkRenderer()
        self.vtkWidget2.GetRenderWindow().AddRenderer(self.ren2)
        self.iren2 = self.vtkWidget2.GetRenderWindow().GetInteractor()
        self.iren2.AddObserver("MouseMoveEvent", self.MoveCursor2)
        self.iren2.AddObserver("LeftButtonPressEvent", self.MouseLeftPress2)
        self.iren2.AddObserver("KeyPressEvent", self.KeyPress2)

        self.ren3 = vtk.vtkRenderer()
        self.vtkWidget3.GetRenderWindow().AddRenderer(self.ren3)
        self.iren3 = self.vtkWidget3.GetRenderWindow().GetInteractor()

        self.set_depthpeeling(True)

        ISTYLES = cyCafe.cyVtkInteractorStyles()
        istyle1 = ISTYLES.get_vtk_interactor_style_volume_3d(2)
        self.istyle2_a = ISTYLES.get_vtk_interactor_style_volume_3d(2)
        self.istyle2_b = vtk.vtkInteractorStyleTrackballActor()
        istyle3 = ISTYLES.get_vtk_interactor_style_volume_3d(2)
        self.vtkWidget1._Iren.SetInteractorStyle(istyle1)
        # self.vtkWidget2._Iren.SetInteractorStyle(self.istyle2_a)
        self.vtkWidget2._Iren.SetInteractorStyle(self.istyle2_b)
        self.vtkWidget3._Iren.SetInteractorStyle(istyle3)

        self.init_volume()
        self.init_polydata()

        self.ren1.ResetCamera()
        self.ren2.ResetCamera()
        self.ren3.ResetCamera()

        _c = self.ren1.GetActiveCamera()
        cam = self.ren2.GetActiveCamera()
        cam.SetPosition(_c.GetPosition())
        cam.SetFocalPoint(_c.GetFocalPoint())
        cam.SetViewUp(_c.GetViewUp())
        cam.SetDistance(_c.GetDistance())

        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)

        self.show()

        self.iren1.Initialize()
        self.iren2.Initialize()
        self.iren3.Initialize()

        self.resize(1400, 600)

    def set_depthpeeling(self, isOn):
        # Depth Peeling
        self.vtkWidget1.GetRenderWindow().SetAlphaBitPlanes(1)
        self.vtkWidget1.GetRenderWindow().SetMultiSamples(0)
        self.ren1.SetUseDepthPeeling(isOn)
        self.ren1.SetUseDepthPeelingForVolumes(isOn)
        self.ren1.SetMaximumNumberOfPeels(100)
        self.ren1.SetOcclusionRatio(0.01)

        self.vtkWidget2.GetRenderWindow().SetAlphaBitPlanes(1)
        self.vtkWidget2.GetRenderWindow().SetMultiSamples(0)
        self.ren2.SetUseDepthPeeling(isOn)
        self.ren2.SetUseDepthPeelingForVolumes(isOn)
        self.ren2.SetMaximumNumberOfPeels(100)
        self.ren2.SetOcclusionRatio(0.01)

        self.vtkWidget3.GetRenderWindow().SetAlphaBitPlanes(1)
        self.vtkWidget3.GetRenderWindow().SetMultiSamples(0)
        self.ren3.SetUseDepthPeeling(isOn)
        self.ren3.SetUseDepthPeelingForVolumes(isOn)
        self.ren3.SetMaximumNumberOfPeels(100)
        self.ren3.SetOcclusionRatio(0.01)

    def init_volume(self):
        # DCM_PATH = "/Users/scott/Dicom/HeadCrack/S0000000008/S0000000008/"
        # DCM_PATH = "/Users/scott/Dicom/OD3DData/In2Guide_DEMO/"
        # DCM_PATH = "/Users/scott/Dicom/OD3DData/20170721/S0000000050/"
        DCM_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/"
        reader = cyCafe.cyDicomReader()
        reader.read_dicom(DCM_PATH)
        vtkImg = reader.get_vtk_img()

        m = vtk.vtkGPUVolumeRayCastMapper()
        p = vtk.vtkVolumeProperty()
        c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
        a = vtk.vtkVolume()

        m.SetInputData(vtkImg)
        m.SetUseJittering(True)
        a.SetMapper(m)
        a.SetProperty(p)

        opacityWindow = 4096
        opacityLevel = 3548
        p.SetIndependentComponents(True)
        c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
        o_f.AddSegment(opacityLevel - 0.5 * opacityWindow, 0.0, opacityLevel + 0.5 * opacityWindow, 1.0)
        p.SetColor(c_f)
        p.SetScalarOpacity(o_f)
        p.SetInterpolationTypeToLinear()

        self.picker1 = vtk.vtkVolumePicker()

        # Locator
        self.locator1 = vtk.vtkCellLocator()
        self.locator1.SetDataSet(vtkImg)
        self.locator1.LazyEvaluationOn()

        self.picker1.SetTolerance(1e-6)
        self.picker1.SetVolumeOpacityIsovalue(0.1)
        self.picker1.AddLocator(self.locator1)

        self.marker1 = MainWindow._landmark()
        self.marker1.src.SetCenter(0, 0, 0)
        self.marker1.actor.GetProperty().SetColor(1, 0, 0)
        self.ren1.AddActor(self.marker1.actor)
        self.marker2 = MainWindow._landmark()
        self.marker2.actor.SetPosition(10, 0, 0)
        self.marker2.actor.GetProperty().SetColor(0, 1, 0)
        self.ren1.AddActor(self.marker2.actor)
        self.marker3 = MainWindow._landmark()
        self.marker3.actor.SetPosition(20, 0, 0)
        self.marker3.actor.GetProperty().SetColor(0, 0, 1)
        self.ren1.AddActor(self.marker3.actor)

        self.ren1.AddActor(a)
        self.ren3.AddActor(a)

    def init_polydata(self):
        # STL_PATH = "/Users/scott/Dicom/OD3DData/In2Guide_DEMO/tmp/GroupGuide_1.stl"
        # STL_PATH = "/Users/scott/Dicom/OD3DData/20170721/S0000000050/tmp/GroupGuide_1.stl"
        STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/GroupGuide_1.stl"

        reader = vtk.vtkSTLReader()
        reader.SetFileName(STL_PATH)
        reader.Update()

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())

        self.actor2 = vtk.vtkActor()
        self.actor2.SetMapper(mapper)

        self.ren2.AddActor(self.actor2)


        color = [0.9, 0.9, 0]
        self.actor2.GetProperty().SetOpacity(0.6)
        self.actor2.GetProperty().SetColor(color)
        self.actor2.GetProperty().SetAmbient(0.3)
        self.actor2.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
        self.actor2.GetProperty().SetDiffuse(0.5)
        self.actor2.GetProperty().SetDiffuseColor(color)
        self.actor2.GetProperty().SetSpecular(1)
        self.actor2.GetProperty().SetSpecularPower(10)
        self.actor2.GetProperty().SetSpecularColor([0.9, 0.9, 0.1])

        self.picker2 = vtk.vtkVolumePicker()

        # Locator
        self.locator2 = vtk.vtkCellLocator()
        self.locator2.SetDataSet(reader.GetOutput())
        self.locator2.LazyEvaluationOn()

        self.picker2.SetTolerance(1e-6)
        self.picker2.SetVolumeOpacityIsovalue(0.1)
        self.picker2.AddLocator(self.locator2)

        self.marker4 = MainWindow._landmark()
        self.marker4.src.SetCenter(0, 0, 0)
        self.marker4.actor.GetProperty().SetColor(1, 0, 0)
        self.ren2.AddActor(self.marker4.actor)
        self.marker5 = MainWindow._landmark()
        self.marker5.actor.SetPosition(10, 0, 0)
        self.marker5.actor.GetProperty().SetColor(0, 1, 0)
        self.ren2.AddActor(self.marker5.actor)
        self.marker6 = MainWindow._landmark()
        self.marker6.actor.SetPosition(20, 0, 0)
        self.marker6.actor.GetProperty().SetColor(0, 0, 1)
        self.ren2.AddActor(self.marker6.actor)

        self.actor3 = vtk.vtkActor()
        self.actor3.ShallowCopy(self.actor2)
        self.ren3.AddActor(self.actor3)

    def get_real_pos(self, iren):
        x, y = iren.GetEventPosition()

        thisScreen = Qt.QApplication.desktop().screenNumber(self.w2)
        print("THIS Screen Number is ", thisScreen)
        screen = Qt.QGuiApplication.screens()[thisScreen]
        ratio = screen.devicePixelRatio()
        print("RATIO :: ", ratio)
        return x * ratio, y * ratio

    def MouseLeftPress1(self, iren, event=""):
        x, y = self.get_real_pos(iren)

        self.picker1.Pick(x, y, 0, self.ren1)

        if hasattr(self, 'selected_marker') and self.selected_marker:
            self.selected_marker.SetPickable(True)

        self.selected_marker = self.picker1.GetActor()

        if self.selected_marker:
            self.selected_marker.SetPickable(False)


    def MoveCursor1(self, iren, event=""):
        x, y = self.get_real_pos(iren)

        self.picker1.Pick(x, y, 0, self.ren1)
        pos_moving_marker = self.picker1.GetPickPosition()
        # vec_moving_marker = self.picker.GetPickNormal()

        print("99999999999999999999 :: ", self.picker1.GetPickedPositions())

        if not hasattr(self, 'selected_marker') \
                or self.selected_marker is None:
            return

        self.selected_marker.SetPosition(pos_moving_marker)
        self.selected_marker.Modified()
        self.ren1.Render()
        iren.Render()


    def MouseLeftPress2(self, iren, event=""):
        x, y = self.get_real_pos(iren)
        self.picker2.Pick(x, y, 0, self.ren2)

        if hasattr(self, 'selected_marker') and self.selected_marker and \
            (self.selected_marker is self.marker4.actor or
            self.selected_marker is self.marker5.actor or
            self.selected_marker is self.marker6.actor):
            self.selected_marker.SetPickable(True)

        a = self.picker2.GetActor()
        if a is self.marker4.actor or a is self.marker5.actor or a is self.marker6.actor:
            self.selected_marker = a
        else:
            self.selected_marker = None

        if self.selected_marker:
            self.selected_marker.SetPickable(False)


    def MoveCursor2(self, iren, event=""):
        x, y = self.get_real_pos(iren)

        self.picker2.Pick(x, y, 0, self.ren2)
        pos_moving_marker = self.picker2.GetPickPosition()
        # vec_moving_marker = self.picker.GetPickNormal()

        if not hasattr(self, 'selected_marker') \
                or self.selected_marker is None:
            return

        self.selected_marker.SetPosition(pos_moving_marker)
        self.selected_marker.Modified()
        self.ren2.Render()
        iren.Render()

    def KeyPress2(self, iren, event=""):
        if iren.GetKeySym() == 'space':
            if self.is_cycamstyle:
                self.vtkWidget2._Iren.SetInteractorStyle(self.istyle2_a)
                # self.vtkWidget2.GetRenderWindow().GetInteractor().AddObserver("MouseMoveEvent", self.MoveCursor2)
                # self.vtkWidget2.GetRenderWindow().GetInteractor().AddObserver("LeftButtonPressEvent", self.MouseLeftPress2)
                print("Ren2 Camera Style : CyCameraInteractor")
            else:
                # self.vtkWidget2.GetRenderWindow().GetInteractor().RemoveObservers("MouseMoveEvent")
                # self.vtkWidget2.GetRenderWindow().GetInteractor().RemoveObservers("LeftButtonPressEvent")
                self.vtkWidget2._Iren.SetInteractorStyle(self.istyle2_b)
                print("Ren2 Camera Style : TrackBallActor")

            self.is_cycamstyle = not self.is_cycamstyle

    @QtCore.pyqtSlot()
    def fn_registration(self):
        def CreatePolyData(polydata, _points):
            points = vtk.vtkPoints()
            for _p in _points:
                points.InsertNextPoint(_p)
            temp = vtk.vtkPolyData()
            temp.SetPoints(points)

            vertexFilter = vtk.vtkVertexGlyphFilter()
            vertexFilter.SetInputData(temp)
            vertexFilter.Update()
            polydata.ShallowCopy(vertexFilter.GetOutput())

        p1 = self.marker1.actor.GetPosition()
        p2 = self.marker2.actor.GetPosition()
        p3 = self.marker3.actor.GetPosition()
        target = vtk.vtkPolyData()
        CreatePolyData(target, [p1, p2, p3])

        p4 = self.marker4.actor.GetPosition()
        p5 = self.marker5.actor.GetPosition()
        p6 = self.marker6.actor.GetPosition()
        src = vtk.vtkPolyData()
        CreatePolyData(src, [p4, p5, p6])

        # Setup ICP transform
        icp = vtk.vtkIterativeClosestPointTransform()
        icp.SetSource(src)
        icp.SetTarget(target)
        icp.GetLandmarkTransform().SetModeToRigidBody()
        icp.SetMaximumNumberOfIterations(100)
        icp.StartByMatchingCentroidsOn()
        icp.Modified()
        icp.Update()

        # Get the resulting transformation matrix(this matrix takes the source points to the target points)
        m = icp.GetMatrix()

        print("PPPPPPPPPPPPP", m)
        self.actor3.ShallowCopy(self.actor2)
        self.actor3.SetUserMatrix(m)

        t = vtk.vtkTransform()
        t.SetMatrix(m)
        tpd = vtk.vtkTransformPolyDataFilter()
        tpd.SetTransform(t)
        tpd.SetInputData(src)
        tpd.Update()

        pts = tpd.GetOutput().GetPoints()
        fsum = 0.0
        val_rmse = 0.0
        for i in range(pts.GetNumberOfPoints()):
            _pt_src = pts.GetPoint(i)
            _pt_target = target.GetPoint(i)

            _val1 = (_pt_src[0] - _pt_target[0]) * (_pt_src[0] - _pt_target[0]);
            _val2 = (_pt_src[1] - _pt_target[1]) * (_pt_src[1] - _pt_target[1]);
            _val3 = (_pt_src[2] - _pt_target[2]) * (_pt_src[2] - _pt_target[2]);
            _val4 = math.sqrt(_val1 + _val2 + _val3)
            fsum += _val4

        val_rmse = fsum / pts.GetNumberOfPoints()

        print("RMSE!!!!!!!!!", val_rmse)




if __name__ == '__main__':

    window = MainWindow()

    _qapp.exec_()

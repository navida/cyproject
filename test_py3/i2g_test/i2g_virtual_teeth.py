from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5.QtWidgets import *

import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtk.util import numpy_support
import numpy as np
import math

import cyCafe
import _qapp


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        self.setMouseTracking(True)  # get all mouse events

        self.frame = QFrame()

        self.vl = QVBoxLayout()
        self.hl = QHBoxLayout()

        btn_x = QPushButton("scale X!", clicked=lambda: self.fn_scale([1.01, 1, 1]))
        btn_y = QPushButton("scale Y!", clicked=lambda: self.fn_scale([1, 1.01, 1]))
        btn_z = QPushButton("scale Z!", clicked=lambda: self.fn_scale([1, 1, 1.01]))
        self.vl.addWidget(btn_x)
        self.vl.addWidget(btn_y)
        self.vl.addWidget(btn_z)

        self.w2 = QWidget()
        self.vl.addWidget(self.w2)
        self.w2.setLayout(self.hl)

        self.vtkWidget = QVTKRenderWindowInteractor()
        self.hl.addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()

        # self.set_depthpeeling(True)

        ISTYLES = cyCafe.cyVtkInteractorStyles()
        istyle1 = ISTYLES.get_vtk_interactor_style_volume_3d(2)
        self.vtkWidget._Iren.SetInteractorStyle(istyle1)

        self.init_virtual_tooth()

        self.ren.ResetCamera()
        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)

        self.show()

        self.iren.Initialize()
        self.resize(600, 700)

    def init_virtual_tooth(self):
        STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/Wax Up Model_2.stl"

        self.reader = vtk.vtkSTLReader()
        self.reader.SetFileName(STL_PATH)
        self.reader.Update()

        self.t = vtk.vtkTransform()
        self.tpd = vtk.vtkTransformPolyDataFilter()
        self.tpd.SetTransform(self.t)
        self.tpd.SetInputData(self.reader.GetOutput())
        self.tpd.Update()

        self.mapper = vtk.vtkPolyDataMapper()
        self.mapper.SetInputData(self.tpd.GetOutput())

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        self.ren.AddActor(self.actor)

        color = [0.9, 0.9, 0.9]
        self.actor.GetProperty().SetColor(color)
        self.actor.GetProperty().SetAmbient(0.3)
        self.actor.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
        self.actor.GetProperty().SetDiffuse(0.5)
        self.actor.GetProperty().SetDiffuseColor(color)
        self.actor.GetProperty().SetSpecular(1)
        self.actor.GetProperty().SetSpecularPower(10)
        self.actor.GetProperty().SetSpecularColor([.95, .95, .95])

    @QtCore.pyqtSlot()
    def fn_scale(self, sacle_xyz):
        self.tpd.GetOutput().ComputeBounds()
        b = self.tpd.GetOutput().GetBounds()
        cx = (b[1] + b[0]) / 2
        cy = (b[3] + b[2]) / 2
        cz = (b[5] + b[4]) / 2

        print("BB", b)
        self.t.Translate(cx, cy, cz)
        self.t.Scale(sacle_xyz)
        self.t.Translate(-cx, -cy, -cz)
        self.tpd.Update()
        self.iren.Render()


if __name__ == '__main__':
    window = MainWindow()
    _qapp.exec_()

from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5.QtWidgets import *

import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtk.util import numpy_support
import numpy as np
import math

import cyCafe
import _qapp


def create_sphere_actor(pos, radius=0.2, color=[1, 0, 0]):
    s = vtk.vtkSphereSource()
    s.SetRadius(radius)
    s.SetCenter(pos)
    s.Update()
    m = vtk.vtkPolyDataMapper()
    m.SetInputData(s.GetOutput())
    a = vtk.vtkActor()
    a.GetProperty().SetColor(color)
    a.SetMapper(m)
    return a

def create_line_actor(pts, radius, color):
    points = vtk.vtkPoints()
    polyline = vtk.vtkPolyLine()
    cells = vtk.vtkCellArray()
    polyData = vtk.vtkPolyData()
    tube = vtk.vtkTubeFilter()

    for p in pts:
        points.InsertNextPoint(p)
    polyline.GetPointIds().SetNumberOfIds(len(pts))
    for i in range(len(pts)):
        polyline.GetPointIds().SetId(i, i)
    cells.InsertNextCell(polyline)
    polyData.SetPoints(points)
    polyData.SetLines(cells)

    tube.SetInputData(polyData)
    tube.SetNumberOfSides(36)
    tube.SetRadius(radius)
    tube.SetCapping(True)
    tube.Update()

    # Transform & Transform Filter
    transform = vtk.vtkTransform()
    tpd_filter = vtk.vtkTransformPolyDataFilter()
    tpd_filter.SetInputData(tube.GetOutput())
    tpd_filter.SetTransform(transform)
    tpd_filter.Update()

    mapper_line = vtk.vtkPolyDataMapper()
    mapper_line.SetInputData(tpd_filter.GetOutput())

    actor_line = vtk.vtkActor()
    actor_line.SetMapper(mapper_line)
    actor_line.GetProperty().SetColor(color)
    return actor_line

def get_vtkmat_from_list(list_mat, transpose=False):
    matrix = vtk.vtkMatrix4x4()
    for i, v in enumerate(list_mat):
        r = i // 4
        c = i - r * 4
        matrix.SetElement(r, c, v)

    if transpose:
        matrix.Transpose()

    return matrix


class DSI_SCALE:
    def __init__(self, obj_info, axis, color=[1, 0, 0], _ren=None, *args, **kwds):

        self._ren = _ren    # for test

        self.obj_actor, self.obj_tpd = obj_info
        self.bound = self.obj_actor.GetBounds() if self.obj_actor else [0, 1, 0, 1, 0, 1]
        self.center = self.obj_actor.GetCenter() if self.obj_actor else [0.5, 0.5, 0.5]

        # extend
        _max = max([self.bound[1] - self.bound[0],
                    self.bound[3] - self.bound[2],
                    self.bound[5] - self.bound[4]])
        _offset = abs(_max * 0.1)
        self.bound = [self.bound[0]-_offset, self.bound[1]+_offset,
                      self.bound[2]-_offset, self.bound[3]+_offset,
                      self.bound[4]-_offset, self.bound[5]+_offset]

        self.axis = axis
        _pt0 = list(self.center)
        _pt1 = list(self.center)
        _pt0[axis] = self.bound[axis * 2]
        _pt1[axis] = self.bound[axis * 2 + 1]

        self.item_scale = _max * 0.005

        vec = np.subtract(_pt1, _pt0)
        if self.axis == 0:
            hor_vec = [0, 1, 0]
        elif self.axis == 1:
            hor_vec = [0, 0, 1]
        elif self.axis == 2:
            hor_vec = [1, 0, 0]
        self.line_param = {'pt': [_pt0, _pt1], 'vec': vec, 'hor_vec': hor_vec}    # Points, Vec
        self.color = color
        self.actors = []
        self.actors_test = []
        self.init_controller()

    def init_controller(self):
        self.init_axis_line()
        self.init_control_handle(0)
        self.init_control_handle(1)
        self.init_arrow(0)
        self.init_arrow(1)

        self.init_circle()

    def init_axis_line(self):
        self.points = vtk.vtkPoints()
        self.polyline = vtk.vtkPolyLine()
        self.cells = vtk.vtkCellArray()
        self.polyData = vtk.vtkPolyData()
        self.tube = vtk.vtkTubeFilter()

        offset = self.line_param['vec'] * 0.05
        P = [self.line_param['pt'][0] - offset, self.line_param['pt'][1] + offset]
        for p in P:
            self.points.InsertNextPoint(p.tolist())
        self.polyline.GetPointIds().SetNumberOfIds(len(self.line_param['pt']))
        for i in range(len(self.line_param['pt'])):
            self.polyline.GetPointIds().SetId(i, i)
        self.cells.InsertNextCell(self.polyline)
        self.polyData.SetPoints(self.points)
        self.polyData.SetLines(self.cells)

        self.tube.SetInputData(self.polyData)
        self.tube.SetNumberOfSides(36)
        self.tube.SetRadius(self.item_scale)
        self.tube.SetCapping(True)
        self.tube.Update()

        # Transform & Transform Filter
        self.transform = vtk.vtkTransform()
        self.tpd_filter = vtk.vtkTransformPolyDataFilter()
        self.tpd_filter.SetInputData(self.tube.GetOutput())
        self.tpd_filter.SetTransform(self.transform)
        self.tpd_filter.Update()

        self.mapper_line = vtk.vtkPolyDataMapper()
        self.mapper_line.SetInputData(self.tpd_filter.GetOutput())

        self.actor_line = vtk.vtkActor()
        self.actor_line.SetMapper(self.mapper_line)
        self.actor_line.GetProperty().SetColor(self.color)
        self.actor_line.SetPickable(False)
        # self.actor_line.SetVisibility(False)
        self.actor_line.GetProperty().SetOpacity(0.15)

        self.actors.append(self.actor_line)

    def init_control_handle(self, idx=0):

        args = [self.item_scale*5, self.line_param['pt'][0], [.2, .7, .4]] if idx == 0 \
            else [self.item_scale*5, self.line_param['pt'][1], [.2, .7, .8]]

        # moving controller
        c = vtk.vtkSphereSource()
        c.SetRadius(args[0])
        c.SetCenter(args[1])
        c.Update()
        setattr(self, 'src_c' + str(idx), c)

        mapper_c = vtk.vtkPolyDataMapper()
        mapper_c.SetInputData(c.GetOutput())
        setattr(self, 'mapper_c' + str(idx), mapper_c)

        actor_c = vtk.vtkActor()
        actor_c.SetMapper(mapper_c)
        actor_c.GetProperty().SetColor(args[2])

        setattr(self, 'actor_c' + str(idx), actor_c)
        self.actors.append(actor_c)

    def init_arrow(self, idx=0):

        arrow_info = [self.actor_c0, -1, 3.14] if idx == 0 else [self.actor_c1, 1, 0]

        # translate to actor c center + offset
        _x, _y, _z = arrow_info[0].GetCenter()
        _offset = self.line_param['vec'] * 0.07 * arrow_info[1]
        _mat1 = get_vtkmat_from_list([1, 0, 0, _x + _offset[0],
                                      0, 1, 0, _y + _offset[1],
                                      0, 0, 1, _z + _offset[2],
                                      0, 0, 0, 1])

        # angle between vectors
        _rot_curve = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        _angle = (vtk.vtkMath.AngleBetweenVectors(self.line_param['vec'], [1, 0, 0]) * -1) + arrow_info[2]

        _eps = np.finfo(float).eps
        _up = [0, 1, 0] if np.inner(self.line_param['vec'], [1, 0, 0]) >= (1-_eps) \
            else np.cross(self.line_param['vec'], [1, 0, 0])
        _up = _up / np.linalg.norm(_up)

        # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
        _v = np.multiply(_up, math.sin(_angle / 2))
        vtk.vtkMath.QuaternionToMatrix3x3([math.cos(_angle / 2), _v[0], _v[1], _v[2]], _rot_curve)
        _mat2 = get_vtkmat_from_list([_rot_curve[0][0], _rot_curve[0][1], _rot_curve[0][2], 0,
                                      _rot_curve[1][0], _rot_curve[1][1], _rot_curve[1][2], 0,
                                      _rot_curve[2][0], _rot_curve[2][1], _rot_curve[2][2], 0,
                                      0, 0, 0, 1])

        # scale
        _mat3 = get_vtkmat_from_list([1 * self.item_scale * 15, 0, 0, 0,
                                      0, 1, 0, 0,
                                      0, 0, 1, 0,
                                      0, 0, 0, 1])

        _mat = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Multiply4x4(_mat1, _mat2, _mat)
        vtk.vtkMatrix4x4.Multiply4x4(_mat, _mat3, _mat)

        s = vtk.vtkArrowSource()
        s.SetShaftResolution(36)
        s.SetShaftRadius(self.item_scale*1.25)
        s.SetTipResolution(36)
        s.SetTipLength(0.5)
        s.SetTipRadius(self.item_scale*3.5)
        s.Update()
        t = vtk.vtkTransform()
        t.Concatenate(_mat)
        tpd = vtk.vtkTransformPolyDataFilter()
        tpd.SetTransform(t)
        tpd.SetInputConnection(s.GetOutputPort())
        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(tpd.GetOutputPort())
        a = vtk.vtkActor()
        a.SetMapper(m)
        a.GetProperty().SetColor([1, 0, 0.5])
        # a.SetUserMatrix(_mat)

        setattr(self, 'src_arrow' + str(idx), s)
        setattr(self, 'tpd_arrow' + str(idx), tpd)
        setattr(self, 'mapper_arrow' + str(idx), m)
        setattr(self, 'actor_arrow' + str(idx), a)
        self.actors.append(a)

        #
        # _n = self.line_param['vec'] / np.linalg.norm(self.line_param['vec'])
        # _offset2 = _n * (0.75/2) * arrow_info[1]
        # _offset3 = _offset + _offset2
        # _aa = create_sphere_actor([_x+_offset3[0], _y+_offset3[1], _z+_offset3[2]], 0.35, [1,0,0])
        # self.actors.append(_aa)
        # # _aa.GetProperty().SetOpacity(0.01)

    def init_circle(self):
        points = vtk.vtkPoints()
        # polyline = vtk.vtkPolyLine()
        # cells = vtk.vtkCellArray()
        polyData = vtk.vtkPolyData()
        tube = vtk.vtkTubeFilter()

        # generate a set of circle points and then, generate polydata
        self.generate_circle_points(center=self.center, num=360, out_points=points)
        self.generate_circle_polydata(src_points=points, ref_polydata=polyData)

        tube.SetInputData(polyData)
        tube.SetNumberOfSides(36)
        tube.SetRadius(self.item_scale * 2)
        tube.SetCapping(True)
        tube.Update()

        m_circle = vtk.vtkPolyDataMapper()
        m_circle.SetInputData(tube.GetOutput())
        a_circle = vtk.vtkActor()
        a_circle.SetMapper(m_circle)
        self.actors.append(a_circle)

        a_circle.GetProperty().SetOpacity(0.1)
        a_circle.SetPickable(False)

        setattr(self, 'polydata_circle', polyData)
        setattr(self, 'tube_circle', tube)
        setattr(self, 'mapper_circle', m_circle)
        setattr(self, 'actor_circle', a_circle)

    def generate_circle_points(self, radius=None, center=None, num=360, out_points=None):
        assert type(out_points) is vtk.vtkPoints, "check out_points."

        # generate a set of circle points
        center = self.center if center is None else center
        norm = np.linalg.norm(self.line_param['vec'])
        radius = norm / 2 if radius is None else radius

        # create circle on [0, 0, 1] and rotate it by axis
        rot_curve = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        angle = 3.14 / 2   # 90 degree

        if self.axis == 0:
            rot_axis = [0, 0, 1]
        elif self.axis == 1:
            rot_axis = [0, 1, 0]
        elif self.axis == 2:
            rot_axis = [1, 0, 0]

        # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
        v = np.multiply(rot_axis, math.sin(angle / 2))
        vtk.vtkMath.QuaternionToMatrix3x3([math.cos(angle / 2), v[0], v[1], v[2]], rot_curve)

        for i in range(num):
            angle = 2 * np.pi * i / num
            pt = [np.cos(angle) * radius, np.sin(angle) * radius, 0]
            pt = np.matmul(rot_curve, pt) + center
            out_points.InsertNextPoint(pt)

    def generate_circle_polydata(self, src_points, ref_polydata):
        _cells = vtk.vtkCellArray()
        _polyline = vtk.vtkPolyLine()
        num = src_points.GetNumberOfPoints()

        _polyline.GetPointIds().SetNumberOfIds(num)
        for i in range(num):
            _polyline.GetPointIds().SetId(i, i)
        _cells.InsertNextCell(_polyline)
        ref_polydata.SetPoints(src_points)
        ref_polydata.SetLines(_cells)

    def update_point(self, pts):
        _ori = np.linalg.norm(np.subtract(self.line_param['pt'][1], self.line_param['pt'][0]))
        _new = np.linalg.norm(np.subtract(pts[1], pts[0]))
        self.ratio = float(_new / _ori)

        # TODO!!!!!
        if self.obj_tpd:
            t = self.obj_tpd.GetTransform()
            tpd = self.obj_tpd
            tpd.GetOutput().ComputeBounds()
            __b = tpd.GetOutput().GetBounds()
            __cx = (__b[1] + __b[0]) / 2
            __cy = (__b[3] + __b[2]) / 2
            __cz = (__b[5] + __b[4]) / 2
            t.Translate(__cx, __cy, __cz)
            _s = [1, 1, 1]
            _s[self.axis] = self.ratio
            t.Scale(_s)
            t.Translate(-__cx, -__cy, -__cz)
            tpd .Update()
            ############@

        # update line
        self.points.Reset()
        self.line_param['pt'][0], self.line_param['pt'][1] = pts[0], pts[1]
        self.line_param['vec'] = np.subtract(pts[1], pts[0])

        offset = self.line_param['vec'] * 0.05
        P = [self.line_param['pt'][0] - offset, self.line_param['pt'][1] + offset]
        for p in P:
            self.points.InsertNextPoint(p.tolist())

        self.polyData.Modified()
        self.tube.Update()
        self.tpd_filter.Update()
        self.mapper_line.Update()
        self.actor_line.Modified()

        # update control_handle
        if hasattr(self, 'src_c0') and hasattr(self, 'src_c1'):
            self.src_c0.SetCenter(pts[0])
            self.src_c0.Update()
            self.mapper_c0.Update()
            self.actor_c0.Modified()
            self.src_c1.SetCenter(pts[1])
            self.src_c1.Update()
            self.mapper_c1.Update()
            self.actor_c1.Modified()

        # update arrows
        if hasattr(self, 'src_arrow0') and hasattr(self, 'src_arrow1'):
            def _update_matrix(__t, __mat):
                __m = vtk.vtkMatrix4x4()
                __m.DeepCopy(__t.GetMatrix())
                __m.SetElement(0, 3, __mat[0])
                __m.SetElement(1, 3, __mat[1])
                __m.SetElement(2, 3, __mat[2])
                __t.SetMatrix(__m)
                __t.Update()

            # arrow0
            _offset = self.line_param['vec'] * 0.07 * -1
            _new_arrow0_pos = np.add(self.actor_c0.GetCenter(), _offset)
            __t = self.tpd_arrow0.GetTransform()
            _update_matrix(__t, _new_arrow0_pos)
            self.tpd_arrow0.Update()
            self.mapper_arrow0.Update()
            self.actor_arrow0.Modified()

            # arrow1
            _offset = self.line_param['vec'] * 0.07
            _new_arrow1_pos = np.add(self.actor_c1.GetCenter(), _offset)
            __t = self.tpd_arrow1.GetTransform()
            _update_matrix(__t, _new_arrow1_pos)
            self.tpd_arrow1.Update()
            self.mapper_arrow1.Update()
            self.actor_arrow1.Modified()

        # update circle
        if hasattr(self, 'actor_circle'):
            _pts = self.polydata_circle.GetPoints()
            _pts.Reset()
            _vec_tmp = np.subtract(pts[1], pts[0])
            _radius = np.linalg.norm(_vec_tmp) / 2
            self.generate_circle_points(radius=_radius, out_points=_pts)
            self.polydata_circle.Modified()
            self.tube_circle.Update()
            self.mapper_circle.Update()
            self.actor_circle.Modified()

    def get_actors(self):
        return self.actors

    def test(self, ren):
        self.ren_for_test = ren

    def interact(self, _actor, _pos, _cam):

        def _get_intersect_infos():
            """
            return : intersect point, plane A, plane B, line C
            """
            # line intersection of two planes
            vec_a = self.line_param['vec']
            up = _cam.GetViewUp()
            _v1 = _cam.GetPosition()
            _v2 = _cam.GetFocalPoint()
            view = np.subtract(_v2, _v1)
            hor = np.cross(up, view / np.linalg.norm(view))
            vec_b = np.array(hor if abs(np.dot(vec_a / np.linalg.norm(vec_a), up)) > 0.6 else up)
            vec_c = np.cross(vec_a, vec_b)  # plane1
            vec_c = vec_c / np.linalg.norm(vec_c)

            vec_d = np.cross(vec_b, view)  # plane2
            vec_d = vec_d / np.linalg.norm(vec_d)

            # equation
            d1 = (-1 * vec_c[0] * self.line_param['pt'][0][0]) + (-1 * vec_c[1] * self.line_param['pt'][0][1]) + (
                -1 * vec_c[2] * self.line_param['pt'][0][2])
            d2 = (-1 * vec_d[0] * _pos[0]) + (-1 * vec_d[1] * _pos[1]) + (-1 * vec_d[2] * _pos[2])
            a = np.array([vec_c, vec_d])
            b = np.array([-1 * d1, -1 * d2])

            # solve
            i = np.linalg.pinv(a)
            b = np.matmul(i, b)

            vec_line = vec_b
            pt0 = (vec_line * -1) + b
            pt1 = (vec_line * 1) + b
            vec1 = np.subtract(pt1, pt0)
            vec1 = vec1 / np.linalg.norm(vec1)
            vec2 = np.array(self.line_param['vec']) / np.linalg.norm(self.line_param['vec'])
            # param T, S
            _aa = np.array([[vec1[0], vec2[0] * -1], [vec1[1], vec2[1] * -1], [vec1[2], vec2[2] * -1]])
            _bb = np.array([[self.line_param['pt'][0][0] - pt0[0]],
                            [self.line_param['pt'][0][1] - pt0[1]],
                            [self.line_param['pt'][0][2] - pt0[2]]])
            pi_aa = np.linalg.pinv(_aa)
            val = np.matmul(pi_aa, _bb)
            _x = float((vec1[0] * val[0]) + pt0[0])
            _y = float((vec1[1] * val[0]) + pt0[1])
            _z = float((vec1[2] * val[0]) + pt0[2])
            return [_x, _y, _z], vec_c, vec_d, self.line_param['vec']

        # scale if arrow actor is picked
        op = 1
        if _actor is self.actor_arrow0 or _actor is self.actor_arrow1:
            if op == 0:
                # Solution 1 (Not Good!!)
                # Vector Projection
                N = np.divide(self.line_param['vec'], np.linalg.norm(self.line_param['vec']))
                A = np.subtract(pos[0:3], self.line_param['pt'][0])

                print("1111 :: ", N)
                print("2222 :: ", np.dot(A, N))
                val = N * np.dot(A, N)

                print("VAL", val)
                if hasattr(self, 'actor_c1'):
                    self.src_c1.SetCenter(val)
                    self.src_c1.Update()
                    self.mapper_c1.Update()
                    self.actor_c1.Modified()
            elif op == 1:
                # Solution 2 (Great!!)
                moved_point, plane_vec_a, plane_vec_b, line_vec_c = _get_intersect_infos()
                self.scale(_actor, moved_point)

                # ########################################
                # # test
                # if hasattr(self, 'ren_for_test') and self.ren_for_test:
                #     #
                #     _aa = create_sphere_actor(moved_point, 0.1, [0, 0, 1])
                #     self.actors_test.append(_aa)
                #     self.ren_for_test.AddViewProp(_aa)
                #     #
                #     __p0 = np.multiply(vec1, -10) + moved_point
                #     __p1 = np.multiply(vec1, 10) + moved_point
                #     _aaa = create_line_actor([__p0.tolist(), __p1.tolist()], 0.05, [0, 0.3, 0.4])
                #     self.actors_test.append(_aaa)
                #     self.ren_for_test.AddViewProp(_aaa)
                # #########################################

        # rotate if actor_c is picked
        if _actor is self.actor_c0 or _actor is self.actor_c1:

            # plane on axis which is cross(vec, hor_vec)
            _vec = self.line_param['vec'] / np.linalg.norm(self.line_param['vec'])
            _plane_vec = np.cross(_vec, self.line_param['hor_vec'])
            _plane_pos = self.center

            # line which is lied on view_vec
            _cam_f = _cam.GetFocalPoint()
            _cam_p = _cam.GetPosition()
            _line_vec = np.subtract(_cam_f, _cam_p)
            _line_pos = list(_pos[0:3])

            # calc intersection point line and plane
            # equation
            d_tmp1 = np.inner(_plane_vec, _plane_pos) * -1
            d_tmp2 = np.inner(_plane_vec, _line_pos)
            ai_bj_ck = np.inner(_plane_vec, _line_vec)
            d = d_tmp1 + d_tmp2

            val = -1*d / ai_bj_ck
            print("VALLLLLLLLLLLLLLL", val)

            _X = _line_vec[0] * val + _line_pos[0]
            _Y = _line_vec[1] * val + _line_pos[1]
            _Z = _line_vec[2] * val + _line_pos[2]

            # Test Actor
            # _aaa = create_sphere_actor([_X, _Y, _Z], 0.15)
            # self._ren.AddActor(_aaa)
            # self.actors_test.append(_aaa)

            # Get Angle
            _pt0 = self.center
            _pt1 = self.src_c1.GetCenter()
            _vec1 = np.subtract(_pt1, _pt0)
            _pt2 = [_X, _Y, _Z]
            _vec2 = np.subtract(_pt2, _pt0)
            _vec2 = _vec2 / np.linalg.norm(_vec2)
            angle = vtk.vtkMath.AngleBetweenVectors(_vec1, _vec2)
            print("Angle :: ", angle)

            _rotate_axis = _plane_vec if np.inner(self.line_param['hor_vec'], _vec2) >= 0 else _plane_vec * -1

            # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
            _rot_curve = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
            _v = np.multiply(_rotate_axis, math.sin(angle / 2))
            vtk.vtkMath.QuaternionToMatrix3x3([math.cos(angle / 2), _v[0], _v[1], _v[2]], _rot_curve)

            _mov = [[1, 0, 0, self.center[0]],
                    [0, 1, 0, self.center[1]],
                    [0, 0, 1, self.center[2]],
                    [0, 0, 0, 1]]
            __mov = [[1, 0, 0, self.center[0] * -1],
                    [0, 1, 0, self.center[1] * -1],
                    [0, 0, 1, self.center[2] * -1],
                    [0, 0, 0, 1]]

            _rot_curve[0] = _rot_curve[0] + [0]
            _rot_curve[1] = _rot_curve[1] + [0]
            _rot_curve[2] = _rot_curve[2] + [0]
            _rot_curve = _rot_curve + [[0, 0, 0, 1]]

            new_pt = list(self.src_c1.GetCenter())
            new_pt = np.matmul(np.array(__mov), new_pt+[1])
            new_pt = np.matmul(_rot_curve, new_pt)
            new_pt = np.matmul(_mov, new_pt)

            # Test Actor
            # _aaa = create_sphere_actor(new_pt[0:3], 0.2, [0,0,1])
            # self._ren.AddActor(_aaa)
            # self.actors_test.append(_aaa)

    def scale(self, picked_actor, picked_point):
        order_idx = (0, 1) if picked_actor is self.actor_arrow0 \
            else ((1, 0) if picked_actor is self.actor_arrow1 else None)

        if order_idx is None:
            return

        src = [self.src_c0, self.src_c1]
        src = [src[i] for i in order_idx]
        sign = [1, -1]
        sign = [sign[i] for i in order_idx]
        # offset from sphere to arrow
        offset1 = self.line_param['vec'] * 0.07 * sign[0]
        # offset from arrow end pos to arrow center
        offset2 = self.line_param['vec'] / np.linalg.norm(self.line_param['vec']) * (0.75 / 2) * sign[0]
        offset = offset1 + offset2
        picked_point = np.add(picked_point, offset)

        _c = list(src[0].GetCenter())
        displacement = np.subtract(picked_point, _c) * -1
        pair_point = np.add(src[1].GetCenter(), displacement)

        dest_pos = [picked_point, pair_point]
        dest_pos = [dest_pos[i] for i in order_idx]

        self.update_point(dest_pos)

    def rotate(self):
        pass


class DSI_3D_OBJ:
    def __init__(self, obj_info, _ren=None, *args, **kwds):
        self.obj_info = obj_info    # 0: actor, 1: transform, 2: transform polydata filter
        dsi_axis_x = DSI_SCALE(obj_info, _ren=_ren, axis=0)
        dsi_axis_y = DSI_SCALE(obj_info, _ren=_ren, axis=1)
        dsi_axis_z = DSI_SCALE(obj_info, _ren=_ren, axis=2)
        self.dsi_axises = [dsi_axis_x, dsi_axis_y, dsi_axis_z]

        # for test
        self._ren = _ren

    def get_actors(self):
        A = []
        for dsi in self.dsi_axises:
            A += dsi.get_actors()
        return A

    def interact(self, picked_actor, pos, cam):
        for dsi in self.dsi_axises:
            if picked_actor in dsi.get_actors():
                self.dsi_axises[dsi.axis].interact(picked_actor, pos, cam)
                break

    def on_mouse_move(self, picked_actor):
        for i, dsi in enumerate(self.dsi_axises):
            if picked_actor in dsi.get_actors():
                # self.dsi_axises[i].actor_line.SetVisibility(True)
                self.dsi_axises[i].actor_line.GetProperty().SetOpacity(1)
                self.dsi_axises[i].actor_circle.GetProperty().SetOpacity(1)

            else:
                # self.dsi_axises[i].actor_line.SetVisibility(False)
                self.dsi_axises[i].actor_line.GetProperty().SetOpacity(0.15)
                self.dsi_axises[i].actor_circle.GetProperty().SetOpacity(0.1)

    def test(self, ren):
        self.ren_for_test = ren
        for dsi in self.dsi_axises:
            dsi.test(ren)

    def get_test_actors(self):
        A = []
        for dsi in self.dsi_axises:
            A += dsi.actors_test
        return A

    def delete_test_actors(self):
        for dsi in self.dsi_axises:
            dsi.actors_test.clear()


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        self.setMouseTracking(True)  # get all mouse events

        self.frame = QFrame()

        self.vl = QVBoxLayout()
        self.hl = QHBoxLayout()

        self.w2 = QWidget()
        self.vl.addWidget(self.w2)
        self.w2.setLayout(self.hl)

        self.vtkWidget = QVTKRenderWindowInteractor()
        self.hl.addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()

        ISTYLES = cyCafe.cyVtkInteractorStyles()
        istyle1 = ISTYLES.get_vtk_interactor_style_volume_3d(2)
        self.vtkWidget._Iren.SetInteractorStyle(istyle1)

        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()
        self.iren.AddObserver("MouseMoveEvent", self.MouseMove)
        self.iren.AddObserver("RightButtonPressEvent", self.MouseRightPress)
        self.iren.AddObserver("KeyPressEvent", self.KeyPress)

        self.init_virtual_tooth()
        self.init_dsi()

        self.ren.ResetCamera()
        self.ren.GetActiveCamera().SetParallelProjection(True)
        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)

        self.show()

        self.iren.Initialize()
        self.resize(600, 700)

    def init_virtual_tooth(self):
        STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/Wax Up Model_2.stl"
        # STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/GroupGuide_1.stl"

        self.reader = vtk.vtkSTLReader()
        self.reader.SetFileName(STL_PATH)
        self.reader.Update()

        self.t = vtk.vtkTransform()
        self.tpd = vtk.vtkTransformPolyDataFilter()
        self.tpd.SetTransform(self.t)
        self.tpd.SetInputData(self.reader.GetOutput())
        self.tpd.Update()

        self.mapper = vtk.vtkPolyDataMapper()
        self.mapper.SetInputData(self.tpd.GetOutput())

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        self.ren.AddActor(self.actor)

        color = [0.9, 0.9, 0.9]
        self.actor.GetProperty().SetColor(color)
        self.actor.GetProperty().SetAmbient(0.3)
        self.actor.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
        self.actor.GetProperty().SetDiffuse(0.5)
        self.actor.GetProperty().SetDiffuseColor(color)
        self.actor.GetProperty().SetSpecular(1)
        self.actor.GetProperty().SetSpecularPower(10)
        self.actor.GetProperty().SetSpecularColor([.95, .95, .95])

    def init_dsi(self):
        self.is_translate = False

        _actor = self.actor if hasattr(self, 'actor') else None
        _tpd = self.tpd if hasattr(self, 'tpd') else None

        self.DSI = DSI_3D_OBJ([_actor, _tpd], _ren=self.ren)
        for a in self.DSI.get_actors():
            self.ren.AddViewProp(a)

        self.picker = vtk.vtkVolumePicker()
        self.picker.SetTolerance(1e-6)
        self.picker.SetVolumeOpacityIsovalue(0.1)

    def MouseRightPress(self, iren, event):
        x, y = iren.GetEventPosition()

    def KeyPress(self, iren, event):
        x, y = iren.GetEventPosition()

        if iren.GetKeySym() == 'space':
            print("Translate Mode :: ", self.is_translate)
            self.is_translate = not self.is_translate
            self.picker.Pick(x, y, 0, self.ren)
        elif iren.GetKeySym() == 'd':
            for a in self.DSI.get_test_actors():
                self.ren.RemoveActor(a)
            self.DSI.delete_test_actors()
            self.iren.Render()
        elif iren.GetKeySym() == 't':
            if not hasattr(self.DSI, 'ren_for_test'):
                self.DSI.test(self.ren)
            else:
                self.DSI.test(None)
                del self.DSI.ren_for_test

    def MouseMove(self, iren, event):
        x, y = iren.GetEventPosition()

        self.ren.SetDisplayPoint(x, y, 0)
        self.ren.DisplayToWorld()
        worldXYZ = self.ren.GetWorldPoint()

        if self.is_translate:
            self.DSI.interact(self.picker.GetActor(), worldXYZ, self.ren.GetActiveCamera())
            self.iren.Render()
        else:
            self.picker.Pick(x, y, 0, self.ren)
            self.DSI.on_mouse_move(self.picker.GetActor())
            self.iren.Render()


if __name__ == '__main__':
    window = MainWindow()
    _qapp.exec_()

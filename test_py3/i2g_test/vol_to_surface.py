import vtk
from vtk.util import numpy_support
import numpy as np

import cyCafe


def vtkImageToNumPy(image, pixelDims):
    pointData = image.GetPointData()
    arrayData = pointData.GetArray(0)
    ArrayDicom = numpy_support.vtk_to_numpy(arrayData)
    ArrayDicom = ArrayDicom.reshape(pixelDims, order='F')
    return ArrayDicom


def vtk_volume_show(vtkImg):
    m = vtk.vtkGPUVolumeRayCastMapper()
    p = vtk.vtkVolumeProperty()
    c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
    a = vtk.vtkVolume()
    ren = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    iren = vtk.vtkRenderWindowInteractor()

    m.SetInputData(vtkImg)
    m.SetUseJittering(True)
    a.SetMapper(m)
    a.SetProperty(p)

    opacityWindow = 4096
    opacityLevel = 3048
    p.SetIndependentComponents(True)
    c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
    o_f.AddSegment(opacityLevel - 0.5 * opacityWindow, 0.0, opacityLevel + 0.5 * opacityWindow, 1.0)
    p.SetColor(c_f)
    p.SetScalarOpacity(o_f)
    p.SetInterpolationTypeToLinear()

    ren.AddActor(a)
    renWin.AddRenderer(ren)
    iren.SetRenderWindow(renWin)
    ISTYLES = cyCafe.cyVtkInteractorStyles()
    istyle = ISTYLES.get_vtk_interactor_style_volume_3d(2)
    iren.SetInteractorStyle(istyle)

    # Render Start
    renWin.Render()
    iren.Initialize()
    iren.Start()


def vtk_poly_show(poly_data):
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(poly_data.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    ren = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    iren = vtk.vtkRenderWindowInteractor()

    ren.AddActor(actor)
    ren.SetBackground(1.0, 1.0, 1.0)
    renWin.AddRenderer(ren)
    iren.SetRenderWindow(renWin)
    ISTYLES = cyCafe.cyVtkInteractorStyles()
    istyle = ISTYLES.get_vtk_interactor_style_volume_3d(2)
    iren.SetInteractorStyle(istyle)

    # Render Start
    renWin.Render()
    iren.Initialize()
    iren.Start()

    # camera = renderer.MakeCamera()
    # camera.SetPosition(-500.0, 245.5, 122.0)
    # camera.SetFocalPoint(301.0, 245.5, 122.0)
    # camera.SetViewAngle(30.0)
    # camera.SetRoll(-90.0)
    # renderer.SetActiveCamera(camera)



if __name__ == '__main__':
    DCM_PATH = "/Users/scott/Dicom/HeadCrack/S0000000008/S0000000008/"
    reader = cyCafe.cyDicomReader()
    reader.read_dicom(DCM_PATH)
    vtkImg = reader.get_vtk_img()

    e = vtkImg.GetExtent()
    s = vtkImg.GetSpacing()

    ConstPixelDims = [e[1] - e[0] + 1, e[3] - e[2] + 1, e[5] - e[4] + 1]
    ConstPixelSpacing = s


    ArrayDicom = vtkImageToNumPy(vtkImg, ConstPixelDims)

    # threshold
    threshold = vtk.vtkImageThreshold()
    threshold.SetInputData(vtkImg)
    threshold.ThresholdByLower(1000)
    threshold.ReplaceInOn()
    threshold.SetInValue(0)  # set all values below x to 0
    threshold.ReplaceOutOn()
    threshold.SetOutValue(1)  # set all values above x to 1
    threshold.Update()

    # marching cube
    dmc = vtk.vtkDiscreteMarchingCubes()
    dmc.SetInputConnection(threshold.GetOutputPort())
    dmc.GenerateValues(1, 1, 1)
    dmc.Update()

    # smoother
    smoothingIterations = 10
    featureAngle = 120.0
    passBand = 0.001
    smoother = vtk.vtkWindowedSincPolyDataFilter()
    smoother.SetInputConnection(dmc.GetOutputPort())
    smoother.SetNumberOfIterations(smoothingIterations)
    smoother.BoundarySmoothingOff()
    smoother.FeatureEdgeSmoothingOff()
    smoother.SetFeatureAngle(featureAngle)
    smoother.SetPassBand(passBand)
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    print("VVVVVVVV", ArrayDicom)
    # vtk_volume_show(vtkImg)
    # vtk_volume_show(threshold.GetOutput())
    vtk_poly_show(smoother)

import numpy as np

import vtk.util.vtkAlgorithm
import vtk.numpy_interface.dataset_adapter as dsa
from vtk.util import numpy_support


class PointsSurfaceExtrudeFilter(vtk.util.vtkAlgorithm.VTKPythonAlgorithmBase):
    """
    Generate surface consisting of points by extruding points of polyline
    """

    def __init__(self, height=100, step=[0, 0, 1]):
        super().__init__(nInputPorts=1, inputType='vtkPolyData',
                         nOutputPorts=1, outputType='vtkPolyData')
        self.__height, self.__step = height, np.array(step)

    def RequestData(self, request, inInfo, outInfo):
        polyline = vtk.vtkPolyData.GetData(inInfo[0].GetInformationObject(0))
        surface = vtk.vtkPolyData.GetData(outInfo.GetInformationObject(0))

        # Construct sample points.
        L = dsa.WrapDataObject(polyline).GetPoints()
        X = L.ravel() + \
            np.tile(np.outer(range(self.__height)[::-1], self.__step), len(L))

        # Make surface that specifies sample points.
        P = vtk.vtkPoints()
        P.SetData(dsa.numpyTovtkDataArray(X.reshape(-1, 3)))
        surface.SetPoints(P)

        # Record image size for later use. (TODO I am not sure this is correct
        #   approach. Maybe, it is better to use RequestInformation() protocol.
        #   It works, anyway.)
        img_size = dsa.numpyTovtkDataArray(np.array([len(L), self.__height]))
        img_size.SetName('IMG SIZE')
        surface.GetFieldData().AddArray(img_size)

        return 1

    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, v):
        self.__height = v
        self.Modified()

    @property
    def step(self):
        return self.__step

    @step.setter
    def step(self, v):
        self.__step = np.array(v)
        self.Modified()


class ImageFlattenFilter(vtk.util.vtkAlgorithm.VTKPythonAlgorithmBase):
    """
    This filter is to convert PolyData (resampled image) to flatten vtkImageData
    """

    def __init__(self):
        super().__init__(nInputPorts=1, inputType='vtkPolyData',
                         nOutputPorts=1, outputType='vtkImageData')

    def RequestData(self, request, inInfo, outInfo):
        poly_data = vtk.vtkPolyData.GetData(inInfo[0].GetInformationObject(0))

        # Get image size.
        img_size = poly_data.GetFieldData().GetArray('IMG SIZE')
        width, height = img_size.GetValue(0), img_size.GetValue(1)

        # Make image.
        flatten = vtk.vtkImageData.GetData(outInfo.GetInformationObject(0))
        flatten.SetDimensions(width, height, 1)
        flatten.GetPointData().SetScalars(poly_data.GetPointData().GetArray(0))

        # Add Real Position
        real_pos = dsa.numpyTovtkDataArray(numpy_support.vtk_to_numpy(poly_data.GetPoints().GetData()))
        real_pos.SetName('REAL POS')
        flatten.GetPointData().AddArray(real_pos)
        # RP = flatten.GetPointData().GetArray('REAL POS')
        # RP = numpy_support.vtk_to_numpy(RP)
        # print("22222222222222222", RP)

        return 1


def make_polyline(P):
    """
    Make polyline connecting points P.
    """
    # Points & cell
    points = vtk.vtkPoints()
    points.SetData(dsa.numpyTovtkDataArray(np.array(P)))
    cells = vtk.vtkCellArray()
    cells.InsertNextCell(len(P), range(len(P)))

    # Make polyline using points and cell
    polyline = vtk.vtkPolyData()
    polyline.SetPoints(points)
    polyline.SetLines(cells)

    return polyline


P = [[52.80917, 74.87506, 0],
     [64.32781, 116.37835, 0],
     [86.53604, 138.93358, 0],
     [96.25214, 168.77589, 0],
     [106.66225, 177.45098, 0],
     [122.97142, 176.75697, 0],
     [138.23957, 148.99668, 0],
     [159.05979, 114.29633, 0],
     [171.37218, 76.19243, 0]]

pano_width0, pano_height0 = 500, 140
direction = [0, 0, 1]  # Sweep the line to form a surface (coronal)

polyline = make_polyline(P)

# Volume
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r"/Users/scott/Dicom/1")
reader.Update()  # for reading volume information for initialization

# Spline
spline = vtk.vtkSplineFilter()
spline.SetInputData(polyline)
print(polyline)
spline.SetSubdivideToSpecified()
spline.SetNumberOfSubdivisions(pano_width0 - 1)

# Point surface
surface = PointsSurfaceExtrudeFilter(pano_height0, direction)
surface.SetInputConnection(spline.GetOutputPort())

# Probe image with extruded surface.
sampler = vtk.vtkProbeFilter()
sampler.SetInputConnection(0, surface.GetOutputPort())
sampler.SetInputConnection(1, reader.GetOutputPort())

# Flatten probed image.
flatten = ImageFlattenFilter()
flatten.SetInputConnection(sampler.GetOutputPort())

if 1:
    # Mapper and actor
    mapper = vtk.vtkDataSetMapper()
    mapper.SetInputConnection(flatten.GetOutputPort())
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    # Window level
    wlLut = vtk.vtkWindowLevelLookupTable()
    wlLut.SetWindow(2000)
    wlLut.SetLevel(100)
    mapper.SetLookupTable(wlLut)
    mapper.SetScalarRange(*reader.GetOutput().GetScalarRange())
else:
    mapper = vtk.vtkImageSliceMapper()
    mapper.SetInputConnection(flatten.GetOutputPort())
    mapper.BorderOn()
    mapper.SliceAtFocalPointOn()
    # mapper.SliceFacesCameraOn()
    actor = vtk.vtkImageSlice()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColorWindow(2000)
    actor.GetProperty().SetColorLevel(100)
    a = mapper.GetSliceNumberMinValue()
    b = mapper.GetSliceNumberMaxValue()
    mapper.SetSliceNumber(b)
    print("!!!!!!!!!!", a, b)

# Renderer and render window
ren = vtk.vtkRenderer()
ren.AddViewProp(actor)
ren.SetBackground(.2, .3, .4)
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(ren)


def _guide():
    P = vtk.vtkPoints()
    P.SetData(dsa.numpyTovtkDataArray(np.array([(0, 0, 0), (0, 100, 0), (0, 0, 100), (0, 200, 200)])))
    C = vtk.vtkCellArray()
    C.InsertNextCell(P.GetNumberOfPoints(), range(P.GetNumberOfPoints()))
    x = vtk.vtkPolyData()
    x.SetPoints(P)
    x.SetVerts(C)
    mpp = vtk.vtkPolyDataMapper()
    mpp.SetInputData(x)
    act = vtk.vtkActor()
    act.GetProperty().SetPointSize(10)
    # act.GetProperty().SetRenderPointsAsSpheres(True)
    # act.GetProperty().RenderPointsAsSpheresOn()
    # act.GetProperty().VertexVisibilityOn()
    act.SetMapper(mpp)
    return act


ren.AddViewProp(_guide())


# Mouse Event
def MouseWheelEvent(iren, e):
    d = (1 if e == 'MouseWheelForwardEvent' else  -1)

    # Position of plane
    P = dsa.WrapDataObject(spline.GetInputDataObject(0, 0)).GetPoints()
    P += [0, d, 0]

    # Number of spline's subdivision
    spline.SetNumberOfSubdivisions(spline.GetNumberOfSubdivisions() + d)

    # Image height and step
    surface.height += d
    surface.step = [0, 0, pano_height0 / surface.height]
    iren.Render()


# Interactor
rwi = vtk.vtkRenderWindowInteractor()
rwi.SetRenderWindow(renderWindow)

if 0:
    rwi.SetInteractorStyle(vtk.vtkInteractorStyleUser())
    rwi.AddObserver('MouseWheelForwardEvent', MouseWheelEvent)
    rwi.AddObserver('MouseWheelBackwardEvent', MouseWheelEvent)
else:
    rwi.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

rwi.Start()

import numpy as np


# 1 : array creation
print("1 :: ")
arr = np.array([1, 2, 3])
print(arr)

print("2 :: ")
# 2 : array creation
arr = np.arange(10)
print(arr)

print("3 :: ")
arr = np.arange(10, 20)
print(arr)

print("4 :: ")
arr = np.arange(10, 20, 2)
print(arr)

print("5 :: ")
# 3 : array creation
arr = np.linspace(0, 9, 10)
print(arr)

print("6 :: ")
# 4 : column stack
a = np.array([1, 2, 3])
b = np.array([7, 8, 9])
arr = np.column_stack((a, b))
print(arr)

print("7 :: ")
# 5 : random array creation
arr = np.random.rand(5, 5) * 25
print(arr.shape)
print(arr)

print("8 :: ")
# 6 : mean
mean = np.mean(arr)
print(mean)

print("9 :: ")
# 7 : variance
var = np.var(arr)
print(var)

print("10 :: ")
# 8 : standard deviation
std = np.std(arr)
print(std)

print("11 :: ")
# 9 : dot product
val = np.dot([1, 0, 0], [0.8, 0.2, 0.1])
print(val)

print("12 :: ")
# 10 : cross product
val = np.cross([1, 0, 0], [0, 1, 0])
print(val)

print("13 :: ")
# 11 : vector norm
val = np.linalg.norm([10, 20, 7])
print(val)

print("14 :: ")
# 12 : histogram
F, B = np.histogram(arr, bins=10)
print(F)
print(B)

print("15 :: ")
# 13 : nonzero
a = np.array([0, 1, 0, 3, 5, 2, 0])
a = a.nonzero()
print(a)

print("16 :: ")
a = np.array([[0, 1, 0, 3, 5, 2, 0], [9, 0, 0, 0, 5, 0, 3]])
y, x = a.nonzero()
print(x, y)

print("17 :: ")
# 14 : allclose
val = np.allclose([1.0, 1.0, 1.0], [1, 1, 1])
print(val)

print("18 :: ")
val = np.allclose([1.0, 1.0, 1.0], [1.2, 1, 1])
print(val)


class Promise():
    """
    Promise

    """
    NO_RESULT = object()

    def __init__(self, func=None):
        self._then_fn = None
        self._result = Promise.NO_RESULT
        if func:
            func(self.resolve)

    def resolve(self, result):
        if self._then_fn:
            self._then_fn(result)
        else:
            self._result = result

    def then(self, callback):
        self._then_fn = callback
        if self._result is not Promise.NO_RESULT:
            self._then_fn(self._result)


def myFunc1(a, b):
    print('This is Function1')
    val = a+b
    return Promise(lambda resolve: resolve(val))


def myFunc2(a, b):
    print('This is Function2')
    val = a*b
    return Promise(lambda resolve: resolve(val))


if __name__ == '__main__':
    myFunc1(3, 4).then(lambda r: myFunc2(r, 2).then(lambda r: print('result : ', r)))

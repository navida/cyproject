import asyncio


async def func1():
    await asyncio.sleep(5)
    return 10


async def func2():
    await asyncio.sleep(3)
    return 20


async def func():
    a = await func1()
    print('aaa')
    b = await func2()
    print('bbb')
    return a+b


def got_result(future):
    print(future.result())
    loop.stop()


loop = asyncio.get_event_loop()
task = loop.create_task(func())
task.add_done_callback(got_result)
loop.run_forever()

from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QRectF, Qt, QUrl, QByteArray, QBuffer, QIODevice, \
    QSize, QTimer, QFile, QObject, QPoint
from PyQt5.QtGui import QColor, QGuiApplication, QPainter, QPen, QImage, QOpenGLFramebufferObject, \
    QOpenGLFramebufferObjectFormat, QWindow, QSurface, QSurfaceFormat, QOpenGLContext, QOffscreenSurface, QOpenGLContext, \
    QPixmap
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import QQuickPaintedItem, QQuickItem, QQuickView, QQuickRenderControl, QQuickWindow, QQuickFramebufferObject, \
    QSGMaterial, QSGMaterialShader, QSGMaterialType, QSGGeometry, QSGGeometryNode, QSGNode

import textwrap


class MyMaterialShader(QSGMaterialShader):
    def __init__(self):
        super().__init__()

        self._matrix_uniform_id = None

        print("MyMaterialShader() created")

    def updateState(self, state, material, old_material):
        if state.isMatrixDirty():
            self.program().setUniformValue(
                self._matrix_uniform_id, state.combinedMatrix())

        if not isinstance(material, MyMaterial):
            return

    def attributeNames(self):
        return ["qt_VertexPosition", "qt_VertexTexCoord"]

    def initialize(self):
        self._matrix_uniform_id = self.program().uniformLocation("qt_Matrix")

    def vertexShader(self):
        return textwrap.dedent("""\
            uniform highp mat4 qt_Matrix;
            attribute highp vec4 qt_VertexPosition;
            attribute highp vec2 qt_VertexTexCoord;
            varying highp vec2 qt_TexCoord;
            void main()
            {
                qt_TexCoord = qt_VertexTexCoord;
                gl_Position = qt_Matrix * qt_VertexPosition;
            }
            """)

    def fragmentShader(self):
        return textwrap.dedent("""\
            varying highp vec2 qt_TexCoord;
            void main()
            {
                gl_FragColor = vec4(qt_TexCoord.x, qt_TexCoord.y, 1, 1);
            }
            """)

    def __del__(self):
        print("MyMaterialShader() destroyed")


class MyMaterial(QSGMaterial):
    _type = QSGMaterialType()

    def __init__(self):
        super().__init__()

    def compare(self, other_material):
        if self is other_material:
            return 0
        else:
            return -1 if id(self) < id(other_material) else 1

    def createShader(self):
        shader = MyMaterialShader()

        # Problem #1: created in Python QSGMaterialShader object is being
        # destroyed when not referenced by Python.
        # QSGMaterialShader's are cached inside Qt and rarely destroyed,
        # so Qt fails with segfault when tries to use destroyed object.

        # Workaround: store Python QSGMaterialShader objects indefinitely.
        self._shader = shader  # uncomment this for workaround

        return shader

    def type(self):
        return self._type


class MyQQuickItem(QQuickPaintedItem):

    sigText = pyqtSignal()
    sigCapture = pyqtSignal(QPainter)

    @pyqtProperty(str)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @pyqtProperty(QColor)
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = QColor(color)

    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self._name = ''
        self._color = QColor()

        self.setFlag(QQuickItem.ItemHasContents)
        self.setAcceptedMouseButtons(Qt.AllButtons)

        # self.fbo = None
        # print('render target (prev) : ', self.renderTarget())
        # self.setRenderTarget(QQuickPaintedItem.FramebufferObject)
        # print('render target (after) : ', self.renderTarget())
        # fbo = QQuickFramebufferObject(self)
        # print(fbo)
        # print(fbo)

    @pyqtSlot()
    def on_component_completed(self):
        print('connect Sig, Slot')
        self.sigCapture.connect(self.capture)

        # self.tm = QTimer()
        # self.tm.timeout.connect(self.capture)
        # self.tm.start(3000)

    def paint(self, painter):
        print('this is paint')
        painter.setPen(QPen(self._color, 2))
        painter.setRenderHints(QPainter.Antialiasing, True)

        rect = QRectF(0, 0, self.width(), self.height()).adjusted(1, 1, -1, -1)
        painter.drawEllipse(rect)

        self.sigCapture.emit(painter)

        ##########################

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # self.wimage.save(buf, 'JPG')
        # self.wimage.save('cccccc.jpg')
        # img.save('ccc.png')

        # QTimer.singleShot(0, self.capture)

        # self.sigCapture.connect(pyqtSlot()(lambda: print('sdfsdfsdf')))

        # self.sigCapture.emit()

    def mouseMoveEvent(self, e):
        print('m_move', e)

    def mousePressEvent(self, e):
        print('m_press', e)
        self.update()
        # self.sigText.emit()

    def test_fun(self):
        print('this is test function!!', self.window().geometry())

    @pyqtSlot(QPainter)
    def capture(self, _painter):
        print('captured!')
        # print('w, h of qquicpainteditem : ', self.width(), self.height())
        win = self.window()
        # print('w, h of qquickview : ', win.size(), win.effectiveDevicePixelRatio())
        img = win.grabWindow()
        # print('w, h of qimage : ', img.width(), img.height())
        img.save('captured.png')

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # img.save(buf, 'PNG')

        # img.save('captured.png')
        #

        # print('captured!')

        # s = QSize(self.width(), self.height())
        # self.wimage = QImage(s, QImage.Format_RGB32)
        # painter = QPainter(self.wimage)
        # self.render(painter)

        # Encode.
        # ba = QByteArray()
        # buf = QBuffer(ba)
        # buf.open(QIODevice.WriteOnly)
        # self.wimage.save(buf, 'PNG')
        # self.wimage.save('captured.png')

    def updatePaintNode(self, root_node, node_data):
        print('update paint node', root_node)
        if root_node is None:
            print('update paint node_inner')

            root_node = QSGGeometryNode()

            material = MyMaterial()
            root_node.setMaterial(material)
            root_node.setFlag(QSGNode.OwnsMaterial)

            x = 0
            y = 0
            width = self.width()
            height = self.height()

            geometry = QSGGeometry(
                QSGGeometry.defaultAttributes_TexturedPoint2D(),
                5)
            geometry.setDrawingMode(QSGGeometry.GL_TRIANGLE_STRIP)
            root_node.setGeometry(geometry)
            root_node.setFlag(QSGNode.OwnsGeometry)

            vertices = geometry.vertexDataAsTexturedPoint2D()
            vertices[0].set(x, y, 0, 0)
            vertices[1].set(x + width, y, 1, 0)
            vertices[2].set(x + width, y + height, 1, 1)
            vertices[3].set(x, y + height, 0, 1)
            vertices[4].set(x, y, 0, 0)

            root_node.markDirty(QSGNode.DirtyGeometry)
            root_node.markDirty(QSGNode.DirtyMaterial)

            # According to documentation, lifetime of QSGNode is managed by Qt,
            # so we cannot safely store it in Python.
            # Transfer ownership to C++ to workaround this issue.
            import sip
            sip.transferto(root_node, root_node)
        else:
            geom = root_node.geometry()
            vertices = geom.vertexDataAsTexturedPoint2D()
            print('list of vertices : ', geom.sizeOfVertex())

        return root_node


# class RenderControl(QQuickRenderControl):
#     def __init__(self, w, *args, **kwds):
#         super().__init__(w, *args, **kwds)
#         print(w)
#         self.win = w
#
#     def renderWindow(self, offset):
#         # if offset:
#         #     offset = QPoint(0, 0);
#
#         return self.win
#

class MyQQuickView(QWindow):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        # self.fbo = None

        # self.renderControl = RenderControl(self)
        # self.view = QQuickWindow(renderControl=self.renderControl)

        self.view = QQuickView()
        self.view.setResizeMode(QQuickView.SizeRootObjectToView)
        self.view.setSource(
            QUrl.fromLocalFile(
                os.path.join(os.path.dirname(__file__), 'app.qml')))

        self.view.beforeRendering.connect(self.on_before_rendering)
        self.view.afterRendering.connect(self.on_after_rendering)
        self.view.sceneGraphInitialized.connect(self.createFbo)

        # self.rootItem = self.view.rootObject()
        # self.rootItem.setFlag(QQuickItem.ItemHasContents)
        # self.rootItem.update()

        def _do_offscreen():
            # self.view.setVisibility(QWindow.Hidden)
            # self.view.setWindowState(Qt.WindowActive)
            self.view.show()
            # self.view.setVisible(False)
            # self.view.windowStateChanged.emit(Qt.WindowActive)
            # self.view.setVisibility(QWindow.Windowed)
            # self.view.setVisibility(QWindow.Hidden)
            # self.view.hide()
        QTimer.singleShot(0, _do_offscreen)

        self.show()


        # self.view.setVisibility(QWindow.Hidden)
        # self.view.hide()
        # self.view.setVisible(True)
        # self.view.show()

        # if self.fbo is None:
        # self.surface_format = QSurfaceFormat()
        # self.surface_format.setDepthBufferSize(16)
        # self.surface_format.setStencilBufferSize(8)
        # self.setFormat(self.surface_format)

        # self.context = QOpenGLContext(self.view)
        # self.context.setFormat(self.surface_format)
        # self.context.create()
        # self.context.makeCurrent(self.view)

        # self.offscreenSurface = QOffscreenSurface()
        # self.offscreenSurface.setFormat(self.context.format())
        # self.offscreenSurface.create()

        # self.format = QOpenGLFramebufferObjectFormat()
        # self.format.setAttachment(QOpenGLFramebufferObject.NoAttachment)
        # self.fbo = QOpenGLFramebufferObject(self.view.size()*self.view.devicePixelRatio(), self.format)
        # self.view.setRenderTarget(self.fbo)

    @pyqtSlot()
    def on_before_rendering(self):
        # print('before rendering!')

        # if self.fbo is None:
        #     format = QOpenGLFramebufferObjectFormat()
        #     format.setAttachment(QOpenGLFramebufferObject.CombinedDepthStencil)
        #     self.fbo = QOpenGLFramebufferObject(self.view.size()*self.view.devicePixelRatio(), format)

            # self.fbo = QOpenGLFramebufferObject(self.view.size())
            # self.view.setRenderTarget(self.fbo.data())
        pass

    @pyqtSlot()
    def on_after_rendering(self):
        # print('after rendering!')
        pass

    def resizeEvent(self, e):
        print('resize!!!!!!!!!!!!!!!!!!!!!!!!!1', e.size())

        # self.view.show()
        # self.view.hide()

        self.view.resize(e.size())
        # self.resize(e.size())
        self.view.resizeEvent(e)
        self.view.update()

        rootItem = self.view.rootObject()
        # rootItem.setWidth(e.size().width())
        # rootItem.setHeight(e.size().height())

        for i in rootItem.childItems():
            if isinstance(i, MyQQuickItem):
                i.update()
                #         i.test_fun()

        # self.win.resizeEvent(e)

        # self.view.setWidth(e.size().width())
        # self.view.setHeight(e.size().height())

        # self.view.contentItem().setWidth(e.size().width())
        # self.view.contentItem().setHeight(e.size().height())

        # self.view.setTextureSize(e.size())
        # self.view.update()
        # self.resizeFbo()

        super().resizeEvent(e)

        # self.view.show()
        # self.view.hide()

    def resizeFbo(self):
        self.createFbo()

    def createFbo(self):
        print('create fbo')
        # fbo_format = QOpenGLFramebufferObjectFormat()
        # fbo_format.setAttachment(QOpenGLFramebufferObject.NoAttachment)
        # self.fbo = QOpenGLFramebufferObject(self.view.size() * self.view.devicePixelRatio(), fbo_format)
        # self.view.setRenderTarget(self.fbo)


if __name__ == '__main__':
    import os
    import sys

    app = QGuiApplication(sys.argv)

    qmlRegisterType(MyQQuickItem, "MyItems", 1, 0, "MyQQuickItem")

    # view = QQuickView()
    v = MyQQuickView()
    # view = v.view
    # view.setResizeMode(QQuickView.SizeRootObjectToView)
    # view.setSource(
    #         QUrl.fromLocalFile(
    #                 os.path.join(os.path.dirname(__file__), 'app.qml')))
    # view.show()

    # rootItem = view.rootObject()
    # rootItem.setFlag(QQuickItem.ItemHasContents)
    # rootItem.update()

    sys.exit(app.exec_())


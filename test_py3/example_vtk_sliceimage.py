import vtk
import cyCafe

import vtk.numpy_interface.dataset_adapter as dsa
import numpy as np

import copy

""" UnCompressed, Signed CT """
DCM_PATH = r"/Users/jeongjihong/dicom/dicom1 복사본/"
""" UnCompressed, UnSigned CT """
# DCM_PATH = r"/Users/scott/Dicom/HeadCrack/S0000000008/S0000000008"""""""


p = vtk.vtkImageProperty()
m, a = vtk.vtkImageResliceMapper(), vtk.vtkImageSlice()
plane = vtk.vtkPlane()


if 1:
    imageData = vtk.vtkImageData()
    dcm_reader = cyCafe.cyDicomReader()
    dcm_reader.read_dicom(DCM_PATH)
    vtk_img = dcm_reader.get_vtk_img()
else:
    # source
    reader = vtk.vtkDICOMImageReader()
    reader.SetDirectoryName(DCM_PATH)
    reader.Update()
    vtk_img = reader.GetOutput()

(xMin, xMax, yMin, yMax, zMin, zMax) = e = vtk_img.GetExtent()
print("Ex!! ", e)
(xSpacing, ySpacing, zSpacing) = s = vtk_img.GetSpacing()
print("Spaci!! ", s)
(x0, y0, z0) = o = vtk_img.GetOrigin()
print("Ori!! ", o)

# center of volume
center = [x0 + xSpacing * 0.5 * (xMin + xMax),
          y0 + ySpacing * 0.5 * (yMin + yMax),
          z0 + zSpacing * 0.5 * (zMin + zMax)]

# set cutting plane
plane.SetOrigin(center)
plane.SetNormal(0, 0, 1)
viewUp = [0, -1, 0]
i, j, k = 0, 0, -zMax

# mapper
# m.SetInputConnection(reader.GetOutputPort())
m.SetInputData(vtk_img)
m.SetSlicePlane(plane)

# property
p.SetColorWindow(2000)
p.SetColorLevel(800)
p.SetAmbient(0.0)
p.SetDiffuse(1.0)
p.SetOpacity(1.0)
p.SetInterpolationTypeToLinear()

# actor
a.SetMapper(m)
a.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# camera
camera = ren.GetActiveCamera()
camera.ParallelProjectionOn()
ren.ResetCameraClippingRange()
camera.SetParallelScale(0.5 * (yMax-yMin+1)*ySpacing)
camera.SetFocalPoint(center)
camera.SetPosition(center[0]+i, center[1]+j, center[2]+k)
camera.SetViewUp(viewUp)

# add actor
ren.AddViewProp(a)

# set interactor style
style = vtk.vtkInteractorStyleUser()
iren.SetInteractorStyle(style)


# custom interaction
def MouseWheelEvent(iren, event=''):
    if event == r"MouseWheelForwardEvent":
        center[2] += 1
    elif event == r"MouseWheelBackwardEvent":
        center[2] -= 1
    plane.SetOrigin(center)
    m.SetSlicePlane(plane)
    renWin.Render()


def MouseLeftPress(iren, event=''):
    _x, _y = iren.GetEventPosition()
    _renders = iren.GetRenderWindow().GetRenderers()
    _ren = _renders.GetFirstRenderer()

    picker = vtk.vtkVolumePicker()
    picker.Pick(_x, _y, 0, _ren)
    _world_pos = picker.GetPickPosition()
    _vol_pos = picker.GetPointIJK()
    _id = picker.GetPointId()
    print('clicked position!! : ', _world_pos, _vol_pos, _id)

    # mask
    A = dsa.WrapDataObject(vtk_img).GetPointData().GetArray(0)
    width, height = (yMax+1), (xMax+1)
    offset = width * height * _vol_pos[2]
    convolution(A, offset, width, height)
    vtk_img.Modified()
    renWin.Render()

def convolution(img, offset, width, height):
    _img = np.array(copy.deepcopy(img[offset:offset + width * height]))

    # Gaussian
    # 3x3
    #MASK = np.array([1, 2, 1,
    #                 2, 4, 2,
    #                 1, 2, 1]) / 16

    # sigma 1
    # MASK = np.array([1, 4, 7, 4, 1,
    #                  4, 16, 26, 16, 4,
    #                  7, 26, 41, 26, 7,
    #                  4, 16, 26, 16, 4,
    #                  1, 4, 7, 4, 1]) / 273

    # sigma 1.4
    # MASK = np.array([2, 4, 5, 4, 2,
    #                  4, 9, 12, 9, 4,
    #                  5, 12, 15, 12, 5,
    #                  4, 9, 12, 9, 4,
    #                  2, 4, 5, 4, 2]) / 115

    #
    #MASK = np.array([-1, 0, 1,
    #                 -1, 0, 1,
    #                 -1, 0, 1])

    # MASK = np.array([1, 1, 1,
    #                  0, 0, 0,
    #                  -1, -1, -1])

    # MASK = np.ones(25) / 25
    # MASK = np.ones(9) / 9
    MASK = np.array([0, -0.5, 0, -0.5, 3, -0.5, 0, -0.5, 0])


    # MASK = np.array([1, 2, 1,
    #                  2, 16, 2,
    #                  1, 2, 1]) / 28


    # RESULT = np.zeros(width * height)
    # for j in range(1, height-1):
    #     for i in range(1, width-1):
    #         value = 0
    #         for u in range(-1, 2):
    #             for v in range(-1, 2):
    #                 value = value + _img[(j + u) * width + (i + v)] * MASK[(v+1) * 3 + (u+1)]
    #         RESULT[j*width+i] = value

    RESULT = np.convolve(_img, MASK, 'same')
    img[offset:offset + width * height] = RESULT


iren.AddObserver("MouseWheelForwardEvent", MouseWheelEvent)
iren.AddObserver("MouseWheelBackwardEvent", MouseWheelEvent)
iren.AddObserver("LeftButtonPressEvent", MouseLeftPress)

# render start
renWin.Render()
iren.Initialize()
iren.Start()


class Promise:
    """
    Promise

    """

    NO_RESULT = object()

    def __init__(self, func=None):
        self._then_fn = None
        self._result = Promise.NO_RESULT
        if func:
            func(self.resolve)

    def resolve(self, result):
        if self._then_fn:
            self._then_fn(result)
        else:
            assert self._result is Promise.NO_RESULT  # TODO Handle repetition.
            self._result = result

    def then(self, callback):
        assert self._then_fn is None  # TODO Handle.
        self._then_fn = callback
        if self._result is not Promise.NO_RESULT:
            self._then_fn(self._result)


def myFunc3():
    print('This is Function3')
    return Promise(lambda resolve: resolve(100))


def myFunc2():
    print('This is Function2')
    return Promise(lambda resolve: resolve(10))


def myFunc1(x, y):
    print('This is Function1')
    return Promise(
        lambda resolve: myFunc2().then(
            lambda v2: myFunc3().then(
                lambda v3: resolve(v2+v3+x+y)
            )
        )
    )


if __name__ == '__main__':
    myFunc1(3, 4).then(lambda r: print('Result :', r))

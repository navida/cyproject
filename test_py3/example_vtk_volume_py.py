import vtk
import cyCafe

import vtk.numpy_interface.dataset_adapter as dsa
import numpy as np

import time


""" UnCompressed, Signed CT """
# DCM_PATH = r"D:\\DICOM\\3D_Demo\\S0000000007\\Demo\\1"
""" UnCompressed, UnSigned CT """
DCM_PATH = r"/Users/jeongjihong/dicom/S0000000019"

""" Compressed """
# DCM_PATH = r"/Users/scott/Dicom/Yoshida"

# DCM_PATH = r"C:\\OnDemand3DApp\\Users\\Common\\MasterDB\\IMGDATA\\20121101\\S0000000024"
# DCM_PATH = r"D:\DICOM\Yoshida Fusion DICOM\DATA_1"
# DCM_PATH = r"D:\DICOM\Yoshida Fusion DICOM\DATA_2"
# DCM_PATH = r"C:\OnDemand3DApp\Users\Common\MasterDB\IMGDATA\20110413\S0000000019"
# DCM_PATH = r"D:\DICOM\서울대병원 CBCT\20150211\CT3D"
# DCM_PATH = r"D:\DICOM\KAVOExamVision\28"

# DCM_PATH = r"C:\OnDemand3DApp\Users\Common\MasterDB\IMGDATA\20150608\S0000000032"
# DCM_PATH = r"C:\OnDemand3DApp\Users\Common\MasterDB\IMGDATA\20110215\S0000000043"

c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
p = vtk.vtkVolumeProperty()
m, v = vtk.vtkSmartVolumeMapper(), vtk.vtkVolume()

opacityWindow = 4096
opacityLevel = 2048

start_time = time.time()

if 1:
    #imageData = vtk.vtkImageData()
    dcm_reader = cyCafe.cyDicomReader()
    # dcm_reader.read_dicom(["AAA", "BBB"])
    dcm_reader.read_dicom(DCM_PATH)
    vtk_img = dcm_reader.get_vtk_img()

else:
    # source
    reader = vtk.vtkDICOMImageReader()
    reader.SetDirectoryName(DCM_PATH)
    reader.Update()
    vtk_img = reader.GetOutput()

# Dicom Reading Time Check
elapsed_time = time.time() - start_time
print("elapsed time : ", elapsed_time)

# Extent
e = vtk_img.GetExtent()
print("Extent : ", e)
o = vtk_img.GetOrigin()
print("Origin : ", o)
s = vtk_img.GetSpacing()
print("Spaci : ", s)

# Histogram
A = dsa.WrapDataObject(vtk_img).GetPointData().GetArray(0)
_min, _max = np.min(A), np.max(A)
histo = np.histogram(A, bins=10)
print("Min, Max, Numpy Histogram", _min, _max, histo)

# mapper
# m.SetInputConnection(reader.GetOutputPort())
m.SetInputData(vtk_img)
m.SetBlendModeToMaximumIntensity()

# property
p.SetIndependentComponents(True)
c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
o_f.AddSegment(opacityLevel - 0.5*opacityWindow, 0.0, opacityLevel + 0.5*opacityWindow, 1.0)
p.SetColor(c_f)
p.SetScalarOpacity(o_f)
p.SetInterpolationTypeToLinear()

# actor
v.SetMapper(m)
v.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

ISTYLES = cyCafe.cyVtkInteractorStyles()
istyle = ISTYLES.get_vtk_interactor_style_volume_3d()
#istyle = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(istyle)

# camera
camera = ren.GetActiveCamera()
c = v.GetCenter()
print("VC ", c)
camera.SetFocalPoint(c)
camera.SetPosition(c[0], c[1]-e[1], c[2])
camera.SetViewUp(0, 0, 1)

ren.AddViewProp(v)
renWin.Render()
iren.Initialize()
iren.Start()

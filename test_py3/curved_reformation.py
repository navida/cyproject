import vtk
import numpy as np
from vtk.util import numpy_support
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase


class MySource(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=0,
                                        nOutputPorts=1, outputType='vtkPolyData')

    def RequestInformation(self, request, inInfo, outInfo):
        print("MySource RequestInformation:")
        #        print outInfo.GetInformationObject(0)
        return 1

    def RequestUpdateExtent(self, request, inInfo, outInfo):
        print("MySource RequestUpdateExtent:")
        #        print outInfo.GetInformationObject(0)
        return 1

    def RequestData(self, request, inInfo, outInfo):
        print("MySource RequestData:")
        #        print outInfo.GetInformationObject(0)
        return 1


class MyFilter(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=1, inputType='vtkPolyData',
                                        nOutputPorts=1, outputType='vtkImageData')
                                        # nOutputPorts=1, outputType='vtkPolyData')

    # def RequestInformation(self, request, inInfo, outInfo):
    #     print("MyFilter RequestInformation:")
    #
    #     in_info = inInfo[0]
    #     input = vtk.vtkPolyData.GetData(inInfo[0])
    #     info = outInfo.GetInformationObject(0)
    #     output = vtk.vtkPolyData.GetData(info)
    #
    #     print('in info : ', in_info)
    #     print('input : ', input)
    #     # print('info : ', info)
    #     # print('output : ', output)
    #     return 1

    # def RequestUpdateExtent(self, request, inInfo, outInfo):
    #     print("MyFilter RequestUpdateExtent:")
    #
    #     in_info = inInfo[0]
    #     input = vtk.vtkPolyData.GetData(inInfo[0])
    #     info = outInfo.GetInformationObject(0)
    #     output = vtk.vtkPolyData.GetData(info)
    #
    #     print('in info : ', in_info)
    #     print('input : ', input)
    #     # print('info : ', info)
    #     # print('output : ', output)
    #     return 1

    def RequestData(self, request, inInfo, outInfo):
        print("MyFilter RequestData:")

        # in_info = inInfo[0]
        # input = vtk.vtkPolyData.GetData(inInfo[0])
        # info = outInfo.GetInformationObject(0)
        # output = vtk.vtkPolyData.GetData(info)
        #
        # print('in info : ', in_info)
        # print('input : ', input)
        # print('info : ', info)
        # print('output : ', output)

        info = inInfo[0].GetInformationObject(0)
        input = vtk.vtkPolyData.GetData(info)
        # print('sssssssssssssssss', input.GetPointData().GetArray(0))

        narray = numpy_support.vtk_to_numpy(input.GetPointData().GetArray(0))
        narray = narray.reshape([140, 1000, 1], order='F')
        tt = numpy_support.numpy_to_vtk(narray.ravel(), deep=True, array_type=vtk.VTK_DOUBLE)
        # tt = np.ravel(narray, order='C')
        # tt = numpy_support.numpy_to_vtk(tt)

        # scarlars = input.GetPointData().GetArray(0)
        # print('xxxxxxxxxxx', narray.shape, len(narray))


        info = outInfo.GetInformationObject(0)
        # outInfo.SetInformationObject(0, -----)

        output = vtk.vtkImageData.GetData(info)
        output.SetDimensions(1000, 1, 140)
        output.GetPointData().SetScalars(tt)
        # for j in range(140):
        #     for i in range(1000):
                # output.SetScalarComponentFromFloat(i, 0, j, 0, narray[i][0][j])
                # output.SetScalarComponentFromFloat(i, 0, j, 0, -10000)

        # output = vtk.vtkPolyData.GetData(info)
        # output.GetPointData().SetDimensions(1000, 1, 140)
        # output.GetPointData().SetScalars(tt)


        # output.GetPointData().SetActiveScalars('DICOMImage')
        # output.GetPointData().SetActiveAttribute(0, vtk.vtkDataSetAttributes.SCALARS)
        # output.Set(vtk.vtkDataObject.DATA_TYPE_NAME(), 'LIM')
        # print('cvcvxcvc', outInfo.GetInformationObject(0))
        # output.AllocateScalars(vtk.VTK_SHORT, 1)



        # for j in range(140):
        #     for i in range(1000):
        #         output.SetScalarComponentFromFloat(i, 0, j, 0, narray[i][0][j])
        # print('cvvvvxcvcxvcxvcxvcxv : ', output.GetPointData())

        # print('zzzzzzzzzzzzzzzz', output)

        return 1


def SweepLine(line, direction, distance, cols):
    rows = line.GetNumberOfPoints()
    # print('cccccccccccccccc', rows)
    spacing = distance / cols
    surface = vtk.vtkPolyData()

    # Generate the points
    cols += 1

    numberOfPoints = rows * cols
    numberOfPolys = (rows - 1) * (cols - 1)
    points = vtk.vtkPoints()
    points.Allocate(numberOfPoints)
    polys = vtk.vtkCellArray()
    polys.Allocate(numberOfPolys * 4)

    x = [0] * 3
    cnt = 0
    for row in range(rows):
        for col in range(cols):
            p = [0] * 3
            line.GetPoint(row, p)
            x[0] = p[0] + direction[0] * col * spacing
            x[1] = p[1] + direction[1] * col * spacing
            x[2] = p[2] + direction[2] * col * spacing
            points.InsertPoint(cnt, x)
            cnt += 1

    # Generate the quads
    pts = [0] * 4
    for row in range(rows - 1):
        for col in range(cols - 1):
            pts[0] = col + row * cols
            pts[1] = pts[0] + 1
            pts[2] = pts[0] + cols + 1
            pts[3] = pts[0] + cols
            polys.InsertNextCell(4, pts)

    surface.SetPoints(points)
    surface.SetPolys(polys)

    return surface



# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/1')
reader.Update()
print('reader : ', reader.GetOutput())

# Read the Polyline
polyLineReader = vtk.vtkPolyDataReader()
polyLineReader.SetFileName('/Users/scott/california/cloud_proto_repo/CurvedReformation/polyline.vtk')
polyLineReader.Update()

pano_width = 1000
pano_height = 140

spline = vtk.vtkSplineFilter()
spline.SetInputConnection(polyLineReader.GetOutputPort())
spline.SetSubdivideToSpecified()
spline.SetNumberOfSubdivisions(pano_width-1)

# Sweep the line to form a surface
# Axial
# direction = [0.0, 1.0, 0.0]
# distance = 200
# Coronal
direction = [0.0, 0.0, 1.0]
distance = pano_height

spline.Update()

surface = SweepLine(spline.GetOutput(), direction, distance, pano_height-1)
# print('surface !!! : ', surface)

# Probe the volume with the extruded surface
sampleVolume = vtk.vtkProbeFilter()
sampleVolume.SetInputConnection(1, reader.GetOutputPort())
sampleVolume.SetInputData(0, surface)
sampleVolume.Update()

# print('qqq222 : ', sampleVolume.GetOutput())
# print('www222 : ', sampleVolume.GetOutput().GetPointData())
# narray = numpy_support.vtk_to_numpy(sampleVolume.GetOutput().GetPointData().GetScalars())
# print('array : ', narray)


# s = MySource()
f = MyFilter()
f.SetInputConnection(sampleVolume.GetOutputPort())
f.Update()
print('Output!!! : ', f.GetOutputDataObject(0))
# alg = myVtkAlgorithm()
# alg.SetInputConnection(sampleVolume.GetOutputPort())
# alg.Update()
# print('qqqqqqqqqqqqqqqqqqqqqqqqq', alg.GetOutputDataObject(0))
# print('zzzzzzzzzzzzzzzzz', alg.GetOutput())


# _t = sampleVolume.GetSource()
# print('qqq222 : ', _t)

# window/level..
wlLut = vtk.vtkWindowLevelLookupTable()
s = reader.GetOutput().GetScalarRange()[0]
e = reader.GetOutput().GetScalarRange()[1]
windowing_range = 500
windowing_level = 20
wlLut.SetWindow(windowing_range)
wlLut.SetLevel(windowing_level)
# wlLut.Set

# lut = vtk.vtkColorTransferFunction()
# lut.AddRGBPoint(s, 0.0, 0.0, 0.0)
# lut.AddRGBPoint(e, 1.0, 0.0, 0.0)

# print('s, e : ', s, e)
# print('range, level : ', range, level, wlLut.GetWindow(), wlLut.GetLevel())

##########################################################
# data extent
(xMin, xMax, yMin ,yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
(xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
(x0, y0, z0) = reader.GetOutput().GetOrigin()

# center of volume
center = [x0 + xSpacing * 0.5 * (xMin + xMax),
          y0 + ySpacing * 0.5 * (yMin + yMax),
          z0 + zSpacing * 0.5 * (zMin + zMax)]

# set cutting plane
# Axial
# viewUp = [0, -1, 0]
# i, j, k = 0, 0, -zMax*3
# Coronal
viewUp = [0, 0, -1]
i, j, k = 0, yMax, 0

# Pano
# center = [int(pano_width/2), 0, 70]
# viewUp = [0, 0, -1]
# i, j, k = 0, 600, 0
# center = [500, 70, 0]
# viewUp = [0, -1, 0]
# i, j, k = 0, 0, -500

# plane = vtk.vtkPlane()
# plane.SetOrigin(center)
# plane.SetNormal(0, -1, 0)
# plane.SetNormal(0, 0, -1)
##########################################################

# Create a mapper and actor
mapper = vtk.vtkDataSetMapper()
# mapper = vtk.vtkImageResliceMapper()
mapper.SetInputConnection(sampleVolume.GetOutputPort())
# mapper.SetSlicePlane(plane)
mapper.SetLookupTable(wlLut)
mapper.SetScalarRange(s, e)

actor = vtk.vtkActor()
# actor = vtk.vtkImageSlice()
actor.SetMapper(mapper)

# Create a renderer, render window, and interactor
renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

# istyle = vtk.vtkInteractorStyleUser()
# renderWindowInteractor.SetInteractorStyle(istyle)

# Add the actors to the scene
renderer.AddActor(actor)
renderer.SetBackground(.2, .3, .4)

# Set the camera for viewing medical images
'''
# renderer.GetActiveCamera().SetViewUp(0, 0, 1)
# renderer.GetActiveCamera().SetPosition(0, 0, 0)
# renderer.GetActiveCamera().SetFocalPoint(0, 200, 200)
'''

# renderer.GetActiveCamera().ParallelProjectionOn()
# renderer.ResetCameraClippingRange()
# renderer.GetActiveCamera().SetParallelScale(0.5 * (yMax-yMin+1)*ySpacing)
renderer.GetActiveCamera().SetFocalPoint(center)
renderer.GetActiveCamera().SetPosition(center[0]+i, center[1]+j, center[2]+k)
renderer.GetActiveCamera().SetViewUp(viewUp)


# Render and interact
renderWindow.Render()
renderWindowInteractor.Start()

from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QRectF, Qt, QUrl, QByteArray, QBuffer, QIODevice, \
    QSize, QTimer, QFile, QObject, QPoint
from PyQt5.QtGui import QColor, QGuiApplication, QPainter, QPen, QImage, QOpenGLFramebufferObject, \
    QOpenGLFramebufferObjectFormat, QWindow, QSurface, QSurfaceFormat, QOpenGLContext, QOffscreenSurface, QOpenGLContext, \
    QPixmap
from PyQt5.QtQml import qmlRegisterType
from PyQt5.QtQuick import QQuickPaintedItem, QQuickItem, QQuickView, QQuickRenderControl, QQuickWindow, QQuickFramebufferObject
from PyQt5.Qt import QQuickWidget, QApplication


class RenderControl(QQuickRenderControl):
    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent)

        self.window = parent

    def renderWindow(self, offset):
        if offset:
            offset = QPoint(0, 0)
        return self.window, offset


class MyQQuickWindow(QWindow):
    def __init__(self, parent=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self.render_control = RenderControl(self)

        self.setSurfaceType(QSurface.OpenGLSurface)
        self.fbo = None
        format_surface = QSurfaceFormat()
        format_surface.setDepthBufferSize(16)
        format_surface.setStencilBufferSize(4)
        self.setFormat(format_surface)
        self.ctx = QOpenGLContext()
        self.ctx.setFormat(format_surface)
        self.ctx.create()
        # ctx.makeCurrent(m_offscreenSurface);

        self.quick_view = QQuickWindow(self.render_control)

        # self.render_control.initialize(self.ctx)
        # self.render_control.renderRequested.connect(lambda: print('render requested!!!!!!!!!!!!!'))


if __name__ == '__main__':
    import os
    import sys

    app = QApplication(sys.argv)

    win = MyQQuickWindow()
    # view.setResizeMode(QQuickView.SizeRootObjectToView)
    # win.quick_view.setSource(
    #         QUrl.fromLocalFile(
    #                 os.path.join(os.path.dirname(__file__), 'app2.qml')))

    # def _func():
    #     img = render_control.grab()
    #     print(img.save('captured.png'))
    # view.afterRendering.connect(_func)
    # view.setAttribute(103)

    win.show()
    # view.setVisible(False)
    # view.hide()

    sys.exit(app.exec_())


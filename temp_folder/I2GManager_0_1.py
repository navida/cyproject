import copy
import gc
import json
import math
import os
from collections import defaultdict

from .I2G_DENTAL_Window import Enum_Topbar_Group2, Enum_Topbar_Group3
from .._CYTHON_PACKAGES import cy_probe
# from ..IO import CyProject
from _IN2GUIDE_DENTAL.blocks.IO.CyProject import Project, I2G_Project, CAD_Project

try:
    import resource
    print_memory = lambda: print('Memory usage : % 2.2f MB' %
                                 round(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.0 / 1024.0, 1))
except ImportError:
    print_memory = lambda: print()

import numpy as np
import vtk
from PyQt5 import QtCore
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot

from _IN2GUIDE_DENTAL.blocks.I2G.Implant import VirtualTooth, Implant, SubGeom
from _IN2GUIDE_DENTAL.blocks.I2G.Nerve.NerveMgr import NerveMgr
from _IN2GUIDE_DENTAL.blocks.IO.Reader import cyReader
from _IN2GUIDE_DENTAL.blocks._SUPPORT_FOR_BLOCK import PolyData, DSI_3D
from _IN2GUIDE_DENTAL.blocks._SUPPORT_FOR_BLOCK import Materials
from _IN2GUIDE_DENTAL.blocks._SUPPORT_FOR_BLOCK import WorkerThread
from _IN2GUIDE_DENTAL.blocks._SUPPORT_FOR_BLOCK import get_vtkmat_from_list, get_matrix_from_vtkmat
from _qapp import create_task
from cyhub.block import Block_meta, cyhub_method
from .._SUPPORT_FOR_WIN import DBM_TreeView


class I2GManager_0_1(QtCore.QObject, metaclass=Block_meta):

    sigProgress = pyqtSignal(object, object)
    sigProgressInfinite = pyqtSignal(object, object)
    sigSetI2GTopbarStates = pyqtSignal(object)
    sigMakeProfile = pyqtSignal(object, object)

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self._key_modifiers = Qt.NoModifier

        self.initial_windowing_value = 3000, 2000
        self.initial_windowing_value_pano = 3500, 2000
        self.current_work_path = None

        self.VirtualTeeth = {}
        self.Implants = {}
        self.selected_implant = None

        self.current_screen_layout_type = 1

    def initialize(self):
        self.leaf_implant_manufacture_treeview_model = DBM_TreeView.CyTreeViewModel(["ManufacturerName", "ID"])
        self.leaf_implant_fixture_treeview_model = DBM_TreeView.CyTreeViewModel(["ManufacturerName", "ID", "FileName", "DiameterNominal", "NominalLength"])
        self.leaf_implant_abutment_treeview_model = DBM_TreeView.CyTreeViewModel(["ModelNumber", "ID", "FileName", "AbutmentHeight", "GingivalHeight", "AssembleDistance"])

        # Shared Image Proprerty
        self.img_prop = vtk.vtkImageProperty()
        self.img_prop.SetColorWindow(self.initial_windowing_value[0])
        self.img_prop.SetColorLevel(self.initial_windowing_value[1])
        self.img_prop.SetOpacity(0.8)
        self.img_prop.SetInterpolationTypeToLinear()

        # Image Prop for Pano
        self.img_prop_pano = vtk.vtkImageProperty()
        self.img_prop_pano.SetColorWindow(self.initial_windowing_value_pano[0])
        self.img_prop_pano.SetColorLevel(self.initial_windowing_value_pano[1])
        self.img_prop_pano.SetOpacity(0.8)
        self.img_prop_pano.SetInterpolationTypeToLinear()

        self.reader = cyReader()
        self.nerve_mgr = NerveMgr()
        self.prj = Project()
        async def _refresh():
            await self.i2g_3d.refresh()
            await self.i2g_pano.refresh()
            await self.i2g_2d_cross.refresh()
            await self.i2g_2d_parallel.refresh()
        self.nerve_mgr.sigUpdatedNerve.connect(lambda: create_task(_refresh()))

        async def _refresh():
            await self.i2g_3d.refresh()
            await self.i2g_3d.refresh()

        async def _do():
            await self.db.leaf_implant_manufacturer_initialize()
            _data = await self.db.get_d_manufacturer()
            self.leaf_implant_manufacture_treeview_model.leaf_implant_manufacture_treeview_item_addData(_data)
        create_task(_do())

        # async def async_init():
        #     await self.i2g_3d.set_nerve_mgr(self.nerve_mgr)
        #     await self.i2g_2d_cross.set_nerve_mgr(self.nerve_mgr)
        #     await self.i2g_2d_parallel.set_nerve_mgr(self.nerve_mgr)
        #     await self.i2g_pano.set_nerve_mgr(self.nerve_mgr)
        # create_task(async_init())

    def __del__(self):
        # TODO!!!
        self.reader.release()
        del self.reader

    @cyhub_method
    async def reset(self):

        if hasattr(self, 'VirtualTeeth'):
            while len(self.VirtualTeeth) > 0:
                i = self.VirtualTeeth.popitem()
                del i
            self.VirtualTeeth.clear()

        if hasattr(self, 'Implants'):
            while len(self.Implants) > 0:
                i = self.Implants.popitem()
                del i
            self.Implants.clear()

        self.selected_implant = None

        if hasattr(self, 'DSI'):
            self.DSI.reset()
            del self.DSI

        if hasattr(self, 'DSI_PANO'):
            self.DSI_PANO.reset()
            del self.DSI_PANO

        self.current_screen_layout_type = 1

        await self.i2g_3d.clear()
        await self.i2g_3d.reset()
        await self.i2g_2d_axial.clear()
        await self.i2g_2d_axial.reset()
        await self.i2g_2d_cross.clear()
        await self.i2g_2d_cross.reset()
        await self.i2g_2d_parallel.clear()
        await self.i2g_2d_parallel.reset()
        await self.i2g_pano.clear()
        await self.i2g_pano.reset()

        await self.reg_mgr.reset()

        self.current_work_path = None

        # Garbage Collect
        gc.collect()
        print_memory()

    @cyhub_method
    async def init_vtk_img(self, DCM_LIST):
        WorkerThread.STOP = False
        @QtCore.pyqtSlot()
        def _do(_DCM_LIST):
            # Step 1
            self.reader.free_dicom_image()
            # self.reader.set_with_analyze(True)

            if len(_DCM_LIST[0]) > 0:
                _tmp = _DCM_LIST[0].rfind('/')
                self.current_work_path = _DCM_LIST[0][0:_tmp]

            # Step 2
            event = WorkerThread.EVENT()
            self.sigProgress.emit(event, 0)
            event.wait()
            for i, dcm in enumerate(_DCM_LIST):
                if WorkerThread.STOP:
                    WorkerThread.STOP = False
                    self.reader.free_dicom_image()
                    event = WorkerThread.EVENT()
                    self.sigProgress.emit(event, 100)
                    return

                n = int((i+1) / len(_DCM_LIST) * 100)
                event = WorkerThread.EVENT()
                self.reader.read_next(dcm)
                self.sigProgress.emit(event, n)
                event.wait()

            # L = self.reader.get_dcm_info(True)
            # print("LLLLLLLLLLLLLL :: ", L)
            # print("PATIENT NAME !! : ", [j[0].decode(self.reader.determine_characterset(j[1].split('\\')[-1]))
            #                              for j in [[i['PatientName'], i['CharacterSet'].decode()]
            #                                        for i in L]])

            # Step 3
            self.reader.generate_vtk_img()
            # Step 4
            self.vtk_img = self.reader.get_vtk_img()

            async def _do_next():
                await self.i2g_3d.initialize_components(self.vtk_img, self.img_prop, self.nerve_mgr)
                await self.i2g_2d_axial.initialize_components(self.vtk_img, self.img_prop)
                self.nerve_mgr.clear_nerver_all()
                self.sigSetI2GTopbarStates.emit([None, Enum_Topbar_Group2.INITIAL, Enum_Topbar_Group3.INITIAL])
                await self.refresh_all()
            create_task(_do_next())

        if WorkerThread.is_running():
            print("Worker Thread is Running....\n It will be stopped!")
            WorkerThread.STOP = True
        WorkerThread.start_worker(_do, DCM_LIST, _finished_func=lambda: print("DICOMS ARE LOADED!! :)"))

    @cyhub_method
    async def init_model(self):
        _model_actor = await self.reg_mgr.get_model()
        Materials.get_property(Materials.MATERIAL_SKIN_GUM, _model_actor.GetProperty())
        _model_actor.GetProperty().SetOpacity(0.55)
        await self.i2g_3d.set_model(_model_actor)
        await self.i2g_2d_cross.set_model(_model_actor)
        await self.i2g_2d_parallel.set_model(_model_actor)
        await self.i2g_2d_axial.set_model(_model_actor)
        await self.refresh_all()

    @cyhub_method
    async def init_dlg_reg_model(self):
        assert self.vtk_img, "vtk_img is None!!!"
        _curve_info = await self.i2g_2d_axial.get_arch_curve()
        await self.reg_mgr.initialize_volume(self.vtk_img, self.img_prop, _curve_info)

    @cyhub_method
    async def create_pano(self, infos, _do_thread=False):
        """
        infos: 0: Points,  1: Tangent Vectors,  2: Horizontal Vectors,  3: Thickness
        """
        if await self.i2g_pano.is_initialized():
            await self.update_pano(*infos, _overview=False, _do_thread=True)
            return

        async def __do():
            # add to 2d img
            await self.i2g_pano.initialize_components(self.vtk_img, self.img_prop_pano, infos, self.nerve_mgr)
            await self.i2g_2d_cross.initialize_components(self.vtk_img, self.img_prop, infos, self.nerve_mgr)
            await self.i2g_2d_parallel.initialize_components(self.vtk_img, self.img_prop, infos, self.nerve_mgr)

            # add to 3d vol
            curved_line = await self.i2g_pano.get_curved_line()
            if curved_line:
                await self.i2g_3d.set_curved_line(curved_line)
            curved_pano = await self.i2g_pano.get_curved_pano()
            if curved_pano:
                await self.i2g_3d.set_curved_pano(curved_pano)
            cross_plane_info = await self.i2g_2d_cross.get_plane()
            if cross_plane_info:
                await self.i2g_3d.set_cross_plane(cross_plane_info)
            parallel_plane_info = await self.i2g_2d_parallel.get_plane()
            if parallel_plane_info:
                await self.i2g_3d.set_parallel_plane(parallel_plane_info)
            # init dsi slab
            axial_plane_info = await self.i2g_2d_axial.get_plane()
            await self.i2g_2d_axial.update_dsi('cross', cross_plane_info)
            await self.i2g_2d_axial.update_dsi('parallel', parallel_plane_info)
            await self.i2g_2d_cross.update_dsi('axial', axial_plane_info)
            await self.i2g_2d_cross.update_dsi('parallel', parallel_plane_info)
            await self.i2g_2d_parallel.update_dsi('axial', axial_plane_info)
            await self.i2g_2d_parallel.update_dsi('cross', cross_plane_info)
            await self.i2g_pano.update_dsi('axial', axial_plane_info)
            await self.i2g_pano.update_dsi('cross', [cross_plane_info[0], [1,0,0]])
            # init nerve
            _pano_origin = await self.i2g_pano.get_origin()
            self.nerve_mgr.set_cut_plane('pano', [_pano_origin, [0, -1, 0]])
            # reset img_prop & refresh all
            # self.img_prop.SetColorWindow(self.initial_windowing_value[0])
            # self.img_prop.SetColorLevel(self.initial_windowing_value[1])
            await self.refresh_all()

        if _do_thread:
            @pyqtSlot()
            def __do_thread():
                e = WorkerThread.EVENT()
                self.sigProgressInfinite.emit(e, True)
                e.wait()
                create_task(__do()).then(lambda x: self.sigProgressInfinite.emit(None, False))
            WorkerThread.start_worker(__do_thread)
        else:
            await __do()

    @cyhub_method
    async def update_pano(self, points, tan_vecs, hor_vecs, _overview=True, _do_thread=False):
        """
        NOTE Update Pano Now!!
        """
        async def __do():
            await self.i2g_pano.update_pano(points, tan_vecs, hor_vecs, _overview=_overview)
            await self.i2g_2d_cross.update_arch_info([points, tan_vecs, hor_vecs])
            await self.i2g_2d_parallel.update_arch_info([points, tan_vecs, hor_vecs])
            await self.updated_pano(_refresh=False)
            # then, set cross/parallel position
            _cross_plane = await self.i2g_2d_cross.get_plane()
            await self.updated_cross(_cross_plane, _refresh=False)

            # update implant pano
            if hasattr(self, 'VirtualTeeth'):
                for k in self.VirtualTeeth:
                    self.VirtualTeeth[k].update_from_generic()

            # update implant pano
            if hasattr(self, 'Implants'):
                for k in self.Implants:
                    self.Implants[k].on_update()

            # Refresh is called at last
            await self.refresh_all()

        if _do_thread:
            @pyqtSlot()
            def __do_thread():
                e = WorkerThread.EVENT()
                self.sigProgressInfinite.emit(e, True)
                e.wait()
                create_task(__do()).then(lambda x: self.sigProgressInfinite.emit(None, False))
            WorkerThread.start_worker(__do_thread, _finished_func=lambda: print("Pano Refresh!!"))
        else:
            await __do()

    @cyhub_method
    async def updated_pano(self, _refresh=True):
        """
        NOTE When Pano is Updated!!!
        """
        curved_line = await self.i2g_pano.get_curved_line()
        if curved_line:
            await self.i2g_3d.set_curved_line(curved_line)
        curved_pano = await self.i2g_pano.get_curved_pano()
        if curved_pano:
            await self.i2g_3d.set_curved_pano(curved_pano)
        cross_plane_info = await self.i2g_2d_cross.get_plane()
        if cross_plane_info:
            await self.i2g_3d.set_cross_plane(cross_plane_info)
        parallel_plane_info = await self.i2g_2d_parallel.get_plane()
        if parallel_plane_info:
            await self.i2g_3d.set_parallel_plane(parallel_plane_info)

        if hasattr(self, 'DSI') and self.DSI:
            # 0: cross,  1: parallel
            self.DSI.check_plane(0, cross_plane_info)
            self.DSI.check_plane(1, parallel_plane_info)

        if _refresh:
            # await self.refresh_all()
            await self.i2g_2d_cross.refresh()
            await self.i2g_2d_parallel.refresh()
            await self.i2g_2d_axial.refresh()

    @cyhub_method
    async def refresh_pano(self):
        curved_pano = await self.i2g_pano.get_curved_pano()
        if curved_pano:
            await self.i2g_3d.set_curved_pano(curved_pano)
        await self.i2g_pano.refresh()
        await self.i2g_3d.refresh()

    @cyhub_method
    async def updated_axial(self, plane_info, _refresh=True):
        await self.i2g_3d.update_axial(plane_info)
        await self.i2g_2d_cross.update_dsi('axial', plane_info)
        await self.i2g_2d_parallel.update_dsi('axial', plane_info)
        await self.i2g_pano.update_dsi('axial', plane_info)

        """
        Plane Check
        """
        # Lable
        await self.i2g_2d_axial.check_plane()

        """
        Refresh
        """
        if _refresh:
            await self.i2g_2d_axial.refresh()
            await self.i2g_2d_cross.refresh()
            await self.i2g_2d_parallel.refresh()
            await self.i2g_pano.refresh()
            await self.i2g_3d.refresh()

    @cyhub_method
    async def updated_cross(self, plane_info, _refresh=True):
        if self.selected_implant is not None and self.selected_implant > 0:
            _implant = self.Implants[self.selected_implant]
            _m = np.array(_implant.matrix).reshape(4,4)
            _up = _m[:3, 2].tolist()
        else:
            _up = [0, 0, 1]
        _parallel_vec = np.cross(plane_info[1], _up)
        _parallel_vec = _parallel_vec / np.linalg.norm(_parallel_vec)
        _parallel_view_up = np.cross(_parallel_vec, plane_info[1])
        _parallel_view_up = _parallel_view_up / np.linalg.norm(_parallel_view_up)
        _dist_sum = await self.i2g_2d_parallel.get_distance_sum()
        _displacement = _parallel_vec * _dist_sum
        _parallel_origin = plane_info[0] + _displacement
        _parallel_plane = [_parallel_origin, _parallel_vec]
        _axial_plane = await self.i2g_2d_axial.get_plane()

        if self.current_screen_layout_type == 1:
            await self.i2g_2d_parallel.set_plane(*_parallel_plane, _camera_fit=True)
            await self.i2g_2d_parallel.set_camera_view_up(_parallel_view_up)
            await self.i2g_2d_parallel.update_dsi('axial', _axial_plane)
            await self.i2g_2d_parallel.update_dsi('cross', plane_info)
            self.nerve_mgr.set_cut_plane('parallel', _parallel_plane)

        await self.i2g_2d_axial.update_dsi('cross', plane_info)
        await self.i2g_2d_axial.update_dsi('parallel', _parallel_plane)
        await self.i2g_2d_cross.update_dsi('axial', _axial_plane)
        await self.i2g_2d_cross.update_dsi('parallel', _parallel_plane)


        # TODO!!!  Rotation Infos of Cross and Pano are different each other.
        # Cross Plane Info on Pano Coordinator
        _angle = await self.i2g_2d_cross.get_angle_sum()
        _result_mat = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        vtk.vtkMath.QuaternionToMatrix3x3([math.cos(_angle/2), *np.multiply([0,-1,0], math.sin(_angle/2))], _result_mat)
        _v = np.matmul(np.array(_result_mat).reshape(3, 3), [1, 0, 0])
        _v = _v / np.linalg.norm(_v)
        await self.i2g_pano.update_dsi('cross', [plane_info[0], _v])

        await self.i2g_3d.set_cross_plane(plane_info)
        await self.i2g_3d.set_parallel_plane(_parallel_plane + [plane_info[1]])

        """
        Plane Check
        """
        # # DSI
        # if hasattr(self, 'DSI') and self.DSI:
        #     # 0: cross,  1: parallel
        #     self.DSI.check_plane(0, plane_info)
        #     self.DSI.check_plane(1, _parallel_plane)
        # check plane
        await self.check_plane()

        """
        Refresh
        """
        if _refresh:
            await self.refresh_all()

    @cyhub_method
    async def updated_cross_by_verification(self, plane_info, _refresh=True):
        if self.selected_implant is not None and self.selected_implant > 0:
            _implant = self.Implants[self.selected_implant]
            _m = np.array(_implant.matrix).reshape(4, 4)
            _up = _m[:3, 2].tolist()
        else:
            _up = [0, 0, 1]
        _parallel_vec = np.cross(plane_info[1], _up)
        _parallel_vec = _parallel_vec / np.linalg.norm(_parallel_vec)
        _parallel_origin = plane_info[0]
        _parallel_plane = [_parallel_origin, _parallel_vec, _up]
        _axial_plane = await self.i2g_2d_axial.get_plane()

        # set 2d
        await self.i2g_2d_axial.update_dsi('cross', plane_info)
        await self.i2g_2d_axial.update_dsi('parallel', _parallel_plane)
        await self.i2g_2d_cross.update_dsi('axial', _axial_plane)
        await self.i2g_2d_cross.update_dsi('parallel', _parallel_plane)

        if self.current_screen_layout_type == 1:
            await self.i2g_2d_parallel.set_plane(*_parallel_plane, _camera_fit=True)
            self.nerve_mgr.set_cut_plane('parallel', _parallel_plane)
            await self.i2g_2d_parallel.update_dsi('axial', _axial_plane)
            await self.i2g_2d_parallel.update_dsi('cross', plane_info)

        # set 3d
        _plane_cross = [plane_info[0], plane_info[1], _parallel_vec]
        _plane_parallel = [plane_info[0], _parallel_vec, plane_info[1] * -1]
        await self.i2g_3d.set_cross_plane(_plane_cross)
        await self.i2g_3d.set_parallel_plane(_plane_parallel)

        # check plane
        await self.check_plane()

        """
        Refresh
        """
        if _refresh:
            await self.refresh_all()

    @cyhub_method
    async def updated_parallel(self, plane_info, _refresh=True):
        await self.i2g_3d.set_parallel_plane(plane_info)
        await self.i2g_2d_cross.update_dsi('parallel', plane_info)
        await self.i2g_2d_axial.update_dsi('parallel', plane_info)

        """
        Plane Check
        """
        # DSI
        if hasattr(self, 'DSI') and self.DSI:
            # 0: cross,  1: parallel
            self.DSI.check_plane(1, plane_info)
        # Lable
        await self.i2g_2d_parallel.check_plane()

        """
        Refresh
        """
        if _refresh:
            await self.i2g_2d_parallel.refresh()
            await self.i2g_2d_cross.refresh()
            await self.i2g_2d_axial.refresh()
            await self.i2g_3d.refresh()

    @cyhub_method
    async def updated_parallel_by_verification(self, plane_info, _refresh=True):
        if self.selected_implant is not None and self.selected_implant > 0:
            _implant = self.Implants[self.selected_implant]
            _m = np.array(_implant.matrix).reshape(4, 4)
            _up = _m[:3, 2].tolist()
        else:
            _up = [0, 0, 1]

        _cross_vec = np.cross(_up, plane_info[1])
        _cross_vec = _cross_vec / np.linalg.norm(_cross_vec)
        _cross_origin = plane_info[0]
        _cross_plane = [_cross_origin, _cross_vec, _up]
        _axial_plane = await self.i2g_2d_axial.get_plane()

        # set 2d
        await self.i2g_2d_axial.update_dsi('cross', _cross_plane)
        await self.i2g_2d_axial.update_dsi('parallel', plane_info)
        await self.i2g_2d_cross.set_plane(*_cross_plane, _camera_fit=True)
        await self.i2g_2d_cross.update_dsi('axial', _axial_plane)
        await self.i2g_2d_cross.update_dsi('parallel', plane_info)
        await self.i2g_2d_parallel.update_dsi('axial', _axial_plane)
        await self.i2g_2d_parallel.update_dsi('cross', _cross_plane)

        self.nerve_mgr.set_cut_plane('cross', _cross_plane)

        # set 3d
        _plane_parallel = [plane_info[0], plane_info[1], _cross_vec]
        _plane_cross = [plane_info[0], _cross_vec, plane_info[1] * -1]
        await self.i2g_3d.set_cross_plane(_plane_cross)
        await self.i2g_3d.set_parallel_plane(_plane_parallel)

        # check plane
        await self.check_plane()

        """
        Refresh
        """
        if _refresh:
            await self.refresh_all()

    @cyhub_method
    async def set_reference_pos(self, idx, picked_pos):
        _axial_normal = [0,0,-1]
        _origin = list(self.vtk_img.GetCenter())
        _origin[2] = picked_pos[2]
        await self.i2g_2d_axial.set_plane(_origin, _axial_normal, _camera_fit=True)
        await self.updated_axial([_origin, _axial_normal], _refresh=False)

        # change corss plane, then the others are changed also.
        await self.i2g_2d_cross.set_reference_number(idx)
        _cross_plane = await self.i2g_2d_cross.get_plane()
        await self.updated_cross(_cross_plane)


    @cyhub_method
    async def do_auto_arch(self):
        SP, T, H = await self.i2g_2d_axial.calc_arch_curve()
        await self.create_pano([SP, T, H], _do_thread=True)

    @cyhub_method
    async def extend_arch_curve(self, displacement, _overview=False):
        cy_probe.stop()
        SP, T, H = await self.i2g_2d_axial.extend_arch_curve(displacement)
        await self.update_pano(*[SP, T, H], _overview=_overview)

    @cyhub_method
    async def visible_actor(self, actor_type, visible):
        await self.i2g_3d.visible_actor(actor_type, visible)
        await self.i2g_pano.visible_actor(actor_type, visible)
        await self.i2g_2d_cross.visible_actor(actor_type, visible)
        await self.i2g_2d_parallel.visible_actor(actor_type, visible)
        await self.i2g_2d_axial.visible_actor(actor_type, visible)
        await self.refresh_all()

    @cyhub_method
    async def reset_windowing(self):
        # reset img_prop & refresh all
        self.img_prop.SetColorWindow(self.initial_windowing_value[0])
        self.img_prop.SetColorLevel(self.initial_windowing_value[1])
        await self.refresh_all()

    @cyhub_method
    async def set_vol_clipping(self, on):
        await self.i2g_3d.set_vol_clipping(on)
        # if on:
        #     self.sigSetLayoutStatusMode.emit('volume_clipping')
        # else:
        #     self.sigSetLayoutStatusMode.emit('default')
        await self.i2g_3d.refresh()

    @cyhub_method
    async def set_vol_camera_trace(self, on):
        await self.i2g_3d.set_vol_camera_trace(on)
        await self.i2g_3d.refresh()

    @cyhub_method
    async def set_screen_layout_type(self, _type):
        """
        :param _type: 0(2-screen), 1(3-screen)
        """
        self.current_screen_layout_type = _type
        await self.i2g_2d_cross.set_screen_layout_type(_type)
        await self.i2g_2d_parallel.set_screen_layout_type(_type)
        await self.i2g_2d_axial.set_screen_layout_type(_type)
        await self.refresh_all()

    @cyhub_method
    async def on_mode_changed(self, _mode, _topbar_states=None):
        # Keep topbar states
        if _topbar_states:
            if not hasattr(self, '__keep_topbar_states__'):
                self.__keep_topbar_states__ = _topbar_states

        if _mode == 'new_nerve':
            if hasattr(self, 'nerve_mgr') and self.nerve_mgr:
                _states = [None, Enum_Topbar_Group2.VOLUME | Enum_Topbar_Group2.NERVE, None]
                self.sigSetI2GTopbarStates.emit(_states)
                self.nerve_mgr.create_new_nerve()
                await self.i2g_2d_cross.set_mode(_mode)
                await self.i2g_2d_parallel.set_mode(_mode)
                await self.i2g_pano.set_mode(_mode)
        elif _mode == 'close_nerve':
            if hasattr(self, 'nerve_mgr') and self.nerve_mgr:
                if hasattr(self, '__keep_topbar_states__'):
                    self.sigSetI2GTopbarStates.emit(self.__keep_topbar_states__)
                    del self.__keep_topbar_states__
                else:
                    self.sigSetI2GTopbarStates.emit([None, Enum_Topbar_Group2.INITIAL, Enum_Topbar_Group3.INITIAL])
                self.nerve_mgr.close_nerve()
                await self.i2g_2d_cross.set_mode(_mode)
                await self.i2g_2d_parallel.set_mode(_mode)
                await self.i2g_pano.set_mode(_mode)
        elif _mode == 'virtual_teeth':
            _states = [None, Enum_Topbar_Group2.MODEL | Enum_Topbar_Group2.WAXUP, Enum_Topbar_Group3.NONE]
            self.sigSetI2GTopbarStates.emit(_states)
            await self.i2g_3d.set_mode(_mode)
            await self.i2g_3d.refresh()
        elif _mode == 'implant_pick_place':
            # TODO initialize implant by virtual tooth
            await self.leafimplant_preview.make_default()

            for tooth_id in self.VirtualTeeth:
                if not tooth_id in self.Implants:
                    _actor, _tpd = await self.leafimplant_preview.get_cloned_actor('implant')
                    _mat = self.VirtualTeeth[tooth_id].matrix

                    # normalize the vectors(i, j, k) to not affect scale
                    _mat = np.array(_mat).reshape(4,4)
                    IJKT = []
                    for i in range(4):
                        _x = _mat[:, i]
                        if i < 3:
                            _x = _x / np.linalg.norm(_x)
                        IJKT.append(np.matrix(_x).T)
                    _mat = np.asarray(np.concatenate(IJKT, axis=1).ravel()).reshape(-1).tolist()

                    # TODO
                    # set offset
                    _mat0 = [1, 0, 0, 0,
                             0, 1, 0, 0,
                             0, 0, 1, -8,
                             0, 0, 0, 1]

                    # matrix multiply
                    _mat0 = get_vtkmat_from_list(_mat0)
                    _mat1 = get_vtkmat_from_list(_mat)
                    result_mat = vtk.vtkMatrix4x4()
                    vtk.vtkMatrix4x4.Multiply4x4(_mat1, _mat0, result_mat)
                    result_mat = np.array(get_matrix_from_vtkmat(result_mat)).ravel().tolist()

                    _world_to_pano, _pano_to_world = await self.i2g_pano.get_fn_coord_converter()
                    self.Implants[tooth_id] = Implant(_actor, _tpd, result_mat, tooth_id,
                                                         _cvt_world_to_pano=_world_to_pano,
                                                         _cvt_pano_to_world=_pano_to_world)
                    await self.i2g_3d.add_implant(self.Implants[tooth_id])
                    await self.i2g_pano.add_implant(self.Implants[tooth_id])
                    await self.i2g_2d_cross.add_implant(self.Implants[tooth_id])
                    await self.i2g_2d_parallel.add_implant(self.Implants[tooth_id])
                    await self.i2g_2d_axial.add_implant(self.Implants[tooth_id])

            await self.check_plane()
            await self.i2g_3d.set_mode(_mode)
            await self.i2g_pano.set_camera_scale(0.7)
            await self.refresh_all()
        else:
            if hasattr(self, '__keep_topbar_states__'):
                self.sigSetI2GTopbarStates.emit(self.__keep_topbar_states__)
                del self.__keep_topbar_states__
            await self.i2g_3d.set_mode(_mode)
            await self.i2g_2d_cross.set_mode(_mode)
            await self.i2g_2d_parallel.set_mode(_mode)
            await self.i2g_pano.set_mode(_mode)
            await self.i2g_pano.set_camera_scale(0.9)
            await self.i2g_3d.refresh()
            await self.i2g_pano.refresh()

    @cyhub_method
    async def set_measure_mode(self, mode, state):
        if state is False:
            await self.i2g_2d_axial.set_measure_mode('')
            await self.i2g_2d_cross.set_measure_mode('')
            await self.i2g_2d_parallel.set_measure_mode('')
            await self.i2g_pano.set_measure_mode('')
        else:
            await self.i2g_2d_axial.set_measure_mode(mode)
            await self.i2g_2d_cross.set_measure_mode(mode)
            await self.i2g_2d_parallel.set_measure_mode(mode)
            await self.i2g_pano.set_measure_mode(mode)

    @cyhub_method
    async def reset_measure(self):
        await self.i2g_2d_axial.measure_reset()
        await self.i2g_2d_cross.measure_reset()
        await self.i2g_2d_parallel.measure_reset()
        await self.i2g_pano.measure_reset()

    @cyhub_method
    def get_current_work_path(self):
        return self.current_work_path

    @cyhub_method
    def get_i2g_widgets(self):
        return self.i2g_3d.as_widget(), self.i2g_2d_axial.as_widget(), self.i2g_2d_cross.as_widget(), \
               self.i2g_2d_parallel.as_widget(), self.i2g_pano.as_widget()

    @cyhub_method
    def get_preview_widgets(self):
        return self.leafimplant_preview.as_widget()

    @cyhub_method
    async def refresh_all(self):
        # await self.i2g_3d.update_windowing()
        await self.i2g_pano.refresh()
        await self.i2g_2d_cross.refresh()
        await self.i2g_2d_parallel.refresh()
        await self.i2g_2d_axial.refresh()
        await self.i2g_3d.refresh()

    @cyhub_method
    async def refresh_3d(self):
        await self.i2g_3d.refresh()

    # @cyhub_method
    # def keyPressEvent(self, event):
    #     """
    #     ['keypress', e.type(), e.key(), int(e.modifiers()), str(e.text()), e.isAutoRepeat(), e.count()]
    #     """
    #     self._key_modifiers = event[3]
    #
    # @cyhub_method
    # def keyReleaseEvent(self, event):
    #     """
    #     ['keyrelease', e.type(), e.key(), int(e.modifiers()), str(e.text()), e.isAutoRepeat(), e.count()]
    #     """
    #     self._key_modifiers = Qt.NoModifier

    @cyhub_method
    def get_LIM_treeview_model(self):
        return self.leaf_implant_manufacture_treeview_model

    @cyhub_method
    def get_LIF_treeview_model(self):
        return self.leaf_implant_fixture_treeview_model

    @cyhub_method
    def get_LIA_treeview_model(self):
        return self.leaf_implant_abutment_treeview_model

    @cyhub_method
    async def leaf_implant_fixture_treeview_model_init(self, index):
        self.leaf_implant_fixture_treeview_model.items.clear()
        _index = index.internalPointer()
        manufacturerID = _index.parentItem.itemData['ID']
        productLineID = _index.itemData['ID']
        _data = await self.db.leaf_implant_fixture_initialize(manufacturerID, productLineID)
        await self.leafimplant_preview.reset_preview()

        self.leaf_implant_fixture_treeview_model.beginResetModel()
        self.leaf_implant_fixture_treeview_model.leaf_implant_fixture_treeview_item_addData(_data)
        self.leaf_implant_fixture_treeview_model.endResetModel()

        self.leaf_implant_abutment_treeview_model.beginResetModel()
        self.leaf_implant_abutment_treeview_model.items.clear()
        self.leaf_implant_abutment_treeview_model.endResetModel()

    @cyhub_method
    async def leaf_implant_abutment_treeview_model_init(self, index):
        self.leaf_implant_abutment_treeview_model.items.clear()
        _index = index.internalPointer()
        implantID = _index.itemData['ID']
        await self.leafimplant_preview.reset_abutment_preview()

        _data = await self.db.leaf_implant_abutment_initialize(implantID)
        if _data:
            self.leaf_implant_abutment_treeview_model.beginResetModel()
            self.leaf_implant_abutment_treeview_model.leaf_implant_abutment_treeview_item_addData(_data)
            self.leaf_implant_abutment_treeview_model.endResetModel()
        else:
            pass

    @cyhub_method
    async def leaf_implant_preview_init(self, m_index, f_index):
        _m_index = m_index.internalPointer()
        _f_index = f_index.internalPointer()
        _m_key = _m_index.itemData['ID']
        _f_key = _f_index.itemData['ID']
        filepath = _f_index.itemData['FileName'].replace("\\", "/")
        await self.leafimplant_preview.make_implant_preview(filepath)
        await self.leafimplant_preview.set_key_info(_m=_m_key, _f=_f_key, _a=None)
        await self.leafimplant_preview.refresh()

    @cyhub_method
    async def leaf_implant_abutment_preview_init(self, a_index):
        _a_index = a_index.internalPointer()
        _a_key = _a_index.itemData['ID']
        filepath = _a_index.itemData['FileName'].replace("\\", "/")
        assemble_distance = _a_index.itemData['AssembleDistance']
        await self.leafimplant_preview.make_abutment_preview(filepath, assemble_distance)
        await self.leafimplant_preview.set_key_info(_a=_a_key)
        await self.leafimplant_preview.refresh()

    @cyhub_method
    async def leaf_implant_apply(self):
        # apply implant
        _actor, _tpd = await self.leafimplant_preview.get_cloned_actor('implant')
        _leaf_implant_key_info = copy.deepcopy(await self.leafimplant_preview.get_key_info())
        _current_implant = self.Implants.pop(self.selected_implant)
        _mat = _current_implant.matrix
        _world_to_pano, _pano_to_world = await self.i2g_pano.get_fn_coord_converter()
        self.Implants[self.selected_implant] = Implant(_actor, _tpd, _mat, self.selected_implant,
                                          _cvt_world_to_pano=_world_to_pano,
                                          _cvt_pano_to_world=_pano_to_world)
        self.Implants[self.selected_implant].SubGeoms = _current_implant.SubGeoms
        self.Implants[self.selected_implant].leaf_implant_key_info = _leaf_implant_key_info
        await self.i2g_3d.add_implant(self.Implants[self.selected_implant])
        await self.i2g_pano.add_implant(self.Implants[self.selected_implant])
        await self.i2g_2d_cross.add_implant(self.Implants[self.selected_implant])
        await self.i2g_2d_parallel.add_implant(self.Implants[self.selected_implant])
        await self.i2g_2d_axial.add_implant(self.Implants[self.selected_implant])

        # apply abutment
        # remove sub geom from renderer
        await self.i2g_3d.remove_implant_sub_geom(self.selected_implant, 'abutment')
        await self.i2g_pano.remove_implant_sub_geom(self.selected_implant, 'abutment')
        await self.i2g_2d_cross.remove_implant_sub_geom(self.selected_implant, 'abutment')
        await self.i2g_2d_parallel.remove_implant_sub_geom(self.selected_implant, 'abutment')
        await self.i2g_2d_axial.remove_implant_sub_geom(self.selected_implant, 'abutment')

        # create sub geom and init sub geom
        _a_tpd = await self.leafimplant_preview.get_cloned_actor('abutment')
        if not _a_tpd is None:
            _geom = SubGeom(None, 'abutment', *_a_tpd)
            _prop = _geom.get_actor().GetProperty()
            Materials.get_property(Materials.MATERIAL_IMPLANT_METAL_2, _prop)
            self.Implants[self.selected_implant].add_geom(_geom)
            # add sub geom to renderer
            await self.i2g_3d.add_implant_sub_geom(self.selected_implant, 'abutment')
            await self.i2g_pano.add_implant_sub_geom(self.selected_implant, 'abutment')
            await self.i2g_2d_cross.add_implant_sub_geom(self.selected_implant, 'abutment')
            await self.i2g_2d_parallel.add_implant_sub_geom(self.selected_implant, 'abutment')
            await self.i2g_2d_axial.add_implant_sub_geom(self.selected_implant, 'abutment')

        await self.select_implant(self.selected_implant, True)
        await self.refresh_all()

        del _current_implant

        # Garbage Collect
        gc.collect()

    @cyhub_method
    async def leaf_implant_preview_search_data(self, search_data):
        _search_data = search_data
        _searched_data = await self.db.manufacturer_select(_search_data)
        _searched_data_key = []

        for v in _searched_data:
            _searched_data_key.append(v['ManufacturerID'])

        self.leaf_implant_manufacture_treeview_model.beginResetModel()
        self.leaf_implant_manufacture_treeview_model.items.clear()

        _data = await self.db.get_d_manufacturer()

        _m = defaultdict(dict)
        _m['Manufacturer'] = {}

        mCount = 0
        for i1 in range(0, len(_data['Manufacturer'])):
            for key in _searched_data_key:
                if key == _data['Manufacturer'][i1]['ManufacturerID']:
                    _m['Manufacturer'][mCount] = _data['Manufacturer'][i1]
                    mCount += 1
                else:
                    continue

        if len(_m['Manufacturer']) != 0:
            self.leaf_implant_manufacture_treeview_model.leaf_implant_manufacture_treeview_item_addData(_m)
        else:
            print("data not found")

        self.leaf_implant_manufacture_treeview_model.endResetModel()

        self.leaf_implant_fixture_treeview_model.beginResetModel()
        self.leaf_implant_fixture_treeview_model.items.clear()
        self.leaf_implant_fixture_treeview_model.endResetModel()

        self.leaf_implant_abutment_treeview_model.beginResetModel()
        self.leaf_implant_abutment_treeview_model.items.clear()
        self.leaf_implant_abutment_treeview_model.endResetModel()

    @cyhub_method
    async def project_load(self, prj):
        if len(self.prj.info['Module']) == 0:
            self.prj = Project(prj)
            # TODO!! i2g_prject_DENTAL.get_info('CurveSeries')
            # TODO!! i2g_prject_DENTAL.get_info('VirtualTeeth')
            # TODO!! i2g_prject_DENTAL.get_info('Implant')
            _curve_series = self.prj.get_i2g_info('DENTAL', 'CurveSeries')
            _virtual_teeth = self.prj.get_i2g_info('DENTAL', 'VirtualTeeth')
            _implant = self.prj.get_i2g_info('DENTAL', 'Implant')
            # --------------------------------------------------------------
            print(_curve_series)
            print(_virtual_teeth)
            print(_implant)
            print(_curve_series)
            print(_virtual_teeth)
            print(_implant)

            print(_curve_series)
            print(_virtual_teeth)
            print(_implant)
            print(_curve_series)
            print(_virtual_teeth)
            print(_implant)
        else:
            #TODO!! when prj already exists, ...
            pass

    @cyhub_method
    async def project_save(self, project_data):
        i2g_prject = I2G_Project()
        i2g_prject_COMMON = I2G_Project.COMMON()
        i2g_prject_DENTAL = I2G_Project.DENTAL()

        data = project_data
        data['Key']['PrjSeriesKey'] = await self.db.projectSeries_insert(data)
        result = await self.db.projectObject_insert(data)
        data['Key']['PrjObjectKey'] = result[0]
        data['path'] = result[1]

        _module_info = {
            'PrjSeriesKey': data['Key']['PrjSeriesKey'],
            'PrjObjectKey': data['Key']['PrjObjectKey']
        }
        data['save_prj'].append(_module_info)

        #NOTE set prj info
        self.prj.info['Info']['Version'] = 1.0
        self.prj.info['Info']['CreateDate'] = data['create_date']
        self.prj.info['Info']['CreateTime'] = data['create_time']
        self.prj.info['Info']['Description'] = data['project_save_desc']

        #NOTE set i2g COMMON info
        i2g_prject_COMMON.set_info('Key', data['Key'])
        i2g_prject_COMMON.set_info('SeriesNo', data['seriesno'])
        i2g_prject_COMMON.set_info('Operator', data['operator'])
        # i2g_prject_COMMON.set_info('CreateDate', data['create_date'])
        # i2g_prject_COMMON.set_info('CreateTime', data['create_time'])
        # i2g_prject_COMMON.set_info('Description', data['project_save_desc'])
        i2g_prject_COMMON.set_info('Path', data['path'])
        i2g_prject_COMMON.set_info('DCM_Interval', data['DCM_Interval'])

        r = i2g_prject_COMMON.get_info()

        i2g_prject.set_info(r[0], r[1])

        # NOTE set i2g DENTAL info
        _curve_series = {
            "Points": [
                [20.11, 59.43, 40.83],
                [20.11, 59.43, 40.83],
                [20.11, 59.43, 40.83]
            ],
            "TangentVecs": [
                [],
                [],
                [],
                []
            ],
            "VerticalVecs": [
                [],
                [],
                [],
                []
            ]
        }
        _nerve = {
            "Points": [
                [20.11, 59.43, 40.83],
                [20.11, 59.43, 40.83],
                [20.11, 59.43, 40.83],
                []
            ]
        }
        _model = {
            "Path": "~~~.stl",
            "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
        }
        _vritual_teeth1 = {
            "Id": 18,
            "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
        }
        _vritual_teeth2 = {
            "Id": 19,
            "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
        }
        _implant1 = {
            "Id": 18,
            "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
            "Manufacturer": "1000",
            "ProductLine": "123",
            "Model": "111",
            "Abutment":
                {
                    "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
                    "Model": "213123"
                }
        }
        _implant2 = {
            "Id": 19,
            "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
            "Manufacturer": 1000,
            "ProductLine": 123,
            "Model": 111,
            "Abutment":
                {
                    "Matrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
                    "Model": 213123
                }
        }

        i2g_prject_DENTAL.set_info("CurveSeries", _curve_series)
        i2g_prject_DENTAL.set_info("Nerve", _nerve)
        i2g_prject_DENTAL.set_info("Model", _model)
        i2g_prject_DENTAL.set_info("VirtualTeeth", _vritual_teeth1)
        i2g_prject_DENTAL.set_info("VirtualTeeth", _vritual_teeth2)
        i2g_prject_DENTAL.set_info("Implant", _implant1)
        i2g_prject_DENTAL.set_info("Implant", _implant2)

        r = i2g_prject_DENTAL.get_info()

        i2g_prject.set_info(r[0], r[1])

        self.prj.set_prj_info(i2g_prject)

        self.prj.write_json()

        return True

    @cyhub_method
    async def select_implant(self, _id, _dsi_on=False):
        _mode = None if (_id == None or _id < 0) else 'verification'
        self.selected_implant = _id
        await self.i2g_2d_cross.set_mode(_mode)
        await self.i2g_2d_parallel.set_mode(_mode)
        await self.i2g_2d_axial.set_mode(_mode)
        await self.i2g_pano.set_mode(_mode)

        if _mode is None:
            await self.i2g_3d.delete_ruler_for_implant_and_nerve()
            return

        _implant = self.Implants[_id]
        _m = _implant.matrix
        _m = np.array(_m).reshape(4, 4)
        _axis_vec = _m[:3, 2]  # get k-vector (third column)
        _axis_vec = _axis_vec / np.linalg.norm(_axis_vec)
        _hor_vec = _m[:3, 0]  # get i-vector (first column)
        _hor_vec = _hor_vec / np.linalg.norm(_hor_vec)
        _ver_vec = _m[:3, 1]  # get j-vector (second column)
        _ver_vec = _ver_vec / np.linalg.norm(_ver_vec)
        _origin = _m[:3, 3]  # get origin (4th column)
        _cen = _implant.actor.GetCenter()

        args = _axis_vec, _hor_vec, _ver_vec, _origin, _cen
        _plane_cross = [_origin, _hor_vec, _ver_vec]
        _plane_parallel = [_origin, _ver_vec, _hor_vec*-1]
        # _plane_axial = [_origin, _axis_vec]

        # set leaf implant item with key info
        await self.leafimplant_preview.set_item_with_key_info(_implant.leaf_implant_key_info)

        # set 2d
        await self.i2g_2d_cross.set_implant(_id, args)
        await self.i2g_2d_parallel.set_implant(_id, args)
        await self.i2g_2d_axial.set_implant(_id, args)

        # set 3d
        await self.i2g_3d.set_cross_plane(_plane_cross)
        await self.i2g_3d.set_parallel_plane(_plane_parallel)
        # await self.i2g_3d.update_axial(_plane_axial)

        await self.i2g_2d_axial.update_dsi('cross', _plane_cross)
        await self.i2g_2d_axial.update_dsi('parallel', _plane_parallel)
        # await self.i2g_2d_cross.update_dsi('axial', _axial_plane)
        await self.i2g_2d_cross.update_dsi('parallel', _plane_parallel)
        # await self.i2g_2d_parallel.update_dsi('axial', _axial_plane)
        await self.i2g_2d_parallel.update_dsi('cross', _plane_cross)
        # await self.i2g_pano.update_dsi('axial', axial_plane_info)
        # await self.i2g_pano.update_dsi('cross', _plane_cross)

        # update nerve contour
        self.nerve_mgr.set_cut_plane('cross', _plane_cross)
        self.nerve_mgr.set_cut_plane('parallel', _plane_parallel)

        # check plane
        await self.check_plane()

        # dsi init
        if _dsi_on:
            a_tpd = _implant.get_a_tpd()
            bnds = a_tpd[0].GetBounds()
            bnds = [bnds[4], bnds[5]] * 3
            await self.i2g_2d_cross.set_dsi_implant(a_tpd, bnds)
            await self.i2g_2d_parallel.set_dsi_implant(a_tpd, bnds)
            # await self.i2g_2d_axial.set_dsi_implant(a_tpd, bnds)

        # measure ruler for implant and nerve
        await self.calc_measure_distance_implant_and_nerve(_id)

        await self.refresh_all()

    @cyhub_method
    async def calc_measure_distance_implant_and_nerve(self, implant_id=None):
        if implant_id is None:
            implant_id = self.selected_implant
        _implant = self.Implants[implant_id]
        _m = _implant.matrix
        _m = np.array(_m).reshape(4, 4)
        _axis_vec = _m[:3, 2]  # get k-vector (third column)
        _axis_vec = _axis_vec / np.linalg.norm(_axis_vec)
        _origin = _m[:3, 3]  # get origin (4th column)

        # Closest Point
        if hasattr(self, 'nerve_mgr') and len(self.nerve_mgr.NERVES) > 0:
            _dir = -1 if np.inner([0, 0, 1], _axis_vec) >= 0 else 1
            a_tpd = _implant.get_a_tpd()
            bnds = a_tpd[1].GetInput().GetBounds()
            length = bnds[5] - bnds[4]
            _p0 = _origin + (_axis_vec * length * _dir)

            for n in self.nerve_mgr.NERVES:
                # Clean the polydata so that the edges are shared !
                cleanPolyData = vtk.vtkCleanPolyData()
                cleanPolyData.SetInputData(n.tube.GetOutput())
                cleanPolyData.Update()
                locator = vtk.vtkCellLocator()
                locator.SetDataSet(cleanPolyData.GetOutput())
                locator.BuildLocator()
                # FindClosestPoint
                pos = [0.0, 0.0, 0.0]
                cell = vtk.vtkGenericCell()
                cell_id = vtk.mutable(0)
                sub_id = vtk.mutable(0)
                d2 = vtk.mutable(0.0)
                radius = 10
                found = locator.FindClosestPointWithinRadius(_p0, radius, pos, cell, cell_id, sub_id, d2)
                del locator
                del cleanPolyData
                if found:
                    """
                    variable 'found' has a 0 or 1 not True/False
                    """
                    break

            if found:
                await self.i2g_pano.make_ruler_for_implant_and_nerve(implant_id, p1=_p0, p2=pos)
                await self.i2g_3d.make_ruler_for_implant_and_nerve(p1=_p0, p2=pos)
            else:
                await self.i2g_pano.delete_ruler_for_implant_and_nerve(implant_id)
                await self.i2g_3d.delete_ruler_for_implant_and_nerve()

    @cyhub_method
    async def measure_virtual_teeth(self, tooth_id, state):
        if state:
            if hasattr(self, 'DSI_VT'):
                self.DSI_VT.set_visible(False)

            self.current_tooth_id = int(tooth_id)
            await self.i2g_3d.set_mode('virtual_teeth_measure')

    @cyhub_method
    async def set_virtual_tooth(self, tooth_id, state, _mat=None, _vec_norm=None):

        if tooth_id is None:
            if hasattr(self, 'DSI_VT'):
                self.DSI_VT.set_visible(False)
        else:
            tooth_id = int(tooth_id)
            if tooth_id < 0:
                tooth_id = self.current_tooth_id

            if not tooth_id in self.VirtualTeeth:
                __path = '%s/../DBM/VirtualTeeth/Lib/%d.pcm' % (os.path.join(os.path.dirname(__file__)), tooth_id)
                data = PolyData(__path)

                if not _mat is None:
                    mat = _mat
                    if not _vec_norm is None:
                        _bounds = [0, 0, 0, 0, 0, 0]
                        data.get_actor().GetBounds(_bounds)
                        s = abs(_vec_norm / (_bounds[1] - _bounds[0]))
                        offset_z = (_bounds[5] - _bounds[4]) / 2
                        m = [s, 0, 0, 0,
                             0, s, 0, 0,
                             0, 0, s, offset_z,
                             0, 0, 0, 1]
                        _m0 = np.array(m).reshape(4,4)
                        _m1 = np.array(mat).reshape(4,4)
                        mat = np.matmul(_m1, _m0).ravel().tolist()
                else:
                    mat = [1, 0, 0, 0,
                           0, 1, 0, 0,
                           0, 0, 1, 0,
                           0, 0, 0, 1]

                a = data.get_actor()
                tpd = data.get_tpd()
                _world_to_pano, _pano_to_world = await self.i2g_pano.get_fn_coord_converter()
                self.VirtualTeeth[tooth_id] = VirtualTooth(a, tpd, mat, tooth_id,
                                                           _cvt_world_to_pano=_world_to_pano,
                                                           _cvt_pano_to_world=_pano_to_world)

            """
            Set Visibility & Initialize DSI
            """
            if state:
                await self.i2g_3d.add_virtual_tooth(self.VirtualTeeth[tooth_id])
                await self.i2g_pano.add_virtual_tooth(self.VirtualTeeth[tooth_id])
                await self.i2g_2d_cross.add_virtual_tooth(self.VirtualTeeth[tooth_id])
                await self.i2g_2d_parallel.add_virtual_tooth(self.VirtualTeeth[tooth_id])
                await self.i2g_2d_axial.add_virtual_tooth(self.VirtualTeeth[tooth_id])
            else:
                await self.i2g_3d.remove_virtual_tooth(tooth_id)
                await self.i2g_pano.remove_virtual_tooth(tooth_id)
                await self.i2g_2d_cross.remove_virtual_tooth(tooth_id)
                await self.i2g_2d_parallel.remove_virtual_tooth(tooth_id)
                await self.i2g_2d_axial.remove_virtual_tooth(tooth_id)
                _vt = self.VirtualTeeth.pop(tooth_id)
                if _vt:
                    del _vt

        await self.i2g_3d.refresh()
        await self.i2g_pano.refresh()

    @cyhub_method
    async def update_virtual_tooth(self, _mat, _vec_norm=None):
        if self.current_tooth_id is None or self.current_tooth_id < 0:
            return
        if not self.current_tooth_id in self.VirtualTeeth:
            return
        tooth = self.VirtualTeeth[self.current_tooth_id]
        a, tpd = tooth.get_a_tpd()
        t = tpd.GetTransform()

        if _vec_norm is not None:
            s = abs(_vec_norm / (tooth.bounds[1] - tooth.bounds[0]))
            offset_z = (tooth.bounds[5] - tooth.bounds[4]) / 2
            _m = [s, 0, 0, 0,
                  0, s, 0, 0,
                  0, 0, s, offset_z,
                  0, 0, 0, 1]
            _m0 = np.array(_m).reshape(4, 4)
            _m1 = np.array(_mat).reshape(4, 4)
            _mat = np.matmul(_m1, _m0).ravel().tolist()

        tooth.matrix = _mat
        a.SetOrigin(_mat[3], _mat[7], _mat[11])     # actor's origin should set before tpd is updated.
        a.Modified()
        t.SetMatrix(get_vtkmat_from_list(_mat))
        t.Update()
        t.Modified()
        tpd.Update()
        tpd.Modified()

        await self.i2g_3d.refresh()
        await self.i2g_pano.refresh()

    @cyhub_method
    async def set_hightlight_virtual_tooth(self, tooth_id, value):
        if tooth_id is None:
            tooth_id = self.current_tooth_id
        else:
            tooth_id = int(tooth_id)
            if tooth_id < 0:
                tooth_id = self.current_tooth_id

        if tooth_id in self.VirtualTeeth:
            if value:
                self.VirtualTeeth[tooth_id].set_property_selected()
            else:
                self.VirtualTeeth[tooth_id].set_property_normal()

    @cyhub_method
    async def set_dsi_virtual_tooth(self, tooth_id, value):
        if tooth_id is None:
            tooth_id = self.current_tooth_id
        else:
            tooth_id = int(tooth_id)
            if tooth_id < 0:
                tooth_id = self.current_tooth_id

        if tooth_id in self.VirtualTeeth:
            if value:
                if not hasattr(self, 'DSI_VT'):
                    self.DSI_VT = DSI_3D.DSI_3D_OBJ(list(self.VirtualTeeth[tooth_id].get_a_tpd()), self.VirtualTeeth[tooth_id].bounds)
                    await self.i2g_3d.set_dsi_vt(self.DSI_VT)
                else:
                    self.DSI_VT.destroy()
                    del self.DSI_VT
                    self.DSI_VT = DSI_3D.DSI_3D_OBJ(list(self.VirtualTeeth[tooth_id].get_a_tpd()), self.VirtualTeeth[tooth_id].bounds)
                    await self.i2g_3d.set_dsi_vt(self.DSI_VT)
            else:
                if hasattr(self, 'DSI_VT'):
                    self.DSI_VT.set_visible(False)

    async def check_plane(self):
        # Lable
        await self.i2g_2d_cross.check_plane()
        await self.i2g_2d_parallel.check_plane()

    @cyhub_method
    async def set_volume_camera_preset(self, object_name):
        await self.i2g_3d.set_volume_camera_preset(object_name)

    @cyhub_method
    async def make_profile(self, _intensity, _distance):
        self.sigMakeProfile.emit(_intensity, _distance)

    @cyhub_method
    async def set_thickness(self, _type, _val):
        if _type == 'pano':
            await self.i2g_pano.set_thickness(_val)
            await self.refresh_pano()
        elif _type == 'cross':
            await self.i2g_2d_cross.set_thickness(_val)
            await self.i2g_2d_cross.refresh()
        elif _type == 'parallel':
            await self.i2g_2d_parallel.set_thickness(_val)
            await self.i2g_2d_parallel.refresh()
        elif _type == 'axial':
            await self.i2g_2d_axial.set_thickness(_val)
            await self.i2g_2d_axial.refresh()

define_roman = [
    {1: 'I', 2: 'V',  3: 'X'},
    {1: 'X', 2: 'L',  3: 'C'},
    {1: 'C', 2: 'D',  3: 'M'},
    {1: 'M', 2: '_V', 3: '_X'},
    {1: '_X', 2: '_L', 3: '_C'},
    {1: '_C', 2: 'D', 3: '_M'},
    {1: '_M'}
]

def magic(i, number):
    _temp = ""
    for j in range(int(str(number)[::-1][i])):
        _temp += define_roman[i][1]

    if _temp.count(define_roman[i][1]) == 3:
        return _temp
    if _temp.count(define_roman[i][1]) == 4:
        _temp = define_roman[i][1] + define_roman[i][2]
    if _temp.count(define_roman[i][1]) == 4:
        _temp = define_roman[i][1] + define_roman[i][2]
    if _temp.count(define_roman[i][1]) == 5:
        _temp = define_roman[i][2]
    if 5 < _temp.count(define_roman[i][1]) < 9:
        _temp = define_roman[i][2]
        _temp += define_roman[i][1] * (int(str(number)[::-1][i]) - 5)
    if _temp.count(define_roman[i][1]) == 9:
        _temp = define_roman[i][1] + define_roman[i][3]

    return _temp

def digitToRoman(number):
    roman_str = ""

    num = len(str(number))

    for i in range(num):

        _str = magic(i, number)
        roman_str = _str + roman_str

    return roman_str

if __name__ == "__main__":
    if(
        digitToRoman(2) == "II" and \
        digitToRoman(3) == "III" and \
        digitToRoman(4) == "IV" and \
        digitToRoman(5) == "V" and \
        digitToRoman(9) == "IX" and \
        digitToRoman(12) == "XII" and \
        digitToRoman(16) == "XVI" and \
        digitToRoman(29) == "XXIX" and \
        digitToRoman(44) == "XLIV" and \
        digitToRoman(45) == "XLV" and \
        digitToRoman(68) == "LXVIII" and \
        digitToRoman(83) == "LXXXIII" and \
        digitToRoman(97) == "XCVII" and \
        digitToRoman(99) == "XCIX" and \
        digitToRoman(500) == "D" and \
        digitToRoman(501) == "DI" and \
        digitToRoman(649) == "DCXLIX" and \
        digitToRoman(798) == "DCCXCVIII" and \
        digitToRoman(891) == "DCCCXCI" and \
        digitToRoman(1000) == "M" and \
        digitToRoman(1004) == "MIV" and \
        digitToRoman(1006) == "MVI" and \
        digitToRoman(1023) == "MXXIII" and \
        digitToRoman(2014) == "MMXIV" and \
        digitToRoman(3999) == "MMMCMXCIX"
    ):
        print(" :) M̅M̅XXI")
    else:
        print(" :( ")


# from collections import OrderedDict
#
#
# import timeit
# def runtime(f):
#     def wrapper(_self, *args, **kwargs):
#         start = timeit.default_timer()
#         x = f(_self)
#         end = timeit.default_timer()
#         print("\telapsed time<%s>"%f.__name__, "==>", (end - start))
#         return x
#     return wrapper
#
#
# class Digit:
#     def __init__(self, digit, *args, **kwds):
#         self.digit = digit
#         self.symbol = OrderedDict({1: 'I',
#                                    4: 'IV', 5: 'V',
#                                    9: 'IX', 10: 'X',
#                                    40: 'XL', 50: 'L',
#                                    90: 'XC', 100: 'C',
#                                    400: 'CD', 500: 'D',
#                                    900: 'CM', 1000: 'M',
#                                    5000: 'V\u0305', 10000: 'X\u0305',
#                                    50000: 'L\u0305', 100000: 'C\u0305',
#                                    500000: 'D\u0305', 1000000: 'M\u0305'})
#
#     def __repr__(self):
#         return super().__repr__() + ' :: value (%d)' % self.digit
#
#     def __call__(self, *args, **kwds):
#         return self.digit
#
#     def __add__(self, other):
#         if not any([type(other) is Digit, type(other) is int]):
#             raise TypeError('r-value should have <Digit> or <int> type')
#
#         if type(other) is int:
#             return Digit(self.digit + other)
#         else:
#             return Digit(self.digit + other.digit)
#
#     def __sub__(self, other):
#         if not any([type(other) is Digit, type(other) is int]):
#             raise TypeError('r-value should have <Digit> or <int> type')
#
#         if type(other) is int:
#             return Digit(self.digit - other)
#         else:
#             return Digit(self.digit - other.digit)
#
#     def __mul__(self, other):
#         if not any([type(other) is Digit, type(other) is int]):
#             raise TypeError('r-value should have <Digit> or <int> type')
#
#         if type(other) is int:
#             return Digit(self.digit * other)
#         else:
#             return Digit(self.digit * other.digit)
#
#     def __truediv__(self, other):
#         if not any([type(other) is Digit, type(other) is int]):
#             raise TypeError('r-value should have <Digit> or <int> type')
#
#         if type(other) is int:
#             return Digit(self.digit / other)
#         else:
#             return Digit(self.digit / other.digit)
#
#     def __mod__(self, other):
#         if not any([type(other) is Digit, type(other) is int]):
#             raise TypeError('r-value should have <Digit> or <int> type')
#
#         if type(other) is int:
#             return Digit(self.digit % other)
#         else:
#             return Digit(self.digit % other.digit)
#
#     @runtime
#     def __toRoman1(self):
#         """
#         NOTE Generic Code
#         """
#         roman_number = ''
#         quotient = 0
#         remainder = self.digit
#         for operand, symbol in reversed(self.symbol.items()):
#             quotient = remainder // operand
#             remainder = remainder % operand
#             roman_number += symbol * quotient
#             # print("\t", operand, symbol, quotient, remainder)
#         # print("roman number :: ", roman_number)
#         return roman_number
#
#     @runtime
#     def __toRoman2(self):
#         """
#         NOTE Pythonic Code
#         """
#         roman_number = ''
#         quotient = 0
#         remainder = self.digit
#
#         def __convert(_key_val):
#             _operand, _symbol = _key_val
#             nonlocal quotient, remainder
#             quotient = remainder // _operand
#             remainder = remainder % _operand
#             return _symbol * quotient
#
#         for _x in map(__convert, reversed(self.symbol.items())):
#             roman_number += _x
#
#         return roman_number
#
#     toRoman = __toRoman2
#
#
# """
# usage of Digit class
# """
#
# # 1> how to initialize
# x = Digit(1004)
# # result of print(instance) can be defined via __repr__
# print("1> --- ", x, '\n')
#
# # 2> class's instance is called as a function????? really???!!!!
# # is it possible????
# # yes!!!
# # instance() can be defined via __call__
# print("2> --- ", x(), '\n')
#
# # 3> how to convert to roman number
# roman_number = x.toRoman()
# print("3> --- ", roman_number, '\n')
#
# # 4> how to use operator
# # class operators can be defined via __add__, __sub__, __mul__, __truediv__, __mod__
# x = Digit(2341)
# y = Digit(4926)
# z = x + y
# print("4> --- ", z, " -> ", z.toRoman(), '\n')
#
# # 5> really big numbers
# x = Digit(2000021)
# print("5> --- ", x, '->', x.toRoman(), '\n')
#
# # print(Digit(1023).toRoman())
# # print(Digit(1023).__toRoman1())


import os
from PyQt5.QtCore import QObject, QVariant, QMetaObject, Q_ARG, Q_RETURN_ARG
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QModelIndex, QVariant, Qt

from PyQt5.QtCore import QByteArray, QBuffer, QIODevice
from PyQt5.QtGui import QImage

from _qapp import create_task

from .I2G_DENTAL_Window import DENTALWindow
from .I2G_MPR_Window import MPRWindow
from .._SUPPORT_FOR_WIN import init_size, force_resize, notify
from .._SUPPORT_FOR_BLOCK import WorkerThread


class I2GWindow(QObject):
    """
    Key!!! : SUB_WIN, i2g_mgr!!!
    """

    sig_repaint = pyqtSignal(object)

    def __init__(self, _main_win, *args, **kwds):
        super().__init__()
        self._win = _main_win
        for k in kwds:
            setattr(self, k, kwds[k])

        self.SUB_WIN = {}
        self.SUB_WIN['dental'] = DENTALWindow(_main_win, *args, **kwds)
        self.SUB_WIN['mpr'] = MPRWindow(_main_win, *args, **kwds)

        self.init_sig_slot()

    async def initialize(self, *args):
        self.leaf_implant_manufacture_treeview_model = await self.i2g_mgr.get_LIM_treeview_model()
        self._win.rootContext().setContextProperty('leaf_implant_manufacture_treeview_model', self.leaf_implant_manufacture_treeview_model)

        """
        NOTE invoked from MainWindow's set_module
        """
        for _w in self.SUB_WIN:
            # First of all, "reset()" should be called to initialize vtk renderer & render window & interactor
            await self.SUB_WIN[_w].reset()
            self.SUB_WIN[_w].init_sig_slot()
            self.SUB_WIN[_w].init_qml_items_size()
            self.SUB_WIN[_w].adjust_qml_items_size()
        await self.i2g_mgr.init_vtk_img(args[0:3])

        self.module_args = args[1]

        self._win.findChild(QObject, 'i2g_dental_win_root').forceActiveFocus()

        self.vtk_preview_item = self._win.rootObject().findChild(QObject, 'vtk_preview')

        self._win.rootObject().findChild(QObject, "col_layout_module_menu").set_module_menu_color(self._win.rootObject().findChild(QObject, "mod_i2g"))
        self.key_info = None

        # notify("Title", "Heres an alert")

    def refresh_size(self):
        for _w in self.SUB_WIN:
            # self.SUB_WIN[_w].init_qml_items_size()
            self.SUB_WIN[_w].adjust_qml_items_size()

    def init_sig_slot(self):
        async def _do(_type, _visible):
            await self.i2g_mgr.visible_actor(_type, _visible)
            self.SUB_WIN['dental'].check_topbar_states(_type, _visible)

        self.btn_vol = self._win.rootObject().findChild(QObject, "btn_vol")
        self.btn_vol.sig_state_changed.connect(lambda x: create_task(_do('vol', x)))
        self.btn_model = self._win.rootObject().findChild(QObject, "btn_model")
        self.btn_model.sig_state_changed.connect(lambda x: create_task(_do('model', x)))
        self.btn_nerve = self._win.rootObject().findChild(QObject, "btn_nerve")
        self.btn_nerve.sig_state_changed.connect(lambda x: create_task(_do('nerve', x)))

        self.btn_waxup = self._win.rootObject().findChild(QObject, "btn_waxup")
        self.btn_waxup.sig_state_changed.connect(lambda x: create_task(_do('waxup', x)))
        self.btn_implant = self._win.rootObject().findChild(QObject, "btn_implant")
        self.btn_implant.sig_state_changed.connect(lambda x: create_task(_do('implant', x)))
        self.btn_abutment = self._win.rootObject().findChild(QObject, "btn_abutment")
        self.btn_abutment.sig_state_changed.connect(lambda x: create_task(_do('abutment', x)))

        self.btn_pano = self._win.rootObject().findChild(QObject, "btn_pano")
        self.btn_pano.sig_state_changed.connect(lambda x: create_task(_do('pano', x)))
        self.btn_axial = self._win.rootObject().findChild(QObject, "btn_axial")
        self.btn_axial.sig_state_changed.connect(lambda x: create_task(_do('axial', x)))
        self.btn_cross = self._win.rootObject().findChild(QObject, "btn_cross")
        self.btn_cross.sig_state_changed.connect(lambda x: create_task(_do('cross', x)))
        self.btn_parallel = self._win.rootObject().findChild(QObject, "btn_parallel")
        self.btn_parallel.sig_state_changed.connect(lambda x: create_task(_do('parallel', x)))

        async def _do2():
            await self.i2g_mgr.reset_windowing()
        self.btn_reset_windowing = self._win.rootObject().findChild(QObject, "btn_reset_windowing")
        self.btn_reset_windowing.sig_state_changed.connect(lambda x: create_task(_do2()))

        self.btn_outline = self._win.rootObject().findChild(QObject, "btn_outline")
        self.btn_outline.sig_state_changed.connect(lambda x: create_task(_do('outline', x)))

        async def _do3(_on):
            await self.i2g_mgr.set_vol_clipping(_on)
            self.SUB_WIN['dental'].check_topbar_states('cropping', _on)
        self.btn_vol_cropping = self._win.rootObject().findChild(QObject, "btn_vol_cropping")
        self.btn_vol_cropping.sig_state_changed.connect(lambda x: create_task(_do3(x)))

        async def _do4(_on):
            await self.i2g_mgr.set_vol_camera_trace(_on)
        self.btn_camera_trace = self._win.rootObject().findChild(QObject, "btn_camera_trace")
        self.btn_camera_trace.sig_state_changed.connect(lambda x: create_task(_do4(x)))

        async def _do_auto_arch():
            await self.i2g_mgr.do_auto_arch()
            self.SUB_WIN['dental'].adjust_qml_items_size()
        self.btn_auto_arch = self._win.rootObject().findChild(QObject, "btn_auto_arch")
        self.btn_auto_arch.clicked.connect(lambda x: create_task(_do_auto_arch()))

        # Registration Dlg
        self.vtk_vol_reg_item = self._win.rootObject().findChild(QObject, "vtk_vol_reg")
        self.vtk_model_reg_item = self._win.rootObject().findChild(QObject, "vtk_model_reg")
        self.vtk_result_reg_3d_item = self._win.rootObject().findChild(QObject, "vtk_result_reg_3d")
        self.vtk_result_reg_axial_item = self._win.rootObject().findChild(QObject, "vtk_result_reg_axial")
        self.vtk_result_reg_cross_item = self._win.rootObject().findChild(QObject, "vtk_result_reg_cross")
        self.btn_registration_model = self._win.rootObject().findChild(QObject, "btn_registration_model")
        self.btn_registration_model.clicked.connect(self.sig_btn_reg_model_clicked)
        self.dlg_registration = self._win.rootObject().findChild(QObject, "registration_dialog_item")
        self.dlg_registration.selected_model.connect(self.sig_selected_file_reg_model)
        self.dlg_registration.closed.connect(self.sig_closed_reg_model)

        async def _do_registration():
            await self.reg_mgr.registration(_execute=True)
        self.dlg_registration.do_registration.connect(lambda: create_task(_do_registration()))
        async def _do_registration_finish(jaw_type):
            await self.i2g_mgr.init_model(jaw_type, _refresh=True)
        self.dlg_registration.ok.connect(lambda x: create_task(_do_registration_finish(x)))

        # btn_i2g_screen_layout
        async def _do_set_2d_camera_scale(_visible):
            self.SUB_WIN['dental'].set_layout_2_3(_visible)
            # re-scale vtk camera
            _type = 1 if _visible else 0
            await self.i2g_mgr.set_screen_layout_type(_type)
        self.btn_i2g_screen_layout = self._win.rootObject().findChild(QObject, "btn_i2g_screen_layout")
        self.btn_i2g_screen_layout.clicked.connect(lambda x: create_task(_do_set_2d_camera_scale(x)))

        # transfer funcion
        self.transfer_func_dialog = self._win.rootObject().findChild(QObject, "transfer_func_dialog")
        self.btn_fransfer_func = self._win.rootObject().findChild(QObject, "btn_fransfer_func")
        self.colormap_item = self._win.rootObject().findChild(QObject, 'colormap')
        self.opacitymap_item = self._win.rootObject().findChild(QObject, 'opacitymap')
        self.windowing_item = self._win.rootObject().findChild(QObject, 'windowing')

        async def _do_set_fransfer_func():
            # Histogram
            hist, _max, _min = await self.i2g_mgr.get_histogram_hist()
            H = hist.tolist()
            _minX = 0
            _maxX = len(H)
            _minY = float(min(H))
            _maxY = float(max(H))
            self._win.rootObject().findChild(QObject, 'histogram_item').set_datas(H, _minX, _maxX, _minY, _maxY)

            #Colormap
            c_v = await self.i2g_mgr.get_color_values()
            for v in c_v:
                v[0] = (v[0] - float(_min)) / (float(_max) - float(_min))
            self.colormap_item.init_colormap(c_v)
            self.colormap_item.setProperty('trigger', True)
            self.colormap_item.setProperty('color_value', c_v)

            #Opacity
            o_v = await self.i2g_mgr.get_opacity_values()
            for v in o_v:
                v[0] = (v[0] - float(_min)) / (float(_max) - float(_min))
            self.opacitymap_item.init_opacitymap(o_v)
            self.opacitymap_item.setProperty('trigger', True)
            self.opacitymap_item.setProperty('opacity_value', o_v)

            # Windowing
            min_value, max_value = float(_min), float(_max)
            arrPos = {"left": 0.0, "right": 1.0}
            arrVal = {"min": min_value, "max": max_value}
            self.windowing_item.init_windowing(arrPos, arrVal)
            self.windowing_item.setProperty('trigger', True)
        self.btn_fransfer_func.clicked.connect(lambda: create_task(_do_set_fransfer_func()))

        async def _do_apply_fransfer_func():
            hist, _max, _min = await self.i2g_mgr.get_histogram_hist()
            c_v = await self.i2g_mgr.get_color_values()
            for v in c_v:
                v[0] = (v[0] - float(_min)) / (float(_max) - float(_min))
            self.colormap_item.setProperty('color_value', c_v)
            self.colormap_item.clear_colormap()

            o_v = await self.i2g_mgr.get_opacity_values()
            for v in o_v:
                v[0] = (v[0] - float(_min)) / (float(_max) - float(_min))
            self.opacitymap_item.setProperty('opacity_value', o_v)
            self.opacitymap_item.clear_opacitymap()

            left = self.windowing_item.property('temp_left_pos')
            right = self.windowing_item.property('temp_right_pos')
            min_value = self.windowing_item.property('temp_min')
            max_value = self.windowing_item.property('temp_max')
            arrPos = {"left": left, "right": right}
            arrVal = {"min": min_value, "max": max_value}
            self.windowing_item.setProperty('arrPositionsCol', arrPos)
            self.windowing_item.setProperty('arrValue', arrVal)
            self.windowing_item.clear_windowing()
        self.transfer_func_dialog.sig_apply.connect(lambda: create_task(_do_apply_fransfer_func()))

        async def _do_rejected_fransfer_func():
            self.colormap_item.clear_colormap()
            self.opacitymap_item.clear_opacitymap()
            self.windowing_item.clear_windowing()
        self.transfer_func_dialog.sig_rejected.connect(lambda: create_task(_do_rejected_fransfer_func()))

        async def _update_color_map(c_v):
            await self.i2g_mgr.change_color_map(c_v.toVariant())
            self.sig_repaint.emit(self.colormap_item)
        self.colormap_item.sig_changed.connect(lambda c_v: create_task(_update_color_map(c_v)))

        async def _update_opacity_map(_pos, _op):
            _pos, _op = _pos.toVariant(), _op.toVariant()
            param = [[_pos[i], _op[j]] for i, j in zip(_pos, _op)]
            #TODO!! slow opacity_ctrl..
            await self.i2g_mgr.change_opacity_map(param)
            self.sig_repaint.emit(self.opacitymap_item)
        self.opacitymap_item.sig_changed.connect(lambda _pos, _op: create_task(_update_opacity_map(_pos, _op)))

        async def _update_windowing(_l, _w):
            l, w = _l, _w
            await self.i2g_mgr.set_windowing(l, w)
            self.sig_repaint.emit(self.windowing_item)
        self.windowing_item.sig_changed.connect(lambda _l, _r: create_task(_update_windowing(_l, _r)))

        # leaf
        self._win.rootObject().findChild(QObject, "leaf_implant_manufacture_treeview").sig_manufacture_item_clicked.connect(self.sig_manufacture_item_clicked)
        self._win.rootObject().findChild(QObject, "manufacturer_search_textfield").sig_search_textfield.connect(self.sig_search_textfield)
        self._win.rootObject().findChild(QObject, "leaf_implant_fixture_treeview").sig_implant_fixture_item_clicked.connect(self.sig_implant_fixture_item_clicked)
        self._win.rootObject().findChild(QObject, "leaf_implant_abutment_treeview").sig_abutment_item_clicked.connect(self.sig_abutment_item_clicked)

        self._win.rootObject().findChild(QObject, "btn_leaf_apply").sig_leaf_apply_clicked.connect(lambda: create_task(self.i2g_mgr.leaf_implant_apply()))
        self._win.rootObject().findChild(QObject, "btn_leaf_reset").sig_leaf_reset_clicked.connect(self.sig_leaf_reset_clicked)

        self._win.rootObject().findChild(QObject,"project_save_dialog_save_button").sig_project_save_dialog_open.connect(self.sig_project_save_dialog_open)

        self.LIM_itemselect_model = self._win.rootObject().findChild(QObject, 'leaf_implant_manufacture_treeview_itemselectionmodel')
        self.LIF_itemselect_model = self._win.rootObject().findChild(QObject, 'leaf_implant_fixture_treeview_itemselectionmodel')
        self.LIA_itemselect_model = self._win.rootObject().findChild(QObject, 'leaf_implant_abutment_treeview_itemselectionmodel')

        # virtual teeth selector
        async def _on_sig_selected(_id, _visible):
            print("Virtual Tooth :: ", _id, _visible)
            if _visible is True:
                await self.i2g_mgr.measure_virtual_teeth(_id, _visible)
            else:
                await self.i2g_mgr.set_dsi_virtual_tooth(_id, False)
                await self.i2g_mgr.set_virtual_tooth(_id, False)
        self._win.rootObject().findChild(QObject, "vt_maxilla").sig_selected.connect(lambda x,y: create_task(_on_sig_selected(x,y)))
        self._win.rootObject().findChild(QObject, "vt_mandible").sig_selected.connect(lambda x,y: create_task(_on_sig_selected(x,y)))

        # implant selector
        async def  _on_implant_sig_selected(_id, _visible):
            print("Implant ID :: ", _id, _visible)
            _id = int(_id)
            if _visible is True:
                await self.i2g_mgr.select_implant(_id, True)
            else:
                pass
        self._win.rootObject().findChild(QObject, "implant_selector").sigSelected.connect(lambda x,y: create_task(_on_implant_sig_selected(x,y)))

        async def _set_mode(mode):
            _states = [None, self.SUB_WIN['dental'].current_topbar_g2_states, self.SUB_WIN['dental'].current_topbar_g3_states]
            await self.i2g_mgr.on_mode_changed(mode, _topbar_states=_states)
            if mode == 'new_nerve':
                pass
            elif mode == 'virtual_teeth':
                self.SUB_WIN['dental'].set_layout_virtual_teeth()
            elif mode == 'implant_pick_place':
                self.SUB_WIN['dental'].set_layout_implant()
            else:
                self.SUB_WIN['dental'].restore_layout()
        self._win.rootObject().findChild(QObject,"dummy_dental").modeChanged.connect(lambda x: create_task(_set_mode(x)))
        self._win.rootObject().findChild(QObject, "row_preset_btn").sig_change_preset.connect(self.sig_change_preset)
        self._win.rootObject().findChild(QObject, "tools_mesure_gridlayout").sig_measure_clicked.connect(self.sig_measure_clicked)
        self._win.rootObject().findChild(QObject, "reset").sig_clicked.connect(self.sig_measure_reset_clicked)

        async def _set_thickness(_type, _val):
            await self.i2g_mgr.set_thickness(_type, _val)
        self._win.rootObject().findChild(QObject, "pano_thickness").sigChanged.connect(lambda x: create_task(_set_thickness('pano', x)))
        self._win.rootObject().findChild(QObject, "cross_thickness").sigChanged.connect(lambda x: create_task(_set_thickness('cross', x)))
        self._win.rootObject().findChild(QObject, "parallel_thickness").sigChanged.connect(lambda x: create_task(_set_thickness('parallel', x)))
        self._win.rootObject().findChild(QObject, "axial_thickness").sigChanged.connect(lambda x: create_task(_set_thickness('axial', x)))

        async def _set_rendering_type(_type, _val):
            await self.i2g_mgr.set_rendering_type(_type, _val)
        self._win.rootObject().findChild(QObject, "pano_2d_format").sigChanged_2d_format.connect(
            lambda x: create_task(_set_rendering_type('pano', x)))
        self._win.rootObject().findChild(QObject, "cross_2d_format").sigChanged_2d_format.connect(
            lambda x: create_task(_set_rendering_type('cross', x)))
        self._win.rootObject().findChild(QObject, "parallel_2d_format").sigChanged_2d_format.connect(
            lambda x: create_task(_set_rendering_type('parallel', x)))
        self._win.rootObject().findChild(QObject, "axial_2d_format").sigChanged_2d_format.connect(
            lambda x: create_task(_set_rendering_type('axial', x)))

        async def _set_image_filter_type(_type, _val):
            await self.i2g_mgr.set_image_filter_type(_type, _val)
        self._win.rootObject().findChild(QObject, "pano_image_filter").sigChanged_image_filter.connect(
            lambda x: create_task(_set_image_filter_type('pano', x)))
        self._win.rootObject().findChild(QObject, "cross_image_filter").sigChanged_image_filter.connect(
            lambda x: create_task(_set_image_filter_type('cross', x)))
        self._win.rootObject().findChild(QObject, "parallel_image_filter").sigChanged_image_filter.connect(
            lambda x: create_task(_set_image_filter_type('parallel', x)))
        self._win.rootObject().findChild(QObject, "axial_image_filter").sigChanged_image_filter.connect(
            lambda x: create_task(_set_image_filter_type('axial', x)))


    def set_leaf_implant_item(self, key_info):
        self.key_info = key_info
        if all(self.key_info[:2]):
            self.set_LIM_item()
        else:
            self._win.rootObject().findChild(QObject, "manufacturer_search_textfield").set_init_m_treeview(True)

    def set_LIM_item(self):
        LIM_model = self.LIM_itemselect_model.model()
        try:
            _select_lim_index = LIM_model.match(LIM_model.index(0, 0, QModelIndex()), 256, self.key_info[0], 1, Qt.MatchRecursive)[0]
        except IndexError:
            print("There is no key matching the manufacture model")
            self._win.rootObject().findChild(QObject, "manufacturer_search_textfield").set_init_m_treeview(False)
        else:
            if _select_lim_index == self.LIM_itemselect_model.currentIndex():
                self.LIM_itemselect_model.clearCurrentIndex()
            self.LIM_itemselect_model.set_current_index(_select_lim_index)

    def set_LIF_item(self):
        LIF_model = self.LIF_itemselect_model.model()
        try:
            _select_lif_index = LIF_model.match(LIF_model.index(0, 0, QModelIndex()), 256, self.key_info[1], 1, Qt.MatchRecursive)[0]
        except IndexError:
            self.LIF_itemselect_model.clearCurrentIndex()
            print("There is no key matching the fixture model")
        else:
            self.LIF_itemselect_model.set_current_index(_select_lif_index)

    def set_LIA_item(self):
        LIA_model = self.LIA_itemselect_model.model()
        try:
            _select_lia_index = LIA_model.match(LIA_model.index(0, 0, QModelIndex()), 256, self.key_info[2], 1, Qt.MatchRecursive)[0]
        except IndexError:
            self.LIA_itemselect_model.clearCurrentIndex()
            print("There is no key matching the abutment model")
        else:
            self.LIA_itemselect_model.set_current_index(_select_lia_index)

    @pyqtSlot()
    def sig_btn_reg_model_clicked(self):
        async def _do():
            _w = await self.reg_mgr.get_reg_widgets()

            self.vtk_vol_reg_item.set_vtk(_w[0])
            self.vtk_model_reg_item.set_vtk(_w[1])
            self.vtk_result_reg_3d_item.set_vtk(_w[2])
            self.vtk_result_reg_axial_item.set_vtk(_w[3])
            self.vtk_result_reg_cross_item.set_vtk(_w[4])

            init_size(self.vtk_vol_reg_item)
            init_size(self.vtk_model_reg_item)
            init_size(self.vtk_result_reg_3d_item)
            init_size(self.vtk_result_reg_axial_item)
            init_size(self.vtk_result_reg_cross_item)

            force_resize(self.vtk_vol_reg_item)
            force_resize(self.vtk_model_reg_item)
            force_resize(self.vtk_result_reg_3d_item)
            force_resize(self.vtk_result_reg_axial_item)
            force_resize(self.vtk_result_reg_cross_item)

            # TODO need to check whether this is first time or not.
            _work_path = await self.i2g_mgr.get_current_work_path()
            self.dlg_registration.open_file_dlg(_work_path)

            await self.i2g_mgr.init_dlg_reg_model()
        create_task(_do())

    @pyqtSlot(str)
    def sig_selected_file_reg_model(self, path):
        async def _do():
            await self.reg_mgr.initialize_model(path)
        create_task(_do())

    @pyqtSlot()
    def sig_closed_reg_model(self):
        WorkerThread.stop_all()
        async def _do():
            await self.reg_mgr.reset()
        create_task(_do())

    @pyqtSlot()
    def sig_leaf_reset_clicked(self):
        if all(self.key_info[:2]):
            self.set_LIM_item()
        else:
            self._win.rootObject().findChild(QObject, "manufacturer_search_textfield").set_init_m_treeview(True)

    @pyqtSlot(QVariant, bool)
    def sig_manufacture_item_clicked(self, index, flag):
        async def _do():
            if flag:
                await self.i2g_mgr.leaf_implant_fixture_treeview_model_init(index)
                leaf_implant_fixture_treeview_model = await self.i2g_mgr.get_LIF_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_fixture_treeview_model', leaf_implant_fixture_treeview_model)
                self.set_LIF_item()
                self.LIM_itemselect_model.setProperty("_flag", False)
                self.LIM_itemselect_model.expand_m_index(index)
            else:
                await self.i2g_mgr.leaf_implant_fixture_treeview_model_init(index)
                leaf_implant_fixture_treeview_model = await self.i2g_mgr.get_LIF_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_fixture_treeview_model', leaf_implant_fixture_treeview_model)
        create_task(_do())

    @pyqtSlot(QVariant, QVariant, bool)
    def sig_implant_fixture_item_clicked(self, m_index, f_index, flag):
        async def _do():
            if flag:
                await self.i2g_mgr.leaf_implant_preview_init(m_index, f_index)
                await self.i2g_mgr.leaf_implant_abutment_treeview_model_init(f_index)
                leaf_implant_abutment_treeview_model = await self.i2g_mgr.get_LIA_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_abutment_treeview_model', leaf_implant_abutment_treeview_model)
                self.set_LIA_item()
                self.LIF_itemselect_model.setProperty("_flag", False)
            else:
                await self.i2g_mgr.leaf_implant_preview_init(m_index, f_index)
                await self.i2g_mgr.leaf_implant_abutment_treeview_model_init(f_index)
                leaf_implant_abutment_treeview_model = await self.i2g_mgr.get_LIA_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_abutment_treeview_model', leaf_implant_abutment_treeview_model)
        create_task(_do())

    @pyqtSlot(QVariant)
    def sig_abutment_item_clicked(self, a_index):
        async def _do():
            await self.i2g_mgr.leaf_implant_abutment_preview_init(a_index)
        create_task(_do())

    @pyqtSlot()
    def sig_btn_leaf_implant_clicked(self):
        async def _do():
            _w = await self.i2g_mgr.get_preview_widgets()
            self.vtk_preview_item.set_vtk(_w)
        create_task(_do())

    @pyqtSlot(str, bool)
    def sig_search_textfield(self, search_data, flag):
        _search_data = search_data
        async def _do():
            if flag:
                await self.i2g_mgr.leaf_implant_preview_search_data(_search_data)
                self.leaf_implant_manufacture_treeview_model = await self.i2g_mgr.get_LIM_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_manufacture_treeview_model',self.leaf_implant_manufacture_treeview_model)
                self.set_LIM_item()
                self._win.rootObject().findChild(QObject, "manufacturer_search_textfield").setProperty("_flag", False)
            else:
                await self.i2g_mgr.leaf_implant_preview_search_data(_search_data)
                self.leaf_implant_manufacture_treeview_model = await self.i2g_mgr.get_LIM_treeview_model()
                self._win.rootContext().setContextProperty('leaf_implant_manufacture_treeview_model', self.leaf_implant_manufacture_treeview_model)
        create_task(_do())

    @pyqtSlot(QVariant)
    def sig_project_save_dialog_open(self, project_data):
        """
        Note First of all, capture screen
             and, save project file(json format)
        """
        # TODO!!! Should check not only where to capture screen but where to save project file, later
        # Example
        # CLIENT PART
        # _img = self._win.grabWindow()
        # _ba = QByteArray()
        # _buf = QBuffer(_ba)
        # _buf.open(QIODevice.WriteOnly)
        # _img.save(_buf, 'JPG', 100)
        # SERVER PART
        # _captured_img, _b = QImage(), QByteArray.fromRawData(_ba.data())
        # if _captured_img.loadFromData(_b):
        #     _captured_img.save('aaa.jpg')
        # # For Standalone
        # _img = self._win.grabWindow()
        # _img.save("aaa.jpg")

        data = project_data.toVariant()
        data['Key'] = self.module_args['Key']
        if 'PrjObjectKey' in data['Key'] and 'PrjObjectKey' in data['Key']:
            del(data['Key']['PrjObjectKey'])
            del(data['Key']['PrjSeriesKey'])
        data['save_prj'] = self.module_args['save_prj']
        data['DCM_Interval'] = self.module_args['DCM_Interval']
        async def _do():
            await self.i2g_mgr.project_save(data)
            dirname = data['path'] + 'PRJ_' + data['seriesno'] + '_' + data['create_date'].replace('-', '') + data['create_time'].replace(':', '') + "/"
            _img = self._win.grabWindow()
            _img.save(dirname + data['seriesno'] + ".jpg")
        create_task(_do())

    @pyqtSlot(str, bool)
    def sig_maxilla_implant(self, id, state):
        print(id, " *** ", state)

    @pyqtSlot(str, bool)
    def sig_mandible_implant(self, id, state):
        print(id, " *** ", state)

    @pyqtSlot(str)
    def sig_change_preset(self, object_name):
        async def _do():
            await self.i2g_mgr.set_volume_camera_preset(object_name)
        create_task(_do())

    @pyqtSlot(str, bool)
    def sig_measure_clicked(self, id, state):
        _id, _state = id, state
        async def _do():
            await self.i2g_mgr.set_measure_mode(_id, _state)
        create_task(_do())

    @pyqtSlot()
    def sig_measure_reset_clicked(self):
        async def _do():
            await self.i2g_mgr.reset_measure()
        create_task(_do())

    def make_profile(self, _intensity, _distance):
        profile_chart_dialog = self._win.rootObject().findChild(QObject, "dialog_profile")
        profile_chart_dialog.set_profile_info(_intensity, _distance)

    def set_reg_dlg_rmse(self, _rmse):
        self.dlg_registration.setProperty('rmse', _rmse)